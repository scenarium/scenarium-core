Configure the workspace
=======================

Auto refresh the project with file system update:

Window -> Preferences: General/Workspace: **and check the option **‘Refresh using native hooks or polling’.

With this, the worktree will update when you modify the project file with an other application.

Use the project:
=================

In this part of the document, you need to start by determining what kink of user you are:

  - Simple user: Use scenrium, download plug-in on the store and maybe create Operators. ** (need scenarium GUI application) **
  - Plugin developper: Develop scenarium plugin for you or an industry ** (need scenarium SDK & eclipse) **
  - A product: use it on docker or other... ** (need scenarium no-gui & eclipse) **
  - A scenarium developper. (eclipse & sources)


Simple USER:
>>>>>>>>>>>>

** NOT READY **

Download the Luncher application: TODO

This **FREE** application contain the simple store/package interface that permit to you to download all scenarium tools and scenarium plugin you want.
You can run application like scenarium-flow.

This is **FREE** and many content are availlable for **FREE**


Plugin developper
>>>>>>>>>>>>>>>>>

** NOT READY **

Download the SDK:


A product
>>>>>>>>>

** NOT READY **

Scenarium is availlable on the cloud (docker-hub)


A scenarium developper
>>>>>>>>>>>>>>>>>>>>>>

This is a little more complex... But ready to use.

### Install the manifest manager tool:

Install Island python tool:
```{.sh}
pip install island --user
```

If you do not have pip on ubuntu:
```{.sh}
sudo apt install python-pip
echo "export PATH=$PATH:/home/$USER/.local/bin/" >> /home/$USER/.bashrc
source /home/$USER/.bashrc
```

### Download all the repository

Execute some command on your console

```{.bash}
# create the workspace directory
mkdir WORKSPACE
cd WORKSPACE
# initialize the workspace
island init git@gitlab.com:scenarium/manifest.git
# download all the sources
island sync
cd ..
```

Or use generic HTTP interface:

```{.bash}
# create the workspace directory
mkdir WORKSPACE
cd WORKSPACE
# initialize the workspace
island init http://gitlab.com/scenarium/manifest.git
# download all the sources
island sync
cd ..
```

If you want the development branch of scenarium jum on the ```dev``` branch

Select the developement branch:
```{.sh}
cd WORKSAPCE
island checkout dev
cd ..
```


### Open the project on eclipse:

Open eclipse on a new workspace (folder ```WORKSPACE```).

Import all the project with:

File -> Open project from file system...
	- Select the ```WORKSPACE``` directory in the ```Import source``` area.
	- Select all the projects
	- Click on finish.


### Run scenarium

Select the java file: ```scenarium-run/src/io/scenarium/run/ScenariumRun.java```

Right click on it -> Debug As ... -> Java Application

**Enjoy...** ;-)

<<<<<<<<<<<<<<<<<<<<<<<<<<

