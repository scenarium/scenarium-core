Contribute to the development of Scenarium
==========================================

The ```master``` branch is managed exclusively by the projects owners (Marc REVILLOUD / Edouard DUPIN). It represent the Stable version of scenarium.

The ```dev``` branch is designed for the development. All Merge-request must be done on this branch.

How to contribute:
==================

You find a Bug !!!
------------------

Create an issue containing the following description sections:

```
**Overview:**
An overview of the bug

**Frequency:**
The frequency it appears (every-run / sometime / "all the time, I can not work anymore..")

**How to reproduce:**
  - XXX
  - WWW 

**Log:**
If you can, the Log

**Code to generate the bug**
If you can the code that create the problem

```

You correct bugs or you create some beautiful things ...
--------------------------------------------------------

Fork the scenarium project.

Push your modification in a personal branch.

Propose bug fixes or improvements, you can make a ```merge request``` into the ```dev``` branch.


Coding Standards and Guidelines 
===============================

The scenarium project uses coding standards.
It's recommended to respect these standards when you propose a 'merge request'.
To follow these standards, you can configure Eclipse as described below.

The Formatter:
--------------

The Eclipse formatter configuration file (```Formatter.xml```) is located at the root of the scenarium project.

You can import the formatter in Eclipse:

  - Right click on the the scenarium projet, ```Properties``` -> ```Java Code Style``` -> ```Formatter```
  - Click on button ```Import...```
  - Select file  ```./scenarium/Formatter.xml```

To apply a formatter on a file press ```CTRL``` + ```SHIFT``` + ```F```

You can apply it when you save a file: ```Window``` -> ```Preferences``` -> ```Java``` -> ```Editor``` -> ```SaveActions``` and check the ```Format source code``` option.

The Cleaner:
------------

The Eclipse formatter configuration file (```CleanUp.xml```) is located at the root of the project.

You can import the cleaner in Eclipse:

  - Right click on the the scenarium projet, ```Properties``` -> ```Java Code Style``` -> ```Clean up```
  - Click on button ```Import...```
  - Select file  ```./scenarium/CleanUp.xml```

The CheckStyle:
---------------

This part is made by an external Eclipse plug-in (https://marketplace.eclipse.org/content/checkstyle-plug). You can install this plug-in from the Eclipse marketplace.

Then import the CheckStyle configuration file in Eclipse:

  - Global configuration: ```Window``` -> ```Preferences``` -> ```Checkstyle```
  - Click on button ```New...```
  - Set a ```Name``` at your conficuration (ex: ```scenarium checkstyle configuration```)
  - Click on button ```Import...```
  - Select file  ```./scenarium/CheckStyle.xml```
  - Click on button ```OK```

You can set this configuration by default if you want.

And finally, set it for your project:

  - Right click on the the scenarium projet: 'Properties' -> 'Checkstyle' -> choose your checkstyle configuration.


Configure tour Name and email for the git commit:
=================================================

Don't forget to specify your name and email address when you propose a 'merge request'.

For Eclipse:

  1. Click ```Window``` -> ```Preferences``` -> ```Team``` -> ```Git``` -> ```Configuration```
  2. Click ```Add Entry``` with the key value pairs:
    - Key: ```user.name```
    - Value: ```Your Name Here```
  3. Click ```Add Entry``` with the key value pairs:
    - Key: ```user.email```
    - Value: ```YourEmailHere@xxxxx.yyy```

In console:

```{.bash}
git config --global user.name "first_name LAST_NAME"
git config --global user.email "MY_NAME@example.com"
```

Best commit comment:
====================

The commit must be configure as follow:

```
[XXX] A simple sentence to describe the work (max 80 characters)
                                                                     <<< an empty line
The full description of your commit, the change, the regression...
You can write the number of line you  want (this is optional)
```

The XXX represent the type of commit:

  - ```API``` Break the API in a development feature (change the MAJOR version of the library if >=1 )
  - ```DEV``` Develop a new feature (change second version number)
  - ```DEBUG``` Correct a bugs (change the third version number)
  - ```CI``` Change the CI interfaces
  - ```TEST``` Add new test to improve code (must be done before the [DEV] commit.
  - ```STYLE``` Style correction
  - ```RELEASE``` Release a new version (change tag and associated with a tag)
  - ```DOC``` Update the documentation
  - ```OTHER``` Can not be categorize

Categorize the type of commit permit to known the type of change in the code and correctly update the version numbers.

