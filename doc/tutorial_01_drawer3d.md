How to display Element in a viewer
==================================

To diplay some things in a ```Viewer``` you need to implement the display.

To permit the draw you need to create a class that implement ```DrawableObject3D```.

For example:

```{.java}

public class SampleLooper implements DrawableObject3D {
	private final float position;

	//...

	@Override
	public void draw(GL2 gl, Color3f color) {
		gl.glLineWidth(5);
		gl.glBegin(GL_LINES);
		float xxx = (float) Math.cos(this.position) * 10.0f;
		float yyy = (float) Math.sin(this.position) * 10.0f;
		gl.glColor3f(color.x, color.y, color.z);
		gl.glVertex3f(xxx + 0.0f, yyy + 0.0f, 0.0f);
		gl.glVertex3f(xxx + 1.0f, yyy + 0.0f, 0.0f);
		gl.glColor3f(color.x, color.y, color.z);
		gl.glVertex3f(xxx + 1.0f, yyy + 0.0f, 0.0f);
		gl.glVertex3f(xxx + 1.0f, yyy + 1.0f, 0.0f);
		gl.glColor3f(color.x, color.y, color.z);
		gl.glVertex3f(xxx + 1.0f, yyy + 1.0f, 0.0f);
		gl.glVertex3f(xxx + 0.0f, yyy + 1.0f, 0.0f);
		gl.glColor3f(color.x, color.y, color.z);
		gl.glVertex3f(xxx + 0.0f, yyy + 1.0f, 0.0f);
		gl.glVertex3f(xxx + 0.0f, yyy + 0.0f, 0.0f);
		gl.glEnd();
	}
}

```

Now you need to declare the capability to draw your class in the ```Drawer3D```. For this you need to declare the drawer in the plugin declaration. In ```Plugin.java```:


```{.java}
public class Plugin implements PluginsSupplier, PluginsGuiCoreSupplier {
	@Override
	public void populateDrawers(DrawersConsumer drawersConsumer) {
		// declare the class "SampleLooper" can be draw by "Drawer3D".
		drawersConsumer.accept(SampleLooper.class, Drawer3D.class);
	}
}
```

Now you can display all the element like this.

Full example availlable [HERE](https://gitlab.com/scenarium/scenarium-core/-/tree/master/doc/tutorial_01/project/project)

Sample Result: 
![Result sample|titleShow=true|align=center](tutorial_01/result.png)


