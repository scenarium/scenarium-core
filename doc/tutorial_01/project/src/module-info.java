
import io.scenarium.pluginManager.PluginsSupplier;
import io.scenarium.sample.gui.core.tutorial01.Plugin;

module scenarium.sample.gui.core.tutorial01 {
	// Request use of the PluginsSupplier module
	uses PluginsSupplier;

	// Say that it privide an external API
	provides PluginsSupplier with Plugin;

	// When you have some graphic interfaces
	requires io.scenarium.gui.flow;

	exports io.scenarium.sample.gui.core.tutorial01.operator.sample;
	exports io.scenarium.sample.gui.core.tutorial01.model;

}