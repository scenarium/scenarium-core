package io.scenarium.sample.gui.core.tutorial01.model;

import static com.jogamp.opengl.GL.GL_LINES;

import javax.vecmath.Color3f;

import io.scenarium.core.struct.DrawableObject3D;

import com.jogamp.opengl.GL2;

public class SampleLooper implements DrawableObject3D {
	private final float position;

	public float getPosition() {
		return this.position;
	}

	public SampleLooper(float position) {
		this.position = position;
	}

	@Override
	public void draw(GL2 gl, Color3f color) {
		gl.glLineWidth(5);
		gl.glBegin(GL_LINES);
		float xxx = (float) Math.cos(this.position) * 10.0f;
		float yyy = (float) Math.sin(this.position) * 10.0f;
		gl.glColor3f(color.x, color.y, color.z);
		gl.glVertex3f(xxx + 0.0f, yyy + 0.0f, 0.0f);
		gl.glVertex3f(xxx + 1.0f, yyy + 0.0f, 0.0f);
		gl.glColor3f(color.x, color.y, color.z);
		gl.glVertex3f(xxx + 1.0f, yyy + 0.0f, 0.0f);
		gl.glVertex3f(xxx + 1.0f, yyy + 1.0f, 0.0f);
		gl.glColor3f(color.x, color.y, color.z);
		gl.glVertex3f(xxx + 1.0f, yyy + 1.0f, 0.0f);
		gl.glVertex3f(xxx + 0.0f, yyy + 1.0f, 0.0f);
		gl.glColor3f(color.x, color.y, color.z);
		gl.glVertex3f(xxx + 0.0f, yyy + 1.0f, 0.0f);
		gl.glVertex3f(xxx + 0.0f, yyy + 0.0f, 0.0f);
		gl.glEnd();
	}

}
