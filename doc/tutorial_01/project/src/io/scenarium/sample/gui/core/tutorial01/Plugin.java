package io.scenarium.sample.gui.core.tutorial01;

import io.scenarium.flow.consumer.ClonerConsumer;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.gui.core.PluginsGuiCoreSupplier;
import io.scenarium.gui.core.consumer.DrawersConsumer;
import io.scenarium.gui.core.display.drawer.Drawer3D;
import io.scenarium.pluginManager.PluginsSupplier;
import io.scenarium.sample.gui.core.tutorial01.model.SampleLooper;
import io.scenarium.sample.gui.core.tutorial01.operator.sample.GuiCoreTutorial01;

public class Plugin implements PluginsSupplier, PluginsGuiCoreSupplier, PluginsFlowSupplier {

	@Override
	public void populateDrawers(DrawersConsumer drawersConsumer) {
		// declare the class "SampleLooper" can be draw by "Drawer3D".
		drawersConsumer.accept(SampleLooper.class, Drawer3D.class);
	}

	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(GuiCoreTutorial01.class);
	}

	@Override
	public void populateCloners(ClonerConsumer clonerConsumer) {
		clonerConsumer.accept(SampleLooper.class, o -> o);
	}

}
