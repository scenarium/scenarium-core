package io.scenarium.sample.gui.core.tutorial01.operator.sample;

import io.scenarium.sample.gui.core.tutorial01.model.SampleLooper;

public class GuiCoreTutorial01 {

	public void birth() {

	}

	public void death() {

	}

	public SampleLooper process(Float value) {
		return new SampleLooper(value / 180.0f * 3.0141596f * 3.0f);
	}

}
