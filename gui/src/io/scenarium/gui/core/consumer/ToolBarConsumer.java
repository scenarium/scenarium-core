package io.scenarium.gui.core.consumer;

import io.scenarium.gui.core.display.RenderPane;
import io.scenarium.gui.core.display.ToolBarInfo;

public class ToolBarConsumer {
	public boolean accept(ToolBarInfo toolBarDesc) {
		return RenderPane.addToolBarDescs(toolBarDesc);
	}
}
