package io.scenarium.gui.core.consumer;

import io.scenarium.gui.core.display.drawer.DrawerManager;
import io.scenarium.gui.core.display.drawer.TheaterPanel;

public class DrawersConsumer {
	public boolean accept(Class<?> classToDraw, Class<? extends TheaterPanel> associatedDrawer) {
		return DrawerManager.registerDrawer(classToDraw, associatedDrawer);
	}
}
