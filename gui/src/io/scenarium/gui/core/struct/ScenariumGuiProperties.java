package io.scenarium.gui.core.struct;

import java.io.File;

import javax.vecmath.Point2d;

import io.beanmanager.BeanManager;
import io.scenarium.gui.core.display.toolbarclass.ToolBarDescriptor;

public class ScenariumGuiProperties {
	private static ScenariumGuiProperties scenariumGuiProperties;
	private String language;
	private String lookAndFeel;
	private Point2d mainFramePosition;
	private Point2d mainFrameDimension;
	private boolean mainFrameAlwaysOnTop;
	private ToolBarDescriptor[] visibleToolsDesc;
	private boolean checkUpdatesAtStarup = true;
	private boolean askBeforeQuit = false;
	private File scenarioChooserPath;
	private File playListChooserPath;
	private File recordChooserPath;
	private boolean hiddenFieldVisible = false;
	private boolean expertFieldVisible = false;
	private boolean showHiddenProperties = false;
	private boolean showExpertProperties = true;

	public static ScenariumGuiProperties get() {
		if (scenariumGuiProperties == null)
			scenariumGuiProperties = new ScenariumGuiProperties();
		return scenariumGuiProperties;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLookAndFeel() {
		return this.lookAndFeel;
	}

	public Point2d getMainFrameDimension() {
		return this.mainFrameDimension;
	}

	public Point2d getMainFramePosition() {
		return this.mainFramePosition;
	}

	public File getPlayListChooserPath() {
		return this.playListChooserPath;
	}

	public File getRecordChooserPath() {
		return this.recordChooserPath;
	}

	public File getScenarioChooserPath() {
		return this.scenarioChooserPath;
	}

	public ToolBarDescriptor[] getVisibleToolsDesc() {
		return this.visibleToolsDesc;
	}

	public boolean isAskBeforeQuit() {
		return this.askBeforeQuit;
	}

	public boolean isCheckUpdatesAtStarup() {
		return this.checkUpdatesAtStarup;
	}

	public boolean isExpertFieldVisible() {
		return this.expertFieldVisible;
	}

	public boolean isHiddenFieldVisible() {
		return this.hiddenFieldVisible;
	}

	public boolean isMainFrameAlwaysOnTop() {
		return this.mainFrameAlwaysOnTop;
	}

	public boolean isShowExpertProperties() {
		return this.showExpertProperties;
	}

	public boolean isShowHiddenProperties() {
		return this.showHiddenProperties;
	}

	public void setAskBeforeQuit(boolean askBeforeQuit) {
		this.askBeforeQuit = askBeforeQuit;
	}

	public void setCheckUpdatesAtStarup(boolean checkUpdatesAtStarup) {
		this.checkUpdatesAtStarup = checkUpdatesAtStarup;
	}

	public void setExpertFieldVisible(boolean expertFieldVisible) {
		this.expertFieldVisible = expertFieldVisible;
	}

	public void setHiddenFieldVisible(boolean hiddenFieldVisible) {
		this.hiddenFieldVisible = hiddenFieldVisible;
	}

	public void setLookAndFeel(String lookAndFeel) {
		this.lookAndFeel = lookAndFeel;
	}

	public void setMainFrameAlwaysOnTop(boolean mainFrameAlwaysOnTop) {
		this.mainFrameAlwaysOnTop = mainFrameAlwaysOnTop;
	}

	public void setMainFrameDimension(Point2d mainFrameDimension) {
		this.mainFrameDimension = mainFrameDimension;
	}

	public void setMainFramePosition(Point2d mainFramePosition) {
		this.mainFramePosition = mainFramePosition;
	}

	public void setPlayListChooserPath(File playListChooserPath) {
		this.playListChooserPath = playListChooserPath;
	}

	public void setRecordChooserPath(File recordChooserPath) {
		this.recordChooserPath = recordChooserPath;
	}

	public void setScenarioChooserPath(File scenarioChooserPath) {
		if (scenarioChooserPath != null && !scenarioChooserPath.isDirectory())
			scenarioChooserPath = scenarioChooserPath.getParentFile();
		this.scenarioChooserPath = scenarioChooserPath;
	}

	public void setShowExpertProperties(boolean showExpertProperties) {
		this.showExpertProperties = showExpertProperties;
		BeanManager.isExpertFieldVisible = showExpertProperties;
	}

	public void setShowHiddenProperties(boolean showHiddenProperties) {
		this.showHiddenProperties = showHiddenProperties;
		BeanManager.isHiddenFieldVisible = showHiddenProperties;
	}

	public void setVisibleToolsDesc(ToolBarDescriptor[] visibleToolsDesc) {
		this.visibleToolsDesc = visibleToolsDesc;
	}

	public boolean isDarkTheme() {
		return this.lookAndFeel != null && this.lookAndFeel.endsWith("_DARK");
	}
}
