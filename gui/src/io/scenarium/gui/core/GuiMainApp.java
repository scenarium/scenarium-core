package io.scenarium.gui.core;

import javax.vecmath.Point2i;

import io.scenarium.core.MainApp;
import io.scenarium.gui.core.display.toolbarclass.ToolBarDescriptor;

public interface GuiMainApp extends MainApp {

	public Point2i getMainFrameDimension();

	public Point2i getMainFramePosition();

	public ToolBarDescriptor[] getVisibleToolsDesc();

	public void updateRecentScenario();
}
