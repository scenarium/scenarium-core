/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import java.io.File;

import io.scenarium.core.filemanager.DataLoader;
import io.scenarium.core.filemanager.playlist.PlayListManager;
import io.scenarium.gui.core.struct.ScenariumGuiProperties;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;

public class PlayListFileChooserFx {

	private PlayListFileChooserFx() {}

	public static File fileChooser(Window window, DataLoader dataLoader, boolean open, File path) {
		FileChooser fc = new FileChooser();
		fc.setTitle("PlayList file chooser");
		File cp = ScenariumGuiProperties.get().getPlayListChooserPath();
		fc.setInitialDirectory(path != null && path.exists() ? path : cp != null && cp.exists() ? cp : new File(System.getProperty("user.dir")));
		fc.getExtensionFilters().add(0, new ExtensionFilter("PlayList", "*." + PlayListManager.ext));
		if (open)
			return fc.showOpenDialog(window);
		return fc.showSaveDialog(window);
	}
}
