/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.toolbarclass;

import io.scenarium.gui.core.display.RenderPane;

import javafx.scene.input.KeyCode;
import javafx.scene.layout.Region;

public abstract class Tool {
	public RenderPane renderPane;
	protected KeyCode keycode;

	public void dispose() {}

	public abstract Region getRegion();

	public abstract boolean instantiate();

	public boolean instantiate(RenderPane renderPane, KeyCode keycode) {
		this.renderPane = renderPane;
		this.keycode = keycode;
		return instantiate();
	};
}
