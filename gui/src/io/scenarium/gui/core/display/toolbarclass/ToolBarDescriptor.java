/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.toolbarclass;

import javax.vecmath.Point2i;

public class ToolBarDescriptor {
	public final String toolType;
	public final Point2i toolPos;
	public boolean alwaysOnTop;

	public ToolBarDescriptor(String toolType, Point2i toolPos, boolean alwaysOnTop) {
		this.toolType = toolType;
		this.toolPos = toolPos;
		this.alwaysOnTop = alwaysOnTop;
	}

	@Override
	public String toString() {
		return "ToolBarDescriptor [toolType=" + this.toolType + ", toolPos=" + this.toolPos + ", alwaysOnTop=" + this.alwaysOnTop + "]";
	}
}
