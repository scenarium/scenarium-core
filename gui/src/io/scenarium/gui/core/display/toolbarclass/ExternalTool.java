/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.toolbarclass;

import javax.vecmath.Point2d;

import io.beanmanager.tools.FxUtils;
import io.scenarium.gui.core.internal.LoadPackageStream;

import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public abstract class ExternalTool extends Tool {
	protected Stage stage;

	@Override
	public void dispose() {
		this.stage.close();
	}

	protected String getIconName() {
		return "scenarium_icon.png";
	}

	public Point2d getPosition() {
		return new Point2d(this.stage.getX(), this.stage.getY());
	}

	@Override
	public boolean instantiate() {
		this.stage = new Stage();
		this.stage.setTitle(getClass().getSimpleName());
		this.stage.getIcons().add(new Image(LoadPackageStream.getStream("/" + getIconName())));
		this.stage.addEventHandler(KeyEvent.KEY_PRESSED, e -> { // Handler -> sauf si quelqu'un a consumed
			if (!e.isConsumed() && !e.isControlDown() && !e.isShiftDown() && e.getCode() == this.keycode && this.renderPane != null)
				this.renderPane.closeTool(getClass());
		});
		Region parent = getRegion();
		if (parent == null)
			throw new IllegalArgumentException("getValue on the class : " + getClass() + " return forbidden value null");
		Scene scene = new Scene(parent, -1, -1);
		this.stage.setScene(scene);
		this.stage.sizeToScene();
		// parent.widthProperty().addListener(e -> Platform.runLater(() -> stage.sizeToScene())); //Les enfant peuvent pas s'adapter sinon
		// parent.heightProperty().addListener(e -> Platform.runLater(() -> stage.sizeToScene()));
		this.stage.setOnCloseRequest(e -> this.renderPane.closeTool(getClass()));
		// ScenicView.show(scene);
		return true;
	}

	public boolean isAlwaysOnTop() {
		return this.stage.isAlwaysOnTop();
	}

	public void setAlwaysOnTop(boolean alwaysOnTop) {
		this.stage.setAlwaysOnTop(alwaysOnTop);
	}

	public void setPosition(Point2D toolPos) {
		if (FxUtils.isLocationInScreenBounds(toolPos)) {
			this.stage.setX(toolPos.getX());
			this.stage.setY(toolPos.getY());
		}
	}

	public void show() {
		this.stage.show();
	}

	public void requestFocus() {
		this.stage.requestFocus();
	}
}
