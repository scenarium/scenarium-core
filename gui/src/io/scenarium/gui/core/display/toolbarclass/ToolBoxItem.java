/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.toolbarclass;

public class ToolBoxItem {
	public final int id;
	public final String imgName;
	public final String tooltip;

	public ToolBoxItem(int id, String imgName, String tooltip) {
		this.id = id;
		this.imgName = imgName;
		this.tooltip = tooltip;
	}
}
