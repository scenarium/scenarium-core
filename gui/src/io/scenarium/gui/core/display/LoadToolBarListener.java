/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import java.util.EventListener;

public interface LoadToolBarListener extends EventListener {
	public void loaded(ToolBarInfo toolBarInfo);

	public void unloaded(ToolBarInfo toolBarInfo);
}
