/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import java.io.File;
import java.io.IOException;

import io.scenarium.core.updater.Updater;

//import de.codecentric.centerdevice.javafxsvg.SvgImageLoaderFactory;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

public class AboutDialog {
	// private static final String MAIL = "admin@scenarium.link";
	private static final String TIKETS = "https://sourceforge.net/p/revpandore/tickets/new/";
	private static Stage aboutDialog;
	// private static String softwareName = "Scenarium";

	public void showAbout(Window owner, HostServices hostServices) {
		if (aboutDialog != null) {
			aboutDialog.close();
			aboutDialog = null;
		}
		aboutDialog = new Stage();
		aboutDialog.initModality(Modality.WINDOW_MODAL);
		aboutDialog.getIcons().add(RenderFrame.SCENARIUM_ICON);
		aboutDialog.setTitle("About " + Updater.softwareNameSupplier.get());

		Label softText = new Label(Updater.softwareNameSupplier.get());
		softText.setFont(new Font(48));
		softText.setPadding(new Insets(5, 3, 0, 3));
		Label l = new Label(Updater.versionSupplier.get());
		l.setPadding(new Insets(5, 0, 5, 0));

		Hyperlink hlLicenses = new Hyperlink(TIKETS);
		hlLicenses.setStyle("-fx-focus-color: transparent;");
		hlLicenses.setOnAction(e -> {
			Stage licensingStage = new Stage();
			licensingStage.initModality(Modality.WINDOW_MODAL);
			WebView webView = new WebView();
			webView.setPrefSize(640, 480);
			webView.getEngine().load(AboutDialog.class.getResource("/Licenses.html").toString());
			licensingStage.setScene(new Scene(webView));
			licensingStage.show();
		});
		hlLicenses.setText("Licensing Information");
		hlLicenses.setPadding(new Insets(0));
		HBox softwareBox = new HBox(10, l, hlLicenses);
		softwareBox.setAlignment(Pos.BASELINE_LEFT);

		VBox textBox = new VBox(softText, softwareBox);
		textBox.setPadding(new Insets(5, 5, 5, 0));

		Updater updater = Updater.getInstance();
		if (updater.isUpdatable()) {
			BorderPane borderPaneVersion = new BorderPane();
			borderPaneVersion.setPrefHeight(25);
			ProgressIndicator pi = new ProgressIndicator();
			pi.setPrefSize(40, 20);
			Label labelUpdate = new Label("Check for updates...");
			BorderPane.setAlignment(labelUpdate, Pos.CENTER_LEFT);
			borderPaneVersion.setCenter(labelUpdate);
			borderPaneVersion.setLeft(pi);
			textBox.getChildren().add(borderPaneVersion);
			Task<Void> task = new Task<>() {
				@Override
				public Void call() {
					Boolean isNewVersion = updater.isNewVersion();
					Platform.runLater(() -> {
						borderPaneVersion.getChildren().clear();
						if (isNewVersion == null) {
							Label label = new Label("Cannot check updates, no response from server");
							borderPaneVersion.setLeft(label);
							BorderPane.setAlignment(label, Pos.CENTER_LEFT);
						} else if (isNewVersion) {
							Button updateButton = new Button("Update");
							updateButton.setMinHeight(0);
							updateButton.prefHeightProperty().bind(borderPaneVersion.prefHeightProperty());
							updateButton.setOnAction(e -> {
								ProgressBar pb = new ProgressBar();
								pb.setProgress(0);
								pb.setPrefSize(400, 30);
								Label text = new Label("0.00%");
								updater.addProgressListener(progress -> Platform.runLater(() -> {
									pb.setProgress(progress);
									text.setText(String.format("%.2f", progress * 100) + "%");
								}));
								borderPaneVersion.getChildren().clear();
								borderPaneVersion.setCenter(new StackPane(pb, text));
								Task<Void> task = new Task<>() {
									@Override
									public Void call() {
										Runnable javaFxTask;
										try {
											File newJarFile = updater.update();
											javaFxTask = () -> {
												borderPaneVersion.getChildren().clear();
												Label rl = new Label("Needs to restart to complete the installation");
												borderPaneVersion.setLeft(rl);
												BorderPane.setAlignment(rl, Pos.CENTER_LEFT);
												Button restartButton = new Button("Restart");
												restartButton.setMinHeight(0);
												restartButton.prefHeightProperty().bind(borderPaneVersion.prefHeightProperty());
												restartButton.setOnAction(e1 -> updater.restart(newJarFile));
												borderPaneVersion.setRight(restartButton);
											};
										} catch (IOException ex) {
											javaFxTask = () -> {
												borderPaneVersion.getChildren().clear();
												borderPaneVersion.setLeft(new Label("Update canceled: " + ex.getMessage()));
											};
										}
										Platform.runLater(javaFxTask);
										return null;
									}
								};
								Thread t1 = new Thread(task);
								t1.start();
							});
							Label label = new Label("New version available: " + updater.getLastVersion());
							borderPaneVersion.setLeft(label);
							BorderPane.setAlignment(label, Pos.CENTER_LEFT);
							borderPaneVersion.setRight(updateButton);
						} else {
							Label label = new Label(Updater.softwareNameSupplier.get() + " is up to date");
							borderPaneVersion.setLeft(label);
							BorderPane.setAlignment(label, Pos.CENTER_LEFT);
						}
					});
					return null;
				}
			};
			new Thread(task).start();
		}
		Hyperlink hl = new Hyperlink(TIKETS);
		hl.setStyle("-fx-focus-color: transparent;");
		hl.setOnAction(e -> hostServices.showDocument(TIKETS));
		hl.setText("create a ticket");
		hl.setPadding(new Insets(0));
		HBox requestBox = new HBox(new Label("If you have a bug or request, you can"), hl);
		requestBox.setSpacing(1);
		requestBox.setPadding(new Insets(5, 0, 5, 0));

		requestBox.setAlignment(Pos.BASELINE_CENTER);
		Label label = new Label("Scenarium is a software developed by Marc Revilloud");
		label.setPadding(new Insets(5, 0, 5, 0));

		textBox.getChildren().addAll(label, requestBox);

		WebView webView = new WebView();
		webView.prefWidthProperty().bind(textBox.heightProperty());
		webView.prefHeightProperty().bind(textBox.heightProperty());
		// webView.getEngine().load(new File("/home/revilloud/workspace/ScenariumFx/resource/scenarium.svg").toURI().toString());
		webView.getEngine().load(AboutDialog.class.getResource("/scenarium.svg").toString());
		webView.zoomProperty().bind(webView.widthProperty().divide(256));
		// webView.setContextMenuEnabled(false);

		HBox hBox = new HBox(webView, textBox);
		hBox.setStyle("-fx-background-color: -fx-control-inner-background;");

		hBox.setSpacing(20);
		hBox.setPadding(new Insets(5));
		hBox.layout();
		aboutDialog.setScene(new Scene(hBox));
		aboutDialog.show();
		aboutDialog.sizeToScene();
		// Platform.runLater(() -> aboutDialog.setResizable(false)); // runlater sinon pas bonne taille
	}
}
