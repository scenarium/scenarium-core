/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import static io.scenarium.gui.core.display.LanguageManager.getText;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Stack;

import javax.imageio.ImageIO;
import javax.vecmath.Point2d;
import javax.vecmath.Point2i;

import io.beanmanager.BeanManager;
import io.beanmanager.editors.basic.InetSocketAddressEditor;
import io.beanmanager.tools.FxUtils;
import io.scenarium.core.Scenarium;
import io.scenarium.core.filemanager.DataLoader;
import io.scenarium.core.filemanager.playlist.PlayListManager;
import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;
import io.scenarium.core.filemanager.scenariomanager.StreamScenario;
import io.scenarium.core.filemanager.scenariomanager.StreamScenarioSource;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.core.timescheduler.SchedulerState;
import io.scenarium.core.timescheduler.VisuableSchedulable;
import io.scenarium.core.tools.AnimationTimerConsumer;
import io.scenarium.core.tools.ViewerAnimationTimer;
import io.scenarium.core.tools.ViewerAnimationTimerSupplier;
import io.scenarium.core.tools.VisuableSchedulableContainer;
import io.scenarium.gui.core.ScenariumGui;
import io.scenarium.gui.core.display.drawer.TheaterPanel;
import io.scenarium.gui.core.display.toolbarclass.ExternalTool;
import io.scenarium.gui.core.display.toolbarclass.Tool;
import io.scenarium.gui.core.display.toolbarclass.ToolBarDescriptor;
import io.scenarium.gui.core.internal.LoadPackageStream;
import io.scenarium.gui.core.internal.Log;
import io.scenarium.gui.core.struct.ScenariumGuiProperties;

import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class RenderFrame implements ScenariumContainer, VisuableSchedulable, LoadToolBarListener {
	public static final Image SCENARIUM_ICON = new Image(LoadPackageStream.getStream("/scenarium_icon.png"));
	private static final String TOOLBAR_PACKAGE_NAME = "toolBar";

	private Stage primaryStage;
	private final DataLoader dataLoader;
	private RenderPane renderPane;
	private HashMap<Class<? extends Tool>, CheckMenuItem> checkMenuItemTools;
	private MenuBar menuBar;
	private Menu menuRecentScenario;
	private HostServices hostServices;
	private ViewerAnimationTimer viewerAnimationTimer;

	private ScenariumGui scenariumFx;

	public RenderFrame(Stage primaryStage, ScenariumGui scenariumFx, final DataLoader dataLoader, boolean isMenuBar) {
		this.primaryStage = primaryStage;
		this.dataLoader = dataLoader;
		this.scenariumFx = scenariumFx;
		this.hostServices = scenariumFx.getHostServices(); // TODO remove field

		setTitle(getTitle());
		primaryStage.getIcons().add(SCENARIUM_ICON);
		LanguageManager.loadDefaultLanguage();
		StackPane renderPane = createRenderPane();
		VBox.setVgrow(renderPane, Priority.ALWAYS);
		VBox vbox = new VBox(initMenuBar(), renderPane);
		Scene scene = new Scene(isMenuBar ? vbox : renderPane, -1, -1);
		// scene.getStylesheets().add("modena_dark.css");
		// scene.setUserAgentStylesheet("modena_dark.css");
		primaryStage.setScene(scene);
		// ScenicView.show(scene);

		primaryStage.setOnCloseRequest(e -> {
			new Thread(new Task<Boolean>() {
				@Override
				protected Boolean call() {
					scenariumFx.getScheduler().stop();
					Platform.runLater(() -> {
						if (ScenariumGuiProperties.get().isAskBeforeQuit()) {
							Alert alert = new Alert(AlertType.CONFIRMATION, "", new ButtonType("Yes", ButtonData.YES), new ButtonType("No", ButtonData.NO),
									new ButtonType("Cancel", ButtonData.CANCEL_CLOSE));
							alert.setTitle("Scenario Save");
							alert.setHeaderText("Save the Scenario modification?");
							ButtonData answer = alert.showAndWait().get().getButtonData();
							if (answer == ButtonData.YES) {
								if (dataLoader.getScenario() != null)
									saveScenario();
								else
									saveScenarioAs(null);
							} else if (answer == ButtonData.CANCEL_CLOSE)
								return;
						}
						// RenderFrameFx.this.primaryStage.close();
						// try {
						// dataLoader.getScenario().closeRecorderStream();
						// } catch (StreamException ex) {
						// Alert a = new Alert(AlertType.ERROR, "patate");
						// a.setTitle(e1.getMessage() + "\nCause: " + e1.getCause().getMessage());
						// a.showAndWait();
						// }
						new BeanManager(scenariumFx.getScheduler(), Scenarium.getWorkspace()).save(new File(Scenarium.getWorkspace() + File.separator + "SchedulerConfig.txt"), false);
						ScenariumGuiProperties scenariumProperties = ScenariumGuiProperties.get();
						scenariumProperties.setMainFramePosition(new Point2d(primaryStage.getX(), primaryStage.getY()));
						scenariumProperties.setMainFrameDimension(new Point2d(primaryStage.getWidth(), primaryStage.getHeight()));
						scenariumProperties.setMainFrameAlwaysOnTop(primaryStage.isAlwaysOnTop());
						scenariumProperties.setVisibleToolsDesc(getVisibleToolsDesc().toArray(ToolBarDescriptor[]::new));
						RenderFrame.this.renderPane.close();
						System.exit(0);
					});
					return true;
				}
			}).start();
			e.consume();
		});
		scene.addEventHandler(DragEvent.DRAG_OVER, e -> {
			Dragboard db = e.getDragboard();
			if (db.hasFiles()) {
				boolean valid = true;
				for (File file : db.getFiles())
					if (!file.isDirectory() && DataLoader.getFileType(file) == DataLoader.UNSUPPORTED) {
						valid = false;
						break;
					}
				if (valid) {
					e.acceptTransferModes(TransferMode.COPY_OR_MOVE);
					e.consume();
				}
			}
		});
		scene.addEventHandler(DragEvent.DRAG_DROPPED, e -> {
			Dragboard db = e.getDragboard();
			if (db.hasFiles()) {
				PlayListManager plm = dataLoader.playListManager;
				plm.clear();
				for (File file : db.getFiles())
					plm.addToPlayList(-1, file);
				new Thread(() -> dataLoader.reload(plm.goToFirstScenario(), false, true)).start();
				e.setDropCompleted(true);
				e.consume();
			}
		});
		if (this.menuBar != null)
			primaryStage.fullScreenProperty().addListener((obs, oldisFullScreen, isFullScreen) -> {
				ObservableList<Node> children = ((VBox) scene.getRoot()).getChildren();
				if (isFullScreen)
					children.remove(0);
				else
					children.add(0, this.menuBar);
			});
		primaryStage.setFullScreenExitHint("");
		Point2d mfp = ScenariumGuiProperties.get().getMainFramePosition();
		if (mfp != null) {
			Point2D mfpd = new Point2D(Math.max(mfp.x, 0), Math.max(mfp.y, 0));
			if (FxUtils.isLocationInScreenBounds(mfpd)) {
				primaryStage.setX(mfp.x);
				primaryStage.setY(mfp.y);
			}
			Rectangle2D currentBound = null;
			for (Screen screen : Screen.getScreens()) {
				Rectangle2D b = screen.getVisualBounds();
				if (b.contains(new Point2D(mfp.x, mfp.y))) {
					currentBound = b;
					break;
				}
			}
			if (currentBound == null)
				currentBound = Screen.getPrimary().getVisualBounds();
			primaryStage.setMaxWidth(currentBound.getWidth());
			primaryStage.setMaxHeight(currentBound.getHeight());

		} else {
			Rectangle2D b = Screen.getPrimary().getVisualBounds();
			primaryStage.setMaxWidth(b.getWidth());
			primaryStage.setMaxHeight(b.getHeight());
		}
		Point2d mfd = ScenariumGuiProperties.get().getMainFrameDimension();
		if (mfd != null) {
			primaryStage.setWidth(mfd.x);
			primaryStage.setHeight(mfd.y);
		} else
			primaryStage.sizeToScene();
		primaryStage.setAlwaysOnTop(ScenariumGuiProperties.get().isMainFrameAlwaysOnTop());
		primaryStage.show();

		if (scenariumFx.getScheduler() != null)
			scenariumFx.getScheduler().addPropertyChangeListener(state -> {
				// FxUtils.runLaterIfNeeded(() -> { //Non!! sinon je peux traiter un start alors qu'on est stop
				if (state == SchedulerState.STARTED) {
					this.viewerAnimationTimer = ViewerAnimationTimerSupplier.create();
					AnimationTimerConsumer animationTimerConsumer = new AnimationTimerConsumer() {
						@Override
						public void register(VisuableSchedulable visuableSchedulable) {
							ViewerAnimationTimer vat = RenderFrame.this.viewerAnimationTimer;
							if (vat != null && visuableSchedulable != null)
								vat.register(visuableSchedulable);
						}

						@Override
						public void unRegister(VisuableSchedulable visuableSchedulable) {
							ViewerAnimationTimer vat = RenderFrame.this.viewerAnimationTimer;
							if (vat != null && visuableSchedulable != null)
								vat.unRegister(visuableSchedulable);
						}
					};
					this.renderPane.setAnimationTimer(animationTimerConsumer);
					Scenario scenario = dataLoader.getScenario();
					if (scenario instanceof VisuableSchedulableContainer)
						((VisuableSchedulableContainer) scenario).setAnimationTimer(animationTimerConsumer);
					this.viewerAnimationTimer.register(this);
					// Log.error("viewerAnimationTimer.start STARTED ");
					this.viewerAnimationTimer.start();
				} else if (state == SchedulerState.PRESTOP) {
					this.renderPane.setAnimationTimer(null);
					Scenario scenario = dataLoader.getScenario();
					if (scenario instanceof VisuableSchedulableContainer)
						((VisuableSchedulableContainer) scenario).setAnimationTimer(null);
					if (this.viewerAnimationTimer != null) {
						// Log.error("viewerAnimationTimer.stop STOPPED " + ProcessHandle.current().pid());
						this.viewerAnimationTimer.stop();
						this.viewerAnimationTimer = null;
					}
				}
				// });
			});
		RenderPane.addLoadToolBarListener(this);
		TheaterPanel tp = this.renderPane.getTheaterPane();
		tp.setIgnoreRepaint(false);
		tp.repaint(true);
	}

	@Override
	public void adaptSizeToDrawableElement() {
		this.renderPane.adaptSizeToDrawableElement();
	}

	private MenuItem createMenuItem(boolean isCheckMenu, String name, String imagePath, KeyCombination keyCombination, EventHandler<ActionEvent> action) {
		// Log.info("menu item created " + name);
		MenuItem menuitem = isCheckMenu ? new CheckMenuItem(getText(name), imagePath == null ? null : new ImageView(new Image(LoadPackageStream.getStream("/" + imagePath))))
				: new MenuItem(getText(name), imagePath == null ? null : new ImageView(new Image(LoadPackageStream.getStream("/" + imagePath))));
		menuitem.setAccelerator(keyCombination); // TODO Leak here JDK-8116412
		menuitem.setOnAction(action);
		return menuitem;
	}

	public StackPane createRenderPane() {
		this.renderPane = new RenderPane(this.dataLoader, this, true, true);
		return this.renderPane.getPane();
	}

	@Override
	public Point2i getDefaultToolBarLocation(String toolType) {
		ToolBarDescriptor[] vtd = ScenariumGuiProperties.get().getVisibleToolsDesc();
		if (vtd != null)
			for (ToolBarDescriptor tbd : vtd)
				if (tbd.toolType.equals(toolType))
					return tbd.toolPos;
		return null;
	}

	public Point2i getDimention() {
		return new Point2i((int) this.primaryStage.getWidth(), (int) this.primaryStage.getHeight());
	}

	private Menu getMenuAbout() {
		Menu aboutMenu = new Menu("?");
		MenuItem aboutItem = createMenuItem(false, "About", null, new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN),
				e -> new AboutDialog().showAbout(this.primaryStage, this.hostServices));
		MenuItem helpItem = createMenuItem(false, "Help", null, new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN),
				e -> this.hostServices.showDocument("https://revpandore.sourceforge.io/fr/index.html"));
		aboutMenu.getItems().addAll(aboutItem, helpItem);
		return aboutMenu;
	}

	private Menu getMenuEdit() {
		Menu editMenu = new Menu(getText("Edit"));
		MenuItem aboutItem = createMenuItem(false, "Scenario Properties", null, new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN),
				e -> ScenarioProperties.showProperties(this.primaryStage, this.dataLoader.getScenario()));
		editMenu.getItems().add(aboutItem);
		return editMenu;
	}

	private Menu getMenuFile() {
		Menu fileMenu = new Menu(getText("File"));
		Menu newScenarioMenu = new Menu(getText("New Scenario"));
		ObservableList<MenuItem> items = newScenarioMenu.getItems();
		for (Class<? extends Scenario> scenarioType : ScenarioManager.getRegisterScenario())
			if (LocalScenario.class.isAssignableFrom(scenarioType))
				try {
					LocalScenario ls = (LocalScenario) scenarioType.getConstructor().newInstance();
					if (ls.canCreateDefault()) {
						MenuItem menuItemNewScenario = new MenuItem(scenarioType.getSimpleName());
						menuItemNewScenario.setOnAction(e -> new Thread(() -> this.dataLoader.reload(scenarioType, true, true)).start());
						items.add(menuItemNewScenario);
					}
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
					ex.printStackTrace();
				}
		items = fileMenu.getItems();
		items.add(newScenarioMenu);
		items.add(createMenuItem(false, "Open Local Scenario", "open.gif", new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN), e -> openLocalScenario()));
		items.add(createMenuItem(false, "Open Network Scenario", "network.png", new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN), e -> openNetworkScenario()));
		this.menuRecentScenario = new Menu(getText("Recent Scenario"));
		items.add(this.menuRecentScenario);
		items.add(createMenuItem(false, "Save Scenario", "save.gif", new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN), e -> saveScenario()));
		items.add(createMenuItem(false, getText("Save Scenario as") + "...", "saveas.gif", new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN),
				e -> saveScenarioAs(null)));
		items.add(createMenuItem(false, "Scenario Information", "information.gif", new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN), e -> {
			ScenarioInformation.showInfo(this.primaryStage, this.dataLoader.getScenario());
		}));
		items.add(createMenuItem(false, "Options", "option.gif", new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN), e -> {
			new PropertyFrame().showProperty(this.primaryStage/* , this */);
		}));
		items.add(new SeparatorMenuItem());
		items.add(createMenuItem(false, "Export Raster To BMP", "bmp.gif", new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN), e -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Save Image");
			File file = fileChooser.showSaveDialog(this.primaryStage);
			if (file != null)
				try {
					ImageIO.write(SwingFXUtils.fromFXImage(this.renderPane.getTheaterPane().snapshot(new SnapshotParameters(), null), null), "png", file);
				} catch (IOException ex) {
					Log.info(ex.getMessage());
				}
		}));
		items.add(new SeparatorMenuItem());
		items.add(createMenuItem(false, "Exit", "exit.gif", new KeyCodeCombination(KeyCode.F4, KeyCombination.ALT_DOWN), e -> System.exit(0)));
		return fileMenu;
	}

	private void getMenuItemRecentScenario() {
		this.menuRecentScenario.getItems().clear();
		List<Object> rs = this.dataLoader.getRecentScenario();
		synchronized (rs) {
			int i = -1;
			ObservableList<MenuItem> items = this.menuRecentScenario.getItems();
			for (final Object recentScenario : rs) {
				if (recentScenario == null)
					continue;
				MenuItem menu = new MenuItem(++i + ": " + recentScenario.toString());
				if (!ScenarioManager.isAvailable(recentScenario))
					menu.setStyle("-fx-text-fill: red");
				if (i < 10)
					menu.setAccelerator(new KeyCodeCombination(KeyCode.getKeyCode("Numpad " + Integer.toString(i)), KeyCombination.CONTROL_DOWN)); // TODO Leak here JDK-8116412
				menu.setOnAction(e -> new Thread(() -> this.dataLoader.reload(recentScenario, true, true)).start());
				items.add(menu);
			}
			this.menuRecentScenario.setDisable(rs.size() == 0);
		}
	}

	private Menu getMenuLanguage(MenuBar menuBar) {
		Menu menuLanguage = new Menu(getText("Language"));
		for (String lt : LanguageManager.AVAILABLELANGUAGE) {
			CheckMenuItem lMenu = (CheckMenuItem) createMenuItem(true, lt, lt.toLowerCase() + ".gif",
					new KeyCodeCombination(KeyCode.getKeyCode(Character.toString(lt.charAt(0))), KeyCombination.ALT_DOWN), e -> {
						LanguageManager.loadLanguage(lt);
						Platform.runLater(() -> initMenuBar()); // TODO Pk Platform.runLater? ca plante sinon depuis JDK10
					});
			if (getText("language").equals(lt))
				lMenu.setSelected(true);
			menuLanguage.getItems().add(lMenu);
		}
		return menuLanguage;
	}

	@SuppressWarnings("unchecked")
	private Menu getMenuView(ToolBarDescriptor[] vtd) {
		Menu viewMenu = new Menu(getText("View"));
		LinkedHashMap<String, Object> mapCheckBoxMenuItemTools = new LinkedHashMap<>();
		ArrayList<ToolBarInfo> toolBarDescs = RenderPane.getToolBarDescs();
		this.checkMenuItemTools = new HashMap<>();
		for (int i = 0; i < toolBarDescs.size(); i++) {
			final ToolBarInfo toolBarDesc = toolBarDescs.get(i);
			WeakReference<Class<? extends Tool>> classWeakRef = new WeakReference<>(toolBarDesc.type); // weakReference used as a workaround for javaFx bug JDK-8116412
			CheckMenuItem cmi = (CheckMenuItem) createMenuItem(true, toolBarDesc.type.getSimpleName(), null, new KeyCodeCombination(toolBarDesc.keyCode), e -> {
				Class<? extends Tool> toolBarDescClass = classWeakRef.get();
				if (toolBarDescClass != null)
					this.renderPane.updateToolView(toolBarDescClass);
			});
			this.checkMenuItemTools.put(toolBarDesc.type, cmi);
			cmi.setSelected(this.renderPane.isDisplayed(toolBarDesc.type));
			String className = toolBarDesc.type.getName();
			className = className.contains(TOOLBAR_PACKAGE_NAME + ".") ? className.substring(className.lastIndexOf(TOOLBAR_PACKAGE_NAME) + TOOLBAR_PACKAGE_NAME.length() + 1) : "";
			LinkedHashMap<String, Object> currentNode = mapCheckBoxMenuItemTools;
			while (className.contains(".")) {
				int fi = className.indexOf(".");
				String parentName = className.substring(0, fi);
				String pn = parentName.replaceFirst(".", Character.toString(Character.toUpperCase(parentName.charAt(0))));
				parentName = LanguageManager.getText(pn);
				if (parentName == null)
					parentName = pn;
				className = className.substring(fi + 1);
				Object son = currentNode.get(parentName);
				if (currentNode.get(parentName) == null) {
					LinkedHashMap<String, Object> sonNode = new LinkedHashMap<>();
					currentNode.put(parentName, sonNode);
					currentNode = sonNode;
				} else if (!(son instanceof HashMap)) {
					Log.error("A tool have the same name than a tool package");
					System.exit(-1);
				} else
					currentNode = (LinkedHashMap<String, Object>) currentNode.get(parentName);
			}
			currentNode.put(className, cmi);
			// cmi.addActionListener((e) -> renderPanel.updateToolView(toolId, getDefaultToolBarLocation(toolBarDesc._class.getSimpleName())));
		}
		Stack<HashMap<?, ?>> stTool = new Stack<>();
		Stack<Menu> stMenu = new Stack<>();
		stTool.push(mapCheckBoxMenuItemTools);
		stMenu.push(viewMenu);
		while (!stTool.isEmpty()) {
			HashMap<?, ?> currToolNode = stTool.pop();
			Menu currToolMenu = stMenu.pop();
			currToolNode.forEach((tool, element) -> {
				if (element instanceof CheckMenuItem)
					currToolMenu.getItems().add((CheckMenuItem) element);
				else {
					Menu toolMenu = new Menu((String) tool);
					currToolMenu.getItems().add(toolMenu);
					stTool.push((HashMap<?, ?>) element);
					stMenu.push(toolMenu);
				}
			});
		}
		if (vtd != null)
			for (ToolBarDescriptor tbd : vtd)
				for (ToolBarInfo tbi : toolBarDescs)
					if (tbi.type.getSimpleName().equals(tbd.toolType))
						this.renderPane.openTool(tbi.type);
		return viewMenu;
	}

	public Point2i getPosition() {
		return new Point2i((int) this.primaryStage.getX(), (int) this.primaryStage.getY());
	}

	public RenderPane getRenderPane() {
		return this.renderPane;
	}

	@Override
	public Scheduler getScheduler() {
		return this.scenariumFx.getScheduler();
	}

	@Override
	public int getSelectedElementFromTheaterEditor() {
		return 0;
	}

	private String getTitle() {
		Scenario scenario = this.dataLoader.getScenario();
		return scenario != null ? scenario.toString() : "Scenarium ";
	}

	public ArrayList<ToolBarDescriptor> getVisibleToolsDesc() {
		ArrayList<ToolBarDescriptor> visibleTools = new ArrayList<>();
		this.checkMenuItemTools.forEach((toolClass, checkMenuItem) -> {
			if (checkMenuItem.isSelected()) {
				Tool tool = this.renderPane.getDisplayedTool(toolClass);
				Point2d pos = null;
				boolean alwaysOnTop = false;
				if (tool instanceof ExternalTool) {
					pos = ((ExternalTool) tool).getPosition();
					alwaysOnTop = ((ExternalTool) tool).isAlwaysOnTop();
				}
				visibleTools.add(new ToolBarDescriptor(toolClass.getSimpleName(), pos == null ? null : new Point2i((int) pos.x, (int) pos.y), alwaysOnTop));
			}
		});
		return visibleTools;
	}

	private MenuBar initMenuBar() {
		ToolBarDescriptor[] vtd;
		if (this.menuBar == null) {
			vtd = ScenariumGuiProperties.get().getVisibleToolsDesc();
			this.menuBar = new MenuBar();
			this.menuBar.setPadding(new Insets(0));
		} else {
			vtd = getVisibleToolsDesc().toArray(new ToolBarDescriptor[0]);
			this.menuBar.getMenus().clear();
		}
		this.menuBar.getMenus().addAll(getMenuFile(), getMenuEdit(), getMenuView(vtd), getMenuLanguage(this.menuBar), getMenuAbout());
		return this.menuBar;
	}

	@Override
	public boolean isDefaultToolBarAlwaysOnTop(String toolType) {
		ToolBarDescriptor[] vtd = ScenariumGuiProperties.get().getVisibleToolsDesc();
		if (vtd != null)
			for (ToolBarDescriptor tbd : vtd)
				if (tbd.toolType.equals(toolType))
					return tbd.alwaysOnTop;
		return false;

	}

	@Override
	public boolean isManagingAccelerator() {
		return true;
	}

	@Override
	public boolean isStatusBar() {
		return this.renderPane.isStatusBar();
	}

	private void openLocalScenario() {
		File scenarioFile = ScenarioFileChooserFx.showOpenDialog(this.primaryStage, this.dataLoader.getScenario(), new File(Scenarium.getWorkspace()));
		if (scenarioFile != null)
			new Thread(() -> this.dataLoader.reload(scenarioFile, true, true)).start();
		// Platform.runLater(() -> ); //Bug de taille de diagramdrawer quand on passe de 3D viewer a diagram...
	}

	private void openNetworkScenario() {
		Stage dialog = new Stage(StageStyle.UTILITY);
		dialog.initModality(Modality.WINDOW_MODAL);
		dialog.initOwner(this.primaryStage);
		dialog.setTitle("Open Network Stream");
		ComboBox<Class<? extends StreamScenario>> comboBox = new ComboBox<>(FXCollections.observableList(new ArrayList<>(ScenarioManager.getStreamScenario())));
		// comboBox.setCellFactory(e -> {
		// return new ListCell()<Class<? extends StreamScenario>>() {
		// @Override
		// protected void updateItem(Class<? extends StreamScenario> c, boolean empty) {
		// super.updateItem(c, empty);
		// setText((c == null || empty) ? null : c.getSimpleName());
		// }
		// };
		// });
		comboBox.setButtonCell(new ListCell<Class<? extends StreamScenario>>() {
			@Override
			protected void updateItem(Class<? extends StreamScenario> item, boolean empty) {
				super.updateItem(item, empty);
				if (item != null && !empty)
					setText(item.getSimpleName());
			}
		});
		comboBox.setCellFactory(lv -> {
			ListCell<Class<? extends StreamScenario>> lc = new ListCell<>() {
				protected void updateItem(Class<? extends StreamScenario> item, boolean empty) {
					super.updateItem(item, empty);
					if (item != null && !empty)
						setText(item.getSimpleName());
				}
			};
			return lc;
		});
		comboBox.getSelectionModel().select(0);
		InetSocketAddressEditor editor = new InetSocketAddressEditor();
		HBox hbox = new HBox(comboBox, editor.getEditor());
		hbox.setSpacing(10);
		hbox.setPadding(new Insets(0, 0, 10, 0));
		BorderPane bp = new BorderPane(hbox);
		Button playButton = new Button("Play");
		playButton.setOnAction(e -> new Thread(() -> this.dataLoader.reload(new StreamScenarioSource(comboBox.getSelectionModel().getSelectedItem(), editor.getValue()), true, true)).start());
		BorderPane.setAlignment(playButton, Pos.CENTER);
		bp.setBottom(playButton);
		bp.setPadding(new Insets(10));
		Scene scene = new Scene(bp);
		dialog.setScene(scene);
		dialog.show();
	}

	@Override
	public void paint() {
		TheaterPanel theaterPane = this.renderPane.getTheaterPane();
		if (theaterPane.isNeedToBeRefresh())
			theaterPane.paintImmediately(true);
	}

	public void reload(boolean resize) {
		this.renderPane.reload(resize);
		setTitle(getTitle());
	}

	@Override
	public void saveScenario() {
		Object scenarioSource = this.dataLoader.getScenario().getSource();
		saveScenarioAs(scenarioSource != null && scenarioSource instanceof File ? (File) scenarioSource : null);
	}

	private void saveScenarioAs(File scenarioFile) {
		if (scenarioFile == null) {
			scenarioFile = ScenarioFileChooserFx.showSaveDialog(this.primaryStage, this.dataLoader.getScenario(), new File(Scenarium.getWorkspace()));
			if (scenarioFile == null)
				return;
		}
		try {
			this.dataLoader.save(scenarioFile);
			if (this.dataLoader.getScenario().getSource() != scenarioFile) {
				File scenarioToReload = scenarioFile;
				new Thread(() -> {
					this.dataLoader.reload(scenarioToReload, false, true);
					this.dataLoader.playListManager.addToPlayList(-1, this.dataLoader.getScenario().getSource());
					String title = getTitle();
					Platform.runLater(() -> setTitle(title));
				}).start();
			}
		} catch (IOException e) {
			AlertUtil.show(e, "Impossible to save scenario file: " + scenarioFile, "Save Error", true);
		}
	}

	@Override
	public void setAnimated(boolean animated) {}

	public void setTitle(String title) {
		this.primaryStage.setTitle(title);
	}

	@Override
	public void showMessage(String message, boolean error) {
		this.renderPane.showMessage(message, error);
	}

	@Override
	public void showTool(Class<? extends Tool> toolClass, boolean closeIfOpen) {
		this.renderPane.showTool(toolClass, closeIfOpen);
	}

	public void updateRecentScenario() {
		FxUtils.runLaterIfNeeded(() -> getMenuItemRecentScenario());
	}

	@Override
	public void updateStatusBar(String... infos) {
		this.renderPane.updateStatusBar(infos);
	}

	@Override
	public void updateToolView(Class<? extends Tool> toolClass, boolean isVisible) {
		this.checkMenuItemTools.get(toolClass).setSelected(isVisible);
	}

	public void updateToolList() {
		Platform.runLater(() -> initMenuBar()); // TODO Pk Platform.runLater? ca plante sinon depuis JDK10
	}

	@Override
	public void loaded(ToolBarInfo toolBarInfo) {
		updateToolList();
	}

	@Override
	public void unloaded(ToolBarInfo toolBarInfo) {
		updateToolList();
	}
}
