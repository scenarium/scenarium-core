/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class SVGView extends ImageView {
	private ByteArrayInputStream bais;

	public SVGView(InputStream stream) {
		try {
			this.bais = new ByteArrayInputStream(convertSteamToByteArray(stream));
			setImage(new Image(this.bais, fitWidthProperty().intValue(), fitHeightProperty().intValue(), false, false));
			fitWidthProperty().addListener(e -> {
				if (getImage().getWidth() != fitWidthProperty().intValue() || getImage().getHeight() != fitHeightProperty().intValue()) {
					setImage(new Image(this.bais, fitWidthProperty().intValue(), fitHeightProperty().intValue(), false, false));
					this.bais.reset();
				}
			});
			fitHeightProperty().addListener(e -> {
				if (getImage().getWidth() != fitWidthProperty().intValue() || getImage().getHeight() != fitHeightProperty().intValue()) {
					setImage(new Image(this.bais, fitWidthProperty().intValue(), fitHeightProperty().intValue(), false, false));
					this.bais.reset();
				}
			});
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private static byte[] convertSteamToByteArray(InputStream stream) throws IOException {
		long size = stream.available();
		if (size > Integer.MAX_VALUE)
			return new byte[0];
		byte[] buffer = new byte[(int) size];
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		int line = 0;
		while ((line = stream.read(buffer)) != -1)
			os.write(buffer, 0, line);
		stream.close();
		os.flush();
		os.close();
		return os.toByteArray();
	}

	public void setImage(InputStream stream) {
		try {
			this.bais = new ByteArrayInputStream(convertSteamToByteArray(stream));
			setImage(new Image(this.bais, fitWidthProperty().intValue(), fitHeightProperty().intValue(), false, false));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
