package io.scenarium.gui.core.display;

import java.util.EventListener;

@FunctionalInterface
public interface UserAgentStylesheetChange extends EventListener {
	void userAgentStylesheetChange();
}
