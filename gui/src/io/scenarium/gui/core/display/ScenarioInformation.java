/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class ScenarioInformation {
	private static Stage infoDialog;

	private ScenarioInformation() {}

	public static void showInfo(Window owner, Object scenarioSource) {
		if (infoDialog != null) {
			infoDialog.close();
			infoDialog = null;
		}

		Scenario scenario = null;
		if (scenarioSource instanceof Scenario) {
			scenario = (Scenario) scenarioSource;
			if (scenario.getSource() == null)
				return;
		} else {
			Class<? extends Scenario> scenarioType = ScenarioManager.getScenarioType(scenarioSource);
			if (scenarioType != null)
				try {
					scenario = scenarioType.getConstructor().newInstance();
					scenario.setSource(scenarioSource);
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
					return;
				}
		}
		try {
			TableView<String> table = new TableView<>();
			TableColumn<String, String> varCol = new TableColumn<>("Variable");
			varCol.setCellValueFactory(param -> new SimpleStringProperty(param.getValue()));
			table.getColumns().add(varCol);
			TableColumn<String, String> valCol = new TableColumn<>("Value");
			HashMap<String, String> info = scenario.getInfo();
			valCol.setCellValueFactory(param -> new SimpleStringProperty(info.get(param.getValue())));
			table.getColumns().add(valCol);
			table.setItems(FXCollections.observableArrayList(info.keySet()));
			table.setFixedCellSize(24);
			table.setPrefHeight(info.size() * table.getFixedCellSize() + 28);
			table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
			table.setPrefWidth(400);
			infoDialog = new Stage(StageStyle.UTILITY);
			infoDialog.initModality(Modality.WINDOW_MODAL);
			infoDialog.initOwner(owner);
			infoDialog.setTitle("Scenario Information");
			infoDialog.setScene(new Scene(table));
			infoDialog.setAlwaysOnTop(true);
			infoDialog.show();
		} catch (IOException e) {
			AlertUtil.show(e, "Cannot get Information from scenario: " + scenario, "Information error", true);
		}
	}
}
