/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import java.util.ArrayList;

import io.scenarium.core.timescheduler.VisuableSchedulable;
import io.scenarium.core.tools.ViewerAnimationTimer;

import javafx.animation.AnimationTimer;

public class ViewerAnimationTimerFX implements ViewerAnimationTimer {
	ArrayList<VisuableSchedulable> needToBeScheduleList = new ArrayList<>();
	private final ArrayList<VisuableSchedulable> visuableSchedulables = new ArrayList<>();
	private AnimationTimer animationTimer;

	@Override
	public synchronized void register(VisuableSchedulable visuableSchedulable) {
		// Log.info("register: " + visuableSchedulable);
		if (this.visuableSchedulables.contains(visuableSchedulable))
			return;
		this.visuableSchedulables.add(visuableSchedulable);
		if (this.animationTimer != null) {
			visuableSchedulable.setAnimated(true);
			if (visuableSchedulable.needToBeSchedule()) {
				ArrayList<VisuableSchedulable> needToBeScheduleList = new ArrayList<>(this.needToBeScheduleList);
				needToBeScheduleList.add(visuableSchedulable);
				this.needToBeScheduleList = needToBeScheduleList;
			}
		}
	}

	@Override
	public synchronized void unRegister(VisuableSchedulable visuableSchedulable) {
		// Log.info("unRegister: " + visuableSchedulable);
		if (this.visuableSchedulables.remove(visuableSchedulable))
			if (this.animationTimer != null) {
				visuableSchedulable.setAnimated(false);
				ArrayList<VisuableSchedulable> needToBeScheduleList = new ArrayList<>(this.needToBeScheduleList);
				needToBeScheduleList.remove(visuableSchedulable);
				this.needToBeScheduleList = needToBeScheduleList;
			}
	}

	// private int frameCount = 0;
	// private long time = System.currentTimeMillis();

	@Override
	public synchronized void start() {
		// Log.error("start of ViewerAnimationTimer " + ProcessHandle.current().pid());
		this.needToBeScheduleList.clear();
		for (VisuableSchedulable visuableSchedulable : this.visuableSchedulables) {
			visuableSchedulable.setAnimated(true);
			if (visuableSchedulable.needToBeSchedule())
				this.needToBeScheduleList.add(visuableSchedulable);
		}
		// Log.info("nb Element to schedule: " + needToBeScheduleList.size());
		this.animationTimer = new AnimationTimer() {
			@Override
			public void handle(long now) {
				// if(System.currentTimeMillis() - time > 1000) {
				// Log.info(frameCount);
				// time = System.currentTimeMillis();
				// frameCount=0;
				// }
				// frameCount++;
				// if(frameCount%100 == 0)
				// Log.info("frameCount: " + frameCount + " time: " + (System.currentTimeMillis() - time));
				// long _time = System.currentTimeMillis();
				for (VisuableSchedulable vs : ViewerAnimationTimerFX.this.needToBeScheduleList)
					vs.paint();
			}
		};
		this.animationTimer.start();
	}

	@Override
	public synchronized void stop() {
		// Log.error("stop ViewerAnimationTimer " + ProcessHandle.current().pid());
		for (VisuableSchedulable visuableSchedulable : this.visuableSchedulables)
			visuableSchedulable.setAnimated(false);
		this.animationTimer.stop(); // NullPointerException double stop
		this.animationTimer = null;
	}
}
