/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

import io.beanmanager.tools.EventListenerList;
import io.scenarium.gui.core.struct.ScenariumGuiProperties;
import javafx.application.Application;

public class UserAgentStylesheetManager {
	private static final String USER_AGENT_STYLESHEET_FOLDER = "/theme";
	private static final String CSS_EXTENSION = ".css";
	private static final EventListenerList<UserAgentStylesheetChange> LISTENERS = new EventListenerList<>();

	private UserAgentStylesheetManager() {}

	public static void main(String[] args) {
		System.out.println(Arrays.toString(getAvailableUserAgentStylesheets()));
	}

	public static String[] getAvailableUserAgentStylesheets() {
		return Stream.concat(Stream.of(Application.STYLESHEET_CASPIAN, Application.STYLESHEET_MODENA),
				getAvailableAdditionnalUserAgentStylesheets().map(p -> p.getFileName().toString()).map(n -> n.substring(0, n.length() - CSS_EXTENSION.length()))).toArray(String[]::new);
	}

	private static Stream<Path> getAvailableAdditionnalUserAgentStylesheets() {
		try {
			URI uri = UserAgentStylesheetManager.class.getResource(USER_AGENT_STYLESHEET_FOLDER).toURI();
			FileSystem fs;
			Path path;
			if (uri.getScheme().equals("jar")) {
				try {
					fs = FileSystems.getFileSystem(uri);
				} catch (FileSystemNotFoundException e) {
					fs = FileSystems.newFileSystem(uri, Collections.<String, String> emptyMap());
				}
				path = fs.getPath(USER_AGENT_STYLESHEET_FOLDER);
			} else {
				fs = FileSystems.getDefault();
				path = Paths.get(uri);
			}
			PathMatcher matcher = fs.getPathMatcher("glob:**" + CSS_EXTENSION);
			return Files.walk(path, 1).filter(matcher::matches);
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
			return Stream.of();
		}
	}

	public static void loadUserAgentStylesheet(String userAgentStylesheet) {
		String currentUserAgentStylesheet = Application.getUserAgentStylesheet();
		if (currentUserAgentStylesheet == null) {
			if (Application.STYLESHEET_MODENA.equals(userAgentStylesheet))
				return;
		} else if (currentUserAgentStylesheet.equals(userAgentStylesheet))
			return;
		Arrays.stream(getAvailableUserAgentStylesheets()).filter(usa -> usa.equals(userAgentStylesheet)).findAny().ifPresent(newUserAgentStylesheet -> {
			String uass;
			if (!newUserAgentStylesheet.equals(Application.STYLESHEET_CASPIAN) && !newUserAgentStylesheet.equals(Application.STYLESHEET_MODENA)) {
				Optional<Path> uassp = getAvailableAdditionnalUserAgentStylesheets().filter(p -> {
					String name = p.getFileName().toString();
					return name.substring(0, name.length() - CSS_EXTENSION.length()).equals(newUserAgentStylesheet);
				}).findAny();
				if (uassp.isPresent())
					try {
						uass = uassp.get().toUri().toURL().toString();
					} catch (MalformedURLException e) {
						e.printStackTrace();
						return;
					}
				else {
					System.err.println("Cannot find: " + userAgentStylesheet + " user agent stylesheet");
					return;
				}
			} else
				uass = newUserAgentStylesheet;
			Application.setUserAgentStylesheet(uass);
			ScenariumGuiProperties.get().setLookAndFeel(newUserAgentStylesheet);
			LISTENERS.forEach(UserAgentStylesheetChange::userAgentStylesheetChange);
		});
	}

	public static void addUserAgentStylesheetChangeListener(UserAgentStylesheetChange listener) {
		LISTENERS.addStrongRef(listener);
	}

	public static void removeUserAgentStylesheetChangeListener(UserAgentStylesheetChange listener) {
		LISTENERS.removeStrongRef(listener);
	}
}
