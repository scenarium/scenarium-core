/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

public interface StackableDrawer {
	public boolean canAddInputToRenderer(Class<?>[] class1);

	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput);

	public default void newData() {

	};
}
