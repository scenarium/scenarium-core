/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import io.scenarium.core.tools.ReadOnlyObservableValue;
import io.scenarium.core.tools.ValueChangeListener;

import javafx.beans.property.DoubleProperty;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class SplashScreen {
	private final Stage splashStage;
	private final ProgressBar progressBar;

	public SplashScreen(Stage splashStage, ReadOnlyObservableValue<Double> progressProperty) {
		this.splashStage = splashStage;
		this.progressBar = new ProgressBar();
		DoubleProperty pp = this.progressBar.progressProperty();
		if (progressProperty != null)
			progressProperty.addValueChangeListener(new ValueChangeListener<Double>() {

				@Override
				public void valueChanged(Double value) {
					pp.setValue(value);
					if (value == 1.0)
						progressProperty.removeValueChangeListener(this);
				}
			});
		Image image = RenderFrame.SCENARIUM_ICON;
		this.progressBar.setMaxWidth(image.getWidth());
		Scene splashScene = new Scene(new VBox(new ImageView(image), this.progressBar));
		splashStage.setScene(splashScene);
		splashStage.initStyle(StageStyle.UNDECORATED);
		splashStage.setTitle("Scenarium");
		splashStage.show();
	}

	public void close() {
		this.splashStage.close();
	}
}
