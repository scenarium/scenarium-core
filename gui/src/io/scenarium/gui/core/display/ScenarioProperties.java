/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import io.beanmanager.BeanManager;
import io.scenarium.core.filemanager.scenariomanager.Scenario;

import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class ScenarioProperties {
	private static Stage propertiesDialog;

	private ScenarioProperties() {}

	public static void showProperties(Window owner, Scenario scenario) {
		if (propertiesDialog != null) {
			propertiesDialog.close();
			propertiesDialog = null;
		}
		BeanManager bm = new BeanManager(scenario, "");
		propertiesDialog = new Stage(StageStyle.UTILITY);
		propertiesDialog.initModality(Modality.WINDOW_MODAL);
		propertiesDialog.initOwner(owner);
		propertiesDialog.setTitle("Scenario Properties");
		propertiesDialog.setScene(new Scene(bm.getEditor()));
		propertiesDialog.setAlwaysOnTop(true);
		propertiesDialog.show();
	}
}
