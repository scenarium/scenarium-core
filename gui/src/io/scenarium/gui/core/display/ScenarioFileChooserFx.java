/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.beanmanager.editors.basic.AbstractPathEditor;
import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;
import io.scenarium.gui.core.struct.ScenariumGuiProperties;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;

public class ScenarioFileChooserFx {

	private ScenarioFileChooserFx() {}

	public static File showOpenDialog(Window window, Scenario scenario, File path) {
		return getOpenFileChooser(scenario, path).showOpenDialog(window);
	}

	public static List<File> showOpenMultipleDialog(Window window, Scenario scenario, File path) {
		return getOpenFileChooser(scenario, path).showOpenMultipleDialog(window);
	}

	public static File showSaveDialog(Window window, Scenario scenario, File path) {
		File file = baseFileChooser(AbstractPathEditor.getExtensionFilters(((LocalScenario) scenario).getFilters()), null, path).showSaveDialog(window);
		if (file == null)
			return null;
		String[] rfn = scenario.getReaderFormatNames();
		if (rfn.length != 0) {
			String extension = scenario.getReaderFormatNames()[0];
			if (!extension.isEmpty() && !file.getName().toLowerCase().endsWith("." + extension))
				file = new File(file.getAbsoluteFile() + "." + extension);
		}
		ScenariumGuiProperties.get().setScenarioChooserPath(file.getParentFile());
		return file;
	}

	private static FileChooser getOpenFileChooser(Scenario scenario, File path) {
		ArrayList<ExtensionFilter> extensionFilters = AbstractPathEditor.getExtensionFilters(ScenarioManager.getReaderFormatNames());
		ExtensionFilter selectedExtensionFilter = scenario == null ? null : extensionFilters.stream().filter(ef -> ef.getDescription().equals(scenario.getClass().getSimpleName())).findFirst().get();
		return baseFileChooser(extensionFilters, selectedExtensionFilter, path);
	}

	private static FileChooser baseFileChooser(ArrayList<ExtensionFilter> extensionFilters, ExtensionFilter selectedExtensionFilter, File path) {
		FileChooser fc = new FileChooser();
		fc.setTitle("Scenario file chooser");
		File cp = ScenariumGuiProperties.get().getScenarioChooserPath();
		fc.setInitialDirectory(path != null && path.exists() ? path : cp != null && cp.exists() ? cp : new File(System.getProperty("user.dir")));
		fc.getExtensionFilters().setAll(extensionFilters != null ? extensionFilters : AbstractPathEditor.getExtensionFilters(ScenarioManager.getReaderFormatNames()));
		fc.getExtensionFilters().sort((a, b) -> a.getDescription().compareTo(b.getDescription()));
		fc.setSelectedExtensionFilter(selectedExtensionFilter);
		return fc;
	}
}
