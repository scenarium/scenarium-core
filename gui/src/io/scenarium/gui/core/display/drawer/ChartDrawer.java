/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.drawer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

import javax.vecmath.Point2d;
import javax.vecmath.Point2i;

import io.beanmanager.editors.DynamicEnableBean;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.container.ArrayInfo;
import io.beanmanager.struct.BooleanProperty;
import io.beanmanager.struct.TreeNode;
import io.beanmanager.struct.TreeRoot;
import io.scenarium.core.struct.curve.BoxMarker;
import io.scenarium.core.struct.curve.Curve;
import io.scenarium.core.struct.curve.CurveSeries;
import io.scenarium.core.struct.curve.Curved;
import io.scenarium.core.struct.curve.Curvei;
import io.scenarium.core.struct.curve.EvolvedCurveSeries;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.gui.core.display.ColorProvider;
import io.scenarium.gui.core.display.ScenariumContainer;
import io.scenarium.gui.core.display.StackableDrawer;
import io.scenarium.gui.core.display.UserAgentStylesheetChange;
import io.scenarium.gui.core.display.UserAgentStylesheetManager;
import io.scenarium.gui.core.internal.LoadPackageStream;
import io.scenarium.gui.core.internal.Log;
import io.scenarium.gui.core.struct.ScenariumGuiProperties;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Dimension2D;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class ChartDrawer extends TheaterPanel implements StackableDrawer, DynamicEnableBean, UserAgentStylesheetChange {
	protected static final String LINKS = "Links";
	protected static final String SYMBOLS = "Symbols";
	protected static final String COORDINATES = "Coordinates";
	protected static final String INDICES = "Indices";
	private ColorProvider colorProvider = new ColorProvider();
	private final Data<Number, Number> first = new LineChart.Data<>(0, 0);
	private final Data<Number, Number> last = new LineChart.Data<>(1, 1);
	private Canvas chartCanvas;
	private Canvas selectionCanvas;
	private Region drawRegion;
	private double ratioX;
	private double ratioY;
	private double offsetX;
	private double offsetY;
	private Point2i anchorPoint;
	private Point2i draggPoint;
	private NumberAxis xAxis;
	private NumberAxis yAxis;
	private double minX = Double.NaN;
	private double maxX = Double.NaN;
	private double minY = Double.NaN;
	private double maxY = Double.NaN;
	private LineChart<Number, Number> chart;
	private Point2i mousePostion = new Point2i();
	private LineChart.Series<Number, Number> series;
	private Point2D oldFirst;
	private Point2D oldLast;
	private ArrayList<Curve> curves;
	@PropertyInfo(index = 0, info = "Auto compute x bounds based on curves bounds")
	private boolean autoFitXaxis = true;
	@PropertyInfo(index = 1, info = "x bounds for the chart")
	private Point2d xBounds = new Point2d(0, 1);
	@PropertyInfo(index = 2, info = "Auto compute y bounds based on curves bounds")
	private boolean autoFitYaxis = true;
	@PropertyInfo(index = 3, info = "y bounds for the chart")
	private Point2d yBounds = new Point2d(0, 1);
	@PropertyInfo(index = 4, info = "Auto compute color bounds based on curves values bounds")
	private boolean autoFitValuesBounds = true;
	@PropertyInfo(index = 5, info = "Bounds for values")
	private Point2d valuesBounds = new Point2d(0, 1);
	@PropertyInfo(index = 6, info = "Margin used to draw charts")
	private double margin = 0.0;
	private boolean boundsChanged;
	private final ArrayList<double[]> previousTransform = new ArrayList<>();
	private ChartLabel tempChartLabel;
	private ArrayList<ChartLabel> chartLabels;
	private Runnable clearChartLabels;

	public ChartDrawer() {
		if (!this.autoFitXaxis) {
			this.first.setXValue(this.xBounds.x);
			this.last.setXValue(this.xBounds.y);
		}
		if (!this.autoFitYaxis) {
			this.first.setYValue(this.yBounds.x);
			this.last.setYValue(this.yBounds.y);
		}
		this.series = new LineChart.Series<>();
		this.series.setName("");
		this.series.getData().add(this.first);
		this.series.getData().add(this.last);

		this.curves = new ArrayList<>();
		// Création du graphique.
		this.xAxis = new NumberAxis(this.first.getXValue().doubleValue(), this.last.getXValue().doubleValue(), 1);
		this.xAxis.setLabel("x");
		this.xAxis.setLowerBound(this.first.getXValue().doubleValue());
		this.xAxis.setUpperBound(this.last.getXValue().doubleValue());
		// this.xAxis.setStyle("-fx-tick-label-fill: #914800;");
		// this.xAxis.lookup(".axis-label").setStyle("-fx-text-fill: #000000;");
		this.yAxis = new NumberAxis(this.first.getYValue().doubleValue(), this.last.getYValue().doubleValue(), 1);
		this.yAxis.setLabel("y");
		this.yAxis.setLowerBound(this.first.getYValue().doubleValue());
		this.yAxis.setUpperBound(this.last.getYValue().doubleValue());
		// this.yAxis.setStyle("-fx-tick-label-fill: #914800;");
		// this.yAxis.lookup(".axis-label").setStyle("-fx-text-fill: #000000;");
		// yAxis.prefWidthProperty().bind(widthprop);

		this.chart = new LineChart<>(this.xAxis, this.yAxis);
		this.chart.setAnimated(false);
		this.chart.setCreateSymbols(false);
		this.chart.getData().add(this.series);
		this.chart.setStyle("CHART_COLOR_1: transparent;");

		Pane drawPane = (Pane) this.chart.lookup(".chart-content");
		drawPane.setPadding(new Insets(5, 20, 0, 0));
		this.drawRegion = (Region) drawPane.getChildren().get(0);
		this.chart.setPadding(new Insets(5, 0, 0, 0));
		final StackPane root = new StackPane();
		this.chartCanvas = new Canvas();
		this.chartCanvas.setManaged(false);
		this.chartCanvas.getGraphicsContext2D().setTextAlign(TextAlignment.CENTER);
		this.chartCanvas.widthProperty().bind(this.drawRegion.widthProperty());
		this.chartCanvas.heightProperty().bind(this.drawRegion.heightProperty());
		this.selectionCanvas = new Canvas();
		this.selectionCanvas.setManaged(false);
		this.selectionCanvas.widthProperty().bind(this.drawRegion.widthProperty());
		this.selectionCanvas.heightProperty().bind(this.drawRegion.heightProperty());
		this.selectionCanvas.setVisible(true);
		this.selectionCanvas.setFocusTraversable(false);
		this.selectionCanvas.setPickOnBounds(false);
		this.selectionCanvas.setMouseTransparent(true);
		root.getChildren().addAll(this.chart, this.chartCanvas, this.selectionCanvas);

		this.chartCanvas.setOnMousePressed(e -> {
			if (e.getButton() == MouseButton.PRIMARY && e.isControlDown()) {
				Object[] closestData = getClosestPoint(e.getX() / this.ratioX + this.offsetX, e.getY() / this.ratioY + this.offsetY);
				if (closestData == null)
					return;
				if (this.tempChartLabel == null || this.tempChartLabel.curve != closestData[0] || this.tempChartLabel.index != (int) closestData[1]) {
					Curve selectedCurve = (Curve) closestData[0];
					int selectedIndex = (int) closestData[1];
					drawSelection();
					double[][] selectedCurveData = selectedCurve.getDatad();
					double valX = selectedCurveData[0][selectedIndex];
					double valY = selectedCurveData[1][selectedIndex];
					double x = (valX - this.offsetX) * this.ratioX;
					double y = (valY - this.offsetY) * this.ratioY;
					root.getChildren().remove(this.tempChartLabel);
					Point2D pt = this.chartCanvas.localToParent(new Point2D(x, y));

					this.tempChartLabel = new ChartLabel(pt.getX(), pt.getY(), 10, selectedCurve, selectedIndex, this.xAxis.getLabel(), this.yAxis.getLabel(), (chartLabel, isPin) -> {
						if (isPin) {
							if (this.chartLabels == null)
								this.chartLabels = new ArrayList<>();
							this.chartLabels.add(chartLabel);
							if (chartLabel == this.tempChartLabel)
								this.tempChartLabel = null;
						} else {
							if (this.tempChartLabel != null)
								root.getChildren().remove(this.tempChartLabel);
							this.tempChartLabel = chartLabel;
						}
					});
					this.tempChartLabel.curve = selectedCurve;
					this.tempChartLabel.index = selectedIndex;
					this.tempChartLabel.updatePos(this.chartCanvas, x, y);
					this.tempChartLabel.updateInfo();
					StackPane.setAlignment(this.tempChartLabel, Pos.TOP_LEFT);
					root.getChildren().add(this.tempChartLabel);
				}
			} else if (e.getButton() == MouseButton.PRIMARY || e.getButton() == MouseButton.SECONDARY) {
				this.anchorPoint = new Point2i((int) e.getX(), (int) e.getY());
				if (e.getButton() == MouseButton.SECONDARY) {
					this.oldFirst = new Point2D(this.first.getXValue().doubleValue(), this.first.getYValue().doubleValue());
					this.oldLast = new Point2D(this.last.getXValue().doubleValue(), this.last.getYValue().doubleValue());
				}
			}
			this.selectionCanvas.setVisible(true);
		});
		this.chartCanvas.setOnMouseDragged(e -> {
			if (e.getButton() == MouseButton.PRIMARY && e.isControlDown()) {
				Object[] closestData = getClosestPoint(e.getX() / this.ratioX + this.offsetX, e.getY() / this.ratioY + this.offsetY);
				this.tempChartLabel.curve = (Curve) closestData[0];
				this.tempChartLabel.index = (int) closestData[1];
				drawSelection();
				// }
			} else if (e.getButton() == MouseButton.PRIMARY) {
				this.draggPoint = new Point2i((int) e.getX(), (int) e.getY());
				drawSelection();
			} else if (e.getButton() == MouseButton.SECONDARY && this.anchorPoint != null) {
				setCursor(Cursor.MOVE);
				double dx = this.anchorPoint.x / this.ratioX - e.getX() / this.ratioX;
				double dy = this.anchorPoint.y / this.ratioY - e.getY() / this.ratioY;
				this.first.setXValue(this.oldFirst.getX() + dx);
				this.first.setYValue(this.oldFirst.getY() + dy);
				this.last.setXValue(this.oldLast.getX() + dx);
				this.last.setYValue(this.oldLast.getY() + dy);
				updateCanvasParam();
				repaint(false);
			}
		});
		this.chartCanvas.setOnMouseMoved(e -> this.mousePostion = new Point2i((int) e.getX(), (int) e.getY()));
		this.chartCanvas.setOnMouseReleased(e -> {
			if (getCursor() != Cursor.DEFAULT)
				setCursor(Cursor.DEFAULT);
			if (this.anchorPoint != null && this.draggPoint != null) {
				double beginX = this.anchorPoint.x / this.ratioX + this.offsetX;
				double endX = this.draggPoint.x / this.ratioX + this.offsetX;
				double beginY = this.draggPoint.y / this.ratioY + this.offsetY;
				double endY = this.anchorPoint.y / this.ratioY + this.offsetY;
				boolean isReinit = false;
				boolean hasChanged = false;
				if (endX - beginX < 0) {
					if (this.first.getXValue().doubleValue() != this.minX || this.last.getXValue().doubleValue() != this.maxX) {
						this.first.setXValue(this.minX);
						this.last.setXValue(this.maxX);
						hasChanged = true;
					}
					isReinit = true;
				}
				if (endY - beginY < 0) {
					if (this.first.getYValue().doubleValue() != this.minY || this.last.getYValue().doubleValue() != this.maxY) {
						this.first.setYValue(this.minY);
						this.last.setYValue(this.maxY);
						hasChanged = true;
					}
					isReinit = true;
				}
				if (!isReinit) {
					// this.chart.setStyle("-fx-background-color: -fx-background");
					// this.chart.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY))); //couleur du fond
					if (endX - beginX > 1E-5) {
						if (this.first.getXValue().doubleValue() != beginX || this.last.getXValue().doubleValue() != endX) {
							this.first.setXValue(beginX);
							this.last.setXValue(endX);
							hasChanged = true;
						}
					} else
						showMessage("Too small X", true);
					if (endY - beginY > 1E-5) {
						if (this.first.getYValue().doubleValue() != beginY || this.last.getYValue().doubleValue() != endY) {
							this.first.setYValue(beginY);
							this.last.setYValue(endY);
						}
					} else
						showMessage("Too small Y", true);
				}
				if (hasChanged) {
					updateCanvasParam();
					repaint(false);
				}
			}
			this.anchorPoint = null;
			this.draggPoint = null;
			if (this.tempChartLabel != null)
				this.selectionCanvas.setVisible(true);
			if (e.getButton() == MouseButton.PRIMARY && !e.isControlDown()) {
				root.getChildren().remove(this.tempChartLabel);
				this.tempChartLabel = null;
			}
			pushTransform();
		});
		this.chartCanvas.setOnScroll(e -> {
			if (e.getDeltaY() != 0) {
				double xf = this.first.getXValue().doubleValue();
				double xl = this.last.getXValue().doubleValue();
				double yf = this.first.getYValue().doubleValue();
				double yl = this.last.getYValue().doubleValue();
				double distX = xl - xf;
				double distY = yl - yf;
				double ratioX = (this.chartCanvas.getWidth() - e.getX()) / this.chartCanvas.getWidth();
				double invRatioX = 1 - ratioX;
				double ratioY = (this.chartCanvas.getHeight() - e.getY()) / this.chartCanvas.getHeight();
				double invRatioY = 1 - ratioY;
				distX = distX / 10;
				distY = distY / 10;
				if (e.getDeltaY() > 0) {
					xf += distX * invRatioX;
					xl -= distX * ratioX;
					yf += distY * ratioY;
					yl -= distY * invRatioY;
				} else {
					xf -= distX * invRatioX;
					xl += distX * ratioX;
					yf -= distY * ratioY;
					yl += distY * invRatioY;
				}
				boolean hasChanged = false;
				if (xl - xf > 1E-5) {
					this.first.setXValue(xf);
					this.last.setXValue(xl);
					hasChanged = true;
				}
				if (yl - yf > 1E-5) {
					this.first.setYValue(yf);
					this.last.setYValue(yl);
					hasChanged = true;
				}
				if (hasChanged) {
					updateCanvasParam();
					repaint(false);
				}
			}
		});
		setOnKeyReleased(this::onKeyReleased);
		Parent parent = this.drawRegion;
		while (parent != this.chart) {
			parent.localToParentTransformProperty().addListener(e -> updateCanvasPos());
			parent = parent.getParent();
		}
		this.chart.localToParentTransformProperty().addListener(e -> updateCanvasPos());
		// this.chart.setStyle("-fx-background-color: -fx-background");
		// this.chart.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));

		this.chartCanvas.widthProperty().addListener(e -> Platform.runLater(() -> updateCanvasPos()));
		this.chartCanvas.heightProperty().addListener(e -> Platform.runLater(() -> updateCanvasPos()));
		root.prefWidthProperty().bind(widthProperty());
		root.prefHeightProperty().bind(heightProperty());
		this.last.XValueProperty().addListener(e -> {
			double distX = Math.abs(this.last.getXValue().doubleValue() - this.first.getXValue().doubleValue());
			double d = distX;
			String pat = "###";
			if (d != 0) {
				if (d < 10)
					pat += ".";
				while (d < 10) {
					d *= 10;
					pat += "0";
				}
				pat += "0";
			}
			double absValue = Math.abs(this.last.getXValue().doubleValue());
			if (absValue > 100000)
				pat += "E0";
			this.yAxis.setPrefWidth(Math.max(new Text(new DecimalFormat(pat).format(this.first.getXValue().doubleValue())).getBoundsInLocal().getWidth(),
					new Text(new DecimalFormat(pat).format(this.last.getXValue().doubleValue())).getBoundsInLocal().getWidth()) + 30);
		});
		this.clearChartLabels = () -> {
			ObservableList<Node> children = root.getChildren();
			if (this.chartLabels != null) {
				this.chartLabels.forEach(children::remove);
				this.chartLabels = null;
			}
			if (this.tempChartLabel != null) {
				children.remove(this.tempChartLabel);
				this.tempChartLabel = null;
			}
		};
		getChildren().add(root);
	}

	@Override
	protected void onKeyPressed(KeyEvent e) {
		super.onKeyPressed(e);
		if (e.isConsumed())
			return;
		switch (e.getCode()) {
		case A:
			fitDocument();
			updateCanvasParam();
			repaint(false);
			break;
		case RIGHT:
			if (e.isControlDown())
				moveCurveSelection(true);
			else {
				double dx = Math.abs(this.last.getXValue().doubleValue() - this.first.getXValue().doubleValue()) / 10.0;
				this.first.setXValue(this.first.getXValue().doubleValue() + dx);
				this.last.setXValue(this.last.getXValue().doubleValue() + dx);
				updateCanvasParam();
			}
			repaint(false);
			break;
		case LEFT:
			if (e.isControlDown())
				moveCurveSelection(false);
			else {
				double dx = Math.abs(this.last.getXValue().doubleValue() - this.first.getXValue().doubleValue()) / 10.0;
				this.first.setXValue(this.first.getXValue().doubleValue() - dx);
				this.last.setXValue(this.last.getXValue().doubleValue() - dx);
				updateCanvasParam();
			}
			repaint(false);
			break;
		case UP:
			if (e.isControlDown())
				moveCurveSelection(true);
			else {
				double dy = Math.abs(this.last.getYValue().doubleValue() - this.first.getYValue().doubleValue()) / 10.0;
				this.first.setYValue(this.first.getYValue().doubleValue() + dy);
				this.last.setYValue(this.last.getYValue().doubleValue() + dy);
				updateCanvasParam();
			}
			repaint(false);
			break;
		case DOWN:
			if (e.isControlDown())
				moveCurveSelection(false);
			else {
				double dy = Math.abs(this.last.getYValue().doubleValue() - this.first.getYValue().doubleValue()) / 10.0;
				this.first.setYValue(this.first.getYValue().doubleValue() - dy);
				this.last.setYValue(this.last.getYValue().doubleValue() - dy);
				updateCanvasParam();
			}
			repaint(false);
			break;
		default:
			break;
		}
	}

	protected void onKeyReleased(KeyEvent e) {
		pushTransform();
	}

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object drawableElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, drawableElement, autoFitIfResize);
		UserAgentStylesheetManager.addUserAgentStylesheetChangeListener(this);
		userAgentStylesheetChange();
	}

	@Override
	public void setDrawableElement(Object drawableElement) {
		super.setDrawableElement(drawableElement);
		this.boundsChanged = true;
		this.clearChartLabels.run();
	}

	@Override
	public void dispose() {
		UserAgentStylesheetManager.removeUserAgentStylesheetChangeListener(this);
		super.dispose();
	}

	@Override
	public void fitDocument() {
		if (Double.isNaN(this.minX) || Double.isNaN(this.minY) || Double.isNaN(this.maxX) || Double.isNaN(this.maxY))
			return;
		if (this.first.getXValue().doubleValue() != this.minX)
			this.first.setXValue(this.minX);
		if (this.first.getYValue().doubleValue() != this.minY)
			this.first.setYValue(this.minY);
		if (this.last.getXValue().doubleValue() != this.maxX)
			this.last.setXValue(this.maxX);
		if (this.last.getYValue().doubleValue() != this.maxY)
			this.last.setYValue(this.maxY);
	}

	@Override
	protected void paint(Object dataElement) {
		ArrayList<Object> des = new ArrayList<>();
		des.add(dataElement);
		if (getAdditionalDrawableElement() != null)
			for (Object de : getAdditionalDrawableElement())
				des.add(de);
		this.curves.clear();
		for (Object de : des) {
			if (de == null)
				continue;
			if (de instanceof Curved)
				this.curves.add((Curved) de);
			else if (de instanceof Curvei)
				this.curves.add((Curve) de);
			else
				this.curves.addAll(((CurveSeries) de).getCurves());
		}
		// if (hasTheaterFilterStructChanged())
		updateCurvesTheaterFilter();
		ObservableList<Series<Number, Number>> chartData = this.chart.getData();

		boolean hasChanged = false;
		if (chartData.size() != this.curves.size()) {
			int diff = this.curves.size() - this.chart.getData().size();
			if (diff > 0) {
				ArrayList<LineChart.Series<Number, Number>> series = new ArrayList<>();
				for (int i = 0; i < diff; i++)
					series.add(new LineChart.Series<>());
				chartData.addAll(series);
			} else
				while (chartData.size() != this.curves.size())
					chartData.remove(chartData.size() - 1);
			hasChanged = true;
		}
		if (dataElement instanceof EvolvedCurveSeries) {
			EvolvedCurveSeries ecs = (EvolvedCurveSeries) dataElement;
			String chartName = ecs.getName();
			if (chartName != null && !chartName.equals(this.chart.getTitle()))
				this.chart.setTitle(chartName);
			String xLabel = ecs.getxLabel();
			if (xLabel != null && !xLabel.equals(this.xAxis.getLabel()))
				this.xAxis.setLabel(xLabel);
			String yLabel = ecs.getyLabel();
			if (yLabel != null && !yLabel.equals(this.yAxis.getLabel()))
				this.yAxis.setLabel(yLabel);
		} else if (this.curves.size() == 1) {
			String chartName = this.curves.get(0).getName();
			if (chartName == null) {
				if (this.chart.getTitle() != null)
					this.chart.setTitle(null);
			} else if (!chartName.equals(this.chart.getTitle()))
				this.chart.setTitle(chartName);
		}
		for (int i = 0; i < this.curves.size(); i++) {
			String name = this.curves.get(i).getName();
			if (name == null)
				name = Integer.toString(i);
			if (name != chartData.get(i).getName()) {
				chartData.get(i).setName(name);
				hasChanged = true;
			}
		}
		if (hasChanged) {
			Set<Node> items = this.chart.lookupAll("Label.chart-legend-item");
			this.colorProvider.resetIndex();
			for (Node item : items) {
				Label label = (Label) item;
				Line line = new Line(0, 0, 6, 0);
				line.setStroke(this.colorProvider.getNextColor());
				line.setStrokeWidth(2);
				label.setGraphic(line);
				label.setOnMouseClicked(e -> {
					updateCurvesTheaterFilter();
					String name = label.getText();
					TreeNode<BooleanProperty> curveElement = getTheaterSon(getTheaterFilter(), name);
					TreeNode<BooleanProperty> linksNode = null;
					TreeNode<BooleanProperty> symbolsNode = null;
					TreeNode<BooleanProperty> coordinatesNode = null;
					TreeNode<BooleanProperty> indicesNode = null;
					for (TreeNode<BooleanProperty> son : curveElement.getChildren())
						switch (son.getValue().name) {
						case LINKS:
							linksNode = son;
							break;
						case SYMBOLS:
							symbolsNode = son;
							break;
						case COORDINATES:
							coordinatesNode = son;
							break;
						case INDICES:
							indicesNode = son;
							break;
						default:
							break;
						}
					boolean isVisible = linksNode.getValue().value || symbolsNode.getValue().value || coordinatesNode.getValue().value || indicesNode.getValue().value;

					if (e.getClickCount() == 1) {
						updateFilterWithPath(new String[] { curveElement.getValue().name, linksNode.getValue().name }, !isVisible, true);
						if (isVisible) {
							updateFilterWithPath(new String[] { curveElement.getValue().name, symbolsNode.getValue().name }, !isVisible, true);
							updateFilterWithPath(new String[] { curveElement.getValue().name, coordinatesNode.getValue().name }, !isVisible, true);
							updateFilterWithPath(new String[] { curveElement.getValue().name, indicesNode.getValue().name }, !isVisible, true);
						}
					} else
						for (TreeNode<BooleanProperty> node : getTheaterFilter().getChildren())
							if (!node.getValue().name.equals(name))
								if (!isVisible) {
									updateFilterWithPath(new String[] { node.getValue().name, getTheaterSon(node, LINKS).getValue().name }, !isVisible, true);
									updateFilterWithPath(new String[] { node.getValue().name, getTheaterSon(node, SYMBOLS).getValue().name }, isVisible, true);
								} else
									updateFilterWithPath(new String[] { node.getValue().name, getTheaterSon(node, LINKS).getValue().name }, !isVisible, true);
				});
			}
		}
		if (this.boundsChanged && (this.autoFitXaxis || this.autoFitYaxis)) {
			double[] bounds;
			if (this.autoFitXaxis && this.autoFitYaxis) {
				bounds = new double[4];
				this.minX = Double.MAX_VALUE;
				this.maxX = -Double.MAX_VALUE;
				this.minY = Double.MAX_VALUE;
				this.maxY = -Double.MAX_VALUE;
				for (Curve curve : this.curves) {
					computeBounds(curve, bounds);
					if (bounds[0] < this.minX)
						this.minX = bounds[0];
					if (bounds[1] > this.maxX)
						this.maxX = bounds[1];
					if (bounds[2] < this.minY)
						this.minY = bounds[2];
					if (bounds[3] > this.maxY)
						this.maxY = bounds[3];
				}
				double dx = Math.abs(this.maxX - this.minX);
				double dy = Math.abs(this.maxY - this.minY);
				this.minX -= dx * this.margin;
				this.maxX += dx * this.margin;
				this.minY -= dy * this.margin;
				this.maxY += dy * this.margin;
				boolean hasBoundChanged = false;
				if (this.first.getXValue().doubleValue() != this.minX) {
					this.first.setXValue(this.minX);
					hasBoundChanged = true;
				}
				if (this.last.getXValue().doubleValue() != this.maxX) {
					this.last.setXValue(this.maxX);
					hasBoundChanged = true;
				}
				if (this.first.getYValue().doubleValue() != this.minY) {
					this.first.setYValue(this.minY);
					hasBoundChanged = true;
				}
				if (this.last.getYValue().doubleValue() != this.maxY) {
					this.last.setYValue(this.maxY);
					hasBoundChanged = true;
				}
				if (hasBoundChanged)
					updateCanvasParam();
			} else if (this.autoFitXaxis) {
				bounds = new double[2];
				this.minX = Double.MAX_VALUE;
				this.maxX = -Double.MAX_VALUE;
				for (Curve curve : this.curves) {
					computeXBounds(curve, bounds);
					if (bounds[0] < this.minX)
						this.minX = bounds[0];
					if (bounds[1] > this.maxX)
						this.maxX = bounds[1];
				}
				double dx = Math.abs(this.maxX - this.minX);
				this.minX -= dx * this.margin;
				this.maxX += dx * this.margin;
				boolean hasBoundChanged = false;
				if (this.first.getXValue().doubleValue() != this.minX) {
					this.first.setXValue(this.minX);
					hasBoundChanged = true;
				}
				if (this.last.getXValue().doubleValue() != this.maxX) {
					this.last.setXValue(this.maxX);
					hasBoundChanged = true;
				}
				if (hasBoundChanged)
					updateCanvasParam();
			} else if (this.autoFitYaxis) {
				bounds = new double[2];
				this.minY = Double.MAX_VALUE;
				this.maxY = -Double.MAX_VALUE;
				for (Curve curve : this.curves) {
					computeYBounds(curve, bounds);
					if (bounds[0] < this.minY)
						this.minY = bounds[0];
					if (bounds[1] > this.maxY)
						this.maxY = bounds[1];
				}
				double dy = Math.abs(this.maxY - this.minY);
				this.minY -= dy * this.margin;
				this.maxY += dy * this.margin;
				boolean hasBoundChanged = false;
				if (this.first.getYValue().doubleValue() != this.minY) {
					this.first.setYValue(this.minY);
					hasBoundChanged = true;
				}
				if (this.last.getYValue().doubleValue() != this.maxY) {
					this.last.setYValue(this.maxY);
					hasBoundChanged = true;
				}
				if (hasBoundChanged)
					updateCanvasParam();
			}
		}
		drawChart();
		this.boundsChanged = false;
		if (this.selectionCanvas.isVisible())
			drawSelection();
	}

	private static void computeBounds(Curve curve, double[] res) {
		double[][] datas = curve.getDatad();
		double[] xs = datas[0];
		double[] ys = datas[1];
		if (xs.length == 0) {
			res[0] = 0;
			res[1] = 1;
			res[2] = 0;
			res[3] = 1;
			return;
		}
		double minX = xs[0];
		double maxX = minX;
		double minY = ys[0];
		double maxY = minY;
		for (int i = xs.length; i-- != 0;) {
			double val = xs[i];
			if (val < minX)
				minX = val;
			else if (val > maxX)
				maxX = val;
			val = ys[i];
			if (val < minY)
				minY = val;
			else if (val > maxY)
				maxY = val;
		}
		res[0] = minX;
		res[1] = maxX;
		res[2] = minY;
		res[3] = maxY;
	}

	private static void computeValueBounds(double[] values, double[] bounds) {
		double minZ = values[0];
		double maxZ = minZ;
		for (int i = values.length; i-- != 0;) {
			double val = values[i];
			if (val < minZ)
				minZ = val;
			else if (val > maxZ)
				maxZ = val;
		}
		bounds[0] = minZ;
		bounds[1] = maxZ;
	}

	private static void computeValueBounds(int[] values, int[] bounds) {
		int minZ = values[0];
		int maxZ = minZ;
		for (int i = values.length; i-- != 0;) {
			int val = values[i];
			if (val < minZ)
				minZ = val;
			else if (val > maxZ)
				maxZ = val;
		}
		bounds[0] = minZ;
		bounds[1] = maxZ;
	}

	private static void computeXBounds(Curve curve, double[] res) {
		double[][] datas = curve.getDatad();
		double[] xs = datas[0];
		double minX = xs[0];
		double maxX = minX;
		for (int i = xs.length; i-- != 0;) {
			double val = xs[i];
			if (val < minX)
				minX = val;
			else if (val > maxX)
				maxX = val;
		}
		res[0] = minX;
		res[1] = maxX;
	}

	private static void computeYBounds(Curve curve, double[] res) {
		double[][] datas = curve.getDatad();
		double[] ys = datas[1];
		double minY = ys[0];
		double maxY = minY;
		for (int i = ys.length; i-- != 0;) {
			double val = ys[i];
			if (val < minY)
				minY = val;
			else if (val > maxY)
				maxY = val;
		}
		res[0] = minY;
		res[1] = maxY;
	}

	private void drawChart() {
		GraphicsContext g = this.chartCanvas.getGraphicsContext2D();
		g.clearRect(0, 0, this.chartCanvas.getWidth(), this.chartCanvas.getHeight());
		if (true/* filterChart */) {
			this.colorProvider.resetIndex();
			ArrayList<TreeNode<BooleanProperty>> sons = getTheaterFilter().getChildren();
			double width = getWidth();
			double height = getHeight();
			for (int i = 0; i < this.curves.size(); i++) {
				Curve curve = this.curves.get(i);
				String name = curve.getName();
				if (name == null)
					name = Integer.toString(i);
				ArrayList<TreeNode<BooleanProperty>> curveFilters = sons.get(i).getChildren();
				boolean filterLinks = curveFilters.get(0).getValue().value;
				boolean filterSymbols = curveFilters.get(1).getValue().value;
				boolean filterCoordinates = curveFilters.get(2).getValue().value;
				boolean filterIndices = curveFilters.get(3).getValue().value;

				boolean additionalFilter = filterSymbols || filterIndices || filterCoordinates;
				double textHeight = filterCoordinates ? new Text("").getLayoutBounds().getHeight() : 0;
				Boolean isVisible = additionalFilter || filterLinks;
				if (!isVisible) {
					this.colorProvider.incrementColorIndex();
					continue;
				}
				if (curve instanceof Curved) {
					double[][] datas = ((Curved) curve).getData();
					double[] xDatas = datas[0];
					double[] yDatas = datas[1];
					int dataLength = xDatas.length;
					Color color = this.colorProvider.getNextColor();
					if (dataLength != 0) {
						double oldX = (xDatas[dataLength - 1] - this.offsetX) * this.ratioX;
						double oldY = (yDatas[dataLength - 1] - this.offsetY) * this.ratioY;
						double[] values = ((Curved) curve).getValues();
						if (values == null) {
							if (filterLinks) {
								g.beginPath();
								g.moveTo(oldX, oldY);
							}
							g.setStroke(color);
							for (int k = dataLength; k-- != 0;) {
								double x = (xDatas[k] - this.offsetX) * this.ratioX;
								double y = (yDatas[k] - this.offsetY) * this.ratioY;
								// g.strokeLine(oldX, oldY, x, y);
								if (filterLinks)
									g.lineTo(x, y);
								if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
									if (filterSymbols) {
										g.setFill(g.getStroke());
										drawShape(g, this.colorProvider.getCurrentIndex(), x, y);
									}
									if (filterIndices) {
										g.setFill(Color.BLACK);
										g.fillText(Integer.toString(k), x, y);
									}
									if (filterCoordinates) {
										g.setFill(Color.BLACK);
										double yPos = y;
										if (filterIndices)
											yPos += textHeight;
										g.fillText(Double.toString(xDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(yDatas[k]), x, yPos);
									}
								}
								// oldX = x;
								// oldY = y;
							}

							double x = (xDatas[dataLength - 1] - this.offsetX) * this.ratioX;
							double y = (yDatas[dataLength - 1] - this.offsetY) * this.ratioY;
							// g.strokeLine(oldX, oldY, x, y);
							if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
								if (filterSymbols) {
									g.setFill(g.getStroke());
									drawShape(g, this.colorProvider.getCurrentIndex(), x, y);
								}
								if (filterIndices) {
									g.setFill(Color.BLACK);
									g.fillText(Integer.toString(dataLength - 1), x, y);
								}
								if (filterCoordinates) {
									g.setFill(Color.BLACK);
									double yPos = y;
									if (filterIndices)
										yPos += textHeight;
									g.fillText(Double.toString(xDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(yDatas[dataLength - 1]), x, yPos);
								}
							}
							if (filterLinks)
								g.stroke();
						} else {
							double min, max;
							if (this.autoFitValuesBounds) {
								double[] bounds = new double[2];
								computeValueBounds(((Curved) curve).getValues(), bounds);
								min = bounds[0];
								max = bounds[1];
							} else {
								min = this.valuesBounds.x;
								max = this.valuesBounds.y;
							}
							double offsetZ = max == min ? max - 1 : min;
							double ratioZ = max == min ? 1 : 1 / (max - min);
							double red = color.getRed();
							double green = color.getGreen();
							double blue = color.getBlue();
							for (int k = dataLength - 1; k-- != 0;) {
								double x = (xDatas[k] - this.offsetX) * this.ratioX;
								double y = (yDatas[k] - this.offsetY) * this.ratioY;
								double z = (values[k] - offsetZ) * ratioZ;
								if (z < 0)
									z = 0;
								else if (z > 1)
									z = 1;
								g.setStroke(new Color(z * red, z * green, z * blue, 1));
								g.strokeLine(oldX, oldY, x, y);
								if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
									if (filterSymbols) {
										g.setFill(g.getStroke());
										drawShape(g, this.colorProvider.getCurrentIndex(), x, y);
									}
									if (filterIndices) {
										g.setFill(Color.BLACK);
										g.fillText(Integer.toString(k), x, y);
									}
									if (filterCoordinates) {
										g.setFill(Color.BLACK);
										double yPos = y;
										if (filterIndices)
											yPos += textHeight;
										g.fillText(Double.toString(xDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(yDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(values[k]), x, yPos);
									}
								}
								oldX = x;
								oldY = y;
							}

							double x = (xDatas[dataLength - 1] - this.offsetX) * this.ratioX;
							double y = (yDatas[dataLength - 1] - this.offsetY) * this.ratioY;
							double z = (values[dataLength - 1] - offsetZ) * ratioZ;
							if (z < 0)
								z = 0;
							else if (z > 1)
								z = 1;
							g.setStroke(new Color(z * red, z * green, z * blue, 1));
							if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
								if (filterSymbols) {
									g.setFill(g.getStroke());
									drawShape(g, this.colorProvider.getCurrentIndex(), x, y);
								}
								if (filterIndices) {
									g.setFill(Color.BLACK);
									g.fillText(Integer.toString(dataLength - 1), x, y);
								}
								if (filterCoordinates) {
									g.setFill(Color.BLACK);
									double yPos = y;
									if (filterIndices)
										yPos += textHeight;
									g.fillText(Double.toString(xDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(yDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(values[dataLength - 1]), x, yPos);
								}
							}
							oldX = x;
							oldY = y;
						}
					}
				} else {
					int[][] datas = ((Curvei) curve).getData();
					int[] xDatas = datas[0];
					int[] yDatas = datas[1];
					int dataLength = xDatas.length;
					Color color = this.colorProvider.getNextColor();
					if (dataLength != 0) {
						double oldX = (xDatas[dataLength - 1] - this.offsetX) * this.ratioX;
						double oldY = (yDatas[dataLength - 1] - this.offsetY) * this.ratioY;
						int[] values = ((Curvei) curve).getValues();
						if (values == null) {
							if (filterLinks) {
								g.beginPath();
								g.moveTo(oldX, oldY);
							}
							g.setStroke(color);
							for (int k = dataLength - 1; k-- != 0;) {
								double x = (xDatas[k] - this.offsetX) * this.ratioX;
								double y = (yDatas[k] - this.offsetY) * this.ratioY;
								// g.strokeLine(oldX, oldY, x, y);
								if (filterLinks)
									g.lineTo(x, y);
								if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
									if (filterSymbols) {
										g.setFill(g.getStroke());
										drawShape(g, this.colorProvider.getCurrentIndex(), x, y);
									}
									if (filterIndices) {
										g.setFill(Color.BLACK);
										g.fillText(Integer.toString(k), x, y);
									}
									if (filterCoordinates) {
										g.setFill(Color.BLACK);
										double yPos = y;
										if (filterIndices)
											yPos += textHeight;
										g.fillText(Double.toString(xDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(yDatas[k]), x, yPos);
									}
								}
								// oldX = x;
								// oldY = y;
							}

							double x = (xDatas[dataLength - 1] - this.offsetX) * this.ratioX;
							double y = (yDatas[dataLength - 1] - this.offsetY) * this.ratioY;
							if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
								if (filterSymbols) {
									g.setFill(g.getStroke());
									drawShape(g, this.colorProvider.getCurrentIndex(), x, y);
								}
								if (filterIndices) {
									g.setFill(Color.BLACK);
									g.fillText(Integer.toString(dataLength - 1), x, y);
								}
								if (filterCoordinates) {
									g.setFill(Color.BLACK);
									double yPos = y;
									if (filterIndices)
										yPos += textHeight;
									g.fillText(Double.toString(xDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(yDatas[dataLength - 1]), x, yPos);
								}
							}
							// oldX = x;
							// oldY = y;
							if (filterLinks)
								g.stroke();
						} else {
							double min, max;
							if (this.autoFitValuesBounds) {
								int[] bounds = new int[2];
								computeValueBounds(((Curvei) curve).getValues(), bounds);
								min = bounds[0];
								max = bounds[1];
							} else {
								min = this.valuesBounds.x;
								max = this.valuesBounds.y;
							}
							double offsetZ = max == min ? max - 1 : min;
							double ratioZ = max == min ? 1 : 1 / (max - min);
							double red = color.getRed();
							double green = color.getGreen();
							double blue = color.getBlue();
							for (int k = dataLength - 1; k-- != 0;) {
								double x = (xDatas[k] - this.offsetX) * this.ratioX;
								double y = (yDatas[k] - this.offsetY) * this.ratioY;
								double z = (values[k] - offsetZ) * ratioZ;
								if (z < 0)
									z = 0;
								else if (z > 1)
									z = 1;
								g.setStroke(new Color(z * red, z * green, z * blue, 1));
								g.strokeLine(oldX, oldY, x, y);
								if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
									if (filterSymbols) {
										g.setFill(g.getStroke());
										drawShape(g, this.colorProvider.getCurrentIndex(), x, y);
									}
									if (filterIndices) {
										g.setFill(Color.BLACK);
										g.fillText(Integer.toString(k), x, y);
									}
									if (filterCoordinates) {
										g.setFill(Color.BLACK);
										double yPos = y;
										if (filterIndices)
											yPos += textHeight;
										g.fillText(Double.toString(xDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(yDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(values[k]), x, yPos);
									}
								}
								oldX = x;
								oldY = y;
							}

							double x = (xDatas[dataLength - 1] - this.offsetX) * this.ratioX;
							double y = (yDatas[dataLength - 1] - this.offsetY) * this.ratioY;
							double z = (values[dataLength - 1] - offsetZ) * ratioZ;
							if (z < 0)
								z = 0;
							else if (z > 1)
								z = 1;
							g.setStroke(new Color(z * red, z * green, z * blue, 1));
							if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
								if (filterSymbols) {
									g.setFill(g.getStroke());
									drawShape(g, this.colorProvider.getCurrentIndex(), x, y);
								}
								if (filterIndices) {
									g.setFill(Color.BLACK);
									g.fillText(Integer.toString(dataLength - 1), x, y);
								}
								if (filterCoordinates) {
									g.setFill(Color.BLACK);
									double yPos = y;
									if (filterIndices)
										yPos += textHeight;
									g.fillText(Double.toString(xDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(yDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(values[dataLength - 1]), x, yPos);
								}
							}
							oldX = x;
							oldY = y;
						}
					}
				}
			}
		}
		Object dataElement = getDrawableElement();
		if (dataElement instanceof EvolvedCurveSeries) {
			EvolvedCurveSeries ecs = (EvolvedCurveSeries) dataElement;
			List<BoxMarker> boxMarkers = ecs.getBoxmarkers();
			if (boxMarkers != null) {
				g.setStroke(Color.BLACK);
				g.setFill(Color.BLACK);
				g.setTextAlign(TextAlignment.CENTER);
				g.setFont(new Font(10));
				for (BoxMarker boxMarker : boxMarkers) {
					Rectangle rect = new Rectangle(boxMarker.x, boxMarker.y, boxMarker.width, boxMarker.height);
					double x = (rect.getX() - this.offsetX) * this.ratioX;
					double y = (rect.getY() + rect.getHeight() - this.offsetY) * this.ratioY;
					double width = rect.getWidth() * this.ratioX;
					double height = -rect.getHeight() * this.ratioY;
					g.strokeRect(x, y, width, height);
					String[] lines = boxMarker.name.split("\n");
					for (int j = 0; j < lines.length; j++)
						g.fillText(lines[j], x + width / 2, y + height / 2 + j * 10);
				}
				double begin = ecs.getBegin();
				double end = ecs.getEnd();
				if (begin != end) {
					g.setFill(new Color(0.5, 0.5, 0.5, 0.7));
					begin = (begin - this.offsetX) * this.ratioX;
					g.fillRect((begin - this.offsetX) * this.ratioX, 0, (end - this.offsetX) * this.ratioX - begin, getHeight());
				}
			}
		}
	}

	private void drawSelection() {
		GraphicsContext g = this.selectionCanvas.getGraphicsContext2D();
		g.clearRect(0, 0, this.selectionCanvas.getWidth(), this.selectionCanvas.getHeight());
		if (this.anchorPoint != null && this.draggPoint != null) {
			g.setStroke(Color.BLACK);
			g.strokeRect(this.anchorPoint.getX() - 0.5, this.anchorPoint.getY() - 0.5, this.draggPoint.getX() - this.anchorPoint.getX(), this.draggPoint.getY() - this.anchorPoint.getY());
		}
		if (this.tempChartLabel != null)
			this.tempChartLabel.updatePos(this.chartCanvas, (this.tempChartLabel.curve.getDatad()[0][this.tempChartLabel.index] - this.offsetX) * this.ratioX,
					(this.tempChartLabel.curve.getDatad()[1][this.tempChartLabel.index] - this.offsetY) * this.ratioY);
		if (this.chartLabels != null)
			for (ChartLabel cl : this.chartLabels)
				cl.updatePos(this.chartCanvas, (cl.curve.getDatad()[0][cl.index] - this.offsetX) * this.ratioX, (cl.curve.getDatad()[1][cl.index] - this.offsetY) * this.ratioY);
	}

	private static void drawShape(GraphicsContext g, int colorIndex, double x, double y) {
		switch (colorIndex) {
		case 0:
			g.fillRect(x - 5, y - 5, 10, 10);
			break;
		case 1:
			g.fillOval(x - 5, y - 5, 10, 10);
			break;
		case 2:
			g.fillPolygon(new double[] { x - 5, x, x + 5, x }, new double[] { y, y + 5, y, y - 5 }, 4);
			break;
		case 3:
			g.fillPolygon(new double[] { x - 5, x, x + 5 }, new double[] { y + 5, y - 5, y + 5 }, 3); // tri bas
			break;
		case 4:
			g.fillRect(x - 5, y - 2, 10, 5);
			break;
		case 5:
			g.fillPolygon(new double[] { x - 5, x, x + 5 }, new double[] { y - 5, y + 5, y - 5 }, 3); // tri droite
			break;
		case 6:
			g.fillRect(x - 2, y - 5, 5, 10);
			break;
		case 7:
			g.fillPolygon(new double[] { x - 5, x + 5, x - 5 }, new double[] { y + 5, y, y - 5 }, 3); // tri droite
			break;
		default:
			break;
		}
	}

	private Object[] getClosestPoint(double xp, double yp) {
		double minDist = Double.MAX_VALUE;
		int indexOfMin = 0;
		Curve closestCurve = null;
		ArrayList<TreeNode<BooleanProperty>> sons = getTheaterFilter().getChildren();
		for (int i = 0; i < this.curves.size(); i++) {
			Curve curve = this.curves.get(i);
			String name = curve.getName();
			if (name == null)
				name = Integer.toString(i);
			ArrayList<TreeNode<BooleanProperty>> curveFilters = sons.get(i).getChildren();
			boolean filterLinks = curveFilters.get(0).getValue().value;
			boolean filterSymbols = curveFilters.get(1).getValue().value;
			boolean filterCoordinates = curveFilters.get(2).getValue().value;
			boolean filterIndices = curveFilters.get(3).getValue().value;

			boolean additionalFilter = filterSymbols || filterIndices || filterCoordinates;
			if (additionalFilter || filterLinks) {
				double[][] data = curve.getDatad();
				double[] xs = data[0];
				double[] ys = data[1];
				for (int j = 0; j < data[0].length; j++) {
					double x = Math.abs(xs[j] - xp) * this.ratioX;
					double y = Math.abs(ys[j] - yp) * this.ratioY;
					double dist = x * x + y * y;
					if (dist < minDist) {
						indexOfMin = j;
						closestCurve = curve;
						minDist = dist;
					}
				}
			}
		}
		return closestCurve == null ? null : new Object[] { closestCurve, indexOfMin };
	}

	private void moveCurveSelection(boolean up) {
		if (this.tempChartLabel == null)
			return;
		Curve selectedCurve = this.tempChartLabel.curve;
		if (selectedCurve == null)
			return;
		if (up) {
			if (this.tempChartLabel.index >= selectedCurve.getDatad()[0].length - 1)
				return;
			this.tempChartLabel.index++;
		} else {
			if (this.tempChartLabel.index <= 0)
				return;
			this.tempChartLabel.index--;
		}
		this.tempChartLabel.updateInfo();
	}

	private void pushTransform() {
		double[] t = new double[] { this.first.getXValue().doubleValue(), this.first.getYValue().doubleValue(), this.last.getXValue().doubleValue(), this.last.getYValue().doubleValue() };
		double[] lastT = this.previousTransform.isEmpty() ? null : this.previousTransform.get(this.previousTransform.size() - 1);
		if (lastT == null || t[0] != lastT[0] || t[1] != lastT[1] || t[2] != lastT[2] || t[3] != lastT[3]) {
			if (this.previousTransform.size() == 10)
				this.previousTransform.remove(0);
			this.previousTransform
					.add(new double[] { this.first.getXValue().doubleValue(), this.first.getYValue().doubleValue(), this.last.getXValue().doubleValue(), this.last.getYValue().doubleValue() });
		}
	}

	private void updateCanvasParam() {
		double distX = Math.abs(this.last.getXValue().doubleValue() - this.first.getXValue().doubleValue());
		double distY = Math.abs(this.last.getYValue().doubleValue() - this.first.getYValue().doubleValue());
		this.xAxis.setTickUnit(distX / 10.0);
		this.yAxis.setTickUnit(distY / 10.0);
		this.xAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(this.xAxis) {
			@Override
			public String toString(Number object) {
				double value = object.doubleValue();
				double d = distX;
				String pat = "##0";
				if (d != 0) {
					if (d < 10)
						pat += ".";
					while (d < 10) {
						d *= 10;
						pat += "0";
					}
					pat += "0";
				}
				double absValue = Math.abs(object.doubleValue());
				if (absValue > 100000)
					pat += "E0";
				return new DecimalFormat(pat).format(value);
			}
		});
		this.yAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(this.xAxis) {
			@Override
			public String toString(Number object) {
				double d = distY;
				String pat = "##0";
				if (d != 0) {
					if (d < 10)
						pat += ".";
					while (d < 10) {
						d *= 10;
						pat += "0";
					}
					pat += "0";
				}
				double value = object.doubleValue();
				double absValue = Math.abs(value);
				if (absValue > 100000)
					pat += "E0";
				return new DecimalFormat(pat).format(value);
			}
		});

		this.xAxis.setLowerBound(this.first.getXValue().doubleValue());
		this.xAxis.setUpperBound(this.last.getXValue().doubleValue());
		this.yAxis.setLowerBound(this.first.getYValue().doubleValue());
		this.yAxis.setUpperBound(this.last.getYValue().doubleValue());

		this.ratioX = this.drawRegion.getWidth() / (this.last.getXValue().doubleValue() - this.first.getXValue().doubleValue());
		this.ratioY = -this.drawRegion.getHeight() / (this.last.getYValue().doubleValue() - this.first.getYValue().doubleValue());
		this.offsetX = this.first.getXValue().doubleValue();
		this.offsetY = this.first.getYValue().doubleValue() - this.chartCanvas.getHeight() / this.ratioY;
	}

	private void updateCanvasPos() {
		Point2D zero = new Point2D(0, 0);
		Point2D pos = this.drawRegion.localToParent(zero);
		Parent parent = this.drawRegion.getParent();
		while (parent != this.chart) {
			pos = pos.add(parent.localToParent(zero));
			parent = parent.getParent();
		}
		pos = pos.add(this.chart.parentToLocal(zero));
		if (this.chartCanvas.getLayoutX() != pos.getX())
			this.chartCanvas.setLayoutX(pos.getX());
		if (this.chartCanvas.getLayoutY() != pos.getY())
			this.chartCanvas.setLayoutY(pos.getY());
		if (this.selectionCanvas.getLayoutX() != pos.getX())
			this.selectionCanvas.setLayoutX(pos.getX());
		if (this.selectionCanvas.getLayoutY() != pos.getY())
			this.selectionCanvas.setLayoutY(pos.getY());
		updateCanvasParam();
		repaint(false);
	}

	private void updateCurvesTheaterFilter() {
		LinkedList<String> toRemove = new LinkedList<>();
		TreeRoot<BooleanProperty> theaterFilter = getTheaterFilter();
		if (theaterFilter.getChildren() != null)
			for (TreeNode<BooleanProperty> son : theaterFilter.getChildren())
				toRemove.add(son.getValue().name);
		boolean hasChanged = false;
		for (int i = 0; i < this.curves.size(); i++) {
			String name = this.curves.get(i).getName();
			if (name == null)
				name = Integer.toString(i);
			if (!toRemove.contains(name)) {
				TreeNode<BooleanProperty> tfcp = new TreeNode<>(new BooleanProperty(name, false));
				tfcp.addChild(new TreeNode<>(new BooleanProperty(LINKS, true)));
				tfcp.addChild(new TreeNode<>(new BooleanProperty(SYMBOLS, false)));
				tfcp.addChild(new TreeNode<>(new BooleanProperty(COORDINATES, false)));
				tfcp.addChild(new TreeNode<>(new BooleanProperty(INDICES, false)));
				theaterFilter.addChild(tfcp);
				hasChanged = true;
			} else
				toRemove.remove(name);
		}
		if (!toRemove.isEmpty()) {
			hasChanged = true;
			for (String curveName : toRemove)
				theaterFilter.getChildren().removeIf((son) -> son.getValue().name.equals(curveName));
		}
		if (hasChanged)
			fireTheaterFilterStructChanged();
	}

	private void updateXAxisBounds() {
		if (!this.autoFitXaxis) {
			this.first.setXValue(this.xBounds.x);
			this.minX = this.xBounds.x;
			this.last.setXValue(this.xBounds.y);
			this.maxX = this.xBounds.y;
			if (this.xAxis != null) {
				updateCanvasParam();
				repaint(false);
			}
		}
		if (this.xAxis != null) {
			this.boundsChanged = true;
			repaint(false);
		}
	}

	private void updateYAxisBounds() {
		if (!this.autoFitYaxis) {
			this.first.setYValue(this.yBounds.x);
			this.minY = this.yBounds.x;
			this.last.setYValue(this.yBounds.y);
			this.maxY = this.yBounds.y;
			if (this.yAxis != null) {
				updateCanvasParam();
				repaint(false);
			}
		}
		if (this.yAxis != null) {
			this.boundsChanged = true;
			repaint(false);
		}
	}

	@Override
	public Dimension2D getDimension() {
		return new Dimension2D(640, 480);
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		if (this.chart == null)
			return true;
		String filterName = filterPath[0];
		boolean isVisible = false;
		for (TreeNode<BooleanProperty> node : getTheaterSon(getTheaterFilter(), filterName).getChildren())
			if (node.getValue().value) {
				isVisible = true;
				break;
			}
		for (Node item : this.chart.lookupAll("Label.chart-legend-item")) {
			Label label = (Label) item;
			if (label.getText().equals(filterName))
				label.setTextFill(isVisible ? Color.BLACK : Color.DARKGRAY);
		}
		repaint(false);
		return true;
	}

	@Override
	public String[] getStatusBarInfo() {
		return new String[] { this.xAxis.getLabel() + ": " + (this.mousePostion.x / this.ratioX + this.offsetX), this.yAxis.getLabel() + ": " + (this.mousePostion.y / this.ratioY + this.offsetY) };
	}

	@Override
	public boolean canAddInputToRenderer(Class<?>[] class1) {
		return true;
	}

	@Override
	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput == null ? false : Curved.class.isAssignableFrom(additionalInput) || CurveSeries.class.isAssignableFrom(additionalInput);
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "xBounds", !this.autoFitXaxis);
		fireSetPropertyEnable(this, "yBounds", !this.autoFitYaxis);
		fireSetPropertyEnable(this, "valuesBounds", !this.autoFitValuesBounds);
	}

	@Override
	public void userAgentStylesheetChange() {
		boolean darkTheme = ScenariumGuiProperties.get().isDarkTheme();
		this.chart.lookup(".chart-plot-background").setStyle("-fx-background-color: " + (darkTheme ? "-fx-base" : "#C0C0C0"));
		this.chart.lookup(".chart-vertical-grid-lines").setStyle(darkTheme ? null : "-fx-stroke: #DEDEDE;");
		this.chart.lookup(".chart-horizontal-grid-lines").setStyle(darkTheme ? null : "-fx-stroke: #DEDEDE;");
	}

	public boolean isAutoFitXaxis() {
		return this.autoFitXaxis;
	}

	public void setAutoFitXaxis(boolean autoFitXaxis) {
		if (this.autoFitXaxis == autoFitXaxis)
			return;
		var oldValue = this.autoFitXaxis;
		this.autoFitXaxis = autoFitXaxis;
		updateXAxisBounds();
		fireSetPropertyEnable(this, "xBounds", !autoFitXaxis);
		repaint(false);
		this.pcs.firePropertyChange("autoFitXaxis", oldValue, this.autoFitXaxis);
	}

	public Point2d getxBounds() {
		return this.xBounds;
	}

	public void setxBounds(Point2d xBounds) {
		if (this.xBounds == xBounds)
			return;
		var oldValue = this.xBounds;
		this.xBounds = xBounds;
		updateXAxisBounds();
		repaint(false);
		this.pcs.firePropertyChange("xBounds", oldValue, this.xBounds);
	}

	public boolean isAutoFitYaxis() {
		return this.autoFitYaxis;
	}

	public void setAutoFitYaxis(boolean autoFitYaxis) {
		if (this.autoFitYaxis == autoFitYaxis)
			return;
		var oldValue = this.autoFitYaxis;
		this.autoFitYaxis = autoFitYaxis;
		updateYAxisBounds();
		fireSetPropertyEnable(this, "yBounds", !autoFitYaxis);
		repaint(false);
		this.pcs.firePropertyChange("autoFitYaxis", oldValue, this.autoFitYaxis);
	}

	public Point2d getyBounds() {
		return this.yBounds;
	}

	public void setyBounds(Point2d yBounds) {
		if (this.yBounds == yBounds)
			return;
		var oldValue = this.yBounds;
		this.yBounds = yBounds;
		updateYAxisBounds();
		repaint(false);
		this.pcs.firePropertyChange("yBounds", oldValue, this.yBounds);
	}

	public boolean isAutoFitValuesBounds() {
		return this.autoFitValuesBounds;
	}

	public void setAutoFitValuesBounds(boolean autoFitValuesBounds) {
		if (this.autoFitValuesBounds == autoFitValuesBounds)
			return;
		var oldValue = this.autoFitValuesBounds;
		this.autoFitValuesBounds = autoFitValuesBounds;
		fireSetPropertyEnable(this, "valuesBounds", !autoFitValuesBounds);
		repaint(false);
		this.pcs.firePropertyChange("autoFitValuesBounds", oldValue, this.autoFitValuesBounds);
	}

	public Point2d getValuesBounds() {
		return this.valuesBounds;
	}

	public void setValuesBounds(Point2d valuesBounds) {
		var oldValue = this.valuesBounds;
		this.valuesBounds = valuesBounds;
		repaint(false);
		this.pcs.firePropertyChange("valuesBounds", oldValue, this.valuesBounds);
	}

	public double getMargin() {
		return this.margin;
	}

	public void setMargin(double margin) {
		if (this.margin == margin)
			return;
		var oldValue = this.margin;
		this.margin = margin;
		this.boundsChanged = true;
		if (this.xAxis != null)
			repaint(false);
		this.pcs.firePropertyChange("margin", oldValue, this.margin);
	}

	@ArrayInfo(prefHeight = 26 * 8 + 2)
	@PropertyInfo(index = 7, nullable = false, info = "Color for the additional input point cloud")
	public Color[] getChartsColor() {
		return this.colorProvider.getColors();
	}

	public void setChartsColor(Color[] chartsColor) {
		var oldValue = this.getChartsColor();
		this.colorProvider = new ColorProvider(chartsColor);
		repaint(false);
		this.pcs.firePropertyChange("chartsColor", oldValue, chartsColor);
	}

	public void setColors(Color[] colors) {
		if (colors != null)
			this.colorProvider = new ColorProvider(colors);
		repaint(false);
	}
}

class ChartLabel extends VBox {
	private static int radius = 10;
	public static final int WIDTH = 210 + radius;
	public static double height = 90;
	private static final Color COLOR = new Color(0, 0, 0, 0.5);
	public Curve curve;
	public int index;
	private final String xLabelName;
	private final String yLabelName;
	private ChartLabelOrientation orientation;
	private double posX;
	private double posY;
	private final VBox labelBox;
	private final Label idLabel;
	private final Label xLabel;
	private final Label yLabel;
	private Label valueLabel;

	public ChartLabel(double posX, double posY, double radius, Curve curve, int index, String xLabelName, String yLabelName, BiConsumer<ChartLabel, Boolean> isPin) {
		this.curve = curve;
		this.index = index;
		this.xLabelName = xLabelName;
		this.yLabelName = yLabelName;
		setTranslateX(posX);
		setTranslateY(posY);
		setBackground(new Background(new BackgroundFill(COLOR, null, null)));
		setPrefSize(WIDTH, height);
		setMaxSize(WIDTH, height);
		setPickOnBounds(false);
		focusedProperty().addListener(e -> setBorder(isFocused() ? new Border(new BorderStroke(Color.LIME, BorderStrokeStyle.SOLID, null, null)) : null));
		// focusedProperty().addListener(e -> {
		// Log.info("cool: " + isFocused());
		// if(isFocused() == false)
		// Log.error("pk");
		// setBorder(isFocused() ? new Border(new BorderStroke(Color.LIME, BorderStrokeStyle.SOLID, null, null)) : null);
		// });

		final ImageView toggleImage = new ImageView(new Image(LoadPackageStream.getStream("/pin2.png")));
		ToggleButton pinButton = new ToggleButton();
		pinButton.setGraphic(toggleImage);
		pinButton.selectedProperty().addListener(e -> {
			if (pinButton.isSelected()) {
				toggleImage.setRotate(-90);
				pinButton.setStyle("-fx-background-color: transparent;-fx-border-width: 1px;-fx-border-color: #303030 #B0B0B0 #B0B0B0 #303030;"); // 3 bas 4 gauche
			} else {
				toggleImage.setRotate(0);
				pinButton.setStyle("-fx-background-color: transparent;-fx-border-width: 1px;-fx-border-color: #B0B0B0 #303030 #303030 #B0B0B0;"); // 3 bas 4 gauche
			}
			isPin.accept(this, pinButton.isSelected());
		});

		this.idLabel = new Label();
		this.idLabel.setTooltip(new Tooltip());
		this.idLabel.setTextFill(Color.WHITE);
		this.xLabel = new Label();
		this.xLabel.setTooltip(new Tooltip());
		this.xLabel.setTextFill(Color.WHITE);
		this.yLabel = new Label();
		this.yLabel.setTextFill(Color.WHITE);
		this.yLabel.setTooltip(new Tooltip());
		// if (value != null) {
		// valueLabel = new Label();
		// valueLabel.setTooltip(new Tooltip());
		// valueLabel.setTextFill(Color.WHITE);
		// }
		// setinfo(selectedIndex, x, y, value);

		BorderPane idPinBox = new BorderPane();
		idPinBox.setLeft(this.idLabel);
		idPinBox.setRight(pinButton);
		BorderPane.setAlignment(this.idLabel, Pos.BOTTOM_LEFT);
		BorderPane.setAlignment(idPinBox, Pos.TOP_RIGHT);
		HBox.setHgrow(this.idLabel, Priority.ALWAYS);
		this.labelBox = new VBox(idPinBox, this.xLabel, this.yLabel);
		// if (valueLabel != null)
		// labelBox.getChildren().add(valueLabel);
		this.labelBox.setPadding(new Insets(5, 5, 1, 5));
		this.labelBox.setAlignment(Pos.CENTER_LEFT);
		this.labelBox.setPickOnBounds(false);
		// labelBox.setMouseTransparent(true);
		getChildren().add(this.labelBox);

		setOnKeyPressed(e -> {
			if (e.isControlDown()) {
				ChartLabelOrientation oldOrientation = this.orientation;
				if (e.getCode() == KeyCode.RIGHT) {
					if (this.orientation == ChartLabelOrientation.TOP_LEFT)
						this.orientation = ChartLabelOrientation.TOP_RIGHT;
					else if (this.orientation == ChartLabelOrientation.BOTTOM_LEFT)
						this.orientation = ChartLabelOrientation.BOTTOM_RIGHT;
					e.consume();
				} else if (e.getCode() == KeyCode.LEFT) {
					if (this.orientation == ChartLabelOrientation.TOP_RIGHT)
						this.orientation = ChartLabelOrientation.TOP_LEFT;
					else if (this.orientation == ChartLabelOrientation.BOTTOM_RIGHT)
						this.orientation = ChartLabelOrientation.BOTTOM_LEFT;
					e.consume();
				} else if (e.getCode() == KeyCode.UP) {
					if (this.orientation == ChartLabelOrientation.BOTTOM_RIGHT)
						this.orientation = ChartLabelOrientation.TOP_RIGHT;
					else if (this.orientation == ChartLabelOrientation.BOTTOM_LEFT)
						this.orientation = ChartLabelOrientation.TOP_LEFT;
					e.consume();
				} else if (e.getCode() == KeyCode.DOWN) {
					if (this.orientation == ChartLabelOrientation.TOP_RIGHT)
						this.orientation = ChartLabelOrientation.BOTTOM_RIGHT;
					else if (this.orientation == ChartLabelOrientation.TOP_LEFT)
						this.orientation = ChartLabelOrientation.BOTTOM_LEFT;
					e.consume();
				}
				if (oldOrientation != this.orientation) {
					updateOrientation();
					setTranslate(new Point2D(this.posX, this.posY));
				}
			} else
				Log.info("je déplace");
		});
		pinButton.setStyle("-fx-background-color: transparent;-fx-border-width: 1px;-fx-border-color: #B0B0B0 #303030 #303030 #B0B0B0;");
		pinButton.setBackground(null);
		pinButton.setPadding(new Insets(1));
		// updateOrientation();
		// setOnMousePressed(e -> requestFocus());
		setOnMousePressed(e -> {
			requestFocus();
			e.consume();
		});

		heightProperty().addListener(e -> {
			height = getPrefHeight();
			updateOrientation();
			setTranslate(new Point2D(this.posX, this.posY));
		});
	}

	public void setTranslate(Point2D pos) {
		this.posX = pos.getX();
		this.posY = pos.getY();
		updateTranslate();
	}

	public void updateInfo() {
		String ids = Integer.toString(this.index);
		this.idLabel.setText("id: " + ids);
		this.idLabel.getTooltip().setText(ids);
		String xs = Double.toString(this.curve.getDatad()[0][this.index]);
		this.xLabel.setText(xs + " " + this.xLabelName);
		this.xLabel.getTooltip().setText(xs);
		String ys = Double.toString(this.curve.getDatad()[1][this.index]);
		this.yLabel.setText(ys + " " + this.yLabelName);
		this.yLabel.getTooltip().setText(ys);
		Double value = this.curve.getValuesd() != null ? this.curve.getValuesd()[this.index] : null;
		if (this.valueLabel != null && value == null) {
			this.labelBox.getChildren().remove(this.valueLabel);
			this.valueLabel = null;
		} else if (this.valueLabel == null && value != null) {
			this.valueLabel = new Label();
			this.valueLabel.setTooltip(new Tooltip());
			this.valueLabel.setTextFill(Color.WHITE);
			this.labelBox.getChildren().add(this.valueLabel);
		}
		if (this.valueLabel != null) {
			String sValue = value.toString();
			this.valueLabel.setText("value: " + sValue);
			this.valueLabel.getTooltip().setText(sValue);
		}
	}

	// public void setinfo(int selectedIndex, double x, double y, Double value) {
	// String ids = Integer.toString(selectedIndex);
	// idLabel.setText("id: " + ids);
	// idLabel.getTooltip().setText(ids);
	// String xs = Double.toString(x);
	// xLabel.setText("x: " + xs);
	// xLabel.getTooltip().setText(xs);
	// String ys = Double.toString(y);
	// yLabel.setText("y: " + ys);
	// yLabel.getTooltip().setText(ys);
	// if (valueLabel != null && value == null) {
	// labelBox.getChildren().remove(valueLabel);
	// valueLabel = null;
	// } else if (valueLabel == null && value != null) {
	// valueLabel = new Label();
	// valueLabel.setTooltip(new Tooltip());
	// valueLabel.setTextFill(Color.WHITE);
	// labelBox.getChildren().add(valueLabel);
	// }
	// if (valueLabel != null) {
	// String values = value.toString();
	// valueLabel.setText("value: " + values);
	// valueLabel.getTooltip().setText(values);
	// }
	// }

	private void updateOrientation() {
		Shape shape;
		switch (this.orientation) {
		case BOTTOM_RIGHT:
			shape = Shape.union(new Rectangle(radius, radius, WIDTH - radius, height - radius), new Circle(radius, radius, radius));
			shape = Shape.subtract(shape, new Circle(radius, radius, 6));
			shape = Shape.union(shape, new Line(radius, 2, radius, radius * 2 - 2));
			shape = Shape.union(shape, new Line(2, radius, radius * 2 - 2, radius));
			break;
		case BOTTOM_LEFT:
			shape = Shape.union(new Rectangle(0, radius, WIDTH - radius, height - radius), new Circle(WIDTH - radius, radius, radius));
			shape = Shape.subtract(shape, new Circle(WIDTH - radius, radius, 6));
			shape = Shape.union(shape, new Line(WIDTH - radius * 2 - 2, radius, WIDTH - 2, radius));
			shape = Shape.union(shape, new Line(WIDTH - radius, 2, WIDTH - radius, radius * 2 - 2));
			break;
		case TOP_RIGHT:
			shape = Shape.union(new Rectangle(radius, 0, WIDTH - radius, height - radius), new Circle(radius, height - radius, radius));
			shape = Shape.subtract(shape, new Circle(radius, height - radius, 6));
			shape = Shape.union(shape, new Line(2, height - radius, radius * 2 - 2, height - radius));
			shape = Shape.union(shape, new Line(radius, height - radius * 2 - 2, radius, height - 2));
			break;
		case TOP_LEFT:
			shape = Shape.union(new Rectangle(0, 0, WIDTH - radius, height - radius), new Circle(WIDTH - radius, height - radius, radius));
			shape = Shape.subtract(shape, new Circle(WIDTH - radius, height - radius, 6));
			shape = Shape.union(shape, new Line(WIDTH - radius * 2 - 2, height - radius, WIDTH - 2, height - radius));
			shape = Shape.union(shape, new Line(WIDTH - radius, height - radius * 2 - 2, WIDTH - radius, height - 2));
			break;
		default:
			return;
		}
		setShape(shape);
	}

	public void updatePos(Canvas canvas, double x, double y) {
		// Log.info("x: " + x);
		boolean isVisible = x >= 0 && y >= 0 && x < canvas.getWidth() && y < canvas.getHeight();
		if (isVisible != isVisible())
			setVisible(isVisible);
		ChartLabelOrientation orientation;
		if (x - ChartLabel.WIDTH + radius < 0)
			orientation = y - ChartLabel.height + radius < 0 ? ChartLabelOrientation.BOTTOM_RIGHT : ChartLabelOrientation.TOP_RIGHT;
		else
			orientation = y - ChartLabel.height + radius < 0 ? ChartLabelOrientation.BOTTOM_LEFT : ChartLabelOrientation.TOP_LEFT;
		if (this.orientation != orientation) {
			this.orientation = orientation;
			updateOrientation();
		}
		setTranslate(canvas.localToParent(new Point2D(x, y)));
	}

	private void updateTranslate() {
		Insets insets;
		switch (this.orientation) {
		case BOTTOM_RIGHT:
			insets = new Insets(radius + 5, 5, 1, 5 + radius);
			setTranslateX(this.posX - radius);
			setTranslateY(this.posY - radius);
			break;
		case BOTTOM_LEFT:
			insets = new Insets(radius + 5, 5 + radius, 1, 5);
			setTranslateX(this.posX - WIDTH + radius);
			setTranslateY(this.posY - radius);
			break;
		case TOP_RIGHT:
			insets = new Insets(5, 5, 5, 5 + radius);
			setTranslateX(this.posX - radius);
			setTranslateY(this.posY - height + radius);
			break;
		case TOP_LEFT:
			insets = new Insets(5, 5 + radius, 5, 5);
			setTranslateX(this.posX - WIDTH + radius);
			setTranslateY(this.posY - height + radius);
			break;
		default:
			return;
		}
		this.labelBox.setPadding(insets);
	}
}

enum ChartLabelOrientation {
	BOTTOM_RIGHT, BOTTOM_LEFT, TOP_RIGHT, TOP_LEFT
}
