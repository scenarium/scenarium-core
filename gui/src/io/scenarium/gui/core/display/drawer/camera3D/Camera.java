/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.drawer.camera3D;

import javax.vecmath.Vector3d;

import com.jogamp.opengl.GL2;

import javafx.scene.Node;

public interface Camera {

	public void drawCameraInfo(GL2 gl);

	public double getDistance();

	public void getMatrix(double[] matrix);

	public Vector3d getViewCenter();

	public void init(CameraChangeListener listener);

	public void reset();

	public void setBounds(float width, float height);

	public void setGl(GL2 gl);

	public void setNode(Node node);
}
