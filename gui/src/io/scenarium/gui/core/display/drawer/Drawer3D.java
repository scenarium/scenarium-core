/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.drawer;

import static com.jogamp.opengl.GL.GL_COLOR_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_TEST;
import static com.jogamp.opengl.GL.GL_LEQUAL;
import static com.jogamp.opengl.GL.GL_LINES;
import static com.jogamp.opengl.GL.GL_NICEST;
import static com.jogamp.opengl.GL.GL_TEXTURE_2D;
import static com.jogamp.opengl.GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_COLOR_MATERIAL;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_FLAT;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_LIGHT0;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_LIGHTING;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_MODELVIEW;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;

import java.util.ArrayList;

import javax.vecmath.Color3f;
import javax.vecmath.GMatrix;
import javax.vecmath.GVector;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.container.BeanInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.beanmanager.struct.BooleanProperty;
import io.beanmanager.struct.TreeNode;
import io.beanmanager.struct.TreeRoot;
import io.scenarium.core.struct.DrawableObject3D;
import io.scenarium.gui.core.display.StackableDrawer;
import io.scenarium.gui.core.display.drawer.camera3D.ArcBallCamera;
import io.scenarium.gui.core.display.drawer.camera3D.Camera;
import io.scenarium.gui.core.display.drawer.camera3D.CameraChangeListener;
import io.scenarium.gui.core.display.drawer.camera3D.FreeFlyCamera;

import com.jogamp.opengl.DefaultGLCapabilitiesChooser;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLDrawableFactory;
import com.jogamp.opengl.GLOffscreenAutoDrawable;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;
import com.jogamp.opengl.util.awt.AWTGLReadBufferUtil;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class Drawer3D extends TheaterPanel implements CameraChangeListener, StackableDrawer {
	protected static Color3f[] colors = new Color3f[] { new Color3f(1, 0, 0), new Color3f(0, 0, 1), new Color3f(0, 1, 0), new Color3f(1, 0, 1), new Color3f(0, 1, 1), new Color3f(1, 1, 0),
			new Color3f(0.5f, 0, 1), new Color3f(1, 0.5f, 0) };
	private static final String LIGHT = "Light";
	private static final String AXIS = "Axis";
	private static final String GRID = "Grid";
	private static final String OBJECT3D = "Object 3D";
	protected static GL2 gl;
	private static GLU glu;
	private static GLOffscreenAutoDrawable drawable;

	protected double[] matrix = new double[16];
	// Theater filter properties
	private boolean filterLight = false;
	private boolean filterAxis = true;
	private boolean filterGrid = true;
	private boolean filter3DObject = true;
	private final ImageView iv;

	@PropertyInfo(index = 0, nullable = false)
	@BeanInfo(possibleSubclasses = { FreeFlyCamera.class, ArcBallCamera.class }, alwaysExtend = true, inline = true)
	private Camera camera = new ArcBallCamera();
	@PropertyInfo(index = 1, nullable = false)
	private Color clearColor = new Color(0.0, 0.0, 0.0, 0.5);
	@PropertyInfo(index = 2)
	@NumberInfo(min = 0)
	private float pointSize = 3;

	private double xMouse;
	private double yMouse;

	public Drawer3D() {
		super();
		this.iv = new ImageView();
		this.iv.setPreserveRatio(false);
		this.iv.addEventHandler(MouseEvent.MOUSE_MOVED, e -> {
			this.xMouse = e.getX();
			this.yMouse = e.getY();
		});
		setOnKeyPressed(this::onKeyPressed);
		this.iv.fitWidthProperty().bind(widthProperty());
		this.iv.fitHeightProperty().bind(heightProperty());
		setFocusTraversable(true);
		this.camera.setNode(this);
		getChildren().add(this.iv);
		this.camera.init(this);
		this.camera.getMatrix(this.matrix);
	}

	@Override
	protected void onKeyPressed(KeyEvent e) {
		if (!e.isConsumed() && e.getCode() == KeyCode.A) {
			adaptViewToScenario();
			e.consume();
		}
		super.onKeyPressed(e);
	}

	@Override
	public void dispose() {
		super.dispose();
		destroyOffscreenAutoDrawable();
	}

	@Override
	public void cameraChange() {
		repaint(false);
		if (gl == null)
			return;
		gl.glMatrixMode(GL_PROJECTION);
		gl.glLoadIdentity();
		double dist = Math.abs(this.camera.getDistance());
		glu.gluPerspective(45.0f, getWidth() / getHeight(), dist / 10, dist * 100); // pas trop grand sinon arcball deconne
		gl.glMatrixMode(GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	@Override
	public void fitDocument() {
		// camera.reset();
	}

	@Override
	public Dimension2D getDimension() {
		return new Dimension2D(1200, 800);
	}

	@Override
	public String[] getStatusBarInfo() {
		gl.glLoadIdentity();
		this.camera.getMatrix(this.matrix);
		gl.glMultMatrixd(this.matrix, 0);
		Point3d p = screenToWorld2(new Point2D(this.xMouse, this.yMouse));
		return new String[] { "x: " + p.x, "y: " + p.y };
	}

	@Override
	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput == null ? false : DrawableObject3D.class.isAssignableFrom(additionalInput) || DrawableObject3D[].class.isAssignableFrom(additionalInput);
	}

	@Override
	public boolean canAddInputToRenderer(Class<?>[] class1) {
		return true;
	}

	@Override
	protected void paint(Object dataElement) {
		if (gl == null)
			initGl();
		gl.glClearColor((float) this.clearColor.getRed(), (float) this.clearColor.getGreen(), (float) this.clearColor.getBlue(), (float) this.clearColor.getOpacity());
		int width = (int) this.iv.getFitWidth();
		int height = (int) this.iv.getFitHeight();
		Drawer3D.drawable.setSurfaceSize(width, height);
		reshape(width, height);
		if (this.filterLight) {
			gl.glEnable(GL_LIGHT0);
			gl.glEnable(GL_LIGHTING);
		} else {
			gl.glDisable(GL_LIGHTING);
			gl.glDisable(GL_LIGHT0);
		}
		gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		gl.glLoadIdentity();
		gl.glPushMatrix();
		this.camera.getMatrix(this.matrix);
		gl.glMultMatrixd(this.matrix, 0);
		this.camera.drawCameraInfo(gl);
		drawWorld();
		gl.glPopMatrix();
		gl.glLoadIdentity();
		gl.glFlush();
		this.iv.setImage(SwingFXUtils.toFXImage(
				new AWTGLReadBufferUtil(drawable.getGLProfile(), true).readPixelsToBufferedImage(drawable.getGL(), 0, 0, drawable.getSurfaceWidth(), drawable.getSurfaceHeight(), true), null));
	}

	protected void initGl() {
		createOffscreenAutoDrawable();
		if (gl == null)
			return;
		gl.glPointSize(this.pointSize);
		this.camera.setGl(gl);
	}

	protected void drawWorld() {
		// drawTorus(gl, 0.30f, 0.5f);
		if (this.filterAxis)
			drawAxis(gl);
		if (this.filterGrid)
			drawGrid(gl, -100, 100);
		if (this.filter3DObject) {
			int colorIndex = 0;
			for (DrawableObject3D obj : getObjects3D())
				if (obj != null) {
					obj.draw(gl, colors[colorIndex++]);
					if (colorIndex == colors.length)
						colorIndex = 0;
				}
		}
	}

	protected void drawGrid(GL2 gl, int min, int max) {
		gl.glLineWidth(1);
		gl.glColor3f(0.0f, 0.0f, 0.0f);
		for (int x = min; x < max + 1; x++) {
			gl.glBegin(GL.GL_LINE_LOOP);
			gl.glVertex3f(x, min, 0);
			gl.glVertex3f(x, max, 0);
			gl.glEnd();
		}
		for (int y = min; y < max + 1; y++) {
			gl.glBegin(GL.GL_LINE_LOOP);
			gl.glVertex3f(min, y, 0);
			gl.glVertex3f(max, y, 0);
			gl.glEnd();
		}
	}

	protected void drawAxis(GL2 gl) {
		gl.glLineWidth(3);
		gl.glBegin(GL_LINES);
		// x axis
		gl.glColor3f(1.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(1.0f, 0.0f, 0.0f);
		// y axis
		gl.glColor3f(0.0f, 1.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 1.0f, 0.0f);
		// Z axis
		gl.glColor3f(0.0f, 0.0f, 1.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 1.0f);
		gl.glEnd();
	}

	protected ArrayList<DrawableObject3D> getObjects3D() {
		ArrayList<DrawableObject3D> objs3D = new ArrayList<>();
		objs3D.add((DrawableObject3D) getDrawableElement());
		Object[] add = getAdditionalDrawableElement();
		if (add == null)
			return objs3D;
		for (Object addInput : add)
			if (addInput instanceof DrawableObject3D[])
				for (DrawableObject3D o : (DrawableObject3D[]) addInput)
					objs3D.add(o);
			else
				objs3D.add((DrawableObject3D) addInput);
		return objs3D;
	}

	protected void reshape(int width, int height) {
		if (gl == null)
			return;
		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL_PROJECTION);
		gl.glLoadIdentity();
		double dist = Math.abs(this.camera.getDistance());
		glu.gluPerspective(45.0f, (float) width / (float) height, dist * 10 / 100, dist * 10); // pas trop grand sinon arcball deconne
		gl.glMatrixMode(GL_MODELVIEW);
		gl.glLoadIdentity();
		this.camera.setBounds(width, height);
	}

	@Override
	protected void populateTheaterFilter(TreeRoot<BooleanProperty> theaterFilter) {
		super.populateTheaterFilter(theaterFilter);
		theaterFilter.addChild(new TreeNode<>(new BooleanProperty(OBJECT3D, true)));
		theaterFilter.addChild(new TreeNode<>(new BooleanProperty(GRID, true)));
		theaterFilter.addChild(new TreeNode<>(new BooleanProperty(AXIS, true)));
		theaterFilter.addChild(new TreeNode<>(new BooleanProperty(LIGHT, false)));
	}

	@Override
	protected boolean updateFilterWithPath(String[] filterPath, boolean value) {
		if (filterPath[filterPath.length - 1].equals(OBJECT3D))
			this.filter3DObject = value;
		else if (filterPath[filterPath.length - 1].equals(AXIS))
			this.filterAxis = value;
		else if (filterPath[filterPath.length - 1].equals(GRID))
			this.filterGrid = value;
		else if (filterPath[filterPath.length - 1].equals(LIGHT)) {
			this.filterLight = value;
			repaint(false);
		} else
			return false;
		repaint(false);
		return true;
	}

	private static synchronized void createOffscreenAutoDrawable() {
		if (gl != null)
			return;
		GLProfile glp = GLProfile.getDefault();
		GLCapabilities caps = new GLCapabilities(glp);
		caps.setHardwareAccelerated(true);
		caps.setDoubleBuffered(false);
		caps.setAlphaBits(8);
		caps.setRedBits(8);
		caps.setBlueBits(8);
		caps.setGreenBits(8);
		caps.setOnscreen(false);
		GLDrawableFactory factory = GLDrawableFactory.getFactory(glp);
		drawable = factory.createOffscreenAutoDrawable(factory.getDefaultDevice(), caps, new DefaultGLCapabilitiesChooser(), 100, 100);
		drawable.display();
		drawable.getContext().makeCurrent();
		gl = drawable.getGL().getGL2();
		gl.glClearDepth(1.0f);
		gl.glDepthFunc(GL_LEQUAL);
		gl.glEnable(GL_DEPTH_TEST);
		gl.glShadeModel(GL_FLAT);
		gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
		glu = GLU.createGLU(gl);
		GLUquadric quadratic = glu.gluNewQuadric();
		glu.gluQuadricNormals(quadratic, GLU.GLU_SMOOTH);
		glu.gluQuadricTexture(quadratic, true);
		gl.glEnable(GL_COLOR_MATERIAL);
		gl.glDisable(GL_TEXTURE_2D);
	}

	private static synchronized void destroyOffscreenAutoDrawable() {
		if (gl == null)
			return;
		gl = null;
		glu = null;
		drawable.getContext().release();
		drawable.destroy();
	}

	private void adaptViewToScenario() {
		this.camera.reset();
	}

	private Point3d screenToWorld2(Point2D point2d) {
		int[] viewport = new int[4];
		double[] mvmatrix = new double[16];
		double[] projmatrix = new double[16];
		int realy = 0;
		double[] wcoord = new double[4]; // gl.glOrtho(0, 50, 50, 0, 0, 100);
		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
		realy = viewport[3] - (int) this.yMouse - 1;
		// position de la camera
		glu.gluUnProject((viewport[2] - viewport[0]) / 2.0, (viewport[3] - viewport[1]) / 2.0, 0, mvmatrix, 0, projmatrix, 0, viewport, 0, wcoord, 0);
		double ax = wcoord[0];
		double ay = wcoord[1];
		double az = wcoord[2];
		glu.gluUnProject(this.xMouse, realy, 1, mvmatrix, 0, projmatrix, 0, viewport, 0, wcoord, 0);
		double bx = wcoord[0];
		double by = wcoord[1];
		double bz = wcoord[2];
		double t = -az / (bz - az); // on calcule t en fonction de la position de la camera(Az) et de (Bz)
		double mx = t * (bx - ax) + ax; // on calcule les positions de M avec t
		double my = t * (by - ay) + ay;
		return new Point3d(mx, my, 0);
	}

	@SuppressWarnings("unused")
	private Point3d screenToWorld(Point2D point2d) {
		int[] viewport = new int[4];
		double[] mvmatrix = new double[16];
		double[] projmatrix = new double[16];
		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
		GMatrix modelMatrix = new GMatrix(0, 0);
		modelMatrix.set(new Matrix4d(mvmatrix));
		modelMatrix.transpose();
		GMatrix projectionMatrix = new GMatrix(0, 0);
		projectionMatrix.set(new Matrix4d(projmatrix));
		projectionMatrix.transpose();

		this.xMouse = point2d.getX();
		this.yMouse = viewport[3] - point2d.getY();
		GVector point = new GVector(new double[] { this.xMouse / getWidth() * 2.0f - 1.0f, this.yMouse / getHeight() * 2.0f - 1.0f, 1, 1 });

		projectionMatrix.mul(modelMatrix);
		projectionMatrix.invert();
		point.mul(projectionMatrix, point);
		return new Point3d(point.getElement(0) / point.getElement(3), point.getElement(1) / point.getElement(3), point.getElement(2));
	}

	// http://www.songho.ca/opengl/gl_transform.html
	@SuppressWarnings("unused")
	private Point2D worldToScreen(Point3d xyzPoint) {
		int[] viewport = new int[4];
		double[] mvmatrix = new double[16];
		double[] projmatrix = new double[16];
		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
		GMatrix viewMatrix = new GMatrix(0, 0);
		viewMatrix.set(new Matrix4d(mvmatrix));
		viewMatrix.transpose();
		GMatrix projectionMatrix = new GMatrix(0, 0);
		projectionMatrix.set(new Matrix4d(projmatrix));
		projectionMatrix.transpose();
		GVector point = new GVector(new double[] { xyzPoint.x, xyzPoint.y, xyzPoint.z, 1 });
		point.mul(viewMatrix, point);
		point.mul(projectionMatrix, point);
		double w = point.getElement(3);
		double xnde = point.getElement(0) / w;
		double ynde = point.getElement(1) / w;
		double xw = getWidth() / 2 * xnde + (viewport[0] + getWidth() / 2);
		double yw = getHeight() / 2 * ynde + (viewport[1] + getHeight() / 2);
		double xndeb = (xw - (viewport[0] + getWidth() / 2)) / (getWidth() / 2);
		double yndeb = (yw - (viewport[1] + getHeight() / 2)) / (getHeight() / 2);
		point = new GVector(new double[] { xndeb, yndeb, -6, 1 });
		projectionMatrix.invert();
		viewMatrix.invert();
		projectionMatrix.mul(viewMatrix);
		point.mul(projectionMatrix, point);
		return new Point2D(xw, getHeight() - yw + 1);
	}

	public Camera getCamera() {
		return this.camera;
	}

	public void setCamera(Camera camera) {
		if (camera == null)
			return;
		var oldValue = this.camera;
		camera.init(this); // je tue les anciennes propriété la
		camera.setNode(this);
		camera.setBounds((float) getWidth(), (float) getHeight());
		if (drawable != null)
			camera.setGl(gl);
		this.camera = camera;
		this.pcs.firePropertyChange("camera", oldValue, this.camera);
	}

	public Color getClearColor() {
		return this.clearColor;
	}

	public void setClearColor(Color clearColor) {
		var oldValue = this.clearColor;
		this.clearColor = clearColor;
		if (drawable != null) {
			gl.glClearColor((float) clearColor.getRed(), (float) clearColor.getGreen(), (float) clearColor.getBlue(), (float) clearColor.getOpacity());
			repaint(false);
		}
		this.pcs.firePropertyChange("clearColor", oldValue, this.clearColor);
	}

	public float getPointSize() {
		return this.pointSize;
	}

	public void setPointSize(float pointSize) {
		var oldValue = this.pointSize;
		this.pointSize = pointSize;
		if (drawable != null) {
			gl.glPointSize(pointSize);
			repaint(false);
		}
		this.pcs.firePropertyChange("pointSize", oldValue, this.pointSize);
	}
}
