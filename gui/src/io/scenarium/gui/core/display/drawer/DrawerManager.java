/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.drawer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.swing.event.EventListenerList;

import io.beanmanager.editors.PropertyEditor;
import io.beanmanager.editors.PropertyEditorManager;
import io.scenarium.gui.core.display.LoadDrawerListener;

public class DrawerManager {
	private static LinkedHashMap<Class<?>, Class<? extends TheaterPanel>> drawerManager = new LinkedHashMap<>();
	private static HashMap<Class<?>, Class<? extends TheaterPanel>> renderPanelClassCache = new HashMap<>();
	private static final EventListenerList DRAWERLISTENERS = new EventListenerList();

	private DrawerManager() {}

	public static boolean registerDrawer(Class<? extends Object> drawerClass, Class<? extends TheaterPanel> theaterPanel) {
		renderPanelClassCache.clear();
		if (drawerManager.put(drawerClass, theaterPanel) == null) {
			fireDrawerLoaded(theaterPanel);
			return true;
		}
		return false;
	}

	public static void replaceDrawer(Class<? extends Object> drawableElement, Class<? extends TheaterPanel> theaterPanel) {
		drawerManager.put(drawableElement, theaterPanel);
		renderPanelClassCache.clear();
	}

	public static void purgeDrawers(Module module) {
		for (Iterator<Class<? extends TheaterPanel>> iterator = drawerManager.values().iterator(); iterator.hasNext();) {
			Class<? extends TheaterPanel> drawerClass = iterator.next();
			if (drawerClass.getModule().equals(module)) {
				iterator.remove();
				fireDrawerUnloaded(drawerClass);
			}
		}
		renderPanelClassCache.clear();
	}

	public static void addLoadDrawerListener(LoadDrawerListener listener) {
		DRAWERLISTENERS.add(LoadDrawerListener.class, listener);
	}

	public static void removeLoadDrawerListener(LoadDrawerListener listener) {
		DRAWERLISTENERS.remove(LoadDrawerListener.class, listener);
	}

	private static void fireDrawerLoaded(Class<? extends TheaterPanel> drawerClass) {
		for (LoadDrawerListener listener : DRAWERLISTENERS.getListeners(LoadDrawerListener.class))
			listener.loaded(drawerClass);
	}

	private static void fireDrawerUnloaded(Class<? extends TheaterPanel> drawerClass) {
		for (LoadDrawerListener listener : DRAWERLISTENERS.getListeners(LoadDrawerListener.class))
			listener.unloaded(drawerClass);
	}

	public static boolean isCompatibleRenderPanel(TheaterPanel theaterPane, Class<? extends Object> drawableElement) {
		Class<? extends TheaterPanel> rpt = drawerManager.get(drawableElement);
		if (rpt == null) {
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(drawableElement, "");
			if (editor != null)
				rpt = PrimitiveDrawer.class;
			if (theaterPane.getClass().equals(rpt))
				return ((PrimitiveDrawer) theaterPane).getEditor().getClass().equals(editor.getClass());
			return false;
		}
		return theaterPane.getClass().equals(rpt);
	}

	public static Class<? extends TheaterPanel> getRenderPanelTypeBase(Class<?> drawableElement) {
		Class<? extends TheaterPanel> rpt = drawerManager.get(drawableElement);
		if (rpt == null) {
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(drawableElement, "");
			if (editor != null && editor.hasCustomEditor())
				return PrimitiveDrawer.class;
		}
		return rpt;
	}

	/** Get the more specialized drawable object class from the registered drawer map correponding to the given drawable object class. The output class must either be the input class or a parent
	 * class.
	 * @param drawableObjectClass the drawable object class
	 * @return the more specialized drawable class correponding to the drawableObjectClass */
	public static Class<?> getSpecializedDrawableObjectClass(Class<?> drawableObjectClass) {
		Class<?> drawableElement = null;
		for (Class<? extends Object> key : DrawerManager.drawerManager.keySet())
			if (key.isAssignableFrom(drawableObjectClass) && (drawableElement == null || drawableElement.isAssignableFrom(key)))
				drawableElement = key;
		return drawableElement == null ? drawableObjectClass : drawableElement;
	}

	/** Get the render panel class corresponding to a given drawable object class.
	 * @param drawableObjectClass the drawable object class
	 * @return the render panel class associated to the drawableObjectClass */
	public static Class<? extends TheaterPanel> getRenderPanelClass(Class<? extends Object> drawableObjectClass) {
		Class<? extends TheaterPanel> renderPanelClass = renderPanelClassCache.get(drawableObjectClass);
		if (renderPanelClass == null) {
			renderPanelClass = getRenderPanelTypeBase(getSpecializedDrawableObjectClass(drawableObjectClass));
			renderPanelClassCache.put(drawableObjectClass, renderPanelClass);
		}
		return renderPanelClass;
	}

	public static ArrayList<Class<? extends TheaterPanel>> getDrawers() {
		return new ArrayList<>(drawerManager.values());
	}
}
