/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.drawer;

import io.beanmanager.editors.PropertyEditor;
import io.beanmanager.editors.PropertyEditorManager;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.gui.core.display.ScenariumContainer;

import javafx.collections.ObservableList;
import javafx.geometry.Dimension2D;
import javafx.scene.Node;
import javafx.scene.layout.Region;

public class PrimitiveDrawer extends TheaterPanel {
	private PropertyEditor<?> editor;
	private Object oldDataElement;
	private Dimension2D dim;

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object dataElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, dataElement, autoFitIfResize);
		if (this.editor == null)
			this.editor = PropertyEditorManager.findEditorWithValue(dataElement, null);
		Region editorComponenet = this.editor.getNoSelectionEditor();
		ObservableList<Node> children = getChildren();
		children.clear();
		children.add(editorComponenet);
		editorComponenet.prefWidthProperty().bind(widthProperty());
		editorComponenet.prefHeightProperty().bind(heightProperty());
	}

	@Override
	public void fitDocument() {}

	@Override
	public Dimension2D getDimension() {
		if (this.editor == null)
			this.editor = PropertyEditorManager.findEditor(getDrawableElement().getClass(), "");
		Region ce = this.editor.getNoSelectionEditor();
		this.dim = new Dimension2D(ce.getPrefWidth(), ce.getPrefHeight());
		return this.dim;
	}

	public PropertyEditor<?> getEditor() {
		return this.editor;
	}

	@Override
	public String[] getStatusBarInfo() {
		return null;
	}

	@Override
	protected void paint(Object dataElement) {
		if (dataElement != this.oldDataElement) {
			if (dataElement != this.editor.getValue()) {
				this.editor.setValueFromObj(dataElement);
				this.editor.updateGUI();
			}
			this.oldDataElement = dataElement;
		}
	}

	@Override
	protected boolean updateFilterWithPath(String[] filterPath, boolean value) {
		return false;
	}
}
