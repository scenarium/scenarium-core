/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.drawer.camera3D;

import static com.jogamp.opengl.GL.GL_LINES;

import java.awt.Point;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Quat4d;
import javax.vecmath.Vector3d;

import io.beanmanager.editors.PropertyInfo;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;

public class ArcBallCamera implements Camera {
	private static final double EPSILON = 1.0e-5f;

	public static void matToDouble(Matrix4d mat, double[] dest) {
		dest[0] = mat.m00;
		dest[1] = mat.m10;
		dest[2] = mat.m20;
		dest[3] = mat.m30;
		dest[4] = mat.m01;
		dest[5] = mat.m11;
		dest[6] = mat.m21;
		dest[7] = mat.m31;
		dest[8] = mat.m02;
		dest[9] = mat.m12;
		dest[10] = mat.m22;
		dest[11] = mat.m32;
		dest[12] = mat.m03;
		dest[13] = mat.m13;
		dest[14] = mat.m23;
		dest[15] = mat.m33;
	}

	Vector3d stVec; // Saved click vector
	Vector3d enVec; // Saved drag vector
	double adjustWidth; // Mouse bounds width
	double adjustHeight; // Mouse bounds height
	private final Matrix4d lastRot = new Matrix4d();
	private boolean dragMode;
	@PropertyInfo(nullable = false)
	private Matrix4d rot = new Matrix4d();
	public double deltaX;
	public double deltaY;
	public double distance = -50;
	public double centerRotX = 0;
	public double centerRotY = 0;
	private Point2D anchorPoint;
	private CameraChangeListener listener;

	private GL2 gl;

	public ArcBallCamera() {
		this.stVec = new Vector3d();
		this.enVec = new Vector3d();
		this.lastRot.setIdentity();
		this.rot.setIdentity();
	}

	public void drag(Point newPt, Quat4d newRot) {
		mapToSphere(newPt, this.enVec);
		if (newRot != null) {
			Vector3d perp = new Vector3d();
			perp.cross(this.stVec, this.enVec);
			if (perp.length() > EPSILON) {
				newRot.x = perp.x;
				newRot.y = perp.y;
				newRot.z = perp.z;
				newRot.w = this.stVec.dot(this.enVec);
			} else
				newRot.x = newRot.y = newRot.z = newRot.w = 0.0f;
		}
	}

	public void drawAxis(GL2 gl) {
		gl.glLineWidth(3);
		gl.glBegin(GL_LINES);
		// x axis
		gl.glColor3f(1.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(1.0f, 0.0f, 0.0f);
		// y axis
		gl.glColor3f(0.0f, 1.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 1.0f, 0.0f);
		// Z axis
		gl.glColor3f(0.0f, 0.0f, 1.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 1.0f);
		gl.glEnd();
	}

	@Override
	public void drawCameraInfo(GL2 gl) {
		gl.glTranslated(this.centerRotX, this.centerRotY, 0);
		drawAxis(gl);
		gl.glTranslated(-this.centerRotX, -this.centerRotY, 0);
	}

	public double getCenterRotX() {
		return this.centerRotX;
	}

	public double getCenterRotY() {
		return this.centerRotY;
	}

	public double getDeltaX() {
		return this.deltaX;
	}

	public double getDeltaY() {
		return this.deltaY;
	}

	@Override
	public double getDistance() {
		return this.distance;
	}

	@Override
	public void getMatrix(double[] matrix) {
		Matrix4d trans = new Matrix4d(this.rot);
		trans.m03 = this.deltaX + this.centerRotX;
		trans.m13 = this.deltaY + this.centerRotY;
		trans.m23 = this.distance;
		Matrix4d t = new Matrix4d();
		t.setIdentity();
		t.m03 = -this.centerRotX;
		t.m13 = -this.centerRotY;
		trans.mul(t);
		matToDouble(trans, matrix);
	}

	public Matrix4d getRot() {
		return this.rot;
	}

	@Override
	public Vector3d getViewCenter() {
		return new Vector3d(this.centerRotX, this.centerRotY, 0);
	}

	@Override
	public void init(CameraChangeListener listener) {
		this.listener = listener;
	}

	public void mapToSphere(Point point, Vector3d vector) {
		Point2d tempPoint = new Point2d(point.x, point.y);
		tempPoint.x = tempPoint.x * this.adjustWidth - 1;
		tempPoint.y = 1 - tempPoint.y * this.adjustHeight;
		double length = tempPoint.x * tempPoint.x + tempPoint.y * tempPoint.y;
		if (length > 1.0f) {
			double norm = 1.0 / Math.sqrt(length);
			vector.x = tempPoint.x * norm;
			vector.y = tempPoint.y * norm;
			vector.z = 0;
		} else {
			vector.x = tempPoint.x;
			vector.y = tempPoint.y;
			vector.z = Math.sqrt(1.0f - length);
		}

	}

	@Override
	public void reset() {
		this.lastRot.setIdentity();
		this.rot.setIdentity();
		this.deltaX = 0;
		this.deltaY = 0;
		this.distance = -50;
		this.centerRotX = 0;
		this.centerRotY = 0;
		this.listener.cameraChange();
	}

	@Override
	public void setBounds(float newWidth, float newHeight) {
		this.adjustWidth = 1.0f / ((newWidth - 1.0f) * 0.5f);
		this.adjustHeight = 1.0f / ((newHeight - 1.0f) * 0.5f);
	}

	public void setCenterRotX(double centerRotX) {
		this.centerRotX = centerRotX;
	}

	public void setCenterRotY(double centerRotY) {
		this.centerRotY = centerRotY;
	}

	public void setDeltaX(double deltaX) {
		this.deltaX = deltaX;
	}

	public void setDeltaY(double deltaY) {
		this.deltaY = deltaY;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	@Override
	public void setGl(GL2 gl) {
		this.gl = gl;
	}

	@Override
	public void setNode(Node node) {
		node.setOnMouseDragged(e -> {
			if (e.getButton() == MouseButton.PRIMARY) {
				if (!this.dragMode) {
					this.dragMode = true;
					this.lastRot.set(this.rot);
					mapToSphere(new Point((int) e.getX(), (int) e.getY()), this.stVec);
				}
				Quat4d quat = new Quat4d();
				drag(new Point((int) e.getX(), (int) e.getY()), quat);
				setRotation(this.rot, quat);
				this.rot.mul(this.rot, this.lastRot);
				this.listener.cameraChange();
			} else if (e.getButton() == MouseButton.SECONDARY) {
				this.deltaX = this.anchorPoint.getX() * 0.01f + e.getX() * 0.01f;
				this.deltaY = this.anchorPoint.getY() * 0.01f - e.getY() * 0.01f;
				this.listener.cameraChange();
			}
		});
		node.setOnMousePressed(e -> {
			this.anchorPoint = null;
			if (e.getButton() == MouseButton.SECONDARY)
				this.anchorPoint = new Point2D(-e.getX() + this.deltaX * 100, e.getY() + this.deltaY * 100);
			else if (e.getButton() == MouseButton.PRIMARY && e.isControlDown()) {
				if (this.gl == null) {
					return;
				}
				this.gl.glLoadIdentity();
				double[] matrix = new double[16];
				getMatrix(matrix);
				this.gl.glMultMatrixd(matrix, 0);

				GLU glu = new GLU();
				int[] viewport = new int[4];
				double[] mvmatrix = new double[16];
				double[] projmatrix = new double[16];
				int realy = 0;
				double[] wcoord = new double[4];
				this.gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
				this.gl.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
				this.gl.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
				realy = viewport[3] - (int) e.getY() - 1;
				// position de la camera
				glu.gluUnProject((viewport[2] - viewport[0]) / 2.0, (viewport[3] - viewport[1]) / 2.0, 0, mvmatrix, 0, projmatrix, 0, viewport, 0, wcoord, 0);
				// Log.info("cam pos: x:" + wcoord[0] + "y:" + wcoord[1] + "z:" + wcoord[2]);
				double ax = wcoord[0];
				double ay = wcoord[1];
				double az = wcoord[2];
				glu.gluUnProject(e.getX(), realy, 1, mvmatrix, 0, projmatrix, 0, viewport, 0, wcoord, 0);
				double bx = wcoord[0];
				double by = wcoord[1];
				double bz = wcoord[2];
				double t = -az / (bz - az); // on calcule t en fonction de la position de la camera(Az) et de (Bz)
				double mx = t * (bx - ax) + ax; // on calcule les positions de M avec t
				double my = t * (by - ay) + ay;
				if (Double.isNaN(mx) || Double.isNaN(my))
					return;
				this.centerRotX = mx;
				this.centerRotY = my;
				this.deltaX = -this.centerRotX;
				this.deltaY = -this.centerRotY;
				this.listener.cameraChange();
			}
		});
		node.setOnScroll(e -> {
			if (e.getDeltaY() != 0) {
				this.distance += this.distance * (e.getDeltaX() + e.getDeltaY() <= 0 ? 1.0 : -1.0)
						/ (e.isShiftDown() && e.isControlDown() ? 100 : e.isShiftDown() ? 1000 : e.isControlDown() ? 30 : 10);
				this.listener.cameraChange();
			}
		});
		node.setOnMouseReleased(e -> this.dragMode = false);
	}

	public void setRot(Matrix4d rot) {
		if (rot == null)
			throw new IllegalArgumentException("Cannot be set to null for a nullable");
		this.rot = rot;
	}

	private static void setRotation(Matrix4d mat, Quat4d q1) {
		double n, s;
		double xs, ys, zs;
		double wx, wy, wz;
		double xx, xy, xz;
		double yy, yz, zz;
		n = q1.x * q1.x + q1.y * q1.y + q1.z * q1.z + q1.w * q1.w;
		s = n > 0.0f ? 2.0f / n : 0.0f;
		xs = q1.x * s;
		ys = q1.y * s;
		zs = q1.z * s;
		wx = q1.w * xs;
		wy = q1.w * ys;
		wz = q1.w * zs;
		xx = q1.x * xs;
		xy = q1.x * ys;
		xz = q1.x * zs;
		yy = q1.y * ys;
		yz = q1.y * zs;
		zz = q1.z * zs;
		mat.m00 = 1.0f - (yy + zz);
		mat.m01 = xy - wz;
		mat.m02 = xz + wy;
		mat.m03 = 0f;
		mat.m10 = xy + wz;
		mat.m11 = 1.0f - (xx + zz);
		mat.m12 = yz - wx;
		mat.m13 = 0f;
		mat.m20 = xz - wy;
		mat.m21 = yz + wx;
		mat.m22 = 1.0f - (xx + yy);
		mat.m23 = 0f;
		mat.m30 = 0f;
		mat.m31 = 0f;
		mat.m32 = 0f;
		mat.m33 = 1f;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
