/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.drawer.camera3D;

import javax.vecmath.Vector3d;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;

public class FreeFlyCamera implements Camera {
	private static final Vector3d AXEVERTICAL = new Vector3d(0, 0, 1);

	private static Vector3d mul(Vector3d v1, double v2) {
		Vector3d result = new Vector3d();
		result.x = v1.x * v2;
		result.y = v1.y * v2;
		result.z = v1.z * v2;
		return result;
	}

	private CameraChangeListener listener;
	float phi = -20;
	float theta = 90;
	float sensibility = 0.5f;
	float speed = 1f;
	public Vector3d orientation = new Vector3d();
	public Vector3d lateralDisplacement = new Vector3d();
	public Vector3d position = new Vector3d(0, -6, 6);
	public Vector3d targetPoint = new Vector3d();

	private Point2D anchorPoint;

	@Override
	public void drawCameraInfo(GL2 gl) {
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glPushMatrix();
		gl.glLoadIdentity();
		new GLU().gluOrtho2D(-1, 1, -1, 1);
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glPushMatrix();
		gl.glLoadIdentity();
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex2d(0.02, -0);
		gl.glVertex2d(-0.02, -0);
		gl.glEnd();
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex2d(0, 0.02);
		gl.glVertex2d(0, -0.02);
		gl.glEnd();
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glPopMatrix();
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glPopMatrix();
	}

	@Override
	public double getDistance() {
		return this.position.z;
	}

	public Vector3d getLateralDisplacement() {
		return this.lateralDisplacement;
	}

	@Override
	public void getMatrix(double[] matrix) {
		Vector3d f = new Vector3d(this.targetPoint);
		f.sub(this.position);
		f.normalize();
		Vector3d s = new Vector3d();
		s.cross(f, AXEVERTICAL);
		s.normalize();
		Vector3d u = new Vector3d(AXEVERTICAL);
		u.cross(s, f);
		u.normalize();
		matrix[0] = s.x;
		matrix[1] = u.x;
		matrix[2] = -f.x;
		matrix[3] = 0.0f;
		matrix[4] = s.y;
		matrix[5] = u.y;
		matrix[6] = -f.y;
		matrix[7] = 0.0f;
		matrix[8] = s.z;
		matrix[9] = u.z;
		matrix[10] = -f.z;
		matrix[11] = 0.0f;
		matrix[12] = -s.dot(this.position);
		matrix[13] = -u.dot(this.position);
		matrix[14] = f.dot(this.position);
		matrix[15] = 1.0f;
	}

	public Vector3d getOrientation() {
		return this.orientation;
	}

	public Vector3d getPosition() {
		return this.position;
	}

	public float getSensibility() {
		return this.sensibility;
	}

	public float getSpeed() {
		return this.speed;
	}

	public Vector3d getTargetPoint() {
		return this.targetPoint;
	}

	@Override
	public Vector3d getViewCenter() {
		return this.targetPoint;
	}

	@Override
	public void init(CameraChangeListener listener) {
		this.listener = listener;
		orienter(0, 0);
	}

	private void orienter(float xRel, float yRel) {
		this.phi += -yRel * this.sensibility;
		this.theta += -xRel * this.sensibility;
		if (this.phi > 89.0)
			this.phi = 89.0f;
		else if (this.phi < -89.0)
			this.phi = -89.0f;
		float phiRadian = (float) (this.phi * Math.PI / 180);
		float thetaRadian = (float) (this.theta * Math.PI / 180);
		if (AXEVERTICAL.x == 1.0) {
			this.orientation.x = (float) Math.sin(phiRadian);
			this.orientation.y = (float) Math.cos(phiRadian) * (float) Math.cos(thetaRadian);
			this.orientation.z = (float) Math.cos(phiRadian) * (float) Math.sin(thetaRadian);
		} else if (AXEVERTICAL.y == 1.0) {
			this.orientation.x = (float) Math.cos(phiRadian) * (float) Math.sin(thetaRadian);
			this.orientation.y = (float) Math.sin(phiRadian);
			this.orientation.z = (float) Math.cos(phiRadian) * (float) Math.cos(thetaRadian);
		} else {
			this.orientation.x = (float) Math.cos(phiRadian) * (float) Math.cos(thetaRadian);
			this.orientation.y = (float) Math.cos(phiRadian) * (float) Math.sin(thetaRadian);
			this.orientation.z = (float) Math.sin(phiRadian);
		}
		this.lateralDisplacement.cross(AXEVERTICAL, this.orientation);
		this.lateralDisplacement.normalize();
		this.targetPoint = new Vector3d(this.position);
		this.targetPoint.add(this.orientation);
	}

	@Override
	public void reset() {
		this.orientation = new Vector3d();
		this.lateralDisplacement = new Vector3d();
		this.position = new Vector3d(0, -6, 6);
		this.targetPoint = new Vector3d();
		orienter(0, 0);
		this.listener.cameraChange();
	}

	@Override
	public void setBounds(float width, float height) {}

	@Override
	public void setGl(GL2 gl) {}

	public void setLateralDisplacement(Vector3d lateralDisplacement) {
		this.lateralDisplacement = lateralDisplacement;
	}

	@Override
	public void setNode(Node node) {
		node.setOnKeyPressed(e -> {
			float vitesse = this.speed;
			if (e.isShiftDown() && e.isControlDown())
				vitesse /= 100;
			else if (e.isShiftDown())
				vitesse /= 1000;
			else if (e.isControlDown())
				vitesse /= 10;

			switch (e.getCode()) {
			case UP:
			case Z:
				this.position.add(mul(this.orientation, vitesse));
				this.targetPoint.set(this.position);
				this.targetPoint.add(this.orientation);
				break;
			case DOWN:
			case S:
				this.position.sub(mul(this.orientation, vitesse));
				this.targetPoint.set(this.position);
				this.targetPoint.add(this.orientation);
				break;
			case LEFT:
			case Q:
				this.position.add(mul(this.lateralDisplacement, vitesse));
				this.targetPoint.set(this.position);
				this.targetPoint.add(this.orientation);
				break;
			case RIGHT:
			case D:
				this.position.sub(mul(this.lateralDisplacement, vitesse));
				this.targetPoint.set(this.position);
				this.targetPoint.add(this.orientation);
				break;
			case NUMPAD3:
				this.position.z -= vitesse;
				this.targetPoint.z -= vitesse;
				break;
			case NUMPAD9:
				this.position.z += vitesse;
				this.targetPoint.z += vitesse;
				break;
			case NUMPAD2:
				this.position.y -= vitesse;
				this.targetPoint.y -= vitesse;
				break;
			case NUMPAD8:
				this.position.y += vitesse;
				this.targetPoint.y += vitesse;
				break;
			default:
				return;
			}
			this.listener.cameraChange();
		});

		node.setOnMouseDragged(e -> {
			if (this.anchorPoint != null) {
				float dx = (float) (e.getX() - this.anchorPoint.getX());
				float dy = (float) (e.getY() - this.anchorPoint.getY());
				this.anchorPoint = new Point2D(e.getX(), e.getY());
				orienter(dx, dy);
				this.listener.cameraChange();
			}
		});
		node.setOnMousePressed(e -> {
			this.anchorPoint = null;
			if (e.getButton() == MouseButton.SECONDARY)
				this.anchorPoint = new Point2D(e.getX(), e.getY());
		});
		node.setOnScroll(e -> {
			if (e.getDeltaY() != 0) {
				float vitesse = this.speed;
				if (e.isShiftDown() && e.isControlDown())
					vitesse /= 100;
				else if (e.isShiftDown())
					vitesse /= 1000;
				else if (e.isControlDown())
					vitesse /= 10;
				if (e.getDeltaY() < 0) {
					this.position.sub(mul(this.orientation, vitesse));
					this.targetPoint.set(this.position);
					this.targetPoint.add(this.orientation);
				} else {
					this.position.add(mul(this.orientation, vitesse));
					this.targetPoint.set(this.position);
					this.targetPoint.add(this.orientation);
				}
				this.listener.cameraChange();
			}
		});
	}

	public void setOrientation(Vector3d orientation) {
		this.orientation = orientation;
	}

	public void setPosition(Vector3d position) {
		this.position = position;
	}

	public void setSensibility(float sensibility) {
		this.sensibility = sensibility;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public void setTargetPoint(Vector3d targetPoint) {
		this.targetPoint = targetPoint;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
