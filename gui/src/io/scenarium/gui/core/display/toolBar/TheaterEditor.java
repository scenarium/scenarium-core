/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.toolBar;

import java.util.ArrayList;

import io.beanmanager.ihmtest.FxTest;
import io.scenarium.gui.core.display.toolbarclass.Border;
import io.scenarium.gui.core.display.toolbarclass.InternalTool;
import io.scenarium.gui.core.display.toolbarclass.ToolBoxItem;
import io.scenarium.gui.core.internal.LoadPackageStream;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

public class TheaterEditor extends InternalTool {

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> {
			TheaterEditor tf = new TheaterEditor();
			return new StackPane(tf.getRegion());
		});
	}

	@Override
	public Border getBorder() {
		return Border.CENTER;
	}

	@Override
	public Region getRegion() {
		ArrayList<ToolBoxItem> toolBoxs = this.renderPane.getTheaterPane().getToolBoxItems();
		ToolBar tb = new ToolBar();
		tb.setOrientation(Orientation.VERTICAL);
		tb.setPadding(new Insets(1, 3, 1, 3));
		if (this.renderPane != null)
			tb.maxHeightProperty().bind(this.renderPane.getCenterPane().heightProperty());
		ToggleGroup group = new ToggleGroup();
		ObservableList<Node> items = tb.getItems();
		for (ToolBoxItem toolBoxItem : toolBoxs) {
			ToggleButton button = new ToggleButton("", new ImageView(new Image(LoadPackageStream.getStream("/" + toolBoxItem.imgName))));
			button.setPadding(new Insets(3));
			button.setToggleGroup(group);
			button.setUserData(toolBoxItem.id);
			button.setTooltip(new Tooltip(toolBoxItem.tooltip));
			items.add(button);
		}
		group.selectedToggleProperty().addListener(e -> this.renderPane.getTheaterPane().setTheaterEditorSelection((int) group.getSelectedToggle().getUserData()));
		tb.setStyle("-fx-background-color: rgba(255,255,255,.8);");

		return tb;
	}
}
