/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.toolBar;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;

import io.beanmanager.editors.primitive.number.ControlType;
import io.beanmanager.editors.primitive.number.IncrementMode;
import io.beanmanager.editors.primitive.number.IntegerEditor;
import io.beanmanager.editors.time.DateEditor;
import io.beanmanager.tools.FxUtils;
import io.scenarium.core.filemanager.DataLoader;
import io.scenarium.core.filemanager.OpenListener;
import io.scenarium.core.filemanager.scenariomanager.IdChangeListener;
import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.filemanager.scenariomanager.MetaScenario;
import io.scenarium.core.filemanager.scenariomanager.RecordingListener;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioColorProvider;
import io.scenarium.core.filemanager.scenariomanager.StartStopChangeListener;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.core.timescheduler.SchedulerPropertyChangeListener;
import io.scenarium.core.timescheduler.SchedulerState;
import io.scenarium.core.timescheduler.VisuableSchedulable;
import io.scenarium.gui.core.convertion.ColorFxUtils;
import io.scenarium.gui.core.display.ToolVisibleListener;
import io.scenarium.gui.core.display.UserAgentStylesheetChange;
import io.scenarium.gui.core.display.UserAgentStylesheetManager;
import io.scenarium.gui.core.display.toolbarclass.ExternalTool;
import io.scenarium.gui.core.display.toolbarclass.Tool;
import io.scenarium.gui.core.internal.LoadPackageStream;
import io.scenarium.gui.core.struct.ScenariumGuiProperties;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Text;

public class Player extends ExternalTool implements VisuableSchedulable, SchedulerPropertyChangeListener, ToolVisibleListener, EventHandler<ActionEvent>, OpenListener, StartStopChangeListener,
		RecordingListener, UserAgentStylesheetChange {
	private static final int PLAY = 0;
	private static final int STOP = 1;
	private static final int REVERSE = 2;
	private static final int PREVIOUS = 3;
	private static final int SLOWER = 4;
	private static final int FASTER = 5;
	private static final int NEXT = 6;
	private static final int STEPBACKWARD = 7;
	private static final int REFRESH = 8;
	private static final int STEPFORWARD = 9;
	private static final int BOUCLEAB = 10;
	private static final int PLAYLIST = 11;
	private static final int LOOPOREPEATMODE = 12;
	private static final int RECORDOFF = 13;
	private static final int[] SPEED_PALLIER = new int[] { 1, 2, 5, 20, 100, 400, 1000, 5000, 10000, 100000 };
	private static final String TEXTSTYLE = "-fx-background-color: transparent;-fx-text-fill: -fx-text-background-color;";
	private static final String[] BUTTONNAMES = new String[] { "Play", "Stop", "Reverse", "Previous", "Slower", "Faster", "Next", "Step_backward", "Refresh", "Step_forward", "Boucle_A_B", "PlayList",
			"Loop_or_Repeat_mode", "RecordOff" };
	private ButtonBase[] buttons;
	private Label endTimelabel;
	private String timePattern;
	private Slider timeSlider;
	private DateEditor timeEditor;
	private IntegerEditor speedEditor;
	private boolean dead = false;
	private TextField scenarioNameLabel;
	private HBox timeBox;
	private double halfImageAWidth;
	private ImageView imageViewA;
	private ImageView imageViewB;
	private ArrayList<Interval> intervals = new ArrayList<>();
	private boolean isUpdating;
	private boolean isUpdatingAB;
	private boolean isSchedulerBusy;
	private boolean canReverse;
	private boolean canModulateSpeed;
	private boolean canRecord;
	private int speed;
	private long beginTime;
	private long endTime;
	private long nbGap;
	private long timePointer;
	private Long timeA;
	private Long timeB;
	private boolean playing;
	private VBox intervalBox;

	@Override
	public void dispose() {
		UserAgentStylesheetManager.removeUserAgentStylesheetChangeListener(this);
		super.dispose();
		if (!this.dead) {
			Scheduler scheduler = getScheduler();
			// new Thread(() -> scheduler.removeVisualScheduleElement(this)).start(); //Pk le thread
			if (scheduler != null) {
				scheduler.removeVisualScheduleElement(this);
				scheduler.removePropertyChangeListener(this);
			}
			this.renderPane.removeToolVisibleListener(this);
			DataLoader dataLoader = this.renderPane.getDataLoader();
			dataLoader.removeOpenListener(this);
			Scenario scenario = dataLoader.getScenario();
			if (scenario instanceof LocalScenario)
				((LocalScenario) scenario).removeStartStopChangeListener(this);
			if (scenario.canRecord())
				scenario.removeRecordingListener(this);
		}
		this.dead = true;
	}

	@Override
	protected String getIconName() {
		return "scenarium_icon_player.png";
	}

	@Override
	public VBox getRegion() {
		DataLoader dataLoader = this.renderPane.getDataLoader();
		Scheduler scheduler = this.renderPane.getContainer().getScheduler();
		Scenario scenario = dataLoader.getScenario();

		this.speed = scheduler.getSpeed();
		this.playing = scheduler.isRunning();
		this.timePointer = scheduler.getTimePointer();
		this.timeA = scheduler.getTimeA();
		this.timeB = scheduler.getTimeB();

		HBox buttonBox = new HBox(2);
		buttonBox.setPadding(new Insets(2, 0, 5, 0));
		buttonBox.setAlignment(Pos.CENTER_LEFT);
		int[] space = new int[] { 3, 7, 10, 13 };
		this.buttons = new ButtonBase[BUTTONNAMES.length];
		int indexSpace = 0;
		for (int i = 0; i < BUTTONNAMES.length; i++) {
			String name = BUTTONNAMES[i];
			ImageView iv = new ImageView();
			ButtonBase button = i == 11 || i == 12 ? new ToggleButton(null, iv) : new Button(null, iv);
			int size = i == 0 ? 34 : 26;
			button.setMaxSize(size, size);
			button.setMinSize(size, size);
			if (indexSpace < space.length && i == space[indexSpace]) {
				HBox.setMargin(button, new Insets(0, 0, 0, 10));
				indexSpace++;
			}
			button.setTooltip(new Tooltip(name.replace("_or_", "/").replace("_", " ")));
			button.setOnAction(this);
			buttonBox.getChildren().add(button);
			this.buttons[i] = button;
			if (i == PLAYLIST)
				((ToggleButton) button).setSelected(this.renderPane.isDisplayed(PlayList.class));
			else if (i == LOOPOREPEATMODE)
				((ToggleButton) button).setSelected(scheduler.isRepeat());
		}
		updateButtonsImage();
		String controlStyle = "-fx-border-width: 1px;-fx-border-color: #969696 #DCDCDC #DCDCDC #969696;";
		controlStyle += TEXTSTYLE;
		HBox boxBottom = new HBox(1);
		boxBottom.setPadding(new Insets(5, 0, 0, 0));
		this.scenarioNameLabel = new TextField();
		this.scenarioNameLabel.setStyle(controlStyle);
		this.scenarioNameLabel.setMinHeight(0);
		this.scenarioNameLabel.setPadding(new Insets(0));
		this.scenarioNameLabel.setMaxWidth(Double.MAX_VALUE);
		this.scenarioNameLabel.setEditable(false);
		this.scenarioNameLabel.setPrefWidth(1);
		int maxSpeed = scheduler.getMaxSpeed();
		HBox speedBox = new HBox();
		this.speedEditor = new IntegerEditor(-maxSpeed, maxSpeed, ControlType.TEXTFIELD);
		// speedEditor.setValue(scheduler.getSpeed());
		this.speedEditor.setIncrementMode(IncrementMode.GEOMETRICPROGRESSION);
		this.speedEditor.setIncrement(1.1);
		this.speedEditor.setStyle("-fx-background-color: transparent;-fx-text-fill: -fx-text-background-color;-fx-text-base: -fx-text-background-color");
		this.speedEditor.addPropertyChangeListener(() -> getScheduler().setSpeed(this.speedEditor.getValue()));
		TextField speedControl = (TextField) ((HBox) this.speedEditor.getNoSelectionEditor()).getChildren().get(0);
		speedControl.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
			if (e.getCode() == KeyCode.UP) {
				faster();
				e.consume();
			} else if (e.getCode() == KeyCode.DOWN) {
				slower();
				e.consume();
			}
		});
		speedControl.setMinHeight(0);
		speedControl.setPadding(new Insets(0));
		speedControl.setAlignment(Pos.CENTER_RIGHT);
		speedControl.setPrefWidth(new Text(Integer.toString(-maxSpeed)).getLayoutBounds().getWidth() + 2);
		Label percentLabel = new Label("%");
		percentLabel.setMaxHeight(Double.MAX_VALUE);
		speedBox.getChildren().addAll(speedControl, percentLabel);
		speedBox.setStyle(controlStyle);
		this.endTimelabel = new Label();
		this.timeSlider = new Slider();
		this.timeSlider.setPadding(new Insets(0, 0, 4, 0));
		this.timeSlider.setOnScroll(e -> {
			if (e.getDeltaY() != 0)
				getScheduler().jump(e.getDeltaY() > 0);
		});
		this.timeSlider.setBlockIncrement(0);
		this.timeSlider.setOnKeyReleased(e -> e.consume());
		this.timeSlider.setOnKeyTyped(e -> e.consume());
		this.timeSlider.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.RIGHT) {
				Scheduler currentScheduler = getScheduler();
				long newVal = currentScheduler.getTimePointer() + 1;
				if (newVal < currentScheduler.getEffectiveStop())
					currentScheduler.setTimePointer(newVal);
				e.consume();
			} else if (e.getCode() == KeyCode.LEFT) {
				Scheduler currentScheduler = getScheduler();
				long newVal = currentScheduler.getTimePointer() - 1;
				if (newVal > currentScheduler.getEffectiveStart())
					currentScheduler.setTimePointer(newVal);
				e.consume();
			}
		});
		this.timeSlider.setMaxWidth(Double.MAX_VALUE);
		this.timeSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
			if (!this.isUpdating) {
				long lNewVal = (long) (newVal.doubleValue() + 0.5);
				long lOldVal = (long) (oldVal.doubleValue() + 0.5);
				if (lNewVal != lOldVal) {
					Scheduler currentScheduler = getScheduler();
					currentScheduler.setTimePointer(lNewVal);
					if (lNewVal != currentScheduler.getTimePointer())
						paint();
				}
			}
		});

		this.timeBox = new HBox();
		this.timeBox.getChildren().addAll(new Label("/"), this.endTimelabel);
		this.timeBox.setStyle(controlStyle);
		HBox.setHgrow(this.scenarioNameLabel, Priority.ALWAYS);
		boxBottom.getChildren().addAll(this.scenarioNameLabel, speedBox, this.timeBox);
		Image image1 = new Image(LoadPackageStream.getStream("/A.png"));
		this.halfImageAWidth = image1.getWidth() / 2.0;
		this.imageViewA = new ImageView(image1);
		this.imageViewA.setOnMouseDragged(e -> {
			if (e.getButton() == MouseButton.PRIMARY && !this.isUpdatingAB)
				scheduler.setTimeA(getTimeForMarker(e.getSceneX()));
		});
		Image image2 = new Image(LoadPackageStream.getStream("/B.png"));
		this.imageViewB = new ImageView(image2);
		this.imageViewB.setOnMouseDragged(e -> {
			if (e.getButton() == MouseButton.PRIMARY && !this.isUpdatingAB)
				scheduler.setTimeB(getTimeForMarker(e.getSceneX()));
		});
		this.timeSlider.boundsInParentProperty().addListener((a, b, c) -> {
			updateABPosition();
			updateIntervalsPosition();
		});
		this.imageViewA.setManaged(false);
		this.imageViewB.setManaged(false);
		StackPane aBBox = new StackPane(this.imageViewA, this.imageViewB);
		aBBox.setPrefHeight(Math.max(image1.getHeight(), image2.getHeight()));
		this.intervalBox = new VBox(0);
		this.intervalBox.setPadding(new Insets(0, 0, 2, 0));
		VBox vbox = new VBox(this.timeSlider, this.intervalBox, aBBox, buttonBox, boxBottom);
		vbox.setPadding(new Insets(5, 5, 5, 5));
		vbox.setFillWidth(true);
		scheduler.addVisualScheduleElement(this);
		scheduler.addPropertyChangeListener(this);
		this.renderPane.addToolVisibleListener(this);
		dataLoader.addOpenListener(this);
		if (scenario instanceof LocalScenario)
			((LocalScenario) scenario).addStartStopChangeListener(this);
		opened(null, scenario);
		startStopChanged();
		updateBoucle();
		updateSpeed();
		updatePlayPause();
		updateHMIIntervalTime();
		UserAgentStylesheetManager.addUserAgentStylesheetChangeListener(this);
		vbox.setOnKeyPressed(e -> {
			if(e.getCode() == KeyCode.SPACE)
				playAction(e.isControlDown());
		});
		vbox.setOnMousePressed(e -> vbox.requestFocus());
		return vbox;
	}

	private void updateButtonsImage() {
		for (int i = 0; i < BUTTONNAMES.length; i++)
			((ImageView) this.buttons[i].getGraphic()).setImage(getImage(BUTTONNAMES[i]));
		recordingPropertyChanged(this.renderPane.getDataLoader().getScenario().isRecording());
	}

	private Scheduler getScheduler() {
		return this.renderPane.getContainer().getScheduler();
	}

	private long getTimeForMarker(double sceneX) {
		Node thumb = this.timeSlider.lookup(".thumb");
		// Scheduler scheduler = getScheduler();
		double thumbWidth = thumb.getLayoutBounds().getWidth();
		long time = (long) (this.beginTime + (sceneX - (this.timeSlider.getLayoutX() + thumbWidth / 2.0)) / (this.timeSlider.getWidth() - thumbWidth) * (this.endTime - this.beginTime));
		if (time < this.beginTime)
			time = this.beginTime;
		else if (time > this.endTime)
			time = this.endTime;
		return time;
	}

	@Override
	public void handle(ActionEvent e) {
		ButtonBase button = (ButtonBase) e.getSource();
		Scheduler scheduler = getScheduler();
		if (this.renderPane != null) {
			int id = -1;
			for (int i = 0; i < this.buttons.length; i++)
				if (this.buttons[i] == button) {
					id = i;
					break;
				}
			switch (id) {
			case PLAY:
				playAction(false);
				break;
			case STOP:
				scheduler.offerTask(() -> scheduler.stop());
				break;
			case REVERSE:
				scheduler.setSpeed(-this.speed); // Opération potentiellement bloquante...
				break;
			case PREVIOUS:
				new Thread(() -> this.renderPane.getDataLoader().goToNextScenario(true)).start();
				break;
			case SLOWER:
				slower();
				break;
			case FASTER:
				faster();
				break;
			case NEXT:
				new Thread(() -> this.renderPane.getDataLoader().goToNextScenario(false));
				break;
			case STEPBACKWARD:
				scheduler.previousStep();
				break;
			case REFRESH:
				getScheduler().refresh();
				break;
			case STEPFORWARD:
				scheduler.nextStep();
				break;
			case BOUCLEAB:
				if (scheduler.getTimeA() == null)
					if (scheduler.getTimeB() == null)
						scheduler.setTimeA(this.timePointer);
					else
						scheduler.setTimeB(null);
				else if (scheduler.getTimeB() == null)
					if(scheduler.getTimeA() == this.timePointer) {
						scheduler.setTimeA(null);
						scheduler.setTimeB(this.timePointer);
					} else
						scheduler.setTimeB(this.timePointer);
				else
					scheduler.setTimeA(null);
				break;
			case PLAYLIST:
				if (this.renderPane.isDisplayed(PlayList.class))
					this.renderPane.closeTool(PlayList.class);
				else
					this.renderPane.openTool(PlayList.class);
				break;
			case LOOPOREPEATMODE:
				scheduler.setRepeat(((ToggleButton) button).isSelected());
				break;
			case RECORDOFF:
				Scenario scenario = this.renderPane.getDataLoader().getScenario();
				scenario.setRecording(!scenario.isRecording());
				break;
			default:
				break;
			}
		}
	}
	
	private void playAction(boolean stop) {
		Scheduler scheduler = getScheduler();
		scheduler.offerTask(stop ? () -> scheduler.stop() : scheduler.isRunning() ? scheduler::pause : () -> scheduler.start());
	}

	private void slower() {
		for (int i = 0; i < SPEED_PALLIER.length - 1; i++)
			if (Math.abs(this.speed) > SPEED_PALLIER[SPEED_PALLIER.length - i - 2]) {
				int newSpeed = SPEED_PALLIER[SPEED_PALLIER.length - i - 2];
				getScheduler().setSpeed(this.speed < 0 ? -newSpeed : newSpeed);
				break;
			}
	}

	private void faster() {
		for (int i = 0; i < SPEED_PALLIER.length; i++)
			if (Math.abs(this.speed) < SPEED_PALLIER[i]) {
				int newSpeed = SPEED_PALLIER[i];
				getScheduler().setSpeed(this.speed < 0 ? -newSpeed : newSpeed);
				break;
			}
	}

	private void updateSlider() {
		this.timeSlider.setMin(this.beginTime);
		this.timeSlider.setMax(this.beginTime >= this.endTime ? this.beginTime + 1 : this.endTime); // Bug fx, si je mets max plus petit que min il mouline a 15% du proc...
		this.timeSlider.setValue(this.timePointer);
		this.timeSlider.setDisable(this.nbGap <= 0);
	}

	private void updateTimeLabel() {
		if (this.timeEditor != null)
			this.timeBox.getChildren().remove(this.timeEditor.getNoSelectionEditor());
		DateEditor timeEditor = new DateEditor(this.beginTime, this.endTime, this.timePattern);
		timeEditor.setStyle(TEXTSTYLE);
		Region editor = timeEditor.getNoSelectionEditor();
		MenuItem timeContexItem = new MenuItem();
		timeContexItem.setOnAction(e -> this.timeEditor.setIgnoreTimePattern(!this.timeEditor.isIgnoreTimePattern()));
		this.endTimelabel.setContextMenu(new ContextMenu(timeContexItem));
		Runnable updateEndLabel = () -> {
			this.endTimelabel.setText(this.endTime == -1 ? "-"
					: this.timePattern == null || this.timeEditor.isIgnoreTimePattern() ? Long.toString(this.endTime) : new SimpleDateFormat(this.timePattern).format(this.endTime));
			timeContexItem.setText(this.timeEditor.isIgnoreTimePattern() ? "As Time" : "As Integer");
		};

		InvalidationListener il = e -> {
			Node box = ((HBox) this.timeEditor.getNoSelectionEditor()).getChildrenUnmodifiable().get(0);
			TextField timeControl = box instanceof HBox ? (TextField) ((HBox) box).getChildren().get(0) : (TextField) box;
			timeControl.setMinHeight(0);
			timeControl.setPadding(new Insets(0));
			timeControl.setAlignment(Pos.CENTER_RIGHT);
			timeControl.setMinWidth(0);
			SimpleDateFormat formatter = this.timeEditor.getDateFormat();
			timeControl.setPrefWidth(new Text(formatter == null ? Long.toString(this.timeEditor.getMax()) : formatter.format(this.timeEditor.getMax())).getLayoutBounds().getWidth() + 2);
			updateEndLabel.run();
		};
		this.timeEditor = timeEditor;
		editor.getChildrenUnmodifiable().addListener(il);
		this.timeEditor.setValue(new Date(this.timePointer));
		this.timeEditor.addPropertyChangeListener(() -> {
			if (!this.isUpdating) {
				Scheduler scheduler = getScheduler();
				long timeFromEditor = this.timeEditor.getValue().getTime();
				if (scheduler.getTimePointer() != timeFromEditor) {
					scheduler.setTimePointer(timeFromEditor);
					paint();
				}
			}
		});
		il.invalidated(null);
		updateEndLabel.run();
		this.timeBox.getChildren().add(0, this.timeEditor.getNoSelectionEditor());
	}

	@Override
	public void opened(Scenario oldScenario, Scenario scenario) {
		if (oldScenario != null && oldScenario instanceof LocalScenario)
			((LocalScenario) oldScenario).removeStartStopChangeListener(this);
		if (oldScenario != null && oldScenario.canRecord())
			oldScenario.removeRecordingListener(this);
		if (scenario == null)
			return;
		this.timePattern = scenario.getTimePattern();
		this.canReverse = scenario.canReverse();
		this.canModulateSpeed = scenario.canModulateSpeed();
		this.canRecord = scenario.canRecord();
		String scenarioName = scenario.toString();
		FxUtils.runLaterIfNeeded(() -> {
			this.scenarioNameLabel.setText(scenarioName);
			updateTimeLabel();
		});
		if (scenario instanceof LocalScenario)
			((LocalScenario) scenario).addStartStopChangeListener(this);
		this.buttons[RECORDOFF].setDisable(!this.canRecord);
		if (scenario.canRecord())
			scenario.addRecordingListener(this);
	}

	@Override
	public void startStopChanged() {
		ArrayList<Interval> oldIntervals = this.intervals;
		this.intervals = new ArrayList<>();
		Scheduler scheduler = getScheduler();
		this.beginTime = scheduler.getBeginningTime();
		this.endTime = scheduler.getEndTime();
		this.nbGap = scheduler.getNbGap();
		Scenario scenario = this.renderPane.getDataLoader().getScenario();
		if (scenario instanceof MetaScenario)
			populateIntervals((MetaScenario) scenario, this.intervals);
		FxUtils.runLaterIfNeeded(() -> {
			oldIntervals.forEach(interval -> {
				interval.dispose();
				this.intervalBox.getChildren().remove(interval.getNode());
			});
			updateIntervalsPosition();
			if (oldIntervals.size() != this.intervals.size())
				Platform.runLater(() -> this.stage.sizeToScene());
		});
	}

	private void populateIntervals(MetaScenario scenario, ArrayList<Interval> intervals) {
		for (Scenario ss : scenario.getAdditionalScenario())
			if (ss instanceof MetaScenario)
				populateIntervals((MetaScenario) ss, intervals);
			else
				intervals.add(new Interval(ss));
	}

	@Override
	public void paint() {
		Scheduler scheduler = getScheduler();
		if (scheduler != null) {
			long timeFromEditor = this.timeEditor.getValue().getTime();
			this.timePointer = scheduler.getTimePointer();
			if (timeFromEditor != scheduler.getTimePointer() || this.timePointer != this.timeSlider.getValue())
				FxUtils.runLaterIfNeeded(() -> {
					this.isUpdating = true;
					if (timeFromEditor != scheduler.getTimePointer())
						this.timeEditor.setValue(new Date(this.timePointer));
					if (this.timePointer != this.timeSlider.getValue())
						this.timeSlider.setValue(this.timePointer);
					this.isUpdating = false;
				});
		}
	}

	@Override
	public void setAnimated(boolean animated) {}

	@Override
	public void stateChanged(final SchedulerState state) {
		Scheduler scheduler = getScheduler();
		if (scheduler == null)
			return;
		switch (state) {
		case STARTED:
		case STOPPED:
		case SUSPENDED:
		case UNSUSPENDED: {
			this.playing = state == SchedulerState.STARTED || state == SchedulerState.UNSUSPENDED;
			FxUtils.runLaterIfNeeded(() -> {
				this.isSchedulerBusy = false;
				updateEnableButtons();
				updatePlayPause();
			});
		}
			break;
		case PRESTART:
		case PRESTOP:
			FxUtils.runLaterIfNeeded(() -> {
				this.isSchedulerBusy = true;
				updateEnableButtons();
			});
			break;
		case SPEEDCHANGED:
			this.speed = scheduler.getSpeed();
			FxUtils.runLaterIfNeeded(() -> updateSpeed());
			break;
		case BOUCLE:
			this.timeA = scheduler.getTimeA();
			this.timeB = scheduler.getTimeB();
			FxUtils.runLaterIfNeeded(() -> updateBoucle());
			break;
		case INTERVALTIMECHANGED:
			this.timePattern = this.renderPane.getDataLoader().getScenario().getTimePattern();
			startStopChanged();
			FxUtils.runLaterIfNeeded(() -> updateHMIIntervalTime());
			break;
		case STOPPING:
			FxUtils.runLaterIfNeeded(() -> {
				this.isSchedulerBusy = true;
				updateEnableButtons();
				updatePlayPause();
			});
		default:
			break;
		}
	}

	@Override
	public void toolVisibleChanged(Class<? extends Tool> toolBarClass, boolean visible) {
		if (toolBarClass == PlayList.class)
			((ToggleButton) this.buttons[PLAYLIST]).setSelected(visible);
	}

	private void updateABPosition() {
		this.isUpdatingAB = true;
		Node thumb = this.timeSlider.lookup(".thumb");
		if (thumb == null)
			return;
		double thumbWidth = thumb.getLayoutBounds().getWidth();
		double trackLength = this.timeSlider.getWidth() - thumbWidth;
		double trackStart = this.timeSlider.getTranslateX() + thumbWidth / 2.0;
		long timeInterval = this.endTime - this.beginTime;
		if (this.timeA != null) {
			this.imageViewA.setTranslateX(trackStart - this.halfImageAWidth + trackLength * (this.timeA - this.beginTime) / timeInterval);
			this.imageViewA.setVisible(true);
		} else
			this.imageViewA.setVisible(false);
		if (this.timeB != null) {
			this.imageViewB.setTranslateX(trackStart - this.halfImageAWidth + trackLength * (this.timeB - this.beginTime) / timeInterval);
			this.imageViewB.setVisible(true);
		} else
			this.imageViewB.setVisible(false);
		this.isUpdatingAB = false;
	}

	private void updateIntervalsPosition() {
		if (this.timeSlider == null)
			return;
		Node thumb = this.timeSlider.lookup(".thumb");
		if (thumb == null)
			return;
		double thumbWidth = thumb.getLayoutBounds().getWidth();
		double trackLength = this.timeSlider.getWidth() - thumbWidth;
		double trackStart = this.timeSlider.getTranslateX() + thumbWidth / 2.0;
		for (Interval interval : this.intervals) {
			interval.updateLine(this.beginTime, this.endTime, trackStart, trackLength);
			Node intervalNode = interval.getNode();
			ObservableList<Node> intervalBoxChildren = this.intervalBox.getChildren();
			if (!intervalBoxChildren.contains(intervalNode))
				intervalBoxChildren.add(intervalNode);
		}
	}

	private void updateBoucle() {
		String boucleIconName = this.timeA != null && this.timeB != null ? "Point_A_B" : this.timeA != null ? "Point_A" : this.timeB != null ? "Point_B" : "Boucle_A_B";
		this.buttons[BOUCLEAB].setGraphic(new ImageView(getImage(boucleIconName)));
		this.buttons[BOUCLEAB].getTooltip().setText(boucleIconName);
		updateABPosition();
	}

	private void updateEnableButtons() {
		if (this.isSchedulerBusy)
			for (ButtonBase buttonBase : this.buttons)
				buttonBase.setDisable(true);
		else {
			this.buttons[PLAY].setDisable(false);
			this.buttons[STOP].setDisable(false);
			this.buttons[PREVIOUS].setDisable(false);
			this.buttons[NEXT].setDisable(false);
			this.buttons[PLAYLIST].setDisable(false);
			Scheduler scheduler = this.renderPane.getContainer().getScheduler();
			if (scheduler == null || scheduler.isOfKind(Scheduler.STREAM_SCHEDULER) || this.timeSlider.getMax() - this.timeSlider.getMin() <= 1) {
				this.buttons[REVERSE].setDisable(true);
				this.buttons[SLOWER].setDisable(true);
				this.buttons[FASTER].setDisable(true);
				this.buttons[STEPBACKWARD].setDisable(true);
				this.buttons[REFRESH].setDisable(true);
				this.buttons[STEPFORWARD].setDisable(true);
				this.buttons[LOOPOREPEATMODE].setDisable(true);
				this.buttons[BOUCLEAB].setDisable(true);
			} else {
				this.buttons[REVERSE].setDisable(!this.canReverse);
				this.buttons[SLOWER].setDisable(!this.canModulateSpeed);
				this.buttons[FASTER].setDisable(!this.canModulateSpeed);
				this.buttons[STEPBACKWARD].setDisable(!this.canReverse);
				this.buttons[REFRESH].setDisable(false);
				this.buttons[STEPFORWARD].setDisable(false);
				this.buttons[LOOPOREPEATMODE].setDisable(false);
				this.buttons[BOUCLEAB].setDisable(false);
			}
			this.buttons[RECORDOFF].setDisable(!canRecord);
		}
	}

	private void updateHMIIntervalTime() {
		this.isUpdating = true;
		updateSlider();
		updateTimeLabel();
		this.isUpdating = false;
		updateEnableButtons();
	}

	private void updatePlayPause() {
		String playIconName = this.playing ? "Pause" : "Play";
		((ImageView) this.buttons[PLAY].getGraphic()).setImage(getImage(playIconName));
		this.buttons[PLAY].getTooltip().setText(playIconName);
	}

	private Image getImage(String playIconName) {
		if (ScenariumGuiProperties.get().isDarkTheme())
			try {
				BufferedImage img = ImageIO.read(LoadPackageStream.getStream("/" + playIconName + ".png"));
				return SwingFXUtils.toFXImage(invertImage(img), null);
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		else
			return new Image(LoadPackageStream.getStream("/" + playIconName + ".png"));
	}

	private void updateSpeed() {
		this.speedEditor.setValue(this.speed);
		String speedIconName = this.speed > 0 ? "Reverse" : "Verse";
		((ImageView) this.buttons[REVERSE].getGraphic()).setImage(getImage(speedIconName));
		this.buttons[REVERSE].getTooltip().setText(speedIconName);
	}

	@Override
	public void recordingPropertyChanged(boolean recording) {
		String recordingIconName = "Record" + (recording ? "On" : "Off");
		((ImageView) this.buttons[RECORDOFF].getGraphic()).setImage(getImage(recordingIconName));
	}

	private BufferedImage invertImage(BufferedImage src) {
		for (int x = 0; x < src.getWidth(); x++)
			for (int k = 0; k < src.getHeight(); k++) {
				int rgba = src.getRGB(x, k);
				int ri = rgba >> 16 & 0xFF;
				int gi = rgba >> 8 & 0xFF;
				int bi = rgba >> 0 & 0xFF;
				float y = 0.299f * ri + 0.587f * gi + 0.114f * bi;
				float u = 0.492f * (bi - y);
				float v = 0.877f * (ri - y);
				y = 255 - y;
				float r = y + 1.13983f * v;
				if (r < 0)
					r = 0;
				else if (r > 255)
					r = 255;
				float g = y - 0.39465f * u - 0.58060f * v;
				if (g < 0)
					g = 0;
				else if (g > 255)
					g = 255;
				float b = y + 2.03211f * u;
				if (b < 0)
					b = 0;
				else if (b > 255)
					b = 255;
				src.setRGB(x, k, (rgba >> 24 & 0xff) << 24 | ((byte) r & 0xFF) << 16 | ((byte) g & 0xFF) << 8 | ((byte) b & 0xFF) << 0);
			}
		return src;
	}

	@Override
	public void userAgentStylesheetChange() {
		updateButtonsImage();
	}
}

class Interval implements IdChangeListener {
	private static final Color GREY = Color.GREY.brighter();
	private StackPane fullLine;
	private Line beginLine;
	private Line line;
	private Line endLine;
	private final Scenario scenario;

	private long beginTime;
	private long endTime;
	private double trackStart;
	private double trackLength;

	public Interval(Scenario scenario) {
		this.scenario = scenario;
		scenario.addIdChangeListener(this);
		idChanged(scenario.getId());
	}

	public void updateLine(long beginTime, long endTime, double trackStart, double trackLength) {
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.trackStart = trackStart;
		this.trackLength = trackLength;
		updateLine();
	}

	private void updateLine() {
		long beginning = this.scenario.getBeginningTime();
		long end = this.scenario.getEndTime();
		Date start = this.scenario.getStartTime();
		Date stop = this.scenario.getStopTime();

		if (this.line == null)
			createLine();
		long timeInterval = this.endTime - this.beginTime;
		if (timeInterval == 0) // avoid infinite width when scenario change due to division with zero. Happens when scheduler have no scenario and so beginTime and endTime == 0
			return;
		long begin = start != null ? start.getTime() : beginning;
		long end2 = stop != null ? stop.getTime() : this.scenario.getEndTime();
		this.line.setTranslateX(this.trackStart + this.trackLength * (beginning - this.beginTime) / timeInterval);
		this.beginLine.setTranslateX(this.line.getTranslateX());
		this.line.setEndX((end - beginning) / (double) timeInterval * this.trackLength);
		if (this.line.getStroke() == null || ((LinearGradient) this.line.getStroke()).getEndX() != this.line.getEndX())
			this.line.setStroke(getLinearGradient((begin - beginning) / (double) (end - beginning), (end2 - beginning) / (double) (end - beginning)));
		this.endLine.setTranslateX(this.line.getTranslateX() + this.line.getEndX());

	}

	private Paint getLinearGradient(double beginRatio, double endRatio) {
		Color color = ColorFxUtils.toFXColor(ScenarioColorProvider.getColor(this.scenario.getId()));
		return new LinearGradient(0, 0, this.line.getEndX(), 0, false, CycleMethod.REPEAT, new Stop(0, GREY), new Stop(beginRatio, GREY), new Stop(beginRatio, color), new Stop(endRatio, color),
				new Stop(endRatio, GREY), new Stop(1, GREY));
	}

	private void createLine() {
		Date start = this.scenario.getStartTime();
		Date stop = this.scenario.getStopTime();
		Color color = ColorFxUtils.toFXColor(ScenarioColorProvider.getColor(this.scenario.getId()));
		this.line = new Line();
		this.line.setStrokeWidth(2);
		this.line.setStrokeLineCap(StrokeLineCap.BUTT);
		this.line.setStroke(null);
		this.fullLine = new StackPane(this.line);
		this.beginLine = new Line();
		this.beginLine.setStrokeWidth(2);
		this.beginLine.setStrokeLineCap(StrokeLineCap.BUTT);
		this.beginLine.setStroke(start == null ? color : GREY);
		this.beginLine.setStartY(-2);
		this.beginLine.setEndY(2);
		this.fullLine.getChildren().add(this.beginLine);
		this.endLine = new Line();
		this.endLine.setStrokeWidth(2);
		this.endLine.setStrokeLineCap(StrokeLineCap.BUTT);
		this.endLine.setStroke(stop == null ? color : GREY);
		this.endLine.setStartY(-2);
		this.endLine.setEndY(2);
		this.fullLine.getChildren().add(this.endLine);
		this.fullLine.setAlignment(Pos.CENTER_LEFT);
		Tooltip.install(this.fullLine, new Tooltip(this.scenario.toString()));
	}

	public Node getNode() {
		return this.fullLine;
	}

	public void dispose() {
		this.scenario.removeIdChangeListener(this);
	}

	@Override
	public void idChanged(int id) {
		updateLine();
	}
}