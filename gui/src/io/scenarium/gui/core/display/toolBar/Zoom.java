/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.toolBar;

import javax.vecmath.Point2i;

import io.beanmanager.BeanManager;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.gui.core.display.PaintListener;
import io.scenarium.gui.core.display.RenderPane;
import io.scenarium.gui.core.display.ScenariumContainer;
import io.scenarium.gui.core.display.drawer.GeometricDrawer;
import io.scenarium.gui.core.display.drawer.TheaterPanel;
import io.scenarium.gui.core.display.toolbarclass.ExternalTool;
import io.scenarium.gui.core.display.toolbarclass.Tool;

import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

public class Zoom extends ExternalTool implements PaintListener, ScenariumContainer {
	private RenderPane renderPaneValues;

	@Override
	public void adaptSizeToDrawableElement() {
		this.renderPaneValues.adaptSizeToDrawableElement();
	}

	@Override
	public void dispose() {
		super.dispose();
		TheaterPanel theaterPanel = this.renderPane.getTheaterPane();
		if (theaterPanel instanceof GeometricDrawer)
			((GeometricDrawer) theaterPanel).setFilterROI(false);
		theaterPanel.removePaintListener(this);
		if (this.renderPaneValues != null) {
			this.renderPaneValues.getTheaterPane().removePaintListener(this);
			this.renderPaneValues.close();
		}
		theaterPanel.repaint(false);
	}

	@Override
	public Point2i getDefaultToolBarLocation(String simpleName) {
		return null;
	}

	@Override
	public Region getRegion() {
		if (this.renderPane.getDataLoader() == null) {
			this.renderPaneValues = new RenderPane((Scheduler) null, this.renderPane.getTheaterPane().getDrawableElement(), this, null, false, false);
			this.renderPane.getTheaterPane().setDrawableElement(this.renderPane.getTheaterPane().getDrawableElement());
		} else
			this.renderPaneValues = new RenderPane(this.renderPane.getDataLoader(), this, false, false);
		this.renderPaneValues.excludeToolBar(Zoom.class);
		// renderPane.getDataLoader() != null ? renderPane.getTheaterPane().getDrawableElement() : renderPane.getDataLoader();
		TheaterPanel tpv = this.renderPaneValues.getTheaterPane();
		TheaterPanel tp = this.renderPane.getTheaterPane();
		BeanManager tpvBm = new BeanManager(tpv, "");
		BeanManager tpBm = new BeanManager(tp, "");
		tpBm.copyTo(tpvBm);
		tpBm.addPropertyChangeListener(evt -> tpvBm.setValue(evt.getPropertyName(), evt.getNewValue()));
		if (tpv instanceof GeometricDrawer) {
			GeometricDrawer gtpv = (GeometricDrawer) tpv;
			GeometricDrawer gtp = (GeometricDrawer) tp;
			gtpv.setFilterROI(false);
			gtpv.addPaintListener(this);
			float scale = gtpv.getZoomScale();
			float[] roi = gtpv.getRoi();
			gtpv.setTransfom(roi[0] * scale, roi[1] * scale, scale);
			roi[2] = (float) (gtpv.getWidth() / scale);
			roi[3] = (float) (gtpv.getHeight() / scale);
			gtp.setFilterROI(true);
		} /* else if (tpv instanceof GeographicalDrawer) { GeographicalDrawer gtpv = (GeographicalDrawer) tpv; GeographicalDrawer gtp = (GeographicalDrawer) tp; gtpv.setFilterROI(false);
			 * gtpv.addPaintListener(this); gtpv.setZoom(Integer.MAX_VALUE); gtp.setFilterROI(true); } */
		StackPane sp = this.renderPaneValues.getPane();
		sp.setPrefSize(600, 600);
		tpv.setIgnoreRepaint(false);
		return sp;
	}

	@Override
	public Scheduler getScheduler() {
		return null;
	}

	@Override
	public int getSelectedElementFromTheaterEditor() {
		return -1;
	}

	@Override
	public boolean isDefaultToolBarAlwaysOnTop(String simpleName) {
		return false;
	}

	@Override
	public boolean isManagingAccelerator() {
		return false;
	}

	@Override
	public boolean isStatusBar() {
		return this.renderPaneValues.isStatusBar();
	}

	@Override
	public void paint(TheaterPanel source) {
		TheaterPanel theaterPanelValues = this.renderPaneValues.getTheaterPane();
		boolean forceToRedraw = false;
		if (this.renderPaneValues.getTheaterPane().getDrawableElement() != this.renderPane.getTheaterPane().getDrawableElement()) {
			this.renderPaneValues.getTheaterPane().setDrawableElement(this.renderPane.getTheaterPane().getDrawableElement());
			forceToRedraw = true;
		}
		float[] roi = source instanceof GeometricDrawer ? ((GeometricDrawer) source).getRoi() : null;
		if (theaterPanelValues instanceof GeometricDrawer) {
			final GeometricDrawer gtpv = (GeometricDrawer) theaterPanelValues;
			double scale = gtpv.getScale();
			GeometricDrawer toRedraw = source != gtpv ? gtpv : (GeometricDrawer) this.renderPane.getTheaterPane();
			toRedraw.removePaintListener(this);
			if (gtpv != source)
				toRedraw.setTransfom(-roi[0] * scale, -roi[1] * scale, scale);
			else
				toRedraw.setRoi((float) (-gtpv.getxTranslate() / scale), (float) (-gtpv.getyTranslate() / scale), (float) (source.getWidth() / scale), (float) (source.getHeight() / scale));
			if (forceToRedraw)
				toRedraw.repaint(false);
			toRedraw.addPaintListener(this);
		} /* else if (theaterPanelValues instanceof GeographicalDrawer) { final GeographicalDrawer gtpv = (GeographicalDrawer) theaterPanelValues; GeographicalDrawer toRedraw = source != gtpv ? gtpv :
			 * (GeographicalDrawer) this.renderPane.getTheaterPane(); toRedraw.removePaintListener(this); if (forceToRedraw) toRedraw.repaint(false); toRedraw.addPaintListener(this); } */
	}

	@Override
	public void saveScenario() {}

	@Override
	public void showMessage(String message, boolean error) {
		this.renderPaneValues.showMessage(message, error);
	}

	@Override
	public void updateStatusBar(String... infos) {
		this.renderPaneValues.updateStatusBar(infos);
	}

	@Override
	public void updateToolView(Class<? extends Tool> toolClass, boolean isVisible) {}

	@Override
	public void showTool(Class<? extends Tool> toolClass, boolean closeIfOpen) {}
}
