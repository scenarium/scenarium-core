/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display.toolBar;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.concurrent.TimeUnit;

import io.beanmanager.ihmtest.FxTest;
import io.scenarium.core.tools.SchedulerUtils;
import io.scenarium.gui.core.display.toolbarclass.ExternalTool;
import io.scenarium.gui.core.internal.Log;

import javafx.event.ActionEvent;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;

public class Console extends ExternalTool {
	public static void main(String[] args) {
		SchedulerUtils.getTimer("").scheduleAtFixedRate(new Runnable() {
			private int id = 0;

			@Override
			public void run() {
				Log.info("test " + this.id++);
			}
		}, 1000, 1000, TimeUnit.MILLISECONDS);
		FxTest.launchIHM(args, (s) -> {
			Console cfx = new Console();
			return cfx.getRegion();
		});
	}

	@Override
	public void dispose() {
		super.dispose();
		System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream(java.io.FileDescriptor.out), 128), true));
		System.setErr(new PrintStream(new BufferedOutputStream(new FileOutputStream(java.io.FileDescriptor.err), 128), true));
	}

	@Override
	public StackPane getRegion() {
		final TextArea textArea = new TextArea();
		PrintStream printStream = new PrintStream(new FilterOutputStream(new ByteArrayOutputStream()) {
			@Override
			public void write(byte[] b) throws IOException {
				textArea.appendText(new String(b));
			}

			@Override
			public void write(byte[] b, int off, int len) throws IOException {
				textArea.appendText(new String(b, off, len));
			}
		});
		System.setOut(printStream);
		System.setErr(printStream);
		textArea.setEditable(false);
		MenuItem mClear = new MenuItem("Clear");
		mClear.setOnAction((ActionEvent event) -> textArea.clear());
		MenuItem mCopy = new MenuItem("Copy");
		mCopy.setOnAction((ActionEvent event) -> textArea.copy());
		MenuItem mSelectAll = new MenuItem("Select All");
		mSelectAll.setOnAction((ActionEvent event) -> textArea.selectAll());
		textArea.setContextMenu(new ContextMenu(mCopy, mSelectAll, mClear));
		StackPane sp = new StackPane();
		sp.getChildren().add(textArea);
		return sp;
	}
}
