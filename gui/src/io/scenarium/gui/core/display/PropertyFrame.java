/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import io.beanmanager.editors.basic.StringEditor;
import io.beanmanager.editors.primitive.BooleanEditor;
import io.scenarium.core.struct.ScenariumProperties;
import io.scenarium.gui.core.internal.LoadPackageStream;
import io.scenarium.gui.core.struct.ScenariumGuiProperties;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class PropertyFrame {
	private static Stage propertyDialog;
	// private RenderFrame renderFrameFx;
	private BooleanEditor askBeforeQuitEditor;
	private BooleanEditor checkUpdatesAtStarupEditor;
	// private BooleanEditor synchroEditor;
	// private BooleanEditor pageFlippingEditor;
	private StringEditor lookAndFeelEditor;
	private BooleanEditor showHiddenPropertiesEditor;
	private BooleanEditor showExpertPropertiesEditor;
	private StringEditor mapTempPathPropertiesEditor;

	private Tab getBeanTab() {
		GridPane propertiesPane = new GridPane();
		propertiesPane.setHgap(10);
		propertiesPane.setVgap(10);
		int i = 0;
		this.showHiddenPropertiesEditor = new BooleanEditor();
		this.showHiddenPropertiesEditor.setValue(ScenariumGuiProperties.get().isShowHiddenProperties());
		propertiesPane.add(new Label("Show hidden properties: "), 0, i);
		Region ce = this.showHiddenPropertiesEditor.getEditor();
		GridPane.setHalignment(ce, HPos.CENTER);
		propertiesPane.add(ce, 1, i++);
		this.showExpertPropertiesEditor = new BooleanEditor();
		this.showExpertPropertiesEditor.setValue(ScenariumGuiProperties.get().isShowExpertProperties());
		propertiesPane.add(new Label("Show expert properties: "), 0, i);
		ce = this.showExpertPropertiesEditor.getEditor();
		GridPane.setHalignment(ce, HPos.CENTER);
		propertiesPane.add(ce, 1, i++);
		propertiesPane.setAlignment(Pos.CENTER);
		propertiesPane.setPadding(new Insets(10));
		return new Tab("Bean", propertiesPane);
	}

	private Tab getDrawerTab() {
		GridPane propertiesPane = new GridPane();
		propertiesPane.setHgap(10);
		propertiesPane.setVgap(10);
		int i = 0;
		this.mapTempPathPropertiesEditor = new StringEditor();
		this.mapTempPathPropertiesEditor.setValue(ScenariumProperties.get().getMapTempPath());
		propertiesPane.add(new Label("Map temp cache path: "), 0, i);
		Region ce = this.mapTempPathPropertiesEditor.getEditor();
		GridPane.setHalignment(ce, HPos.CENTER);
		propertiesPane.add(ce, 1, i++);
		propertiesPane.setAlignment(Pos.CENTER);
		propertiesPane.setPadding(new Insets(10));
		return new Tab("Drawer", propertiesPane);
	}

	private Tab getGeneralTab() {
		GridPane propertiesPane = new GridPane();
		propertiesPane.setHgap(10);
		propertiesPane.setVgap(10);
		int i = 0;
		this.askBeforeQuitEditor = new BooleanEditor();
		this.askBeforeQuitEditor.setValue(ScenariumGuiProperties.get().isAskBeforeQuit());
		propertiesPane.add(new Label("Ask confirmation before quit: "), 0, i);
		Region ce = this.askBeforeQuitEditor.getEditor();
		GridPane.setHalignment(ce, HPos.CENTER);
		propertiesPane.add(ce, 1, i++);
		this.checkUpdatesAtStarupEditor = new BooleanEditor();
		this.checkUpdatesAtStarupEditor.setValue(ScenariumGuiProperties.get().isCheckUpdatesAtStarup());
		propertiesPane.add(new Label("Check for updates at startup: "), 0, i);
		ce = this.checkUpdatesAtStarupEditor.getEditor();
		GridPane.setHalignment(ce, HPos.CENTER);
		propertiesPane.add(ce, 1, i++);
		// this.synchroEditor = new BooleanEditor();
		// this.synchroEditor.setValue(ScenariumGuiProperties.get().isSynchronization());
		// propertiesPane.add(new Label("Synchonized Data: "), 0, i);
		// ce = this.synchroEditor.getEditor();
		// GridPane.setHalignment(ce, HPos.CENTER);
		// propertiesPane.add(ce, 1, i++);
		// this.pageFlippingEditor = new BooleanEditor();
		// this.pageFlippingEditor.setValue(ScenariumGuiProperties.get().isPageFlipping());
		// propertiesPane.add(new Label("Page flipping: "), 0, i);
		// ce = this.pageFlippingEditor.getEditor();
		// GridPane.setHalignment(ce, HPos.CENTER);
		// propertiesPane.add(ce, 1, i++);
		this.lookAndFeelEditor = new StringEditor();
		this.lookAndFeelEditor.setPossibilities(UserAgentStylesheetManager.getAvailableUserAgentStylesheets());
		this.lookAndFeelEditor.setValue(ScenariumGuiProperties.get().getLookAndFeel());
		this.lookAndFeelEditor.addPropertyChangeListener(() -> UserAgentStylesheetManager.loadUserAgentStylesheet(this.lookAndFeelEditor.getValue()));
		propertiesPane.add(new Label("Look and fell: "), 0, i);
		ce = this.lookAndFeelEditor.getEditor();
		GridPane.setHalignment(ce, HPos.CENTER);
		propertiesPane.add(ce, 1, i++);
		propertiesPane.setAlignment(Pos.CENTER);
		propertiesPane.setPadding(new Insets(10));
		return new Tab("General", propertiesPane);
	}

	private Node getValidationPane() {
		Button applyButton = new Button("Apply");
		applyButton.setOnAction(e -> validateProperty());
		Button okButton = new Button("Ok");
		okButton.setOnAction(e -> {
			validateProperty();
			propertyDialog.close();
		});
		Button cancelButton = new Button("Cancel");
		cancelButton.setOnAction(e -> {
			Application.setUserAgentStylesheet(ScenariumGuiProperties.get().getLookAndFeel());
			propertyDialog.close();
		});
		HBox hbox = new HBox(applyButton, okButton, cancelButton);
		hbox.setSpacing(10);
		hbox.setPadding(new Insets(5));
		hbox.setAlignment(Pos.CENTER);
		return hbox;
	}

	public void showProperty(Window owner/* , RenderFrame renderFrameFx */) {
		if (propertyDialog != null) {
			propertyDialog.close();
			propertyDialog = null;
		}
		// this.renderFrameFx = renderFrameFx;
		propertyDialog = new Stage(StageStyle.UTILITY);
		propertyDialog.initModality(Modality.WINDOW_MODAL);
		propertyDialog.initOwner(owner);
		propertyDialog.getIcons().add(new Image(LoadPackageStream.getStream("/option.gif")));

		propertyDialog.setTitle("Properties");
		TabPane tabPane = new TabPane(getGeneralTab(), getBeanTab(), getDrawerTab());
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		VBox.setVgrow(tabPane, Priority.ALWAYS);
		tabPane.setMinSize(320, 240);
		VBox vBox = new VBox(tabPane, getValidationPane());
		propertyDialog.setOnCloseRequest(e -> validateProperty());
		propertyDialog.setScene(new Scene(vBox));
		propertyDialog.setAlwaysOnTop(true);
		propertyDialog.show();
	}

	private void validateProperty() {
		ScenariumGuiProperties scenariumProperties = ScenariumGuiProperties.get();
		scenariumProperties.setAskBeforeQuit(this.askBeforeQuitEditor.getValue());
		scenariumProperties.setCheckUpdatesAtStarup(this.checkUpdatesAtStarupEditor.getValue());
		// boolean synchro = this.synchroEditor.getValue();
		// if (synchro != scenariumProperties.isSynchronization()) {
		// scenariumProperties.setSynchronization(synchro);
		// try {
		// this.renderFrameFx.getRenderPane().getDataLoader().setSynchronized(synchro);
		// } catch (Exception e) {}
		// }
		// boolean pageFlipping = this.pageFlippingEditor.getValue();
		// if (pageFlipping != scenariumProperties.isPageFlipping()) {
		// scenariumProperties.setPageFlipping(pageFlipping);
		// Object de = this.renderFrameFx.getRenderPane().getDataLoader().getScenario().getScenarioData();
		// if (de instanceof BufferedStrategy)
		// ((BufferedStrategy<?>) de).setPageFlipping(pageFlipping);
		// }
		scenariumProperties.setLookAndFeel(this.lookAndFeelEditor.getValue());
		scenariumProperties.setShowHiddenProperties(this.showHiddenPropertiesEditor.getValue());
		scenariumProperties.setShowExpertProperties(this.showExpertPropertiesEditor.getValue());
		ScenariumProperties.get().setMapTempPath(this.mapTempPathPropertiesEditor.getValue());
	}
}
