/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import java.io.PrintWriter;
import java.io.StringWriter;

import io.beanmanager.tools.FxUtils;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class AlertUtil {

	private AlertUtil() {}

	public static void show(String title, String headerText, String content) {
		FxUtils.runLaterIfNeeded(() -> {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setHeaderText(headerText);
			if (title != null)
				alert.setTitle(title);
			alert.setContentText(content);
			alert.showAndWait();
		});
	}

	public static void show(Exception e, boolean stackTrace) {
		show(e, null, stackTrace);
	}

	public static void show(Exception e, String prefix, boolean stackTrace) {
		show(e, prefix, null, stackTrace);
	}

	public static void show(Exception e, String prefix, String title, boolean stackTrace) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setHeaderText(e.getClass().getSimpleName());
		if (title != null)
			alert.setTitle(title);
		StringBuilder errorMessage = new StringBuilder();
		if (prefix != null)
			errorMessage.append(prefix + System.lineSeparator());
		errorMessage.append("Due to: " + System.lineSeparator() + e.getMessage());
		Throwable cause = e.getCause();
		if (cause != null)
			errorMessage.append(System.lineSeparator() + "Caused by: " + System.lineSeparator() + cause.getMessage());
		alert.setContentText(errorMessage.toString());
		alert.getDialogPane().setMinWidth(640);
		if (stackTrace) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			TextArea textArea = new TextArea(sw.toString());
			textArea.setEditable(false);
			textArea.setWrapText(true);
			textArea.setMaxWidth(Double.MAX_VALUE);
			textArea.setMaxHeight(Double.MAX_VALUE);
			GridPane.setVgrow(textArea, Priority.ALWAYS);
			GridPane.setHgrow(textArea, Priority.ALWAYS);
			GridPane expContent = new GridPane();
			expContent.setMaxWidth(Double.MAX_VALUE);
			expContent.add(new Label("The exception stacktrace was:"), 0, 0);
			expContent.add(textArea, 0, 1);
			alert.getDialogPane().setExpandableContent(expContent);
		}
		alert.showAndWait();
	}
}
