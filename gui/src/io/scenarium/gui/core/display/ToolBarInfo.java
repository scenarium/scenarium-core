/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import java.util.List;

import io.scenarium.gui.core.display.drawer.TheaterPanel;
import io.scenarium.gui.core.display.toolbarclass.Tool;

import javafx.scene.input.KeyCode;

public class ToolBarInfo {
	public final Class<? extends Tool> type;
	public final KeyCode keyCode;
	public final boolean onlyMainFrame;
	public final List<Class<? extends TheaterPanel>> scenarios;

	public ToolBarInfo(Class<? extends Tool> toolBarClass, KeyCode keyCode, boolean onlyMainFrame, List<Class<? extends TheaterPanel>> list) {
		this.type = toolBarClass;
		this.keyCode = keyCode;
		this.onlyMainFrame = onlyMainFrame;
		this.scenarios = list;
	}

	@Override
	public String toString() {
		return this.type.getSimpleName() + "KeyCode: " + this.keyCode;
	}
}
