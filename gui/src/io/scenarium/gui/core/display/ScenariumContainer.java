/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.display;

import javax.vecmath.Point2i;

import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.gui.core.display.toolbarclass.Tool;

public interface ScenariumContainer {
	public void adaptSizeToDrawableElement();

	public Point2i getDefaultToolBarLocation(String simpleName);

	public Scheduler getScheduler();

	public int getSelectedElementFromTheaterEditor();

	public boolean isDefaultToolBarAlwaysOnTop(String simpleName);

	public boolean isManagingAccelerator();

	public boolean isStatusBar();

	public void saveScenario();

	public void showMessage(String message, boolean error);

	public void updateStatusBar(String... infos);

	public void updateToolView(Class<? extends Tool> toolClass, boolean isVisible);

	public void showTool(Class<? extends Tool> toolClass, boolean closeIfOpen);
}
