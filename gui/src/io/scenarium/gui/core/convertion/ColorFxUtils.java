package io.scenarium.gui.core.convertion;

import javax.vecmath.Color3f;
import javax.vecmath.Color4f;

import javafx.scene.paint.Color;

public class ColorFxUtils {

	private ColorFxUtils() {}

	public static Color toFXColor(Color3f color) {
		return new Color(color.x, color.y, color.z, 1);
	}

	public static Color toFXColor(Color4f color) {
		return new Color(color.x, color.y, color.z, color.z);
	}

	public static Color3f toColor3f(Color color) {
		return new Color3f((float) color.getRed(), (float) color.getGreen(), (float) color.getBlue());
	}

	public static Color4f toColor4f(Color color) {
		return new Color4f((float) color.getRed(), (float) color.getGreen(), (float) color.getBlue(), (float) color.getOpacity());
	}
}
