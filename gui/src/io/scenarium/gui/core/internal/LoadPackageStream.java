package io.scenarium.gui.core.internal;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.stream.Stream;

public class LoadPackageStream {
	private LoadPackageStream() {}

	public static InputStream getStream(String resourceName) {
		Log.verbose("Load resource: '/resources" + resourceName + "'");
		InputStream out = LoadPackageStream.class.getResourceAsStream("/resources" + resourceName);
		if (out == null) {
			Log.error("Can not load resource: '" + resourceName + "'");
			for (Path elem : LoadPackageStream.getResources(LoadPackageStream.class.getResource("/resources")).toArray(Path[]::new))
				Log.warning("  - '" + elem + "'");
		}
		return out;
	}

	public static Stream<Path> getResources(final URL element) {
		try {
			URI uri = element.toURI();
			FileSystem fs;
			Path path;
			if (uri.getScheme().contentEquals("jar")) {
				try {
					fs = FileSystems.getFileSystem(uri);
				} catch (FileSystemNotFoundException e) {
					fs = FileSystems.newFileSystem(uri, Collections.<String, String> emptyMap());
				}
				String pathInJar = "/";
				String tmpPath = element.getPath();
				int idSeparate = tmpPath.indexOf('!');
				if (idSeparate != -1) {
					pathInJar = tmpPath.substring(idSeparate + 1);
					while (pathInJar.startsWith("/"))
						pathInJar = pathInJar.substring(1);
				}
				path = fs.getPath(pathInJar);
			} else {
				fs = FileSystems.getDefault();
				path = Paths.get(uri);
			}
			return Files.walk(path, 1);
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
			return Stream.of();
		}
	}

}
