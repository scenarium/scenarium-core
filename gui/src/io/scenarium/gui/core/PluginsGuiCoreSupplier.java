package io.scenarium.gui.core;

import io.scenarium.gui.core.consumer.DrawersConsumer;
import io.scenarium.gui.core.consumer.ToolBarConsumer;

public interface PluginsGuiCoreSupplier {

	default void populateDrawers(DrawersConsumer drawersConsumer) {}

	default void populateToolBars(ToolBarConsumer toolBarConsumer) {}
}
