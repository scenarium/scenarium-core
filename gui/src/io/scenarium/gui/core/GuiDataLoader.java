package io.scenarium.gui.core;

import java.io.File;

import io.beanmanager.BeanManager;
import io.scenarium.core.Scenarium;
import io.scenarium.gui.core.display.LanguageManager;
import io.scenarium.gui.core.display.UserAgentStylesheetManager;
import io.scenarium.gui.core.internal.Log;
import io.scenarium.gui.core.struct.ScenariumGuiProperties;

public class GuiDataLoader {
	private final File guiConfigFile;

	public GuiDataLoader() {
		this.guiConfigFile = new File(Scenarium.getWorkspace() + File.separator + "ScenariumGuiConfig.txt");
	}

	public boolean loadSofwareConfig() {
		Log.verbose("loadSofwareConfig()");
		if (!this.guiConfigFile.exists()) {
			Log.verbose("loadSofwareConfig() [DONE]");
			return false;
		}
		Log.verbose("ScenariumGuiProperties.get()");
		ScenariumGuiProperties pp = ScenariumGuiProperties.get();
		new BeanManager(pp, Scenarium.getWorkspace()).load(this.guiConfigFile);
		Log.verbose("!this.guiConfigFile.exists()");
		if (!this.guiConfigFile.exists())
			LanguageManager.loadDefaultLanguage();
		Log.verbose("pp.getLookAndFeel()");
		String look = pp.getLookAndFeel();
		Log.verbose("UserAgentStylesheetManager.loadUserAgentStylesheet(look) : " + look);
		UserAgentStylesheetManager.loadUserAgentStylesheet(look);
		if (pp.getLanguage() == null || pp.getLanguage().isEmpty() || !LanguageManager.loadLanguage(pp.getLanguage())) {
			Log.verbose("LanguageManager.loadDefaultLanguage()");
			LanguageManager.loadDefaultLanguage();
		}
		Log.verbose("pp.isHiddenFieldVisible()");
		BeanManager.isHiddenFieldVisible = pp.isHiddenFieldVisible();
		BeanManager.isExpertFieldVisible = pp.isExpertFieldVisible();
		Log.verbose("loadSofwareConfig() [DONE]");
		return true;
	}

	public void saveSoftwareConfig() {
		ScenariumGuiProperties pp = ScenariumGuiProperties.get();
		pp.setHiddenFieldVisible(BeanManager.isHiddenFieldVisible);
		pp.setExpertFieldVisible(BeanManager.isExpertFieldVisible);
		new BeanManager(pp, Scenarium.getWorkspace()).save(this.guiConfigFile, false);
	}
}
