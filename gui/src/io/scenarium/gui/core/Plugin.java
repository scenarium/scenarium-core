package io.scenarium.gui.core;

import io.beanmanager.PluginsBeanSupplier;
import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.beanmanager.consumer.EditorConsumer;
import io.scenarium.core.struct.OldObject3D;
import io.scenarium.core.struct.curve.Curve;
import io.scenarium.core.struct.curve.CurveSeries;
import io.scenarium.core.tools.ViewerAnimationTimerSupplier;
import io.scenarium.gui.core.consumer.DrawersConsumer;
import io.scenarium.gui.core.consumer.ToolBarConsumer;
import io.scenarium.gui.core.display.RenderPane;
import io.scenarium.gui.core.display.ViewerAnimationTimerFX;
import io.scenarium.gui.core.display.drawer.ChartDrawer;
import io.scenarium.gui.core.display.drawer.Drawer3D;
import io.scenarium.gui.core.display.drawer.DrawerManager;
import io.scenarium.gui.core.display.toolbarclass.ToolBarDescriptor;
import io.scenarium.gui.core.editors.ToolDescriptorEditor;
import io.scenarium.pluginManager.PluginsSupplier;

public class Plugin implements PluginsSupplier, PluginsBeanSupplier, PluginsGuiCoreSupplier {
	@Override
	public void birth() {
		try {
			Class.forName(DrawerManager.class.getName());
			Class.forName(RenderPane.class.getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		ViewerAnimationTimerSupplier.setInstanceSupplier(ViewerAnimationTimerFX::new);
	}

	@Override
	public void loadPlugin(Object pluginInterface) {
		if (pluginInterface instanceof PluginsGuiCoreSupplier) {
			// TODO REFACTO move this in GUI plug-in
			PluginsGuiCoreSupplier plugins = (PluginsGuiCoreSupplier) pluginInterface;
			// Add drawer interfaces
			plugins.populateDrawers(new DrawersConsumer());
			// Add tool bar interfaces
			plugins.populateToolBars(new ToolBarConsumer()); // Leak here JDK-8116412 workaround: weakReference in RenderFrame
		}
	}

	@Override
	public void unregisterModule(Module module) {
		// V) ToolBars
		RenderPane.purgeToolBars(module);
		// IV) Drawers
		DrawerManager.purgeDrawers(module);
	}

	

	
	@Override
	public void populateDrawers(DrawersConsumer drawersConsumer) {
		drawersConsumer.accept(Curve.class, ChartDrawer.class);
		drawersConsumer.accept(CurveSeries.class, ChartDrawer.class);
		drawersConsumer.accept(OldObject3D.class, Drawer3D.class);
	}

	@Override
	public void populateEditors(EditorConsumer editorConsumer) {
		editorConsumer.accept(ToolBarDescriptor.class, ToolDescriptorEditor.class);
	}

	@Override
	public void populateClassNameRedirection(ClassNameRedirectionConsumer classNameRedirectionConsumer) {
		classNameRedirectionConsumer.accept("io.scenarium.core.display.drawer.camera3D.ArcBallCamera", "io.scenarium.gui.core.display.drawer.camera3D.ArcBallCamera");
	}
}
