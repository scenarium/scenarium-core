/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.vecmath.Point2i;

import io.scenarium.core.Scenarium;
import io.scenarium.core.StartMessageConsumer;
import io.scenarium.core.TaskConsumer;
import io.scenarium.core.filemanager.LoadingException;
import io.scenarium.core.filemanager.LoadingOperationStartListener;
import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;
import io.scenarium.core.filemanager.scenariomanager.ScheduleChangeListener;
import io.scenarium.core.struct.ScenariumProperties;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.core.timescheduler.SchedulerPropertyChangeListener;
import io.scenarium.core.timescheduler.SchedulerState;
import io.scenarium.core.tools.ReadOnlyObservableValue;
import io.scenarium.core.updater.Updater;
import io.scenarium.gui.core.display.AlertUtil;
import io.scenarium.gui.core.display.LoadDrawerListener;
import io.scenarium.gui.core.display.RenderFrame;
import io.scenarium.gui.core.display.SplashScreen;
import io.scenarium.gui.core.display.drawer.DrawerManager;
import io.scenarium.gui.core.display.drawer.TheaterPanel;
import io.scenarium.gui.core.display.toolbarclass.ToolBarDescriptor;
import io.scenarium.gui.core.internal.Log;
import io.scenarium.gui.core.struct.ScenariumGuiProperties;
import io.scenarium.logger.Logger;
import io.scenarium.pluginManager.ModuleManager;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ScenariumGui extends Application implements GuiMainApp, SchedulerPropertyChangeListener, ScheduleChangeListener, TaskConsumer, LoadingOperationStartListener {
	private static AtomicBoolean isJavaFxThreadRunning = new AtomicBoolean(false);
	public static ScenariumGui scenariumGui;
	public Scenarium scenarium;

	private RenderFrame mainFrame;
	private SplashScreen splashScreen;
	private long launchingTime;

	public ScenariumGui() throws InstantiationException {
		Log.debug("Initialize the application: " + ScenariumGui.class.getCanonicalName());

		this.scenarium = Scenarium.buildInstance(this);
		DrawerManager.addLoadDrawerListener(new LoadDrawerListener() {

			@Override
			public void unloaded(Class<? extends TheaterPanel> drawerClass) {
				if (ScenariumGui.this.mainFrame != null && ScenariumGui.this.mainFrame.getRenderPane().getClass().equals(drawerClass))
					loadDefaultScenario(false);
			}

			@Override
			public void loaded(Class<? extends TheaterPanel> drawerClass) {}
		});
		this.scenarium.addScenarioScheduledListener((scenario, schedulerChanged) -> {
			Runnable runnable = () -> {
				Log.debug("startup time: " + (System.currentTimeMillis() - this.launchingTime));
				if (this.mainFrame == null) {
					if (this.splashScreen != null) {
						this.splashScreen.close();
						this.splashScreen = null;
					}
					this.mainFrame = new RenderFrame(new Stage(), ScenariumGui.this, this.scenarium.getDataLoader(), true); // 370ms 150 ms minimum
				} else if (this.mainFrame != null)
					this.mainFrame.reload(true);
				TheaterPanel tp = this.mainFrame.getRenderPane().getTheaterPane();
				tp.setScheduler(this.scenarium.getScheduler());
				scenario.addUpdateListener(hasChanged -> {
					TheaterPanel theaterPanel = this.mainFrame.getRenderPane().getTheaterPane();
					if (theaterPanel != null) {
						if (hasChanged)
							theaterPanel.setDrawableElement(scenario.getScenarioData());
						theaterPanel.repaint(true);
					}
				});
				if (this.mainFrame != null)
					tp.repaint(true);
				updateRecentScenario();
			};
			if (Platform.isFxApplicationThread())
				runnable.run();
			else
				Platform.runLater(runnable);
		});
		this.scenarium.addScenarioSchedulerChangeListener((scenario, newScheduler) -> this.mainFrame.getRenderPane().getTheaterPane().setScheduler(newScheduler));
		Log.verbose("Initiazlize the application |DONE]");
	}

	// this is a simple run with no generic parameters ...
	public static void main(String[] inputArgument) {
		List<String> args = new ArrayList<>(Arrays.asList(inputArgument));
		Logger.init(args);
		ModuleManager.birth();
		run(args.toArray(new String[0]));
	}

	public static void run(String[] inputArgument) {
		launch(inputArgument);
	}

	public static void waitForJavaFXThreadStart() {
		if (!isJavaFxThreadRunning.getAndSet(true)) {
			CountDownLatch cdl = new CountDownLatch(1);
			Platform.startup(() -> cdl.countDown());
			Platform.setImplicitExit(false); // utile pour remote viewer
			try {
				cdl.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean isJavaFXThreadStarted() {
		return isJavaFxThreadRunning.get();
	}

	public RenderFrame getMainFrame() {
		return this.mainFrame;
	}

	@Override
	public Point2i getMainFrameDimension() {
		return this.mainFrame == null ? null : this.mainFrame.getDimention();
	}

	@Override
	public Point2i getMainFramePosition() {
		return this.mainFrame == null ? null : this.mainFrame.getPosition();
	}

	@Override
	public ToolBarDescriptor[] getVisibleToolsDesc() {
		return this.mainFrame == null ? null : this.mainFrame.getVisibleToolsDesc().toArray(ToolBarDescriptor[]::new);
	}

	@Override
	public void loadingOperationStarted(ReadOnlyObservableValue<Double> progressProperty) {
		if (this.mainFrame != null)
			this.mainFrame.getRenderPane().loadingOperation(progressProperty);
	}

	void loadExternalModules() {
		Log.debug("Load scenarium external modules");
		Path[] modules = ScenariumProperties.get().getExternModules();
		if (modules != null)
			ModuleManager.registerExternalModules(modules);
	}

	@Override
	public void start(Stage splashStage) throws Exception {
		Log.error("Application start " + splashStage);
		isJavaFxThreadRunning.set(true);
		this.launchingTime = System.currentTimeMillis();
		DrawerManager.addLoadDrawerListener(new LoadDrawerListener() {

			@Override
			public void unloaded(Class<? extends TheaterPanel> drawerClass) {
				if (ScenariumGui.this.mainFrame != null && ScenariumGui.this.mainFrame.getRenderPane().getClass().equals(drawerClass))
					loadDefaultScenario(false);
			}

			@Override
			public void loaded(Class<? extends TheaterPanel> drawerClass) {}
		});
		this.scenarium.addShutdownListener(() -> new GuiDataLoader().saveSoftwareConfig());
		this.scenarium.start(new StartMessageConsumer() {

			@Override
			public void consumeInformation(String message) {
				new Alert(AlertType.INFORMATION, message).showAndWait();
			}

			@Override
			public void consumeException(Exception exception) {
				AlertUtil.show(exception, false);
			}

			@Override
			public void consumeError(String message) {
				Alert alert = new Alert(AlertType.ERROR, message + "\n");
				// Size length bug code: 8087981, to remove after
				alert.getDialogPane().getChildren().stream().filter(node -> node instanceof Label).forEach(node -> ((Label) node).setMinHeight(Region.USE_PREF_SIZE));
				alert.showAndWait();
			}
		}, () -> new GuiDataLoader().loadSofwareConfig(), pp -> this.splashScreen = new SplashScreen(splashStage, pp));
		this.scenarium.getDataLoader().addLoadingOperationStartListener(this);
		Updater updater = Updater.getInstance();
		updater.cleanFiles();
		if (ScenariumGuiProperties.get().isCheckUpdatesAtStarup() && updater.isUpdatable()) {
			Thread t = new Thread(new Task<>() {
				@Override
				public Void call() {
					if (updater.isNewVersion())
						Platform.runLater(() -> {
							Alert alert = new Alert(AlertType.CONFIRMATION);
							alert.setTitle("New version available");
							alert.setHeaderText("New version available");
							alert.setContentText("Update Scenarium to: " + updater.getLastVersion() + " ?");
							if (alert.showAndWait().get() == ButtonType.OK) {
								Stage dialog = new Stage(StageStyle.UTILITY);
								dialog.initModality(Modality.WINDOW_MODAL);
								dialog.initOwner(splashStage);
								dialog.setTitle("Downloading Scenarium version: " + updater.getLastVersion());
								ProgressBar pb = new ProgressBar();
								pb.setProgress(0);
								pb.setPrefSize(400, 30);
								Label text = new Label("0.00%");
								updater.addProgressListener(progress -> Platform.runLater(() -> {
									pb.setProgress(progress);
									text.setText(String.format("%.2f", progress * 100) + "%");
								}));
								dialog.setScene(new Scene(new StackPane(pb, text)));
								dialog.show();
								Task<Void> task = new Task<>() {
									@Override
									public Void call() {
										try {
											File newJarFile = updater.update();
											Platform.runLater(() -> {
												if (newJarFile != null) {
													dialog.close();
													Alert restartAlert = new Alert(AlertType.CONFIRMATION);
													restartAlert.setTitle("Scenarium needs to restart");
													restartAlert.setHeaderText("Scenarium needs to restart to complete the installation");
													restartAlert.setContentText("Restart to complete installation ?");
													if (restartAlert.showAndWait().get() == ButtonType.OK)
														updater.restart(newJarFile);
													newJarFile.delete();
													dialog.close();
												} else {
													dialog.close();
													Alert cancelAlert = new Alert(AlertType.INFORMATION);
													cancelAlert.setTitle("Update canceled");
													cancelAlert.setHeaderText("Update canceled");
													cancelAlert.setContentText("Update canceled");
													cancelAlert.showAndWait();
												}
											});
										} catch (Exception e) {
											Platform.runLater(() -> {
												Alert downloadAlert = new Alert(AlertType.ERROR);
												downloadAlert.setTitle("Error while downloading Scenarium update");
												downloadAlert.setHeaderText("Error while downloading Scenarium update");
												downloadAlert.setContentText("Error while downloading Scenarium update: " + e.getMessage());
												downloadAlert.showAndWait();
												e.printStackTrace();
												dialog.close();
											});
										}
										return null;
									}
								};
								Thread t1 = new Thread(task);
								t1.setPriority(Thread.MIN_PRIORITY);
								t1.start();
							}
						});
					return null;
				}
			});
			t.setPriority(Thread.MIN_PRIORITY);
			t.start();
			Log.verbose("Application start [DONE]");
		}
	}

	@Override
	public void updateRecentScenario() {
		if (this.mainFrame != null)
			this.mainFrame.updateRecentScenario();
	}

	private void loadDefaultScenario(boolean backgroundLoading) {
		// TODO REFACTO This is temporary, need to create a real start interface
		Optional<Class<? extends LocalScenario>> value = ScenarioManager.getFirstRunableScenario();
		if (value.isPresent())
			try {
				this.scenarium.getDataLoader().secureLoad(value.get(), false, backgroundLoading);
			} catch (LoadingException e) {
				e.printStackTrace();
			}
		else {
			Log.error("Cannot create a default scenario");
			System.exit(-1);
		}
	}

	/** Run the specified Runnable synchronously with other Scenarium tasks. This is not a blocking method.
	 * @param runnable the Runnable whose run method will be executed synchronously Scenarium. */
	@Override
	public void runTask(Runnable runnable) {
		if (Platform.isFxApplicationThread())
			runnable.run();
		else
			Platform.runLater(() -> runnable.run());
	}

	/** Run the specified Runnable synchronously with other Scenarium tasks. This method blocks until the execution of the runnable.
	 * @param runnable the Runnable whose run method will be executed synchronously Scenarium. */
	@Override
	public void runTaskAndWait(Runnable runnable) {
		if (Platform.isFxApplicationThread())
			runnable.run();
		else {
			CountDownLatch cdl = new CountDownLatch(1);
			Platform.runLater(() -> {
				runnable.run();
				cdl.countDown();
			});
			try {
				cdl.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void removeToSchedule(Scenario scenario) {
		this.scenarium.removeToSchedule(scenario);
	}

	@Override
	public void scheduleChange() {
		this.scenarium.scheduleChange();
	}

	@Override
	public void stateChanged(SchedulerState state) {
		this.scenarium.stateChanged(state);
	}

	@Override
	public boolean addToSchedule(Scenario scenario) {
		return this.scenarium.addToSchedule(scenario);
	}

	public Scheduler getScheduler() {
		return this.scenarium.getScheduler();
	}
}
