/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.core.editors;

import io.beanmanager.editors.PropertyEditor;
import io.beanmanager.editors.multiNumber.point.Point2iEditor;
import io.beanmanager.editors.primitive.BooleanEditor;
import io.scenarium.gui.core.display.toolbarclass.ToolBarDescriptor;

public class ToolDescriptorEditor extends PropertyEditor<ToolBarDescriptor> {
	private static final String SEPARATOR_NAME = ": ";
	private static final String SEPARATOR = ";";

	@Override
	public String getAsText() {
		ToolBarDescriptor td = getValue();
		Point2iEditor pe = new Point2iEditor();
		pe.setValue(td.toolPos);
		return td.toolType + SEPARATOR_NAME + pe.getAsText() + SEPARATOR + td.alwaysOnTop;
	}

	@Override
	public boolean hasCustomEditor() {
		return false;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text.equals(String.valueOf((Object) null))) {
			setValue(null);
			return;
		}
		int separatorIndex = text.indexOf(SEPARATOR_NAME);
		String properties = text.substring(separatorIndex + SEPARATOR_NAME.length());
		int separatorPropIndex = properties.indexOf(SEPARATOR);
		Point2iEditor pe = new Point2iEditor();
		pe.setAsText(properties.substring(0, separatorPropIndex));
		BooleanEditor be = new BooleanEditor();
		be.setAsText(properties.substring(separatorPropIndex + SEPARATOR.length()));
		setValue(new ToolBarDescriptor(text.substring(0, separatorIndex), pe.getValue(), be.getValue()));
	}
}
