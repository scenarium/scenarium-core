import io.scenarium.gui.core.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

module io.scenarium.gui.core {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	exports io.scenarium.gui.core;
	exports io.scenarium.gui.core.consumer;
	exports io.scenarium.gui.core.display;
	exports io.scenarium.gui.core.display.drawer;
	exports io.scenarium.gui.core.display.drawer.camera3D;
	exports io.scenarium.gui.core.display.toolBar;
	exports io.scenarium.gui.core.display.toolbarclass;
	exports io.scenarium.gui.core.editors;
	exports io.scenarium.gui.core.struct;

	requires transitive io.scenarium.core;
	requires transitive javafx.graphics;
	requires javafx.controls;
	requires javafx.web;
	requires javafx.swing;
}
