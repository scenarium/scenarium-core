
/* *****************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public License, v.2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors: Revilloud Marc - initial API and implementation
 ******************************************************************************/

import io.scenarium.core.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

/** @author revilloud */
open module io.scenarium.core {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	exports io.scenarium.core.math;
	exports io.scenarium.core;
	exports io.scenarium.core.tools;
	exports io.scenarium.core.updater;
	exports io.scenarium.core.math.thresholding;
	exports io.scenarium.core.filemanager;
	exports io.scenarium.core.editors;
	exports io.scenarium.core.tools.dynamiccompilation;
	exports io.scenarium.core.timescheduler;
	exports io.scenarium.core.struct;
	exports io.scenarium.core.filemanager.scenariomanager;
	exports io.scenarium.core.struct.curve;
	exports io.scenarium.core.math.association;
	exports io.scenarium.core.filemanager.playlist;
	exports io.scenarium.core.consumer;

	// Project
	requires transitive io.beanmanager;
	requires transitive io.scenarium.river;

	// Default Java module
	requires transitive java.compiler;
	requires java.desktop;
	requires java.management;
	requires java.rmi;
	requires java.xml;
	requires java.base;

	// Libraries not compliant with Java module system
	requires transitive jogamp.fat;
	requires vecmath;
}