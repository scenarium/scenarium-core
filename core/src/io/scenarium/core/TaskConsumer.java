package io.scenarium.core;

public interface TaskConsumer {
	public void runTask(Runnable runnable);

	public void runTaskAndWait(Runnable runnable);
}
