/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.updater;

public class Encryptor {
	private final byte[] buffer;
	private final int blockSize;
	private int buffered = 0;
	private boolean decrypting = false;
	private final EncryptionAlgorithm ea;

	public Encryptor(EncryptionAlgorithm ea) {
		this.buffer = new byte[ea.getBlockSize() * 2];
		this.blockSize = ea.getBlockSize();
		this.decrypting = ea.isDecrypting();
		this.ea = ea;
	}

	void decrypt(byte[] in, int inOff, int len, byte[] out, int outOff) {
		while (len >= this.blockSize) {
			this.ea.decryptBlock(in, inOff, out, outOff);
			len -= this.blockSize;
			inOff += this.blockSize;
			outOff += this.blockSize;
		}
	}

	byte[] doFinal(byte[] input, int inputOffset, int inputLen) {
		byte[] output = null;
		byte[] out = null;
		output = new byte[getOutputSize(inputLen)];
		int len = doFinal(input, inputOffset, inputLen, output, 0);
		if (len < output.length) {
			out = new byte[len];
			if (len != 0)
				System.arraycopy(output, 0, out, 0, len);
		} else
			out = output;
		return out;
	}

	int doFinal(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset) {
		int totalLen = this.buffered + inputLen;
		int paddedLen = totalLen;
		int paddingLen = padLength(totalLen);
		if (paddingLen > 0 && paddingLen != this.blockSize && this.decrypting)
			throw new IllegalArgumentException("Input length must be multiple of " + this.blockSize + " when decrypting with padded cipher");
		if (!this.decrypting)
			paddedLen += paddingLen;
		if (output == null)
			throw new IllegalArgumentException("Output buffer is null");
		int outputCapacity = output.length - outputOffset;
		if (!this.decrypting && outputCapacity < paddedLen || this.decrypting && outputCapacity < paddedLen - this.blockSize)
			throw new IllegalArgumentException("Output buffer too short: " + outputCapacity + " bytes given, " + paddedLen + " bytes needed");
		byte[] finalBuf = input;
		int finalOffset = inputOffset;
		if (this.buffered != 0 || !this.decrypting) {
			finalOffset = 0;
			finalBuf = new byte[paddedLen];
			if (this.buffered != 0)
				System.arraycopy(this.buffer, 0, finalBuf, 0, this.buffered);
			if (inputLen != 0)
				System.arraycopy(input, inputOffset, finalBuf, this.buffered, inputLen);
			if (!this.decrypting)
				padWithLen(finalBuf, totalLen, paddingLen);
		}
		if (this.decrypting) {
			byte[] outWithPadding = new byte[totalLen];
			totalLen = finalNoPadding(finalBuf, finalOffset, outWithPadding, 0, totalLen);
			int padStart = unpad(outWithPadding, 0, totalLen);
			if (padStart < 0)
				throw new IllegalArgumentException("Given final block not " + "properly padded");
			totalLen = padStart;
			if (output.length - outputOffset < totalLen)
				throw new IllegalArgumentException("Output buffer too short: " + (output.length - outputOffset) + " bytes given, " + totalLen + " bytes needed");
			for (int i = 0; i < totalLen; i++)
				output[outputOffset + i] = outWithPadding[i];
		} else
			totalLen = finalNoPadding(finalBuf, finalOffset, output, outputOffset, paddedLen);
		this.buffered = 0;
		return totalLen;
	}

	void encrypt(byte[] in, int inOff, int len, byte[] out, int outOff) {
		while (len >= this.blockSize) {
			this.ea.encryptBlock(in, inOff, out, outOff);
			len -= this.blockSize;
			inOff += this.blockSize;
			outOff += this.blockSize;
		}
	}

	private int finalNoPadding(byte[] in, int inOff, byte[] out, int outOff, int len) {
		if (in == null || len == 0)
			return 0;
		if (this.decrypting)
			decrypt(in, inOff, len, out, outOff);
		else
			encrypt(in, inOff, len, out, outOff);
		return len;
	}

	int getOutputSize(int inputLen) {
		int totalLen = this.buffered + inputLen;
		return this.decrypting ? totalLen : totalLen + this.blockSize - totalLen % this.blockSize;
	}

	public int padLength(int len) {
		return this.blockSize - len % this.blockSize;
	}

	public void padWithLen(byte[] in, int off, int len) {
		if (in == null)
			return;
		if (off + len > in.length)
			throw new IllegalArgumentException("Buffer too small to hold padding");
		byte paddingOctet = (byte) (len & 0xff);
		for (int i = 0; i < len; i++)
			in[i + off] = paddingOctet;
	}

	public int unpad(byte[] in, int off, int len) {
		if (in == null || len == 0)
			return 0;
		byte lastByte = in[off + len - 1];
		int padValue = lastByte & 0x0ff;
		if (padValue < 0x01 || padValue > this.blockSize)
			return -1;
		int start = off + len - (lastByte & 0x0ff);
		if (start < off)
			return -1;
		for (int i = 0; i < (lastByte & 0x0ff); i++)
			if (in[start + i] != lastByte)
				return -1;
		return start;
	}

	byte[] update(byte[] input, int inputOffset, int inputLen) {
		byte[] output = null;
		byte[] out = null;
		output = new byte[getOutputSize(inputLen)];
		int len = update(input, inputOffset, inputLen, output, 0);
		if (len == output.length)
			out = output;
		else {
			out = new byte[len];
			System.arraycopy(output, 0, out, 0, len);
		}
		return out;
	}

	int update(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset) {
		int len = this.buffered + inputLen;
		if (this.decrypting)
			len -= this.blockSize;
		len = len > 0 ? len - len % this.blockSize : 0;
		if (len != 0) {
			byte[] in = new byte[len];
			int inputConsumed = len - this.buffered;
			int bufferedConsumed = this.buffered;
			if (inputConsumed < 0) {
				inputConsumed = 0;
				bufferedConsumed = len;
			}
			if (this.buffered != 0)
				System.arraycopy(this.buffer, 0, in, 0, bufferedConsumed);
			if (inputConsumed > 0)
				System.arraycopy(input, inputOffset, in, bufferedConsumed, inputConsumed);
			if (this.decrypting)
				decrypt(in, 0, len, output, outputOffset);
			else
				encrypt(in, 0, len, output, outputOffset);
			inputLen -= inputConsumed;
			inputOffset += inputConsumed;
			outputOffset += len;
			this.buffered -= bufferedConsumed;
			if (this.buffered > 0)
				System.arraycopy(this.buffer, bufferedConsumed, this.buffer, 0, this.buffered);
		}
		if (inputLen > 0)
			System.arraycopy(input, inputOffset, this.buffer, this.buffered, inputLen);
		this.buffered += inputLen;
		return len;
	}

	public final byte[] update1(byte[] input, int inputOffset, int inputLen) {
		if (input == null || inputOffset < 0 || inputLen > input.length - inputOffset || inputLen < 0)
			throw new IllegalArgumentException("Bad arguments");
		if (inputLen == 0)
			return null;
		return update(input, inputOffset, inputLen);
	}
}
