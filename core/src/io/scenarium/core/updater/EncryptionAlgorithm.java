/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.updater;

public interface EncryptionAlgorithm {
	void decryptBlock(byte[] in, int inOff, byte[] out, int outOff);

	void encryptBlock(byte[] in, int inOff, byte[] out, int outOff);

	int getBlockSize();

	boolean isDecrypting();
}
