/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.updater;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.function.Supplier;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

import javax.swing.event.EventListenerList;

import io.scenarium.core.Scenarium;
import io.scenarium.core.internal.LoadPackageStream;
import io.scenarium.core.internal.Log;

public class Updater {
	public static String updatepath = "https://sourceforge.net/projects/revpandore/files/";
	public static String versionjarname = "VersionJar";
	public static String versionexename = "VersionExe";
	public static String softwarejarname = "Scenarium";
	public static String softwareexename = "Scenarium";
	public static String updatekey = null;// "9it9E)t9k9y$oRhNdj$a9Q548$W384(t3)KM3W2j)CFH6q3ZH4)*dmhb";
	public static String entryclasspoint = Scenarium.class.getName();
	public static Supplier<String> versionSupplier = () -> Scenarium.VERSION;
	public static Supplier<String> softwareNameSupplier = () -> "Scenarium";

	private static Updater instance = null;
	private static final String UPDATELAUNCHERFILENAME = "UpdaterLauncher.jar";
	private static final String CRYPTEXT = ".cry";
	private static final String SOFTWAREUPDATEJARNAME = "SoftwareUpdate.jar";
	private static final String SOFTWAREUPDATEEXENAME = "SoftwareUpdate.exe";
	private static Boolean isJarModeLaunchedFromMainSoftware;

	public static synchronized Updater getInstance() {
		if (instance == null)
			instance = new Updater();
		return instance;
	}

	private final EventListenerList listeners = new EventListenerList();

	private String newVersion;

	private Updater() {}

	public void addProgressListener(final ProgressListener listener) {
		this.listeners.add(ProgressListener.class, listener);
	}

	public void cleanFiles() {
		File fileToDelete = new File(UPDATELAUNCHERFILENAME);
		if (fileToDelete.exists())
			fileToDelete.delete();
		fileToDelete = new File(SOFTWAREUPDATEJARNAME);
		if (fileToDelete.exists())
			fileToDelete.delete();
		fileToDelete = new File(SOFTWAREUPDATEEXENAME);
		if (fileToDelete.exists())
			fileToDelete.delete();
	}

	private void fireProgressChanged(float progress) {
		for (ProgressListener listener : this.listeners.getListeners(ProgressListener.class))
			listener.setProgress(progress);
	}

	public String getLastVersion() {
		if (this.newVersion == null)
			try {
				this.newVersion = getVersion(new URL(updatepath + getVersionFileName()));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		return this.newVersion;
	}

	private String getProcFileName() {
		return isJarModeLaunchedFromMainSoftware() == null ? null : (isJarModeLaunchedFromMainSoftware ? softwarejarname : softwareexename) + (updatekey != null ? CRYPTEXT : ".txt");
	}

	private static String getVersion(URL url) {
		URLConnection connection = null;
		try {
			connection = url.openConnection();
			int length = connection.getContentLength();
			if (length == -1)
				throw new IOException("Cannot get the version file");
			try (InputStream input = connection.getInputStream()) {
				StringBuilder out = new StringBuilder();
				Reader in = new InputStreamReader(input, StandardCharsets.UTF_8);
				char[] buffer = new char[1024];
				while (true) {
					int rsz = in.read(buffer, 0, buffer.length);
					if (rsz < 0)
						break;
					out.append(buffer, 0, rsz);
				}
				return out.toString().trim();
			}
		} catch (MalformedURLException e) {
			Log.error("Problème avec l'URL : " + url);
		} catch (IOException e) {
			Log.error(e.getClass().getSimpleName() + ": " + e.getMessage());
		}
		return null;
	}

	private String getVersionFileName() {
		return isJarModeLaunchedFromMainSoftware() == null ? null : (isJarModeLaunchedFromMainSoftware ? versionjarname : versionexename) + ".txt";
	}

	private Boolean isJarModeLaunchedFromMainSoftware() {
		if (isJarModeLaunchedFromMainSoftware != null)
			return isJarModeLaunchedFromMainSoftware;
		try {
			File file = new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
			if (file.isDirectory())
				return null;
			isJarModeLaunchedFromMainSoftware = !file.getName().endsWith(".exe");
			if (isJarModeLaunchedFromMainSoftware && !isLaunchFromMainSoftware(file))
				isJarModeLaunchedFromMainSoftware = null;
			Log.info("" + isJarModeLaunchedFromMainSoftware);
			return isJarModeLaunchedFromMainSoftware;
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static boolean isLaunchFromMainSoftware(File file) {
		try (JarInputStream jarStream = new JarInputStream(new FileInputStream(file))) {
			Manifest mf = jarStream.getManifest();
			jarStream.close();
			String mainClass = mf.getMainAttributes().getValue("Main-Class");
			return mainClass != null && mainClass.equals(entryclasspoint);
		} catch (IOException e) {
			return false;
		}
	}

	public Boolean isNewVersion() {
		getLastVersion();
		if (this.newVersion == null)
			return null;
		try {
			StringTokenizer cvst = new StringTokenizer(versionSupplier.get(), ".");
			StringTokenizer nvst = new StringTokenizer(this.newVersion, ".");
			boolean isNewVersion = false;
			while (cvst.hasMoreElements())
				if (Integer.parseInt(cvst.nextToken()) < Integer.parseInt(nvst.nextToken())) {
					isNewVersion = true;
					break;
				}
			return isNewVersion;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isUpdatable() {
		return isJarModeLaunchedFromMainSoftware() != null;
	}

	public void removeProgressListener(final ProgressListener listener) {
		this.listeners.remove(ProgressListener.class, listener);
	}

	public void restart(File newFile) {
		Log.info("restart Scenarium with new file: " + newFile);
		if (isJarModeLaunchedFromMainSoftware() == null)
			return;
		Path path = Path.of(UPDATELAUNCHERFILENAME);

		try {
			Files.copy(LoadPackageStream.getStream("/" + UPDATELAUNCHERFILENAME), Path.of(UPDATELAUNCHERFILENAME));
			// Exe4j -> et si le gars change le nom du soft?
			// For exe4j: new File(isJarMode ?ScenariumJARNAME : ScenariumEXENAME);
			String currentFile = URLDecoder.decode(getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath(), StandardCharsets.UTF_8);
			Log.info("Exec: " + Arrays.toString(new String[] { "java", "-jar", path.toString(), currentFile, newFile.getAbsolutePath() }));
			Runtime.getRuntime().exec(new String[] { "java", "-jar", path.toString(), currentFile, newFile.getAbsolutePath() });
			System.exit(1);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}

	public File update() throws IOException {
		if (isJarModeLaunchedFromMainSoftware() == null)
			return null;
		try {
			URLConnection connection = new URL(updatepath + getProcFileName()).openConnection();
			long length = connection.getContentLengthLong();
			if (length == -1)
				throw new IOException("Cannot get the Scenarium file");
			File outFile = new File(Scenarium.getWorkspace() + File.separator + (isJarModeLaunchedFromMainSoftware ? SOFTWAREUPDATEJARNAME : SOFTWAREUPDATEEXENAME));
			try (FileOutputStream writeFile = new FileOutputStream(outFile)) {
				try (InputStream is = updatekey != null ? new CryptInputStream(connection.getInputStream(), new BlowFishCrypt(true, updatekey.getBytes())) : connection.getInputStream()) {
					int read;
					long currentBit = 0;
					byte[] buffer = new byte[1024];
					while ((read = is.read(buffer)) > 0) {
						writeFile.write(buffer, 0, read);
						currentBit += read;
						fireProgressChanged(currentBit / (float) length);
					}
					is.close();
					writeFile.flush();

				}
			}
			return outFile;
		} catch (MalformedURLException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
	}
}
