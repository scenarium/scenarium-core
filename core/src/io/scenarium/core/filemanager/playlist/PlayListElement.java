/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.filemanager.playlist;

public class PlayListElement {
	public ScenarioDesc scenarioDesc;
	public int index;

	public PlayListElement(ScenarioDesc scenarioDesc, int index) {
		this.scenarioDesc = scenarioDesc;
		this.index = index;
	}

	@Override
	public String toString() {
		return this.scenarioDesc.scenarioSource.toString();
	}
}
