/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.filemanager;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;

import javax.swing.event.EventListenerList;

import io.beanmanager.BeanManager;
import io.scenarium.core.MainApp;
import io.scenarium.core.Scenarium;
import io.scenarium.core.filemanager.playlist.PlayListManager;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioDescriptor;
import io.scenarium.core.filemanager.scenariomanager.ScenarioException;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;
import io.scenarium.core.internal.Log;
import io.scenarium.core.struct.ScenariumProperties;
import io.scenarium.core.tools.ReadOnlyObservableValue;
import io.scenarium.pluginManager.ModuleManager;

public class DataLoader {
	public static final int SCENARIO = 0;
	public static final int PLAYLIST = 1;
	public static final int UNSUPPORTED = 2;
	private static final List<Supplier<Boolean>> BACKGROUND_LOADING_TASK = Collections.synchronizedList(new ArrayList<Supplier<Boolean>>());

	public static void addToBackgroundloadingtask(Supplier<Boolean> backgroundTask) {
		BACKGROUND_LOADING_TASK.add(backgroundTask);
	}

	public static int getFileType(File file) {
		String fileName = file.getName();
		String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1, file.getName().length());
		if (ScenarioManager.getScenarioType(file) != null)
			return DataLoader.SCENARIO;
		else if (fileExt.equals(PlayListManager.ext))
			return DataLoader.PLAYLIST;
		return DataLoader.UNSUPPORTED;
	}

	private final File configFile;
	private final Object reloadLock;
	public PlayListManager playListManager = new PlayListManager();
	public Integer currentIndexInRecord = 0;
	public int recordCount;
	public HashMap<String, String> language = new HashMap<>();
	private final MainApp mainApp;
	private final EventListenerList listeners = new EventListenerList();
	private Thread synchroThread;

	private Scenario scenario;

	public final List<Object> recentScenario = Collections.synchronizedList(new ArrayList<>());

	public DataLoader(MainApp mainapp) {
		this.configFile = new File(Scenarium.getWorkspace() + File.separator + "ScenariumConfig.txt");
		this.mainApp = mainapp;
		this.reloadLock = new Object();
	}

	public List<Object> getRecentScenario() {
		return this.recentScenario;
	}

	public Scenario getScenario() {
		return this.scenario;
	}

	public boolean goToNextScenario(boolean reverse) {
		Object newScenario = this.playListManager.goToNextScenario(this.scenario.getSource(), reverse);
		if (newScenario == null)
			return false;
		return reload(newScenario, false, false); // Implique un close de l'ancien diagram
	}

	/** Get the scenario name in Absolute path instead of relative of the workspace (store in file in relative).
	 * @param path Basic path of the scenario
	 * @return The path with the full system name if relative input. */
	private static String getScenarioFullPath(String path) {
		return path == null ? null : new File(path).isAbsolute() ? path : Scenarium.getWorkspace() + File.separator + path;
	}

	public Object loadSofwareConfig() throws IOException {
		if (!this.configFile.exists())
			throw new IOException();
		ScenariumProperties pp = ScenariumProperties.get();
		new BeanManager(pp, Scenarium.getWorkspace()).load(this.configFile);
		ArrayList<Object> recentScenario = new ArrayList<>();
		if (pp.getRecentScenarios() != null)
			for (ScenarioDescriptor rs : pp.getRecentScenarios())
				if (rs != null)
					recentScenario.add(ScenarioManager.getSource(rs.sourceType, DataLoader.getScenarioFullPath(rs.scenarioPath)));
		this.recentScenario.addAll(recentScenario);
		Object scenarioSource = null;
		if (pp.getPlayList() != null) {
			for (ScenarioDescriptor ple : pp.getPlayList())
				if (ple != null)
					this.playListManager.addToPlayList(-1, ScenarioManager.getSource(ple.sourceType, DataLoader.getScenarioFullPath(ple.scenarioPath)));
			try {
				scenarioSource = this.playListManager.getPlayList().get(pp.getPlayListIndex());
			} catch (IndexOutOfBoundsException e) {}
		}
		return scenarioSource;
	}

	public boolean reload(Object scenarioSource, boolean resetPlayList, boolean backgroundLoading) {
		synchronized (this.reloadLock) {
			try {
				secureLoad(scenarioSource, resetPlayList, backgroundLoading);
				ReadOnlyObservableValue<Double> progressProperty = this.scenario.getProgressProperty();
				if (progressProperty != null)
					fireLoadingOperationStarted(progressProperty);
				// this.mainApp.loadingOperation(progressProperty);
				return true;
			} catch (LoadingException e) {
				Log.error("Cannot load the scenario: " + scenarioSource + "\nCause: " + e.getMessage());
				return false;
			}
		}
	}

	// @SuppressWarnings("unchecked")
	// public DataLoader clone(Scenario scenario) {
	// DataLoader dataLoader = new DataLoader(mainApp, reloadLock);
	// dataLoader.scenario = scenario;
	// dataLoader.recentScenario.addAll(recentScenario);
	// dataLoader.language = (HashMap<String, String>) language.clone();
	// return dataLoader;
	// }

	public void save(File scenarioFile) throws IOException {
		boolean sync = ScenariumProperties.get().isSynchronization();
		if (sync)
			setSynchronized(false);
		Log.info("save scenario: " + scenarioFile);
		this.scenario.save(scenarioFile);
		if (sync)
			setSynchronized(true);
	}

	public void saveSoftwareConfig() {
		ScenariumProperties pp = ScenariumProperties.get();
		int i = 0;
		ScenarioDescriptor[] recentScenarios = new ScenarioDescriptor[this.recentScenario.size()];
		for (Object rs : this.recentScenario)
			if (rs != null)
				recentScenarios[i++] = ScenarioManager.getDescriptor(rs);
		pp.setRecentScenarios(recentScenarios);
		i = 0;
		ArrayList<Object> pl = this.playListManager.getPlayList();
		ScenarioDescriptor[] playList = new ScenarioDescriptor[pl.size()];
		for (Object sc : pl) {
			if (sc == null)
				continue;
			if (this.scenario != null && pl.equals(this.scenario.getSource()))
				pp.setPlayListIndex(i);
			playList[i++] = ScenarioManager.getDescriptor(sc);
		}
		pp.setPlayList(playList);
		pp.setExternModules(ModuleManager.getExternModules());
		new BeanManager(pp, Scenarium.getWorkspace()).save(this.configFile, false);
	}

	public void secureLoad(Object scenarioSource, boolean resetPlayList, boolean backgroundLoading) throws LoadingException {
		if (scenarioSource == null)
			throw new LoadingException("Scenario source is null");
		if (!(scenarioSource instanceof Class<?>) && !ScenarioManager.isAvailable(scenarioSource))
			throw new LoadingException("Cannot find the scenario : " + ScenarioManager.getPath(scenarioSource));
		try {
			Scenario newScenario = Scenario.buildScenario(scenarioSource);
			if (newScenario == null)
				throw new LoadingException("Cannot find scenario manager for the kind of scenario: " + scenarioSource.getClass());
			// StreamRecorder sr = null;
			if (this.scenario != null) {
				this.scenario.synchroDestroy(); // synchroDestroy puis setScheduler null sinon je me désinscrit pas dans scheduler
				this.scenario.setScheduler(null);
				this.scenario.synchroDestroy();
				this.mainApp.removeToSchedule(this.scenario);
				// sr = scenario.getStreamRecorder();
			}
			Scenario oldScenario = this.scenario;
			this.scenario = newScenario;
			// StreamRecorder _sr = sr;
			newScenario.addLoadListener(() -> {
				if (ScenariumProperties.get().isSynchronization())
					updateSynchro();
				this.mainApp.addToSchedule(this.scenario);
				// newScenario.setStreamRecorder(_sr);
				fireOpened(oldScenario);
			});
			newScenario.load(scenarioSource instanceof Class<?> ? null : scenarioSource, backgroundLoading);
			scenarioSource = this.scenario.getSource();
			if (scenarioSource != null) {
				if (this.recentScenario.contains(scenarioSource))
					this.recentScenario.remove(scenarioSource);
				this.recentScenario.add(0, scenarioSource);
			}
			while (this.recentScenario.size() > 20)
				this.recentScenario.remove(this.recentScenario.size() - 1);
			if (resetPlayList)
				this.playListManager.reset(scenarioSource);
		} catch (IOException | OutOfMemoryError | ScenarioException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			e.printStackTrace();
			throw new LoadingException(ScenarioManager.getErrorMessage(e));
		}
		Iterator<Supplier<Boolean>> it = BACKGROUND_LOADING_TASK.iterator();
		while (it.hasNext()) {
			Boolean needToRemove = it.next().get();
			if (needToRemove != null && needToRemove)
				it.remove();
		}
	}

	public void setSynchronized(boolean synchro) {
		ScenariumProperties scenariumProperties = ScenariumProperties.get();
		if (scenariumProperties.isSynchronization() == synchro)
			return;
		if (synchro)
			updateSynchro();
		else
			this.synchroThread.interrupt();
		scenariumProperties.setSynchronization(synchro);
	}

	private void updateSynchro() {
		if (this.synchroThread != null)
			this.synchroThread.interrupt();
		if (this.scenario == null || !(this.scenario.getSource() instanceof File))
			return;
		this.synchroThread = new Thread(new Runnable() {
			private long timer;

			@Override
			public void run() {
				WatchKey watckKey = null;
				try {
					File scenarioFile = (File) DataLoader.this.scenario.getSource();
					Path myDir = Paths.get(scenarioFile.getParent());
					WatchService watcher = myDir.getFileSystem().newWatchService();
					myDir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);
					this.timer = System.currentTimeMillis();
					while (true) {
						watckKey = watcher.take();
						for (WatchEvent<?> event : watckKey.pollEvents())
							if (scenarioFile.getName().equals(event.context().toString()) && System.currentTimeMillis() - this.timer > 100) {
								Thread.sleep(100);
								this.timer = System.currentTimeMillis();
								new Thread(() -> reload(scenarioFile, false, true)).start();
								return;
							}
						watckKey.reset();
					}
				} catch (InterruptedException e) {
					if (watckKey != null)
						watckKey.cancel();
				} catch (IOException e) {
					Log.error("Cannot synchronized the scenario: " + e.toString());
				}
			}
		});
		this.synchroThread.start();
	}

	public void addLoadingOperationStartListener(LoadingOperationStartListener listener) {
		this.listeners.add(LoadingOperationStartListener.class, listener);
	}

	public void removeLoadingOperationStartListener(LoadingOperationStartListener listener) {
		this.listeners.remove(LoadingOperationStartListener.class, listener);
	}

	private void fireLoadingOperationStarted(ReadOnlyObservableValue<Double> progressProperty) {
		for (LoadingOperationStartListener listener : this.listeners.getListeners(LoadingOperationStartListener.class))
			listener.loadingOperationStarted(progressProperty);
	}

	public void addOpenListener(OpenListener listener) {
		this.listeners.add(OpenListener.class, listener);
	}

	public void removeOpenListener(OpenListener listener) {
		this.listeners.remove(OpenListener.class, listener);
	}

	private void fireOpened(Scenario oldScenario) {
		for (OpenListener listener : this.listeners.getListeners(OpenListener.class))
			listener.opened(oldScenario, this.scenario);
	}
}
