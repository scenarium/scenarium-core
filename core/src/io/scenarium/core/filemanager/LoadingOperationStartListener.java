package io.scenarium.core.filemanager;

import java.util.EventListener;

import io.scenarium.core.tools.ReadOnlyObservableValue;

@FunctionalInterface
public interface LoadingOperationStartListener extends EventListener {
	public void loadingOperationStarted(ReadOnlyObservableValue<Double> progressProperty);
}
