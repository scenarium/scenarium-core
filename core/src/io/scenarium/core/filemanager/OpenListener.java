/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.filemanager;

import java.util.EventListener;

import io.scenarium.core.filemanager.scenariomanager.Scenario;

@FunctionalInterface
public interface OpenListener extends EventListener {
	void opened(Scenario oldScenario, Scenario scenario);
}
