package io.scenarium.core.filemanager.scenariomanager;

import java.util.EventListener;

@FunctionalInterface
public interface RecordingListener extends EventListener {
	public void recordingPropertyChanged(boolean recording);
}
