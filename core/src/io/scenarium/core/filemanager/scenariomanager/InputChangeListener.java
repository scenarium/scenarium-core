package io.scenarium.core.filemanager.scenariomanager;

import java.util.EventListener;

public interface InputChangeListener extends EventListener {
	void inputChanged(String[] names, Class<?>[] types);
}
