/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.filemanager.scenariomanager;

public class ScenarioDescriptor {
	public final String sourceType;
	public final String scenarioPath;

	public ScenarioDescriptor(String sourceType, String scenarioPath) {
		this.sourceType = sourceType;
		this.scenarioPath = scenarioPath;
	}
}
