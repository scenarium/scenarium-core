/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.filemanager.scenariomanager;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;

import javax.swing.event.EventListenerList;

import io.beanmanager.BeanPropertiesInheritanceLimit;
import io.beanmanager.editors.DynamicVisibleBean;
import io.beanmanager.editors.TransientProperty;
import io.scenarium.core.internal.Log;
import io.scenarium.core.struct.BufferedStrategy;
import io.scenarium.core.timescheduler.Schedulable;
import io.scenarium.core.timescheduler.SchedulerInterface;
import io.scenarium.core.tools.ObservableValue;
import io.scenarium.core.tools.ReadOnlyObservableValue;

@BeanPropertiesInheritanceLimit
public abstract class Scenario implements Schedulable, DynamicVisibleBean {
	// public static final String MINUTE_PATTERN = "ss.SSS";
	// public static final String HOUR_PATTERN = "mm:ss.SSS";
	public static final String DAY_PATTERN = "HH:mm:ss.SSS";
	public static final String DATE_PATTERN = "yyyy-MM-dd " + DAY_PATTERN;
	public static final int LOADED = 0;
	public static final int SCHEDULED = 1;
	protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	protected ReadOnlyObservableValue<Double> progressProperty;
	private final EventListenerList listeners = new EventListenerList();
	protected Object scenarioData;
	@TransientProperty
	protected SchedulerInterface schedulerInterface;
	// private TheaterPanel theaterPanel;
	private boolean isManagedScenario = true;

	protected ScenarioTrigger st;
	@TransientProperty
	private int id = -1;

	@SuppressWarnings("unchecked")
	public static Scenario buildScenario(Object source)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Scenario scenario = null;
		if (source instanceof Class<?>)
			scenario = ((Class<? extends LocalScenario>) source).getConstructor().newInstance();
		else {
			Class<? extends Scenario> scenarioType = ScenarioManager.getScenarioType(source);
			if (scenarioType == null)
				return null;
			scenario = scenarioType.getConstructor().newInstance();
			scenario.setSource(source);
		}
		scenario.setManagedScenario(false);
		return scenario;
	}

	public void addLoadListener(LoadListener listener) {
		this.listeners.add(LoadListener.class, listener);
	}

	public void addScheduleChangeListener(ScheduleChangeListener listener) {
		this.listeners.add(ScheduleChangeListener.class, listener);
	}

	public void addSourceChangeListener(SourceChangeListener listener) {
		this.listeners.add(SourceChangeListener.class, listener);
	}

	public void addSourceChangeListenerIfAbsent(SourceChangeListener listener) {
		for (SourceChangeListener l : this.listeners.getListeners(SourceChangeListener.class))
			if (l == listener)
				return;
		addSourceChangeListener(listener);
	}

	public boolean canModulateSpeed() {
		return true;
	}

	public boolean canReverse() {
		return true;
	}

	/** Close all resources (File, stream...) linked to the scenario
	 * @return true if no error during close */
	protected abstract boolean close();

	/** Method called before its dereferencing by Scenarium. It can however still be referenced as a bean by BeanManager. Calls the {@link #close() close} method by default. Can be override if needed,
	 * e.g. to remove listeners.
	 * @return true if no error during dispose */
	public boolean dispose() {
		return close();
	}

	/** Method called if this scenario is used as an operator and is therefore linked to a block. */
	public void birth() throws IOException, ScenarioException {}

	/** Method called if this scenario is used as an operator and is therefore linked to a block. It ensure that the scenario is {@link #close() closed} and set to null scenarioData. */
	public void death() {
		close();
		this.scenarioData = null;
	}

	protected void fireLoadChanged() {
		if (this.progressProperty != null) {
			((ObservableValue<Double>) this.progressProperty).setValue(1.0);
			this.progressProperty = null;
		}
		for (LoadListener listener : this.listeners.getListeners(LoadListener.class))
			listener.scenarioLoaded();
	}

	protected void fireScheduleChanged() {
		for (ScheduleChangeListener listener : this.listeners.getListeners(ScheduleChangeListener.class))
			listener.scheduleChange();
	}

	protected void fireSourceChanged(boolean planChanged) {
		for (SourceChangeListener listener : this.listeners.getListeners(SourceChangeListener.class))
			listener.sourceChanged(planChanged);
	}

	public abstract long getBeginningTime();

	public abstract long getEndTime();

	public abstract Date getStartTime();

	public abstract Date getStopTime();

	public abstract Class<?> getDataType();

	public LinkedHashMap<String, String> getInfo() throws IOException {
		LinkedHashMap<String, String> info = new LinkedHashMap<>();
		getInfo(info);
		return info;
	}

	public abstract void getInfo(LinkedHashMap<String, String> info) throws IOException;

	public ReadOnlyObservableValue<Double> getProgressProperty() {
		return this.progressProperty;
	}

	public abstract String[] getReaderFormatNames();

	public Object getScenarioData() {
		return this.scenarioData;
	}

	public SchedulerInterface getSchedulerInterface() {
		return this.schedulerInterface;
	}

	public abstract int getSchedulerType();

	@TransientProperty
	public abstract Object getSource();

	public Schedulable getTaskFromId(int id) {
		return this;
	}

	public int getTaskId(Schedulable task) {
		return 0;
	}

	public abstract void initOrdo();

	public void updateIOStructure() {
		Class<?> type = getDataType();
		if (type != null)
			fireOutputChanged(new String[] { type.getSimpleName() }, new Class<?>[] { type });
		else
			fireOutputChanged(new String[] {}, new Class<?>[] {});
	}

	public boolean isPaintableProperties() {
		return false;
	}

	public boolean isStreamScenario() {
		return this instanceof StreamScenario;
	}

	// protected abstract boolean isTimeRepresentation();

	public abstract void load(Object scenarioSource, boolean backgroundLoading) throws IOException, ScenarioException;

	public abstract void populateInfo(LinkedHashMap<String, String> info) throws IOException;

	public abstract void process(Long timePointer) throws Exception;

	protected void reload() {
		try {
			synchroLoad(getSource(), false);
			// fireLoaded();
		} catch (IOException | ScenarioException e) {
			e.printStackTrace();
		}
	}

	public void removeLoadListener(LoadListener listener) {
		this.listeners.remove(LoadListener.class, listener);
	}

	public void removeScheduleChangeListener(ScheduleChangeListener listener) {
		this.listeners.remove(ScheduleChangeListener.class, listener);
	}

	public void removeSourceChangeListener(SourceChangeListener listener) {
		this.listeners.remove(SourceChangeListener.class, listener);
	}

	public abstract void save(File file) throws IOException;

	public void setScheduler(SchedulerInterface schedulerInterface) {
		this.schedulerInterface = schedulerInterface;
		if (schedulerInterface != null)
			initOrdo();
	}

	public abstract void setSource(Object source);

	// public Scheduler getScheduler() {
	// return scheduler;
	// }

	// public void setTheaterPanel(TheaterPanel theaterPanel) {
	// this.theaterPanel = theaterPanel;
	// }

	public boolean stop() {
		return this.schedulerInterface.stop();
	}

	public void synchroDestroy() {
		Callable<Boolean> closeTask = () -> {
			synchronized (this) {
				return dispose();
			}
		};
		try {
			if (this.schedulerInterface != null) {
				this.schedulerInterface.synchronisedCall(closeTask); // Il ne faut pas que le scheduler s'arrête pendant le rechargement
				this.schedulerInterface.removeScheduleElement(this);
			} else
				closeTask.call();
		} catch (Exception e) {} // Impossible normalement

	}

	public void synchroLoad(Object scenarioSource, boolean backgroundLoading) throws IOException, ScenarioException {
		Callable<Boolean> loadTask = () -> {
			synchronized (this) {
				load(scenarioSource, backgroundLoading);
				return true;
			}
		};
		try {
			if (this.schedulerInterface != null)
				this.schedulerInterface.synchronisedCall(loadTask);
			else
				loadTask.call();
		} catch (IOException | ScenarioException e) {
			throw e;
		} catch (Exception e) {} // Impossible normalement

	}

	private boolean synchroUpdate(long timePointer) {
		Object oldScenarioData = this.scenarioData;
		try {
			process(timePointer);
			if (this.scenarioData instanceof BufferedStrategy)
				((BufferedStrategy<?>) this.scenarioData).flip();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		fireUpdated(oldScenarioData != this.scenarioData);
		return true;
	}

	public void setTrigger(ScenarioTrigger st) {
		this.st = st;
	}

	@Override
	public synchronized void update(long timePointer) {
		if (this.scenarioData == null) {
			if (this.st == null || this.st.isAlive())
				Log.error("no scenario data for " + this);
			return;
		}
		boolean success;
		if (this.scenarioData instanceof BufferedStrategy<?>) {
			BufferedStrategy<?> rasterStrategy = (BufferedStrategy<?>) this.scenarioData;
			if (rasterStrategy.isPageFlipping())
				success = synchroUpdate(timePointer);
			else
				synchronized (rasterStrategy.getDrawElement()) {
					success = synchroUpdate(timePointer);
				}
		} else
			synchronized (this.scenarioData) {
				success = synchroUpdate(timePointer);
			}
		if (success && this.st != null && this.st.isActive())
			this.st.triggerOutput(new Object[] { this.scenarioData instanceof BufferedStrategy<?> ? ((BufferedStrategy<?>) this.scenarioData).getDrawElement() : this.scenarioData },
					this.schedulerInterface.getTimeStamp());
	}

	public String getTimePattern() {
		LocalDateTime endDate = Instant.ofEpochMilli(getEndTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
		LocalDateTime beginningDate = Instant.ofEpochMilli(getBeginningTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
		if (!endDate.truncatedTo(ChronoUnit.DAYS).equals(beginningDate.truncatedTo(ChronoUnit.DAYS)))
			return DATE_PATTERN;
		return /* getEndTime() < 1000 * 60 ? MINUTE_PATTERN : getEndTime() < 1000 * 60 * 60 ? HOUR_PATTERN : */DAY_PATTERN;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		if (this.id != id) {
			this.id = id;
			fireIdChanged(id);
		}
	}

	public boolean canRecord() {
		return false;
	}

	public boolean isRecording() {
		return false;
	}

	public void setRecording(boolean recording) {}

	@Override
	public void setVisible() {
		fireSetPropertyVisible(this, "recording", canRecord());
		fireSetPropertyVisible(this, "file", this.isManagedScenario);
	}

	public void addRecordingListener(RecordingListener listener) {
		this.listeners.add(RecordingListener.class, listener);
	}

	public void removeRecordingListener(RecordingListener listener) {
		this.listeners.remove(RecordingListener.class, listener);
	}

	protected void fireRecordingChanged(boolean recording) {
		for (RecordingListener listener : this.listeners.getListeners(RecordingListener.class))
			listener.recordingPropertyChanged(recording);
	}

	@Override
	public boolean canTrigger(long time) {
		return this.scenarioData != null;
	}

	public void addOutputChangeListener(OutputChangeListener listener) {
		this.listeners.add(OutputChangeListener.class, listener);
	}

	public void removeOutputChangeListener(OutputChangeListener listener) {
		this.listeners.remove(OutputChangeListener.class, listener);
	}

	protected void fireOutputChanged(String[] names, Class<?>[] types) {
		for (OutputChangeListener listener : this.listeners.getListeners(OutputChangeListener.class))
			listener.outputChanged(names, types);
	}

	public void addInputChangeListener(InputChangeListener listener) {
		this.listeners.add(InputChangeListener.class, listener);
	}

	public void removeInputChangeListener(InputChangeListener listener) {
		this.listeners.remove(InputChangeListener.class, listener);
	}

	protected void fireInputChanged(String[] names, Class<?>[] types) {
		for (InputChangeListener listener : this.listeners.getListeners(InputChangeListener.class))
			listener.inputChanged(names, types);
	}

	public boolean isManagedScenario() {
		return this.isManagedScenario;
	}

	private void setManagedScenario(boolean isManagedScenario) {
		this.isManagedScenario = isManagedScenario;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	public void addIdChangeListener(IdChangeListener listener) {
		this.listeners.add(IdChangeListener.class, listener);
	}

	public void removeIdChangeListener(IdChangeListener listener) {
		this.listeners.remove(IdChangeListener.class, listener);
	}

	private void fireIdChanged(int id) {
		for (IdChangeListener listener : this.listeners.getListeners(IdChangeListener.class))
			listener.idChanged(id);
	}

	public void addUpdateListener(UpdateListener listener) {
		this.listeners.add(UpdateListener.class, listener);
	}

	public void removeUpdateListener(UpdateListener listener) {
		this.listeners.remove(UpdateListener.class, listener);
	}

	private void fireUpdated(boolean hasChanged) {
		for (UpdateListener listener : this.listeners.getListeners(UpdateListener.class))
			listener.updated(hasChanged);
	}
}
