/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.filemanager.scenariomanager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;

import javax.swing.event.EventListenerList;

import io.scenarium.core.Scenarium;

public class ScenarioManager {
	private static final HashMap<String, Class<? extends LocalScenario>> REGISTERED_LOCAL_SCENARIOS = new HashMap<>();
	private static final HashSet<Class<? extends StreamScenario>> REGISTERED_STREAM_SCENARIOS = new HashSet<>();
	private static final EventListenerList SCENARIO_LISTENERS = new EventListenerList();

	private ScenarioManager() {}

	public static boolean registerScenario(Class<? extends Scenario> scenarioClass) {
		boolean added = false;
		if (LocalScenario.class.isAssignableFrom(scenarioClass)) {
			Class<? extends LocalScenario> lsc = scenarioClass.asSubclass(LocalScenario.class);
			String[] extensions = getExtensions(lsc);
			if (extensions == null)
				return false;
			for (String ext : extensions)
				added |= REGISTERED_LOCAL_SCENARIOS.putIfAbsent(ext.toLowerCase(), lsc) == null;
		} else
			added = REGISTERED_STREAM_SCENARIOS.add(scenarioClass.asSubclass(StreamScenario.class));
		if (added)
			fireScenarioLoaded(scenarioClass);
		return added;
	}

	private static String[] getExtensions(Class<? extends LocalScenario> c) {
		try {
			return c.getConstructor().newInstance().getReaderFormatNames();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void replaceScenario(Class<? extends LocalScenario> scenarioClass) {
		Class<? extends LocalScenario> lsc = scenarioClass.asSubclass(LocalScenario.class);
		String[] extensions = getExtensions(lsc);
		if (extensions != null) {
			for (String ext : extensions)
				REGISTERED_LOCAL_SCENARIOS.put(ext.toLowerCase(), lsc);
			fireScenarioLoaded(scenarioClass);
		}
	}

	public static void purgeScenarios(Module module) {
		for (Iterator<Class<? extends LocalScenario>> iterator = REGISTERED_LOCAL_SCENARIOS.values().iterator(); iterator.hasNext();) {
			Class<? extends LocalScenario> scenario = iterator.next();
			if (scenario.getModule().equals(module)) {
				iterator.remove();
				fireScenarioUnloaded(scenario);
			}
		}
		for (Iterator<Class<? extends StreamScenario>> iterator = REGISTERED_STREAM_SCENARIOS.iterator(); iterator.hasNext();) {
			Class<? extends StreamScenario> scenario = iterator.next();
			if (scenario.getModule().equals(module)) {
				iterator.remove();
				fireScenarioUnloaded(scenario);
			}
		}
	}

	/** Get the first scenario to start scenarium GUI TODO. this is BAD and we need to add a luncher interface with managing the workspace and the scenario possibilities
	 * @return a scenario class or null */
	public static Optional<Class<? extends LocalScenario>> getFirstRunableScenario() {
		return REGISTERED_LOCAL_SCENARIOS.values().stream().filter(c -> {
			try {
				return LocalScenario.class.isAssignableFrom(c) && ((LocalScenario) c.getConstructor().newInstance()).canRecord();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			return false;
		}).findFirst();
	}

	public static void addLoadScenarioListener(LoadScenarioListener listener) {
		SCENARIO_LISTENERS.add(LoadScenarioListener.class, listener);
	}

	public static void removeLoadScenarioListener(LoadScenarioListener listener) {
		SCENARIO_LISTENERS.remove(LoadScenarioListener.class, listener);
	}

	private static void fireScenarioLoaded(Class<? extends Scenario> scenarioClass) {
		for (LoadScenarioListener listener : SCENARIO_LISTENERS.getListeners(LoadScenarioListener.class))
			listener.loaded(scenarioClass);
	}

	private static void fireScenarioUnloaded(Class<? extends Scenario> scenarioClass) {
		for (LoadScenarioListener listener : SCENARIO_LISTENERS.getListeners(LoadScenarioListener.class))
			listener.unloaded(scenarioClass);
	}

	public static ScenarioDescriptor getDescriptor(Object sc) {
		Class<?> srcType = getSourceType(sc);
		String pathOut;
		// Create the path relative to the current workspace if possible.
		if (srcType.equals(File.class) && sc != null) {
			Path path = ((File) sc).toPath();
			Path dir = Path.of(Scenarium.getWorkspace());
			if (path.startsWith(dir))
				path = dir.relativize(path);
			pathOut = path.toString();
		} else
			pathOut = getPath(sc);
		return new ScenarioDescriptor(srcType.getSimpleName(), pathOut);
	}

	public static File getDescriptorFromScenario(File scenarioDescriptor) {
		String fileName = scenarioDescriptor.getName();
		String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1, scenarioDescriptor.getName().length());
		if (fileExt.equals("raw")) {
			File infFile = new File(scenarioDescriptor.getAbsolutePath().substring(0, scenarioDescriptor.getAbsolutePath().length() - 3).concat("inf"));
			return infFile.exists() ? infFile : null;
		} else if (fileExt.equals("inf")) {
			File rawFile = new File(scenarioDescriptor.getAbsolutePath().substring(0, scenarioDescriptor.getAbsolutePath().length() - 3).concat("raw"));
			return rawFile.exists() ? scenarioDescriptor : null;
		} else
			return scenarioDescriptor;
	}

	public static String getErrorMessage(Throwable e) {
		if (e instanceof FileNotFoundException)
			return "Missing file: " + e.getMessage();
		else if (e instanceof IOException)
			return "Corrupt scenario file: " + e.getMessage();
		else if (e instanceof OutOfMemoryError)
			return "Not enough memory for the scenario: ";
		else if (e instanceof ScenarioException)
			return "Scenario format error: " + e.getMessage();
		else if (e instanceof InstantiationException)
			return "Cannot instantiate the scenario: " + e.getMessage();
		else if (e instanceof IllegalAccessException)
			return "Cannot access to the scenario: " + e.getMessage();
		return null;
	}

	public static int getLenght(Object source) {
		if (source instanceof File)
			return LocalScenario.getScenarioLenght((File) source);
		else if (source instanceof StreamScenarioSource)
			return -1;
		return 0;
	}

	public static String getPath(Object source) {
		if (source instanceof File)
			return LocalScenario.getScenarioPath((File) source);
		else if (source instanceof StreamScenarioSource)
			return StreamScenario.getScenarioPath((StreamScenarioSource) source);
		return null;
	}

	public static String[] getReaderFormatNames() {
		ArrayList<String> readerFormatNames = new ArrayList<>();
		REGISTERED_LOCAL_SCENARIOS.values().stream().distinct().forEach(sc -> {
			try {
				String[] formats = sc.getConstructor().newInstance().getReaderFormatNames();
				StringBuilder rf = new StringBuilder(sc.getSimpleName());
				for (String format : formats)
					rf.append(" " + format);
				readerFormatNames.add(rf.toString());
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		});
		return readerFormatNames.toArray(String[]::new);
	}

	public static HashSet<Class<? extends Scenario>> getRegisterScenario() {
		return new HashSet<>(REGISTERED_LOCAL_SCENARIOS.values());
	}

	public static Class<? extends Scenario> getScenarioType(Object source) {
		if (source instanceof File) {
			String ap = ((File) source).getAbsolutePath();
			return REGISTERED_LOCAL_SCENARIOS.get(ap.substring(ap.lastIndexOf(".") + 1).toLowerCase());
		} else if (source instanceof StreamScenarioSource)
			return ((StreamScenarioSource) source).type;
		return null;
	}

	// TODO to remove
	public static Object getSource(String type, String value) {
		Object scenarioSource = LocalScenario.getScenarioSource(type, value);
		if (scenarioSource == null)
			scenarioSource = StreamScenario.getScenarioSource(type, value);
		return scenarioSource;
	}

	public static Class<?> getSourceType(Object source) {
		if (source instanceof File)
			return File.class;
		else if (source instanceof StreamScenarioSource)
			return ((StreamScenarioSource) source).type;
		return null;
	}

	public static HashSet<Class<? extends StreamScenario>> getStreamScenario() {
		HashSet<Class<? extends StreamScenario>> streamScenarios = new HashSet<>();
		for (Class<? extends StreamScenario> type : REGISTERED_STREAM_SCENARIOS)
			if (StreamScenario.class.isAssignableFrom(type))
				streamScenarios.add(type.asSubclass(StreamScenario.class));
		return streamScenarios;
	}

	public static boolean isAvailable(Object source) {
		if (source instanceof File)
			return LocalScenario.isAvailable((File) source);
		else if (source instanceof StreamScenarioSource)
			return StreamScenario.isAvailable((StreamScenarioSource) source);
		return false;
	}
}
