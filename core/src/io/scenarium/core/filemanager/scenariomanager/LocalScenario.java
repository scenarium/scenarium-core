/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.filemanager.scenariomanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Objects;

import javax.swing.event.EventListenerList;

import io.beanmanager.editors.DynamicAnnotationBean;
import io.beanmanager.editors.TransientProperty;
import io.scenarium.core.internal.Log;

public abstract class LocalScenario extends Scenario implements DynamicAnnotationBean {
	@TransientProperty
	protected File file;
	protected final EventListenerList listeners = new EventListenerList();

	public LocalScenario() {}

	@Override
	public void birth() throws IOException, ScenarioException {
		if (this.file != null)
			load((Object) this.file, false);
	}

	public abstract boolean canCreateDefault();

	public abstract double getPeriod();

	public File getFile() {
		return this.file;
	}

	public String[] getFilters() {
		String[] rf = getReaderFormatNames();
		StringBuilder sb = new StringBuilder(getClass().getSimpleName());
		for (int i = 0; i < rf.length; i++)
			sb.append(" " + rf[i]);
		return new String[] { sb.toString() };
	}

	@Override
	public boolean hasSomeThingsToPlan() {
		return this.file != null && this.file.exists();
	}

	@Override
	public void getInfo(LinkedHashMap<String, String> info) throws IOException {
		info.put("Type of scenario", "Local-" + getClass().getSimpleName());
		populateInfo(info);
		File scenarioFile = getFile();
		String fileName = scenarioFile.getName();
		info.put("File type", fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase());
		info.put("File path", scenarioFile.getParent());
		info.put("File Name", fileName);
		info.put("File size", NumberFormat.getInstance().format(getFile().length()));
		info.put("Last Modified", new SimpleDateFormat("dd/MM/YYYY, kk:mm:ss").format(new Date(scenarioFile.lastModified())));
	}

	@Override
	public Object getSource() {
		return getFile();
	}

	@Override
	public void initOrdo() {}

	public abstract void load(File scenarioFile, boolean backgroundLoading) throws IOException, ScenarioException;

	@Override
	public synchronized void load(Object scenarioSource, boolean backgroundLoading) throws IOException, ScenarioException {
		// if(scenarioSource == null)
		// return;
		if (scenarioSource == null || ((File) scenarioSource).exists()) {
			load((File) scenarioSource, backgroundLoading);
			if (this.scenarioData == null && !backgroundLoading)
				Log.error("Error while loading the scenario source: '" + scenarioSource + "'\nThe loader associated: '" + getClass().getSimpleName() + "' didn't return a scenarioData");
		}
		// if (!backgroundLoading) //scenarioData != null ne pas faire l'ancienne version sinon 2 event loadchanged si chargement rapide
		// fireLoadChanged();
		fireAnnotationChanged(this, "startTime");
		fireAnnotationChanged(this, "stopTime");
	}

	public void addPeriodChangeListener(PeriodListener listener) {
		this.listeners.add(PeriodListener.class, listener);
	}

	public void addPeriodChangeListenerIfAbsent(PeriodListener listener) {
		for (PeriodListener l : this.listeners.getListeners(PeriodListener.class))
			if (l == listener)
				return;
		addPeriodChangeListener(listener);
	}

	public void removePeriodChangeListener(PeriodListener listener) {
		this.listeners.remove(PeriodListener.class, listener);
	}

	public void setFile(File file) {
		if (Objects.equals(file, this.file))
			return;
		boolean hadSomeThingsToPlan = hasSomeThingsToPlan();
		if (file != null) {
			String fileName = file.getName();
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			if (!Arrays.asList(getReaderFormatNames()).contains(fileExt))
				if (fileName.length() != 0)
					Log.error(getClass().getSimpleName() + " reader cannot take " + fileExt + " file");
		}
		this.file = file;
		boolean planChanged = hadSomeThingsToPlan | hasSomeThingsToPlan();
		try {
			updateIOStructure();
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Cannot init the struct, " + file.getName());
		}
		fireSourceChanged(planChanged);
	}

	public void addStartStopChangeListener(StartStopChangeListener listener) {
		this.listeners.add(StartStopChangeListener.class, listener);
	}

	public void addStartStopChangeListenerIfAbsent(StartStopChangeListener listener) {
		for (StartStopChangeListener l : this.listeners.getListeners(StartStopChangeListener.class))
			if (l == listener)
				return;
		addStartStopChangeListener(listener);
	}

	public void removeStartStopChangeListener(StartStopChangeListener listener) {
		this.listeners.remove(StartStopChangeListener.class, listener);
	}

	protected void fireStartStopTimeChanged() {
		for (StartStopChangeListener listener : this.listeners.getListeners(StartStopChangeListener.class))
			listener.startStopChanged();
	}

	@Override
	public void setSource(Object source) {
		setFile((File) source);
	}

	@Override
	public String toString() {
		return this.file != null ? this.file.getName().toString() : "no file";
	}

	public static int getScenarioLenght(File source) {
		String ext = source.getAbsolutePath().substring(source.getAbsolutePath().length() - 3, source.getAbsolutePath().length());
		if (ext.equals("inf")) {
			String ligne;
			try (BufferedReader br = new BufferedReader(new FileReader(source))) {
				ligne = br.readLine();
				return Integer.parseInt(ligne.substring(0, ligne.indexOf(" ")).trim());
			} catch (IOException | NumberFormatException | StringIndexOutOfBoundsException e) {
				return 1;
			}
		}
		return 1;
	}

	public static String getScenarioPath(File source) {
		return source.getAbsolutePath();
	}

	public static Object getScenarioSource(String type, String value) {
		if (type.equals(File.class.getSimpleName()))
			return new File(value);
		return null;
	}

	public static boolean isAvailable(File source) {
		return source.exists();
	}

}
