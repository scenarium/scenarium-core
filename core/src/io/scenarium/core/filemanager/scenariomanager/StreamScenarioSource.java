/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.filemanager.scenariomanager;

import java.net.InetSocketAddress;
import java.util.Objects;

public class StreamScenarioSource {
	public Class<? extends StreamScenario> type;
	public InetSocketAddress isa;

	public StreamScenarioSource(Class<? extends StreamScenario> type, InetSocketAddress isa) {
		this.type = type;
		this.isa = isa;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof StreamScenarioSource)
			return ((StreamScenarioSource) obj).type == this.type && this.isa.equals(((StreamScenarioSource) obj).isa);
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.type, this.isa);
	}

	@Override
	public String toString() {
		return this.type.getSimpleName() + " : " + (this.isa == null ? "" : this.isa.toString());
	}
}
