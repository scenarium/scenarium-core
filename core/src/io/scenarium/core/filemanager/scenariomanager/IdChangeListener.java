package io.scenarium.core.filemanager.scenariomanager;

import java.util.EventListener;

public interface IdChangeListener extends EventListener {
	void idChanged(int id);
}