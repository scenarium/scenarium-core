package io.scenarium.core.filemanager.scenariomanager;

import java.util.EventListener;

public interface UpdateListener extends EventListener {
	public void updated(boolean hasChanged);
}
