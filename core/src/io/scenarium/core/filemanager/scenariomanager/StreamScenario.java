/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.filemanager.scenariomanager;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.LinkedHashMap;

import io.beanmanager.editors.basic.InetSocketAddressEditor;

public abstract class StreamScenario extends Scenario {
	public static String getScenarioPath(StreamScenarioSource streamScenarioSource) {
		InetSocketAddressEditor isae = new InetSocketAddressEditor();
		isae.setValue(streamScenarioSource.isa);
		return isae.getAsText();
	}

	public static Object getScenarioSource(String type, String value) {
		for (Class<? extends StreamScenario> streamScenario : ScenarioManager.getStreamScenario())
			if (streamScenario.getSimpleName().equals(type)) {
				InetSocketAddressEditor isae = new InetSocketAddressEditor();
				isae.setAsText(value);
				return new StreamScenarioSource(streamScenario, isae.getValue());
			}
		return null;
	}

	public static boolean isAvailable(StreamScenarioSource source) {
		return true;
	}

	@Override
	public boolean hasSomeThingsToPlan() {
		return false;
	}

	protected InetSocketAddress inetSocketAddress;

	protected boolean needToFitViewer = false;

	@Override
	public void birth() throws IOException, ScenarioException {
		if (this.inetSocketAddress != null)
			load(this.inetSocketAddress, false);
	}

	@Override
	public long getBeginningTime() {
		return 0;
	}

	@Override
	public long getEndTime() {
		return -1;
	}

	public InetSocketAddress getInetSocketAddress() {
		return this.inetSocketAddress;
	}

	@Override
	public void getInfo(LinkedHashMap<String, String> info) throws IOException {
		info.put("Type of scenario", "Stream");
		info.put("Type of stream", getClass().getSimpleName());
		InetSocketAddressEditor isae = new InetSocketAddressEditor();
		isae.setValue(this.inetSocketAddress);
		info.put("Adresse réseau", isae.getAsText());
		populateInfo(info);
	}

	@Override
	public Object getSource() {
		return new StreamScenarioSource(getType(), this.inetSocketAddress);
	}

	public abstract Class<? extends StreamScenario> getType();

	public boolean isFirstTime() {
		return this.needToFitViewer;
	}

	public abstract void load(InetSocketAddress scenarioAdd, boolean backgroundLoading) throws IOException, ScenarioException;

	@Override
	public void load(Object scenarioSource, boolean backgroundLoading) throws IOException, ScenarioException {
		load(((StreamScenarioSource) scenarioSource).isa, backgroundLoading);
		// fireLoaded();
	}

	public void setInetSocketAddress(InetSocketAddress inetSocketAddress) {
		this.inetSocketAddress = inetSocketAddress;
		// reload();
	}

	@Override
	public void setSource(Object source) {
		setInetSocketAddress(((StreamScenarioSource) source).isa);
	}

	@Override
	public String toString() {
		return new StreamScenarioSource(getType(), this.inetSocketAddress).toString();
	}
}
