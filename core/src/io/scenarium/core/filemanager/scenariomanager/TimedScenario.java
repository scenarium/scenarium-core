/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.filemanager.scenariomanager;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.beanmanager.editors.time.DynamicDateInfo;
import io.scenarium.core.editors.NotChangeableAtRuntime;
import io.scenarium.core.internal.Log;

public abstract class TimedScenario extends LocalScenario {
	@PropertyInfo(index = 1)
	@NumberInfo(min = 1)
	@NotChangeableAtRuntime
	private double period = 40.0;
	@PropertyInfo(index = 2)
	@NotChangeableAtRuntime
	@DynamicDateInfo(minMethodName = "getMinStartTime", maxMethodName = "getMaxStartTime", timePatternMethodName = "getTimePattern")
	private Long startTime = null;
	@PropertyInfo(index = 3)
	@DynamicDateInfo(minMethodName = "getMinStopTime", maxMethodName = "getMaxStopTime", timePatternMethodName = "getTimePattern")
	@NotChangeableAtRuntime
	private Long stopTime = null;

	private long testedStartTime = Long.MAX_VALUE;
	private long testedStopTime = Long.MIN_VALUE;

	@Override
	public void birth() throws IOException, ScenarioException {
		super.birth();
		if (this.scenarioData == null) {
			Log.info("No scenarioData " + ProcessHandle.current().pid());
			this.testedStartTime = Long.MAX_VALUE;
			this.testedStopTime = Long.MIN_VALUE;
		} else {
			this.testedStartTime = this.startTime == null ? Long.MIN_VALUE : this.startTime;
			this.testedStopTime = this.stopTime == null ? Long.MAX_VALUE : this.stopTime;
		}
	}

	@Override
	public void setFile(File file) {
		this.testedStartTime = Long.MAX_VALUE;
		this.testedStopTime = Long.MIN_VALUE;
		super.setFile(file);
		checkStartAndStopTime();
	}

	private void checkStartAndStopTime() {
		if (this.startTime != null && (this.startTime < getMinStartTime() || this.startTime > getMaxStartTime()))
			setStartTime(null);
		if (this.stopTime != null && (this.stopTime < getMinStopTime() || this.stopTime > getMaxStopTime()))
			setStopTime(null);
	}

	@Override
	protected boolean close() {
		this.testedStartTime = Long.MAX_VALUE;
		this.testedStopTime = Long.MIN_VALUE;
		return false;
	}

	public abstract long getNbFrame();

	@Override
	public long getEndTime() {
		return (long) (getBeginningTime() + getNbFrame() * getPeriod());
	}

	@Override
	public double getPeriod() {
		return this.period;
	}

	public void setPeriod(double period) {
		if (this.period != period) {
			double oldPeriod = this.period;
			this.period = period;
			checkStartAndStopTime();
			this.pcs.firePropertyChange("period", oldPeriod, period);
			firePeriodChanged();
		}
	}

	private void firePeriodChanged() {
		for (PeriodListener listener : this.listeners.getListeners(PeriodListener.class))
			listener.periodChanged(this.period);
	}

	@Override
	public Date getStartTime() {
		return this.startTime == null ? null : new Date(this.startTime);
	}

	public Long getMinStartTime() {
		long time = getBeginningTime();
		return time == -1 ? Long.MIN_VALUE : time;
	}

	public Long getMaxStartTime() {
		long time = this.stopTime == null ? getEndTime() : Math.min(getEndTime(), this.stopTime);
		return time <= 0 ? Long.MAX_VALUE : time;
	}

	public void setStartTime(Date startTime) {
		Long oldStartTime = this.startTime;
		this.startTime = startTime == null ? null : startTime.getTime();
		this.pcs.firePropertyChange("startTime", oldStartTime == null ? null : new Date(oldStartTime), startTime);
		fireStartStopTimeChanged();
		fireAnnotationChanged(this, "stopTime");
	}

	@Override
	public Date getStopTime() {
		return this.stopTime == null ? null : new Date(this.stopTime);
	}

	public Long getMinStopTime() {
		return this.startTime == null ? getBeginningTime() : Math.max(getBeginningTime(), this.startTime);
	}

	public Long getMaxStopTime() {
		long time = getEndTime();
		return time == 0 ? Long.MAX_VALUE : time;
	}

	public void setStopTime(Date stopTime) {
		Long oldStopTime = this.stopTime;
		this.stopTime = stopTime == null ? null : stopTime.getTime();
		this.pcs.firePropertyChange("stopTime", oldStopTime == null ? null : new Date(oldStopTime), stopTime);
		fireAnnotationChanged(this, "startTime");
		fireStartStopTimeChanged();
	}

	@Override
	public boolean canTrigger(long time) {
		return time >= this.testedStartTime && time <= this.testedStopTime;
	}
}
