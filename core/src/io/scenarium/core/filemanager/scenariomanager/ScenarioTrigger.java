package io.scenarium.core.filemanager.scenariomanager;

public interface ScenarioTrigger {

	public boolean isAlive();

	public boolean isActive();

	// public boolean hasBlock();

	public boolean triggerOutput(Object[] objects, long timeStamp);

	public Object[] generateOuputsVector();

	public boolean triggerOutput(Object[] ouputsVector, long[] timeStamps);

	public boolean triggerOutput(Object outputValue, long timeStamp);

	public void setWarning(String warning);

	public boolean holdLock();

	public String getName();

	public void runLater(Runnable task);

	public Object[] getAdditionalInputs();

	public long getTimeStamp(int i);
}
