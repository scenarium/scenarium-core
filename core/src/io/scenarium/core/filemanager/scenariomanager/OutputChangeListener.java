package io.scenarium.core.filemanager.scenariomanager;

import java.util.EventListener;

public interface OutputChangeListener extends EventListener {
	void outputChanged(String[] names, Class<?>[] types);
}
