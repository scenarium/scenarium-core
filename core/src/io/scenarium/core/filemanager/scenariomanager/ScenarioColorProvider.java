package io.scenarium.core.filemanager.scenariomanager;

import javax.vecmath.Color3f;

public class ScenarioColorProvider {
	private static final Color3f GRAY = new Color3f(0.5019608f, 0.5019608f, 0.5019608f);
	protected static final Color3f[] COLORS = new Color3f[] { new Color3f(77 / 255.0f, 175 / 255.0f, 74 / 255.0f), new Color3f(55 / 255.0f, 126 / 255.0f, 184 / 255.0f),
			new Color3f(255 / 255.0f, 127 / 255.0f, 0 / 255.0f), new Color3f(166 / 255.0f, 86 / 255.0f, 40 / 255.0f), new Color3f(247 / 255.0f, 129 / 255.0f, 191 / 255.0f),
			new Color3f(152 / 255.0f, 78 / 255.0f, 163 / 255.0f), new Color3f(228 / 255.0f, 26 / 255.0f, 26 / 255.0f) };

	private ScenarioColorProvider() {}

	public static Color3f getColor(int index) {
		return index < 0 ? GRAY : COLORS[index % COLORS.length];
	}
}