package io.scenarium.core;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import javax.swing.event.EventListenerList;

import io.beanmanager.BeanManager;
import io.scenarium.core.filemanager.DataLoader;
import io.scenarium.core.filemanager.LoadingException;
import io.scenarium.core.filemanager.scenariomanager.LoadScenarioListener;
import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;
import io.scenarium.core.filemanager.scenariomanager.ScheduleChangeListener;
import io.scenarium.core.internal.Log;
import io.scenarium.core.struct.ScenariumProperties;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.core.timescheduler.SchedulerInterface;
import io.scenarium.core.timescheduler.SchedulerPropertyChangeListener;
import io.scenarium.core.timescheduler.SchedulerState;
import io.scenarium.core.timescheduler.TimerScheduler;
import io.scenarium.core.tools.ReadOnlyObservableValue;
import io.scenarium.logger.Logger;
import io.scenarium.pluginManager.ModuleManager;

public class Scenarium implements MainApp, SchedulerPropertyChangeListener, ScheduleChangeListener {
	// TODO Bug Linux Si Q sur diagramdrawer, je perd la barre des menu...
	// TODO Bug si j'ai un block CorCPP sans propriété je crash

	// TODO TICKET faire report + ticket pour LibJImageBug (que quand je coupe le soft en debug)
	// TODO TICKET faire ticket pour https://github.com/javafxports/openjdk-jfx/issues/578#event-2708100170
	// TODO TICKET mauvais fps sur linux

	// TODO EP1.2 PB viewer generateouputimage et runlater
	// TODO EP1.3 PB sous diagram et rmi
	// - Block en conflit démarre quand même Voir comment synchronizer isConflict
	// - Viewer birth mais pas enregistré donc pas possible de voir la sortie Faire RMI scenario + VisuableSchedulable
	// - Voir quoi faire de isRunning, stop, setStartLock Brancher le stop, isRunning -> true, setStartLock à voir
	// - Crash si RMI dans RMI A voir
	// - Si je change la structure IO pendant qu'il tourne, crash il faut voir pour vérouiller le block distant
	// TODO EP1.5 les propriétés en entrées/sorties ne fonctionne pas avec RMI
	// TODO EP1.7 Transtypage et moteur d'unité

	// TODO EP2 Si entré sur file editeur, double validation, si tab, simple validation
	// TODO EP2 Revoir comment s'enregistre des BufferedImage et autres
	// TODO EP2 Gérer les crash de block natif en isolé, afficher code erreur
	// TODO EP2 Pouvoir changer le nom des entrées sur le recorder ainsi que le type de recorder (ex: Raw/avi, csv/binaire)
	// TODO EP2 Faire plusieurs sorties pour les scénario (video + son)
	// TODO EP2 Ajouter offset sur scenario evenement et begin sur autre
	// TODO EP2 changer serveur client avec enum et désactiver des propriétés si nécessaire
	// TODO EP2 Faire des listener pour toutes les propriété de mes blocks (viewer entre autres)
	// TODO EP2 gérer le trigger vectorizer/simulink avec règle logique en propriété
	// TODO EP2 method sur editor pour savoir si l'entrée est valide (edition du nom d'un block)
	// TODO EP2 min max sur Point2i pour crop par exemple
	// TODO EP2 ctrl espace ou espace sur player
	// TODO EP2 Afficher les icones des erreur et les cause dans les tooltip
	// TODO EP2 Mettre les annotations d'entrées de bloc sur les variables
	// TODO EP2 maj supr et supr sur clic droit block
	// TODO EP2 Mettre Icone barre des taches pour les tools
	// TODO EP2 Barre de chargement pour les scenario en dessous des blocs
	// TODO EP2 Inline sur un tableau ou sur un arbre de bean
	// TODO EP2 Faire Color3f editor

	// TODO EP2.5 ValuesFx GeographicalDrawer roi
	// TODO EP2.5 Finir les viewer de courbes
	// TODO EP2.5 Que faire du streamScheduler? interaction avec scenario scenariumScheduler
	// TODO EP2.5 Ctrl+z sur les diagrammes
	// TODO EP2.5 Chargement directement des .psf sans diagramme ne charge que le premier
	// TODO EP2.5 Modif de log!!! -> enlever les System.lineSeparator() qui peuvent être problèmatique..., passer plutot par les putstring et getstring
	// ->enlever writeBytes de PropertyStreamRecorder et SerializableObjectStreamRecorder, remplacer par putstring getstring

	// TODO EP3 Couleur si raw uniquement -> Faire un format custom RGB, gérer le yuv dans le format raster
	// TODO EP3 Faire un error/warning viewer pour bilan temporelle
	// TODO EP3 Faire un CAN analyzer et finir de tester l'encoder CAN
	// TODO EP3 Rec manipulator time and data selection
	// TODO EP3 TimeStamp haute précision
	// TODO EP3 Ajouter plugin manager pour charger, décharger, voir un dépot internet
	// TODO EP3 Ajouter indicateur sur la vitesse réelle quand on rejoue un scénario en MONOCORE

	// TODO EP4 mettre le bon type de sortie/entré des canencoder/decoder, mais ca fait que j'ai des double, int, etc pas compatibles (pas temps que je gère pas les int -> double et autre)
	// TODO EP4 Prendre le menu de gluon: http://docs.gluonhq.com/charm/4.4.1/

	public static final String VERSION = "0.6.00";
	public static final boolean DEBUG_MODE = true;
	// private static final int JAVA_MINIMUM_VERSION = 14;
	// private static final String JAVA_DOWNLOAD_WEBPAGE = "https://bell-sw.com/";// "http://www.oracle.com/technetwork/java/javase/downloads";
	private static boolean backgroundLoading = true;
	private static File defaultScenarioFile = null;
	private static String userScenarioFile = null;
	public static Consumer<Scenarium> onStartup;
	private static Scenarium instance;

	private final EventListenerList listeners = new EventListenerList();
	// private final String workspaceDirectory;
	private final TaskConsumer taskConsumer;
	private boolean started = false;

	private DataLoader dataLoader;
	private Scheduler scheduler;

	/** Define all the property names */
	static final String PROPERTY_SCENARIUM_CORE_WORKSPACE = "scenarium.core.workspace";
	static final String PROPERTY_SCENARIUM_CORE_RUN_AT_START_UP = "scenarium.core.runAtStartUp";
	static final String PROPERTY_SCENARIUM_CORE_SCHEDULER_SPEED = "scenarium.core.scheduler.speed";
	static final String PROPERTY_SCENARIUM_CORE_SCHEDULER_REPEAT = "scenarium.core.scheduler.repeat";
	/** Global scenarium start when wake up request */
	private static boolean needToStartAtNextScheduling;
	/** Global scenarium workspace */
	private static String workspace;
	/** global init parameter of scheduler: speed */
	private static Integer schedulerDefaultSpeed = null;
	/** global init parameter of scheduler: loop */
	private static Boolean schedulerDefaultRepeat = null;
	/** Initialize global property: - scenarium.core.workspace: Path of the scenarium workspace. - scenarium.core.runAtStartUp: true/false start the current scenario or the input scenario when loaded.
	 * - scenarium.core.scheduler.speed: Speed off the scenario (100 is 100% of speed: normal) - scenarium.core.scheduler.loop: Loop the scenario at the end */
	static {
		String value = System.getProperty(PROPERTY_SCENARIUM_CORE_WORKSPACE);
		if (value != null)
			workspace = value;
		else
			workspace = System.getProperty("user.dir");
		value = System.getProperty(PROPERTY_SCENARIUM_CORE_RUN_AT_START_UP);
		if (value != null)
			if (value.contentEquals("true") || value.contentEquals("1"))
				needToStartAtNextScheduling = true;
			else if (value.contentEquals("false") || value.contentEquals("0"))
				needToStartAtNextScheduling = false;
			else
				System.out.println("error in " + PROPERTY_SCENARIUM_CORE_RUN_AT_START_UP + " value '" + value + "' ==> not in range [true, false, 0, 1]");
		else
			needToStartAtNextScheduling = false;
		value = System.getProperty(PROPERTY_SCENARIUM_CORE_SCHEDULER_SPEED);
		if (value != null)
			schedulerDefaultSpeed = Integer.decode(value);
		value = System.getProperty(PROPERTY_SCENARIUM_CORE_SCHEDULER_REPEAT);
		if (value != null)
			schedulerDefaultRepeat = Boolean.getBoolean(value);
	}

	public static void setRunAtStartUp(boolean value) {
		needToStartAtNextScheduling = value;
	}

	public static void setUserScenarioFile(String value) {
		userScenarioFile = value;
	}

	public Scenarium(TaskConsumer taskConsumer) {
		this.taskConsumer = taskConsumer;
	}

	/** Get current scenarium workspace directory
	 * @return The current path of scenarium workspace note: a generic scenarium workspace is constituated with: workspace - modules (plugin-manager workspace) */
	public static String getWorkspace() {
		return workspace;
	}

	public static void setWorkspace(String workspace) {
		Scenarium.workspace = workspace;
		Log.warning("Set scenarium workspace: '" + workspace + "'");
	}

	/** This main is for basic scenarium running with no GUI, if you want a full application run it with scenarium-run
	 * @param inputArgument generic main arguments. */
	public static void main(String[] inputArgument) {
		Log.debug("Initialize the application: " + Scenarium.class.getCanonicalName());
		List<String> args = new ArrayList<>(Arrays.asList(inputArgument));
		Logger.init(args);
		ModuleManager.birth();
		run(args.toArray(new String[0]));
		Log.debug("Application end");
	}

	public static void run(String[] inputArgument) {
		try {
			Scenarium.buildInstance(new NoGuiMainThread());
		} catch (InstantiationException e) {
			return;
		}
		instance.start(new StartMessageConsumer() {

			@Override
			public void consumeInformation(String message) {
				Log.info(message);
			}

			@Override
			public void consumeException(Exception exception) {
				exception.printStackTrace();
			}

			@Override
			public void consumeError(String message) {
				Log.error(message);
			}
		}, null, null);
	}

	public static Scenarium buildInstance(TaskConsumer taskConsumer) throws InstantiationException {
		if (instance != null)
			throw new IllegalAccessError("Scenarium is already started");
		instance = new Scenarium(taskConsumer);
		return instance;
	}

	public void start(StartMessageConsumer messageConsumer, Runnable loadConfigCallback, Consumer<ReadOnlyObservableValue<Double>> progressPropertyConsumer) {
		Log.verbose("Scenarium.start()");
		Objects.requireNonNull(messageConsumer);
		if (this.started)
			throw new IllegalArgumentException("Scenarium is already started");
		this.started = true;
		ModuleManager.setRunInSpecificContext(this.taskConsumer::runTaskAndWait);
		ModuleManager.birth();
		// A voir mais plus utile
		// // CheckJavaVersion
		// if (Runtime.version().feature() < JAVA_MINIMUM_VERSION) {
		// if (guiMode) {
		// String message = "Scenarium requires at least java version " + JAVA_MINIMUM_VERSION + ".\nYour version is: " + Runtime.version();
		// Hyperlink hl = new Hyperlink(JAVA_DOWNLOAD_WEBPAGE);
		// hl.setOnAction(e -> hostServices.showDocument(JAVA_DOWNLOAD_WEBPAGE));
		// hl.setText("JDK download page");
		// Alert alert = new Alert(AlertType.ERROR, message, ButtonType.FINISH);
		// alert.getDialogPane().setContent(new VBox(new Label(message), hl));
		// alert.showAndWait();
		// } else
		// Log.error(message);
		// throw new InstantiationException("Scenarium requires at least java version " + JAVA_MINIMUM_VERSION + ".\nYour version is: " + Runtime.version());
		// }
		this.dataLoader = new DataLoader(this);
		ScenarioManager.addLoadScenarioListener(new LoadScenarioListener() {

			@Override
			public void unloaded(Class<? extends Scenario> scenarioClass) {
				Scenario scenario = Scenarium.this.dataLoader.getScenario();
				if (scenario != null && scenario.getClass().equals(scenarioClass))
					loadDefaultScenario(false);
			}

			@Override
			public void loaded(Class<? extends Scenario> drawerClass) {}
		});
		File scenarioFile = defaultScenarioFile;
		if (userScenarioFile != null) {
			scenarioFile = new File(userScenarioFile);
		}
		Object scenarioSource = null;
		try {
			scenarioSource = this.dataLoader.loadSofwareConfig();
		} catch (Exception e) {
			if (e instanceof IOException)
				messageConsumer.consumeInformation("Enjoy Scenarium Software ;)");
			else
				e.printStackTrace();
		}
		if (loadConfigCallback != null)
			loadConfigCallback.run();
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			// After loadSofwareConfig otherwise I can store something empty
			if (this.dataLoader != null)
				this.dataLoader.saveSoftwareConfig();
			fireShutdown();
		}));
		try {
			loadExternalModules();
		} catch (IllegalArgumentException e) {
			messageConsumer.consumeException(e);
		}
		boolean loadDefaultScenario = scenarioFile == null && scenarioSource == null;
		if (!loadDefaultScenario)
			try {
				// Log.info("startup time2: " + (System.currentTimeMillis() - launchingTime));
				this.dataLoader.secureLoad(scenarioFile != null ? scenarioFile : scenarioSource, false, backgroundLoading);
			} catch (LoadingException e) {
				messageConsumer.consumeError("Cannot load the scenario :\n" + (scenarioFile != null ? scenarioFile : scenarioSource));
				loadDefaultScenario = true;
			}
		if (loadDefaultScenario)
			loadDefaultScenario(true);

		if (progressPropertyConsumer != null)
			if (this.dataLoader.getScenario() != null) {
				ReadOnlyObservableValue<Double> pp = this.dataLoader.getScenario().getProgressProperty();
				if (pp != null)
					progressPropertyConsumer.accept(pp);
			} else
				progressPropertyConsumer.accept(null);
		if (onStartup != null)
			onStartup.accept(this);
		Log.verbose("Scenarium.start() [DONE]");
	}

	@Override
	public boolean addToSchedule(Scenario scenario) {
		int schedulerType = scenario.getSchedulerType();
		boolean needNewScheduler = this.scheduler == null || !this.scheduler.isOfKind(schedulerType);
		if (needNewScheduler) {
			Scheduler newScheduler = Scheduler.createScheduler(schedulerType, scenario.canReverse());
			boolean running = false;
			if (this.scheduler != null) {
				this.scheduler.death();
				this.scheduler.removePropertyChangeListener(this);
				newScheduler.copyProperty(this.scheduler);
				running = this.scheduler.isRunning();
			}
			this.scheduler = newScheduler;
			// Configure the user default property of the scheduler
			if (schedulerDefaultSpeed != null) {
				Log.debug("Set scheduler speed at: " + schedulerDefaultSpeed);
				this.scheduler.setSpeed(schedulerDefaultSpeed);
				// This is only available for the first scenario
				schedulerDefaultSpeed = null;
			}
			if (schedulerDefaultRepeat != null) {
				Log.debug("Set scheduler repeat at: " + schedulerDefaultRepeat);
				this.scheduler.setRepeat(schedulerDefaultRepeat);
				// This is only available for the first scenario
				schedulerDefaultRepeat = null;
			}

			scenario.setScheduler(new SchedulerInterface(this.scheduler));
			if (this.scheduler != null) {
				File schedulerFile = new File(Scenarium.getWorkspace() + File.separator + "SchedulerConfig.txt");
				if (schedulerFile.exists())
					new BeanManager(this.scheduler, Scenarium.getWorkspace()).load(schedulerFile);
			}
			if (!this.scheduler.setTimePointer(this.scheduler.getSpeed() < 0 ? this.scheduler.getEffectiveStop() : this.scheduler.getEffectiveStart()))
				this.scheduler.refresh();
			this.scheduler.addPropertyChangeListener(this);
			if (this.scheduler instanceof TimerScheduler)
				((TimerScheduler) this.scheduler).addScheduleElement(scenario, -1);
			if (running)
				this.scheduler.start(); // Bug étrange... si true true, il ne démarre pas
			// scheduler.setRunning(running, true);
		} else {
			this.scheduler.clean();
			scenario.setScheduler(new SchedulerInterface(this.scheduler));
			if (this.scheduler instanceof TimerScheduler)
				((TimerScheduler) this.scheduler).addScheduleElement(scenario, -1);
			if (!this.scheduler.setTimePointer(this.scheduler.getSpeed() < 0 ? this.scheduler.getEffectiveStop() : this.scheduler.getEffectiveStart()))
				this.scheduler.refresh();
		}
		scenario.addScheduleChangeListener(() -> {
			Scenario sce = this.dataLoader.getScenario();
			removeToSchedule(sce);
			if (addToSchedule(sce))
				fireScenarioSchedulerChanged(scenario, this.scheduler);
			// TheaterPanel tp = this.mainFrame.getRenderPane().getTheaterPane();
			// if (schedulerChanged)
			// tp.setScheduler(this.scheduler);
		});
		fireScenarioScheduled(scenario, needNewScheduler);
		if (needToStartAtNextScheduling) {
			needToStartAtNextScheduling = false;
			if (this.scheduler != null)
				runTask(() -> this.scheduler.start());
		}
		return needNewScheduler;
	}

	public void addScenarioScheduledListener(ScenarioScheduledListener listener) {
		this.listeners.add(ScenarioScheduledListener.class, listener);
	}

	public void removeScenarioScheduledListener(ScenarioScheduledListener listener) {
		this.listeners.remove(ScenarioScheduledListener.class, listener);
	}

	private void fireScenarioScheduled(Scenario scenario, boolean schedulerChanged) {
		for (ScenarioScheduledListener listener : this.listeners.getListeners(ScenarioScheduledListener.class))
			listener.scenarioScheduled(scenario, schedulerChanged);
	}

	public void addScenarioSchedulerChangeListener(ScenarioSchedulerChangeListener listener) {
		this.listeners.add(ScenarioSchedulerChangeListener.class, listener);
	}

	public void removeScenarioSchedulerChangeListener(ScenarioSchedulerChangeListener listener) {
		this.listeners.remove(ScenarioSchedulerChangeListener.class, listener);
	}

	private void fireScenarioSchedulerChanged(Scenario scenario, Scheduler newScheduler) {
		for (ScenarioSchedulerChangeListener listener : this.listeners.getListeners(ScenarioSchedulerChangeListener.class))
			listener.scenarioSchedulerChanged(scenario, newScheduler);
	}

	public void addShutdownListener(ShutdownListener listener) {
		this.listeners.add(ShutdownListener.class, listener);
	}

	public void removeShutdownListener(ShutdownListener listener) {
		this.listeners.remove(ShutdownListener.class, listener);
	}

	private void fireShutdown() {
		for (ShutdownListener listener : this.listeners.getListeners(ShutdownListener.class))
			listener.shutdown();
	}

	public DataLoader getDataLoader() {
		return this.dataLoader;
	}

	public Scheduler getScheduler() {
		return this.scheduler;
	}

	public boolean goToNextScenario(boolean reverse) {
		return this.dataLoader.goToNextScenario(reverse);
	}

	@Override
	public void removeToSchedule(Scenario scenario) {
		if (this.scheduler instanceof TimerScheduler)
			((TimerScheduler) this.scheduler).removeScheduleElement(scenario);
		scenario.removeScheduleChangeListener(this);
	}

	@Override
	public void scheduleChange() {
		Scenario scenario = this.dataLoader.getScenario();
		removeToSchedule(scenario);
		addToSchedule(scenario);
	}

	static void loadExternalModules() {
		Log.info("Load scenarium external modules");
		Path[] modules = ScenariumProperties.get().getExternModules();
		if (modules != null)
			ModuleManager.registerExternalModules(modules);
	}

	@Override
	public void stateChanged(SchedulerState state) {
		if (state == SchedulerState.ENDREACHED)
			new Thread(() -> {
				if (!goToNextScenario(this.scheduler.getSpeed() < 0))
					this.scheduler.pause();
				else
					this.scheduler.start();
			}).start();
	}

	private void loadDefaultScenario(boolean backgroundLoading) {
		// TODO REFACTO This is temporary, need to create a real start interface
		Optional<Class<? extends LocalScenario>> value = ScenarioManager.getFirstRunableScenario();
		if (value.isPresent())
			try {
				this.dataLoader.secureLoad(value.get(), false, backgroundLoading);
			} catch (LoadingException e) {
				e.printStackTrace();
			}
		else {
			Log.error("No default scenario available and no scenrio in arguments. Scenarium is stopping.");
			System.exit(-1);
		}
	}

	/** Run the specified Runnable synchronously with other Scenarium tasks. This is not a blocking method.
	 * @param runnable the Runnable whose run method will be executed synchronously Scenarium. */
	public void runTask(Runnable runnable) {
		this.taskConsumer.runTask(runnable);
	}

	/** Run the specified Runnable synchronously with other Scenarium tasks. This method blocks until the execution of the runnable.
	 * @param runnable the Runnable whose run method will be executed synchronously Scenarium. */
	public void runTaskAndWait(Runnable runnable) {
		this.taskConsumer.runTaskAndWait(runnable);
	}
}
