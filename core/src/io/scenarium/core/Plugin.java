package io.scenarium.core;

import java.io.File;

import io.beanmanager.BeanManager;
import io.beanmanager.PluginsBeanSupplier;
import io.beanmanager.consumer.EditorConsumer;
import io.scenarium.core.consumer.ScenarioConsumer;
import io.scenarium.core.editors.CurveSeriesEditor;
import io.scenarium.core.editors.CurvedEditor;
import io.scenarium.core.editors.CurveiEditor;
import io.scenarium.core.editors.ScenarioDescriptorEditor;
import io.scenarium.core.filemanager.scenariomanager.ScenarioDescriptor;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;
import io.scenarium.core.internal.Log;
import io.scenarium.core.struct.curve.CurveSeries;
import io.scenarium.core.struct.curve.Curved;
import io.scenarium.core.struct.curve.Curvei;
import io.scenarium.pluginManager.PluginsSupplier;

public class Plugin implements PluginsSupplier, PluginsBeanSupplier {

	@Override
	public void birth() {
		BeanManager.switchDefaultBeanLocation(Scenarium.getWorkspace() + File.separator + "Operator" + File.separator);
		// We must do this kind of things to be sure that no external dynamic module have create the class
		// in his path, and the instance in well managed by the plug-in.
		try {
			Class.forName(ScenarioManager.class.getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void loadPlugin(Object pluginInterface) {
		Log.print("Load the scenarium plugin on " + pluginInterface.getClass().getCanonicalName());
		// check if the plug-in is supported locally:
		if (pluginInterface instanceof PluginsCoreSupplier) {
			PluginsCoreSupplier plugins = (PluginsCoreSupplier) pluginInterface;
			// Load all scenarios interfaces
			plugins.populateScenarios(new ScenarioConsumer());
		}
	}

	@Override
	public void unregisterModule(Module module) {
		// VI) Scenarios
		ScenarioManager.purgeScenarios(module);
	}

	@Override
	public void populateEditors(EditorConsumer editorConsumer) {
		editorConsumer.accept(ScenarioDescriptor.class, ScenarioDescriptorEditor.class);
		editorConsumer.accept(Curved.class, CurvedEditor.class);
		editorConsumer.accept(Curvei.class, CurveiEditor.class);
		editorConsumer.accept(CurveSeries.class, CurveSeriesEditor.class);
	}

}
