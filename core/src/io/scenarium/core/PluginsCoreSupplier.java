package io.scenarium.core;

import io.scenarium.core.consumer.ScenarioConsumer;

public interface PluginsCoreSupplier {
	default void populateScenarios(ScenarioConsumer scenarioConsumer) {}
}
