/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.timescheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.swing.event.EventListenerList;

import io.beanmanager.BeanPropertiesInheritanceLimit;
import io.beanmanager.editors.TransientProperty;

@BeanPropertiesInheritanceLimit
public abstract class Scheduler {
	public static final int TIMER_SCHEDULER = 0;
	public static final int EVENT_SCHEDULER = 1;
	public static final int STREAM_SCHEDULER = 2;
	protected List<VisuableSchedulable> visualScheduleElements = new ArrayList<>(); // Remove Collections.synchronizedList because of lock
	private EventListenerList listeners = new EventListenerList();
	protected long timePointer = 0;
	protected long beginTime;
	protected long endTime;
	@TransientProperty
	protected long startTime = Long.MIN_VALUE; // Ajout
	@TransientProperty
	protected long stopTime = Long.MAX_VALUE; // Ajout
	private Long timeA = null;
	private Long timeB = null;
	protected int speed = 100;
	private int maxSpeed = 100000;
	protected boolean repeat = false;
	protected boolean prestop = false;
	protected boolean stop = true;
	private final boolean canReverse;
	private CountDownLatch startLock;
	protected Runnable stopTimeOutTask;

	public static Scheduler createScheduler(int schedulerType, boolean canReverse) {
		if (schedulerType == TIMER_SCHEDULER)
			return new TimerScheduler(canReverse);
		else if (schedulerType == EVENT_SCHEDULER)
			return new EvenementScheduler(canReverse);
		else if (schedulerType == STREAM_SCHEDULER)
			return new StreamScheduler(canReverse);
		return null;
	}

	public Scheduler(boolean canReverse) {
		this.canReverse = canReverse;
	}

	public void addPropertyChangeListener(SchedulerPropertyChangeListener listener) {
		this.listeners.add(SchedulerPropertyChangeListener.class, listener);
	}

	public void addVisualScheduleElement(VisuableSchedulable visuableSchedulable) {
		this.visualScheduleElements.add(visuableSchedulable);
	}

	public abstract void clean();

	public void copyProperty(Scheduler scheduler) {
		this.visualScheduleElements = scheduler.visualScheduleElements; // Pour garder le listener du player
		this.listeners = scheduler.listeners;
		this.timeA = scheduler.timeA;
		this.timeB = scheduler.timeB;
		this.speed = scheduler.speed;
		this.repeat = scheduler.repeat;
		this.maxSpeed = scheduler.maxSpeed;
		if (this.timeA != null && this.timePointer < this.timeA)
			this.timePointer = this.timeA;
		if (this.timeB != null && this.timePointer > this.timeB)
			this.timePointer = this.timeB;
	}

	public void death() {
		suspend();
	}

	protected void endReached() {
		firePropertyChangeEvent(SchedulerState.ENDREACHED);
	}

	protected void firePropertyChangeEvent(SchedulerState state) {
		for (SchedulerPropertyChangeListener listener : this.listeners.getListeners(SchedulerPropertyChangeListener.class))
			listener.stateChanged(state);
	}

	public long getBeginningTime() {
		return this.beginTime;
	}

	public abstract long getTimeStamp();

	public long getEndTime() {
		return this.endTime;
	}

	public long getStartTime() {
		return this.startTime;
	}

	public void setStartTime(long time) {
		if (time == this.startTime || time < this.beginTime || time > this.endTime - 1)
			return;
		if (this.timePointer < time)
			setTimePointer(time);
		this.startTime = time;
	}

	public long getStopTime() {
		return this.stopTime;
	}

	public void setStopTime(long time) {
		if (time == this.stopTime || time > this.endTime || time < this.beginTime)
			return;
		if (this.timePointer > time)
			setTimePointer(time);
		this.stopTime = time;
	}

	public long getEffectiveStop() {
		return Math.min(this.timeB != null ? this.timeB : this.endTime, this.stopTime);
	}

	public abstract long getIndex();

	public int getMaxSpeed() {
		return this.maxSpeed;
	}

	public abstract long getNbGap();

	public int getSpeed() {
		return this.speed;
	}

	public long getEffectiveStart() {
		return Math.max(this.timeA != null ? this.timeA : this.beginTime, this.startTime);
	}

	public Long getTimeA() {
		return this.timeA;
	}

	public Long getTimeB() {
		return this.timeB;
	}

	public long getTimePointer() {
		return this.timePointer;
	}

	public boolean isOfKind(int schedulerType) {
		if (schedulerType == TIMER_SCHEDULER && this instanceof TimerScheduler)
			return true;
		else if (schedulerType == EVENT_SCHEDULER && this instanceof EvenementScheduler)
			return true;
		return schedulerType == STREAM_SCHEDULER && this instanceof StreamScheduler;
	}

	public boolean isRepeat() {
		return this.repeat;
	}

	public abstract boolean isRunning();

	public boolean isStopped() {
		return this.stop;
	}

	public void jump(boolean forward) {
		long startTime = getEffectiveStart();
		long newTimePointer;
		if (forward) {
			newTimePointer = this.timePointer + (this.endTime - this.beginTime) / 50;
			if (newTimePointer > this.endTime)
				newTimePointer = this.endTime;
		} else {
			newTimePointer = this.timePointer - (this.endTime - this.beginTime) / 50;
			if (newTimePointer < startTime)
				newTimePointer = startTime;
		}
		setTimePointer(newTimePointer);
	}

	public void moveSpeed(boolean inc) {
		int oldSimulationSpeed = this.speed;
		int newSpeed;
		if (inc) {
			newSpeed = this.speed + Math.abs(this.speed) / 10;
			if (oldSimulationSpeed == this.speed)
				newSpeed++;
		} else {
			newSpeed = this.speed - Math.abs(this.speed) / 10;
			if (oldSimulationSpeed == this.speed)
				newSpeed--;
		}
		setSpeed(newSpeed);
	}

	public abstract void nextStep();

	// public synchronized void pause() {
	// pause(true);
	// }

	public synchronized void pause() {
		if (isRunning())
			// if (true) {
			suspend();
		this.schedulerState = SchedulerState.SUSPENDED;
		firePropertyChangeEvent(this.schedulerState);
		// } else
		// suspend(() -> {
		// this.stop = false;
		// schedulerState = SchedulerState.SUSPENDED;
		// firePropertyChangeEvent(schedulerState);
		// });
	}

	// public synchronized void pauseAndWait() {
	// pause(true);
	// }

	public abstract void previousStep();

	public void refresh() {
		timePointerChanged();
	}

	public void removePropertyChangeListener(SchedulerPropertyChangeListener listener) {
		this.listeners.remove(SchedulerPropertyChangeListener.class, listener);
	}

	public void removeVisualScheduleElement(VisuableSchedulable visuableSchedulable) {
		if (this.visualScheduleElements.contains(visuableSchedulable))
			this.visualScheduleElements.remove(visuableSchedulable);
	}

	public void reset() {
		setTimeA(null);
		setTimeB(null);
		setTimePointer(this.beginTime);
	}

	protected abstract void setIndex(long index);

	public void setRepeat(boolean repeat) {
		this.repeat = repeat;
		firePropertyChangeEvent(SchedulerState.REPEAT);
	}

	public void setSpeed(int speed) {
		int oldSimulationSpeed = this.speed;
		int minSpeed = this.canReverse ? -this.maxSpeed : 0;
		if (speed > this.maxSpeed)
			this.speed = this.maxSpeed;
		else if (speed < minSpeed)
			this.speed = minSpeed;
		else
			this.speed = speed;
		if (oldSimulationSpeed != this.speed) {
			updateSpeed();
			firePropertyChangeEvent(SchedulerState.SPEEDCHANGED);
		}
	}

	public void setStartLock(CountDownLatch startLock) {
		this.startLock = startLock;
	}

	public void setTimeA(Long time) {
		if (time == this.timeA)
			return;
		if (time != null) {
			if (time < this.beginTime || this.timeB != null && time >= this.timeB)
				return;
			else if (time > this.endTime - 1)
				return;
			if (time == this.beginTime) {
				time = null;
				if (this.timeB != null && this.timeB == this.beginTime + 1)
					this.timeB = null;
			} else if (this.timePointer < time)
				setTimePointer(time);
		}
		this.timeA = time;
		firePropertyChangeEvent(SchedulerState.BOUCLE);
	}

	public void setTimeB(Long time) {
		if (time == this.timeB)
			return;
		if (time != null) {
			if (time > this.endTime || this.timeA != null && time <= this.timeA)
				return;
			else if (time < this.beginTime)
				return;
			if (time == this.endTime) {
				time = null;
				if (this.timeA != null && this.timeA == this.endTime - 1)
					this.timeA = null;
			} else if (this.timePointer > time)
				setTimePointer(time);
		}
		this.timeB = time;
		firePropertyChangeEvent(SchedulerState.BOUCLE);
	}

	public boolean setTimePointer(long timePointer) {
		if (this.timePointer == timePointer) // en plus
			return false;
		long startTime = getEffectiveStart();
		long finishTime = getEffectiveStop();
		if (timePointer < startTime)
			timePointer = startTime;
		else if (timePointer > finishTime)
			timePointer = finishTime;
		this.timePointer = timePointer;
		timePointerChanged();
		return true;
	}

	public void setToIndex(long index) {
		if (index >= 0 && index < getNbGap())
			setIndex(index);
	}

	public synchronized boolean start() {
		if (!isRunning()) {
			if (this.stop) {
				try {
					this.startStopLock.acquire();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				// Log.error("PRESTART diagram: " + c);
				this.schedulerState = SchedulerState.PRESTART;
				firePropertyChangeEvent(this.schedulerState); // lance le load, le pb c'est que le mono il peut prendre du temps...
				// Log.error("STARTING diagram: " + c);
				this.schedulerState = SchedulerState.STARTING;
				firePropertyChangeEvent(this.schedulerState); // lance le load, le pb c'est que le mono il peut prendre du temps...
				start(true); // lance le scheduler, peut être avant la fin du mono...
				this.stop = false;
				// Log.error("STARTED diagram: " + c);
				this.schedulerState = SchedulerState.STARTED;
				firePropertyChangeEvent(this.schedulerState);
				this.startStopLock.release();
			} else {
				start(false); // lance le scheduler, peut être avant la fin du mono...
				// Log.error("UNSUSPENDED diagram: " + c);
				this.schedulerState = SchedulerState.UNSUSPENDED;
				firePropertyChangeEvent(this.schedulerState);
			}
			return true;
		}
		return false;
	}

	protected abstract void start(boolean refresh);

	private final Semaphore startStopLock = new Semaphore(1);
	private SchedulerState schedulerState = SchedulerState.STOPPED;;

	public synchronized boolean stop() { // must not be stopping while starting...
		if (!this.stop) {
			try {
				this.startStopLock.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.prestop = true;
			// Log.error("PRESTOP diagram: " + c);
			this.schedulerState = SchedulerState.PRESTOP;
			firePropertyChangeEvent(SchedulerState.PRESTOP);
			// Log.error("event PRESTOP done: " + c);
			Runnable stopTask = () -> {
				if (this.timePointer != getEffectiveStart()) // On a stopper, on doit refresh apres
					setTimePointer(getEffectiveStart());
				// Log.error("STOPPING diagram: " + c);
				this.schedulerState = SchedulerState.STOPPING;
				firePropertyChangeEvent(SchedulerState.STOPPING); /// Tue les opérateurs...
				// Log.error("scheduler stopped");
				// Log.error("STOPPED diagram: " + c);
				this.schedulerState = SchedulerState.STOPPED;
				firePropertyChangeEvent(SchedulerState.STOPPED);
				this.startStopLock.release();
			};
			// if (true) {
			suspend(); // Bloqué ici dans certain cas
			stopTask.run();
			// } else
			// suspend(stopTask); // Tue le scheduler dans une tache
			this.stop = true;
			this.prestop = false;
			return true;
		}
		return false;
	}

	// protected abstract void suspend(Runnable taskWhenSuspented);

	protected abstract void suspend();

	protected abstract void timePointerChanged();

	@Override
	public String toString() {
		return "Time: " + getTimeStamp();
	}

	public abstract void updateBeginAndEndTime();

	protected abstract void updateSpeed();

	protected void waitStarted() throws InterruptedException {
		if (this.startLock != null) {
			this.startLock.await();
			this.startLock = null;
		}
	}

	private final ThreadPoolExecutor schedulerExecutor = new ThreadPoolExecutor(0, 1, 1L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

	public void offerTask(Runnable task) {
		this.schedulerExecutor.execute(() -> {
			synchronized (Scheduler.this) {
				task.run();
			}
		});
	}

	public SchedulerState getSchedulerState() {
		return this.schedulerState;
	}
}
