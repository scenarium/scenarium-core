/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.timescheduler;

public interface VisuableSchedulable {
	default boolean needToBeSchedule() {
		return true;
	}

	public void paint();

	public void setAnimated(boolean animated);
}
