/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.timescheduler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.function.BiConsumer;

import io.beanmanager.editors.TransientProperty;

public class EvenementScheduler extends Scheduler {
	// private ArrayList<ScheduleTask> taskStack = new ArrayList<>(); //JDK12-13 Value Type
	private Schedulable[] taskStack = new Schedulable[0];
	private long[] timeStampStack = new long[0];
	private long[] timeOfIssueStack = new long[0];
	private long[] seekIndexStack = new long[0];
	private SchedulerThread scheduler;
	@TransientProperty
	private int index = 0;
	private volatile long restTime = 0;
	private long timeStamp;

	private TriggerMode triggerMode;
	// private Semaphore startLock = new Semaphore(1);

	public EvenementScheduler(boolean canReverse) {
		super(canReverse);
	}

	public void addTasks(ArrayList<ScheduleTask> schedulableTasks, BiConsumer<Long, Long> minMaxSupplier) {
		if (this.timeStampStack.length != 0)
			this.triggerMode = TriggerMode.TIME_OF_ISSUE;
		if (minMaxSupplier != null) {
			long min;
			long max;
			if (schedulableTasks.isEmpty()) {
				min = 0;
				max = 0;
			} else if (this.triggerMode == TriggerMode.TIMESTAMP) {
				min = schedulableTasks.get(0).timeStamp;
				max = schedulableTasks.get(0).timeStamp;
				for (int i = 0; i < schedulableTasks.size(); i++) {
					long time = schedulableTasks.get(i).timeStamp;
					if (time < min)
						min = time;
					else if (time > max)
						max = time;
				}
			} else if (this.triggerMode == TriggerMode.TIME_OF_ISSUE) {
				min = schedulableTasks.get(0).timeofIssue;
				max = schedulableTasks.get(0).timeofIssue;
				for (int i = 0; i < schedulableTasks.size(); i++) {
					long time = schedulableTasks.get(i).timeofIssue;
					if (time < min)
						min = time;
					else if (time > max)
						max = time;
				}
			} else {
				min = schedulableTasks.get(0).timeofIssue;
				max = schedulableTasks.get(schedulableTasks.size() - 1).timeofIssue;
			}
			minMaxSupplier.accept(min, max);
		}
		ArrayList<ScheduleTask> tasks = new ArrayList<>();
		if (this.timeStampStack.length != 0) {
			for (int i = 0; i < this.timeStampStack.length; i++)
				tasks.add(new ScheduleTask(this.timeOfIssueStack[i], this.timeStampStack[i], this.taskStack[i], this.seekIndexStack[i]));
			this.triggerMode = TriggerMode.TIME_OF_ISSUE; // Si on mélange deux log il faut bien les réordonner
		}
		tasks.addAll(schedulableTasks);
		setTasks(tasks);
	}

	@Override
	public void clean() {
		stop();
		// setRunning(false, false);
		this.timeStampStack = new long[0];
		this.timeOfIssueStack = new long[0];
		this.taskStack = new Schedulable[0];
		this.seekIndexStack = new long[0];
		updateBeginAndEndTime(); // Attention, j'efface timeA et timeB si je fais ca...
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public long getIndex() {
		return this.index;
	}

	private int getIndexFromTimePointer(long timePointer) {
		long[] timeStack = this.triggerMode == TriggerMode.TIMESTAMP ? this.timeStampStack : this.timeOfIssueStack;
		if (timePointer == -1)
			return -1;
		int ts = timeStack.length;
		if (timePointer == this.beginTime)
			return 0;
		else if (timePointer == this.endTime)
			return ts - 1;
		double ratio = ts / (double) (this.endTime - this.beginTime);
		int index = (int) ((timePointer - this.beginTime) * ratio);
		long a = index == 0 ? 0 : timeStack[index - 1];
		long b = timeStack[index];
		int nbIt = 0;
		while (timePointer < a || timePointer > b) {
			double delta = timePointer - (a + b) * 0.5;
			if (nbIt < 5) {
				index += (int) (delta * ratio);
				nbIt++;
			} else
				index += delta < 0 ? -1 : 1;
			if (index < 1)
				index = 1;
			if (index >= ts)
				index = ts - 1;
			a = timeStack[index - 1];
			b = timeStack[index];
		}
		while (timeStack[index - 1] == timeStack[index])
			index--;
		return index;
	}

	@Override
	public long getNbGap() {
		return this.timeStampStack.length;
	}

	@Override
	public boolean isRunning() {
		return this.scheduler != null && !this.scheduler.refreshAndDie;
	}

	@Override
	public void nextStep() {
		if (this.index >= this.timeStampStack.length - 1)
			return;
		this.index++;
		long[] timeStack = this.triggerMode == TriggerMode.TIMESTAMP ? this.timeStampStack : this.timeOfIssueStack;
		while (this.index != timeStack.length - 1 && timeStack[this.index - 1] == timeStack[this.index])
			this.index++;
		super.setTimePointer(timeStack[this.index]);
	}

	@Override
	public void previousStep() {
		if (this.index == 0)
			return;
		long[] timeStack = this.triggerMode == TriggerMode.TIMESTAMP ? this.timeStampStack : this.timeOfIssueStack;
		while (this.index != 0 && timeStack[this.index - 1] == timeStack[this.index])
			this.index--;
		if (this.index != 0)
			this.index--;
		while (this.index != 0 && timeStack[this.index - 1] == timeStack[this.index])
			this.index--;
		super.setTimePointer(timeStack[this.index]);
	}

	@Override
	public void setIndex(long index) {
		this.index = (int) index;
		long[] timeStack = this.triggerMode == TriggerMode.TIMESTAMP ? this.timeStampStack : this.timeOfIssueStack;
		if (timeStack.length != 0)
			super.setTimePointer(timeStack[this.index]);

	}

	public void setTasks(ArrayList<ScheduleTask> schedulableTasks) {
		if (schedulableTasks == null) {
			this.taskStack = new Schedulable[0];
			this.timeStampStack = new long[0];
			this.timeOfIssueStack = new long[0];
			this.seekIndexStack = new long[0];
		} else {
			if (this.triggerMode != TriggerMode.NATURAL_ORDER)
				Collections.sort(schedulableTasks, this.triggerMode == TriggerMode.TIMESTAMP ? (a, b) -> Long.compare(a.timeStamp, b.timeStamp) : (a, b) -> Long.compare(a.timeofIssue, b.timeofIssue)); // Il
			// fallait
			// bien
			// trier...
			this.taskStack = new Schedulable[schedulableTasks.size()];
			this.timeStampStack = new long[this.taskStack.length];
			this.timeOfIssueStack = new long[this.taskStack.length];
			this.seekIndexStack = new long[this.taskStack.length];
			for (int i = 0; i < schedulableTasks.size(); i++) {
				ScheduleTask task = schedulableTasks.get(i);
				this.taskStack[i] = task.task;
				this.timeStampStack[i] = task.timeStamp;
				this.timeOfIssueStack[i] = task.timeofIssue;
				this.seekIndexStack[i] = task.seekIndex;
			}
		}
		updateBeginAndEndTime();
		setIndex(0);
	}

	@Override
	public boolean setTimePointer(long timePointer) {
		if (timePointer <= getEffectiveStart())
			timePointer = getEffectiveStart();
		else if (timePointer >= getEffectiveStop())
			timePointer = getEffectiveStop();
		this.index = getIndexFromTimePointer(timePointer);
		return super.setTimePointer(timePointer);
	}

	public void setTriggerMode(TriggerMode triggerMode) {
		this.triggerMode = triggerMode;
	}

	@Override
	public void start(final boolean refresh) {
		start(refresh, false, false);
	}

	private void start(boolean refreshAtStartup, boolean ignoreRestTime, boolean refreshAndDie) {
		SchedulerThread oldScheduler = this.scheduler;
		if (oldScheduler != null)
			oldScheduler.interrupt();
		this.scheduler = new SchedulerThread(oldScheduler, refreshAtStartup, ignoreRestTime, refreshAndDie);
		if (this.speed != 0)
			this.scheduler.start();
		else
			this.restTime = 0;
	}

	// protected void suspend(Runnable taskWhenSuspented) {
	// // Log.error("suspend: " + SchedulerThread.currentThread().getId());
	// SchedulerThread scheduler = this.scheduler;
	// if (scheduler != null) {
	// // Log.error("suspend _scheduler " + SchedulerThread.currentThread().getId());
	// scheduler.interrupt();
	// if (taskWhenSuspented == null)
	// this.scheduler = null;
	// else
	// new Thread(() -> {
	// try {
	// scheduler.join();
	// this.scheduler = null;
	// } catch (InterruptedException e) {
	// e.printStackTrace();
	// }
	// taskWhenSuspented.run();
	// }).start();
	// } else if (taskWhenSuspented != null)
	// taskWhenSuspented.run();
	// }

	@Override
	protected void suspend() {
		if (this.scheduler != null) {
			this.scheduler.interrupt();
			try {
				this.scheduler.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.scheduler = null;
		}
	}

	@Override
	protected void timePointerChanged() {
		if (isRunning())
			start(true, true, false); // redémarre le scheduler
		else if (!this.stop && !this.prestop)
			start(true, true, true); // Créer beaucoup de thread....
		for (VisuableSchedulable visuableSchedulable : this.visualScheduleElements)
			visuableSchedulable.paint();

		// else
		// ;//updateSchedulableElement(); //Ici ok on run pas... le thread javafx ici...
		// long[] timeStack = triggerMode == TriggerMode.TimeStamp ? timeStampStack : timeOfIssueStack;
		// if (!stop && !isRunning()) {
		// int nextIndex = speed > 0 ? index + 1 : index - 1;
		// while (nextIndex >= 0 && nextIndex <= timeStack.length - 1 && timeStack[index] == timeStack[nextIndex]) {
		// index = nextIndex;
		// //updateSchedulableElement(); //Il faut pas le faire ici, seul ce scheduler devrait pouvoir faire ca, sinon apelle de updateSchedulableElement asynchrone... le thread javafx ici...
		// nextIndex = speed > 0 ? index + 1 : index - 1;
		// }
		// }
	}

	@Override
	public void updateBeginAndEndTime() {
		this.startTime = Long.MIN_VALUE;
		this.stopTime = Long.MAX_VALUE;
		long[] timeStack = this.triggerMode == TriggerMode.TIMESTAMP ? this.timeStampStack : this.timeOfIssueStack;
		if (timeStack.length == 0) {
			this.beginTime = 0; // -1 avant mais pose problème car sinon on fait un setTimePointer à -1 à la fin et le scheduler se lance pas
			this.endTime = 0;
		} else {
			this.beginTime = timeStack.length == 0 ? 0 : timeStack[0];
			this.endTime = timeStack[timeStack.length - 1];
		}
		if (getTimeA() != null && (getTimeA() < this.beginTime || getTimeA() > this.endTime))
			setTimeA(null);
		if (getTimeB() != null && (getTimeB() < this.beginTime || getTimeB() > this.endTime))
			setTimeB(null);
		if (this.timePointer < getBeginningTime())
			setTimePointer(getBeginningTime());
		else if (this.timePointer > getEndTime())
			setTimePointer(getEndTime());
		firePropertyChangeEvent(SchedulerState.INTERVALTIMECHANGED);
	}

	@Override
	protected void updateSpeed() {
		if (this.scheduler != null && !this.scheduler.refreshAndDie)
			start(false);
	}

	class SchedulerThread extends Thread {
		private SchedulerThread oldScheduler;
		private final boolean refreshAtStartup;
		private final boolean ignoreRestTime;
		private final boolean refreshAndDie;

		public SchedulerThread(SchedulerThread oldScheduler, boolean refreshAtStartup, boolean ignoreRestTime, boolean refreshAndDie) {
			this.oldScheduler = oldScheduler;
			this.refreshAtStartup = refreshAtStartup;
			this.ignoreRestTime = ignoreRestTime;
			this.refreshAndDie = refreshAndDie;
			setName(EvenementScheduler.this.getClass().getSimpleName());
		}

		@Override
		public void run() {
			if (this.oldScheduler != null)
				try {
					this.oldScheduler.join();
				} catch (InterruptedException e) {
					return;
				}
			this.oldScheduler = null;
			try {
				waitStarted();
			} catch (InterruptedException e) {
				return;
			}
			long[] timeStack = EvenementScheduler.this.triggerMode == TriggerMode.TIMESTAMP ? EvenementScheduler.this.timeStampStack : EvenementScheduler.this.timeOfIssueStack;
			long beginTime = System.currentTimeMillis();
			if (EvenementScheduler.this.index == -1 || timeStack.length == 0)
				return;
			if ((this.refreshAtStartup || this.refreshAndDie) && timeStack[EvenementScheduler.this.index] == EvenementScheduler.this.timePointer
					&& EvenementScheduler.this.taskStack[EvenementScheduler.this.index].canTrigger(timeStack[EvenementScheduler.this.index])) {
				if (EvenementScheduler.this.taskStack[EvenementScheduler.this.index].canTrigger(EvenementScheduler.this.seekIndexStack[EvenementScheduler.this.index]))
					updateSchedulableElement();
				int nextIndex = EvenementScheduler.this.speed > 0 ? EvenementScheduler.this.index + 1 : EvenementScheduler.this.index - 1;
				while (nextIndex >= 0 && nextIndex <= timeStack.length - 1 && timeStack[EvenementScheduler.this.index] == timeStack[nextIndex]) {
					EvenementScheduler.this.index = nextIndex;
					if (EvenementScheduler.this.taskStack[EvenementScheduler.this.index].canTrigger(EvenementScheduler.this.seekIndexStack[EvenementScheduler.this.index]))
						updateSchedulableElement(); // Close de temps en temps...
					nextIndex = EvenementScheduler.this.speed > 0 ? EvenementScheduler.this.index + 1 : EvenementScheduler.this.index - 1;
				}
			}
			if (this.refreshAndDie)
				return;
			long firstTaskTime = timeStack[EvenementScheduler.this.index];
			int speed = EvenementScheduler.this.speed;
			long timeToReach = System.currentTimeMillis();
			if (isInterrupted())
				return;
			try {
				if (EvenementScheduler.this.restTime != 0 && !this.ignoreRestTime) {
					long timeTowait = (long) (EvenementScheduler.this.restTime * 100.0 / Math.abs(speed));
					timeToReach = System.currentTimeMillis() + timeTowait;
					if (timeTowait > 0)
						synchronized (this) {
							wait(timeTowait);
						}
					EvenementScheduler.this.restTime = 0;
				}
				while (!isInterrupted()) {
					int newIndex = speed > 0 ? EvenementScheduler.this.index + 1 : EvenementScheduler.this.index - 1;
					if (speed > 0 && (newIndex >= timeStack.length || timeStack[newIndex] > getEffectiveStop()) || speed < 0 && (newIndex < 0 || timeStack[newIndex] < getEffectiveStart()))
						if (EvenementScheduler.this.repeat) {
							EvenementScheduler.this.timePointer = speed > 0 ? getEffectiveStart() : getEffectiveStop();
							newIndex = getIndexFromTimePointer(EvenementScheduler.this.timePointer);
							beginTime = System.currentTimeMillis();
							firstTaskTime = timeStack[newIndex];
						} else {
							endReached();
							return;
						}
					long timetask = timeStack[newIndex];
					// if (EvenementScheduler.this.taskStack[newIndex].canTrigger(timetask)) {
					if (timetask != EvenementScheduler.this.timePointer) {
						long timeGap = (long) ((timetask - firstTaskTime) * 100.0 / Math.abs(speed));
						timeToReach = beginTime + (speed > 0 ? timeGap : -timeGap);
						long timeTowait = timeToReach - System.currentTimeMillis();
						if (timeTowait > 0)
							synchronized (this) {
								wait(timeTowait);
							}
						EvenementScheduler.this.timePointer = timetask;
					}
					EvenementScheduler.this.index = newIndex;
					updateSchedulableElement();
					/* } else EvenementScheduler.this.index = newIndex; */
				}
			} catch (InterruptedException ex) {}
			long time = System.currentTimeMillis();
			EvenementScheduler.this.restTime = !isRunning() ? 0 : speed * EvenementScheduler.this.speed > 0 ? (long) ((timeToReach - time) * Math.abs(speed) / 100.0) : 0;
		}

		private void updateSchedulableElement() {
			int index = EvenementScheduler.this.index;
			long[] timeStampStack = EvenementScheduler.this.timeStampStack;
			if (index == -1 || timeStampStack.length == 0)
				return;
			if (Thread.currentThread().isInterrupted())
				return;
			// Log.info("index: " + index + " at: " + timePointer);
			EvenementScheduler.this.timeStamp = timeStampStack[index];
			Schedulable task = EvenementScheduler.this.taskStack[index];
			if (task.canTrigger(EvenementScheduler.this.timePointer))
				task.update(EvenementScheduler.this.seekIndexStack[index]);
			if (Thread.currentThread().isInterrupted())
				return;
			if (!isRunning())
				for (VisuableSchedulable visuableSchedulable : EvenementScheduler.this.visualScheduleElements)
					visuableSchedulable.paint();
		}
	}
}
