/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.timescheduler;

public interface Schedulable {
	public default boolean hasSomeThingsToPlan() {
		return true;
	}

	public default boolean canTrigger(long time) {
		return true;
	}

	public void update(long timePointer);
}
