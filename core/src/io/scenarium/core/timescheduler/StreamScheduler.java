/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.timescheduler;

public class StreamScheduler extends Scheduler {
	Schedulable scheduleElement;
	private boolean isRunning;
	private Thread streamThread;

	public StreamScheduler(boolean canReverse) {
		super(canReverse);
		this.beginTime = -1;
		this.endTime = -1;
	}

	@Override
	public void clean() {}

	@Override
	public long getTimeStamp() {
		return System.currentTimeMillis();
	}

	@Override
	public long getIndex() {
		return -1;
	}

	@Override
	public long getNbGap() {
		return -1;
	}

	@Override
	public boolean isRunning() {
		return this.isRunning;
	}

	@Override
	public void nextStep() {}

	@Override
	public void previousStep() {}

	@Override
	protected void setIndex(long index) {}

	public void setTime(long timePointer) {
		this.timePointer = timePointer;
	}

	@Override
	protected void start(boolean refresh) {
		this.isRunning = true;
		Thread oldStreamThread = this.streamThread;
		if (oldStreamThread != null)
			oldStreamThread.interrupt();
		this.streamThread = new Thread() {
			@Override
			public void run() {
				if (oldStreamThread != null)
					try {
						oldStreamThread.join();
					} catch (InterruptedException e) {
						return;
					}
				try {
					waitStarted();
				} catch (InterruptedException ex) {
					return;
				}
				while (StreamScheduler.this.isRunning)
					// boolean hasStreamScenario = false;
					// synchronized (lock) {
					// for (Schedulable schedulable : scheduleElements)
					// if (schedulable instanceof StreamScenario) {
					// hasStreamScenario = true;
					// break;
					// }
					// }
					// if (hasStreamScenario)
					// synchronized (lock) {
					try {
						StreamScheduler.this.scheduleElement.update(StreamScheduler.this.timePointer);
						if (!isRunning())
							for (VisuableSchedulable visuableSchedulable : StreamScheduler.this.visualScheduleElements)
								visuableSchedulable.paint();
					} catch (Exception e) {
						e.printStackTrace();
					}
				// }
				// else
				// try {
				// Thread.sleep(1000);
				// } catch (InterruptedException e) {}
			}
		};
		this.streamThread.start();
	}

	@Override
	protected void suspend() {
		this.isRunning = false;
		if (this.streamThread != null)
			this.streamThread.interrupt();
		try {
			this.streamThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void timePointerChanged() {}

	@Override
	public void updateBeginAndEndTime() {}

	@Override
	protected void updateSpeed() {}
}
