/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.timescheduler;

public enum SchedulerState {
	PRESTART // Désactivation des bouttons du player
	, STARTING // Création du DiagramScheduler
				// Lancement du scheduler de temps
	, STARTED // Lancement du viewerAnimationTimer, du canvasInfo, Réactivation des bouttons du player
	, PRESTOP // Désactivation des bouttons du player, Destruction du viewerAnimationTimer, du canvasInfo.
				// Suspension du scheduler de temps
	, STOPPING // Destruction du DiagramScheduler
	, STOPPED // Réactivation des bouttons du player
	, SUSPENDED, UNSUSPENDED, SPEEDCHANGED, REPEAT, BOUCLE, ENDREACHED, INTERVALTIMECHANGED;
}
