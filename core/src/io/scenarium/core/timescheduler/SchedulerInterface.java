package io.scenarium.core.timescheduler;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.function.BiConsumer;

// TODO REFACTO it seams to have a wrapping pob on this class, it does not contain the list of register element, then when remove, the sceduler event is propagated ==> need to be wrap inside to have a correct functionnal interface
public class SchedulerInterface {
	private final Scheduler scheduler;

	public SchedulerInterface() {
		this.scheduler = null;
	}

	public SchedulerInterface(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public SchedulerInterface(SchedulerInterface schedulerInterface) {
		this(schedulerInterface.scheduler);
	}

	public void addPropertyChangeListener(SchedulerPropertyChangeListener schedulerPropertyChangeListener) {
		this.scheduler.addPropertyChangeListener(schedulerPropertyChangeListener);
	}

	public void addTasks(ArrayList<ScheduleTask> schedulableTasks, BiConsumer<Long, Long> minMaxSupplier) {
		if (this.scheduler instanceof EvenementScheduler)
			((EvenementScheduler) this.scheduler).addTasks(schedulableTasks, minMaxSupplier);
	}

	public void clean() {
		this.scheduler.clean();
	}

	public long getBeginTime() {
		return this.scheduler.getBeginningTime();
	}

	public long getTimeStamp() {
		return this.scheduler.getTimeStamp();
	}

	public long getEndTime() {
		return this.scheduler.getEndTime();
	}

	public void removePropertyChangeListener(SchedulerPropertyChangeListener schedulerPropertyChangeListener) {
		this.scheduler.removePropertyChangeListener(schedulerPropertyChangeListener);
	}

	public void removeScheduleElement(Schedulable schedulable) {
		if (this.scheduler instanceof TimerScheduler)
			((TimerScheduler) this.scheduler).removeScheduleElement(schedulable);
	}

	public void setStartLock(CountDownLatch startLock) {
		this.scheduler.setStartLock(startLock);
	}

	public void setTriggerMode(TriggerMode triggerMode) {
		if (this.scheduler instanceof EvenementScheduler)
			((EvenementScheduler) this.scheduler).setTriggerMode(triggerMode);
	}

	public boolean stop() {
		SchedulerState ss = this.scheduler.getSchedulerState();
		return ss == SchedulerState.PRESTOP || ss == SchedulerState.STOPPING || ss == SchedulerState.STOPPED || this.scheduler.stop();
	}

	public void setStartTime(long startTime) {
		this.scheduler.setStartTime(startTime);
	}

	public void setStopTime(long stopTime) {
		this.scheduler.setStopTime(stopTime);
	}

	public <T> T synchronisedCall(Callable<T> callable) throws Exception {
		synchronized (this.scheduler) {
			return callable.call();
		}
	}

	public boolean isRunning() {
		return this.scheduler.isRunning();
	}

	public void offerTask(Runnable task) {
		this.scheduler.offerTask(task);
	}
}