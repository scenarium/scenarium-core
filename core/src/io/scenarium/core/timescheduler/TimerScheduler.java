/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.timescheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.filemanager.scenariomanager.PeriodListener;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.TimedScenario;
import io.scenarium.core.internal.Log;
import io.scenarium.core.tools.SchedulerUtils;

public class TimerScheduler extends Scheduler implements PeriodListener {
	protected ArrayList<Schedulable> scheduleElements = new ArrayList<>();
	private ScheduledExecutorService simulationTimer;
	private int period;
	private LocalScenario mainSchedulable;
	private Thread timerThread;

	public static void main(String[] args) {
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(() -> {
			Log.info("begin");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				Log.error("Stop sleep");
			}
			Log.info("Executors: " + System.currentTimeMillis());
		}, 0, 1000, TimeUnit.MILLISECONDS);
		int i = 0;
		while (true) {
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			int id = i++;
			Log.info("Add task " + id + " to execution");
			Runnable r = () -> Log.info("Task " + id + ": " + System.currentTimeMillis());
			executor.execute(r);
			if (i == 10) {
				Log.info("Cancel");
				// handle.cancel(true);
				List<Runnable> l = executor.shutdownNow();
				for (Runnable runnable : l) {
					FutureTask<?> ft = (FutureTask<?>) runnable;
					ft.run();
					try {
						ft.get();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (ExecutionException e) {
						e.printStackTrace();
					}
					ft.run();
				}
				try {
					Log.info("Termination: " + executor.awaitTermination(3, TimeUnit.SECONDS));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				break;
			}
		}
		// handle.cancel(true);
		// executor.sc
		// executor.submit(task)
	}

	public TimerScheduler(boolean canReverse) {
		super(canReverse);
	}

	public void addScheduleElement(Schedulable scheduleElement, int index) {
		boolean isRunning = isRunning();
		if (isRunning)
			suspend();
		ArrayList<Schedulable> newScheduleElements = new ArrayList<>(this.scheduleElements);
		if (!newScheduleElements.contains(scheduleElement)) {
			newScheduleElements.add(index == -1 ? newScheduleElements.size() : index, scheduleElement);
			if (scheduleElement instanceof Scenario)
				updateBeginAndEndTime(newScheduleElements);
		}
		this.scheduleElements = newScheduleElements;
		if (isRunning)
			start(true);
	}

	@Override
	public void clean() {
		if (this.mainSchedulable != null)
			this.mainSchedulable.removePeriodChangeListener(this);
	}

	@Override
	public long getTimeStamp() {
		return this.timePointer;
	}

	@Override
	public long getIndex() {
		return this.timePointer;
	}

	@Override
	public long getNbGap() {
		return this.endTime - this.beginTime;
	}

	@Override
	public boolean isRunning() {
		return this.simulationTimer != null;
	}

	@Override
	public void nextStep() {
		setTimePointer(this.timePointer + this.period);
	}

	@Override
	public void periodChanged(double period) {
		this.period = (int) Math.round(period);
		if (this.simulationTimer != null)
			start(false);
	}

	@Override
	public void previousStep() {
		setTimePointer(this.timePointer - this.period);
	}

	public void removeScheduleElement(Schedulable scheduleElement) {
		boolean isRunning = isRunning();
		if (isRunning)
			suspend();
		ArrayList<Schedulable> newScheduleElements = new ArrayList<>(this.scheduleElements);
		if (newScheduleElements.contains(scheduleElement)) {
			newScheduleElements.remove(scheduleElement);
			if (scheduleElement instanceof Scenario)
				updateBeginAndEndTime(newScheduleElements);
		}
		this.scheduleElements = newScheduleElements;
		if (isRunning)
			start(true);
	}

	public void resetScheduleElement() {
		this.scheduleElements = new ArrayList<>();
	}

	public void run() {
		this.timePointer = this.speed > 0 ? this.timePointer + this.period : this.timePointer - this.period;
		if (this.timePointer >= getEffectiveStop() && this.speed > 0 || this.timePointer <= getEffectiveStart() && this.speed < 0)
			if (this.repeat)
				this.timePointer = this.speed > 0 ? getEffectiveStart() : getEffectiveStop();
			else {
				pause(); // Pour ne rien faire apèrs
				endReached();
				return;
			}
		updateSchedulableElement();
	}

	@Override
	public void setIndex(long index) {
		setTimePointer(index);
	}

	@Override
	public void start(final boolean refresh) {
		Thread oldTimerThread = this.timerThread;
		stopTimer();
		this.simulationTimer = SchedulerUtils.getTimer(TimerScheduler.this.getClass().getSimpleName());
		this.timerThread = new Thread() {
			@Override
			public void run() {
				if (oldTimerThread != null)
					try {
						oldTimerThread.join();
					} catch (InterruptedException e) {
						return;
					}
				try {
					waitStarted();
				} catch (InterruptedException ex) {
					return;
				}
				if (refresh)
					updateSchedulableElement();
				int ms = (int) (100.0 / Math.abs(TimerScheduler.this.speed) * TimerScheduler.this.period);
				if (ms == 0)
					ms = 1;
				if (TimerScheduler.this.simulationTimer == null)
					return;

				try {
					TimerScheduler.this.simulationTimer.scheduleAtFixedRate(() -> {
						TimerScheduler.this.run();
					}, ms / 2, ms, TimeUnit.MILLISECONDS);
				} catch (RejectedExecutionException e) {} // Le timer est peut �tre d�ja cancel
			}
		};
		this.timerThread.start();
	}

	private void stopTimer() {
		if (this.simulationTimer != null) {
			this.simulationTimer.shutdownNow();
			this.simulationTimer = null;
			// if (taskWhenSuspented == null)
			this.timerThread = null;
			// else
			// new Thread(() -> {
			// try {
			// this.timerThread.join();
			// this.timerThread = null;
			// } catch (InterruptedException e) {
			// e.printStackTrace();
			// }
			// taskWhenSuspented.run();
			// }).start();
		} /* else if (taskWhenSuspented != null) taskWhenSuspented.run(); */
	}

	// @Override
	// public void suspend() {
	// stopTimer(run);
	// }

	@Override
	protected void suspend() {
		Thread timerThread = this.timerThread;
		stopTimer();
		try {
			if (timerThread != null)
				timerThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void timePointerChanged() {
		updateSchedulableElement();
	}

	@Override
	public void updateBeginAndEndTime() {
		updateBeginAndEndTime(this.scheduleElements);
	}

	private void updateBeginAndEndTime(ArrayList<Schedulable> scheduleElements) {
		this.beginTime = 0;
		this.endTime = 0;
		if (this.mainSchedulable != null)
			this.mainSchedulable.removePeriodChangeListener(this);
		this.mainSchedulable = null;
		for (Schedulable schedulable : scheduleElements)
			if (schedulable instanceof Scenario) {
				long beginTimeScenario = ((Scenario) schedulable).getBeginningTime();
				long endTimeScenario = ((Scenario) schedulable).getEndTime();
				if (this.beginTime == -1 || beginTimeScenario < this.beginTime)
					this.beginTime = beginTimeScenario;
				if (endTimeScenario != -1) {
					endTimeScenario--;
					if (endTimeScenario > this.endTime)
						this.endTime = endTimeScenario;
				}
				if (schedulable instanceof TimedScenario)
					this.mainSchedulable = (TimedScenario) schedulable;
			}
		if (this.mainSchedulable != null) {
			this.period = (int) Math.round(this.mainSchedulable.getPeriod());
			this.mainSchedulable.addPeriodChangeListener(this);
		}
		firePropertyChangeEvent(SchedulerState.INTERVALTIMECHANGED);
	}

	private void updateSchedulableElement() {
		try {
			for (Schedulable so : this.scheduleElements)
				so.update(this.timePointer);
			if (!isRunning())
				for (VisuableSchedulable visuableSchedulable : this.visualScheduleElements)
					visuableSchedulable.paint();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void updateSpeed() {
		if (this.simulationTimer != null)
			start(false);
	}
}
