/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.timescheduler;

public class ScheduleTask {
	public final long timeofIssue;
	public final long timeStamp;
	public final Schedulable task;
	public final long seekIndex;

	// Record
	public ScheduleTask(long timeofIssue, long timeStamp, Schedulable task, long seekIndex) {
		this.timeofIssue = timeofIssue;
		this.timeStamp = timeStamp;
		this.task = task;
		this.seekIndex = seekIndex;
	}

	// public int compareTo(ScheduleTask o) {
	// return Long.compare(timeStamp, o.timeStamp);
	// }

	@Override
	public String toString() {
		return "TimeStamp: " + this.timeStamp + " TimeOfIssue: " + this.timeofIssue + " task: " + this.task.toString() + " seekIndex: " + this.seekIndex;
	}
}