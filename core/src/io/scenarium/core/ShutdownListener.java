package io.scenarium.core;

import java.util.EventListener;

public interface ShutdownListener extends EventListener {
	public void shutdown();
}
