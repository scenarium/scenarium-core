package io.scenarium.core.consumer;

import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;

public class ScenarioConsumer {
	public boolean accept(Class<? extends Scenario> scenarioClass) {
		return ScenarioManager.registerScenario(scenarioClass);
	}
}
