/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.struct;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.util.Objects;

import javax.vecmath.Color3f;

public class Texture3D extends OldObject3D {
	public static final long serialVersionUID = 2L;
	public BufferedImage img;

	public Texture3D(BufferedImage img, float[] texCoord) {
		super(OldObject3D.TEXTURE, texCoord, new Color3f(1, 1, 1), 3);
		Objects.requireNonNull(img);
		Objects.requireNonNull(this.data);
		if (this.data.length != 8)
			throw new IllegalArgumentException("texCoord must be non null and with a size of 8");
		this.img = img;
	}

	@Override
	public Texture3D clone() {
		BufferedImage bi = this.img;
		ColorModel cm = bi.getColorModel();
		return new Texture3D(new BufferedImage(cm, bi.copyData(null), cm.isAlphaPremultiplied(), null), this.data);
	}

	@Override
	public String toString() {
		return "ElementType: Texture" + " Data size: " + this.data.length;
	}

}
