/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.struct;

import javax.vecmath.Color3f;

import com.jogamp.opengl.GL2;

public interface DrawableObject3D {
	public void draw(GL2 gl, Color3f color);
}
