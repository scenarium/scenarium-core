/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.struct;

public class Mesh {
	// private float[] facets;
	private float[] colors;
	private final int[] ids;
	private final float[] points;

	// public Mesh(float[] facets) {
	// if (facets == null || facets.length == 0 || facets.length % 9 != 0)
	// throw new IllegalArgumentException("data not valid");
	// this.facets = facets;
	// }
	//
	// public Mesh(float[] facets, float[] colors) {
	// if (facets == null || facets.length == 0 || facets.length % 9 != 0 || (colors != null && colors.length != facets.length))
	// throw new IllegalArgumentException("data not valid");
	// this.facets = facets;
	// this.colors = colors;
	// }
	public Mesh(int[] ids, float[] points) {
		if (ids == null || points == null || points.length == 0 || ids.length == 0 || ids.length % 3 != 0)
			throw new IllegalArgumentException("data not valid");
		this.ids = ids;
		this.points = points;
	}

	public Mesh(int[] ids, float[] points, float[] colors) {
		this(ids, points);
		if (colors != null && colors.length * 3 != ids.length)
			throw new IllegalArgumentException("data not valid");
		this.colors = colors;
	}

	@Override
	public Mesh clone() {
		return new Mesh(this.ids, this.points, this.colors);
	}

	public float[] getColors() {
		return this.colors;
	}

	public int[] getIds() {
		return this.ids;
	}

	public int getNbFacets() {
		return this.ids.length / 3;
	}

	public float[] getPoints() {
		return this.points;
	}
}
