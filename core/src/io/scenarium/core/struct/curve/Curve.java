/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.struct.curve;

import java.io.Serializable;

public abstract class Curve implements Serializable {
	private static final long serialVersionUID = 1L;
	protected String name;

	@Override
	public abstract Curve clone();

	public abstract double[][] getDatad();

	public String getName() {
		return this.name;
	}

	public abstract double[] getValuesd();

	public void setName(String name) {
		this.name = name;
	}
}
