/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.struct.curve;

import java.util.ArrayList;

public class Curved extends Curve {
	private static final long serialVersionUID = 1L;

	public static Curved getCurve(ArrayList<Double> values) {
		int size = values.size();
		double[][] datas = new double[2][size];
		for (int i = 0; i < size; i++) {
			datas[0][i] = i;
			datas[1][i] = values.get(i);
		}
		return new Curved(datas);
	}

	private final double[][] data;

	private final double[] values;

	public Curved(double[][] data) {
		if (data == null)
			throw new IllegalArgumentException("The data may not be null");
		if (data.length <= 1)
			throw new IllegalArgumentException("The data array must have length <= 1");
		if (data[0].length != data[1].length)
			throw new IllegalArgumentException("The data array array must contain two arrays with equal length");
		this.data = data;
		this.values = null;
	}

	public Curved(double[][] data, double[] values) {
		if (data == null)
			throw new IllegalArgumentException("The data may not be null");
		if (data.length <= 1)
			throw new IllegalArgumentException("The data array must have length <= 1");
		if (data[0].length != data[1].length)
			throw new IllegalArgumentException("The data array array must contain two arrays with equal length");
		this.data = data;
		if (values != null && values.length != data[0].length)
			throw new IllegalArgumentException("The values array must have length == data array");
		this.values = values;
	}

	public Curved(double[][] data, double[] values, String name) {
		this(data, values);
		this.name = name;
	}

	public Curved(double[][] data, String name) {
		this(data);
		this.name = name;
	}

	@Override
	public Curved clone() {
		double[][] temp = this.data.clone();
		for (int i = 0; i < temp.length; i++)
			temp[i] = this.data[i].clone();
		return new Curved(temp, this.values != null ? this.values.clone() : null, this.name);
	}

	public double[][] getData() {
		return this.data;
	}

	@Override
	public double[][] getDatad() {
		return this.data;
	}

	public double[] getValues() {
		return this.values;
	}

	@Override
	public double[] getValuesd() {
		return this.values;
	}

	public int nbPoints() {
		return this.data[0].length;
	}
}
