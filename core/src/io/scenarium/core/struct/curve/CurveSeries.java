/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.struct.curve;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CurveSeries implements Serializable {
	private static final long serialVersionUID = 1L;
	protected List<Curve> curves;

	public CurveSeries() {
		this.curves = new ArrayList<>();
	}

	public CurveSeries(List<Curve> curves) {
		for (Curve curve : curves)
			if (curve == null)
				throw new IllegalArgumentException("All Curve need to be non null");
		this.curves = curves;
	}

	public CurveSeries(Curve... curves) {
		this(new ArrayList<>(Arrays.asList(curves)));
	}

	public void addCurve(Curve curve) {
		if (curve == null)
			throw new IllegalArgumentException("Curve need to be non null");
		this.curves.add(curve);
	}

	@Override
	public CurveSeries clone() {
		return new CurveSeries(new ArrayList<>(this.curves));
	}

	public List<Curve> getCurves() {
		return this.curves;
	}

	public void removeCurve(Curve curve) {
		if (curve == null)
			throw new IllegalArgumentException("Curve need to be non null");
		this.curves.remove(curve);
	}

	public void setCurves(ArrayList<Curve> curves) {
		this.curves = curves;
	}
}
