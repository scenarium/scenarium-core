/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.struct.curve;

import java.util.ArrayList;
import java.util.List;

public class EvolvedCurveSeries extends CurveSeries {
	private static final long serialVersionUID = 1L;
	private List<BoxMarker> boxmarkers;
	private String name;
	private String xLabel;
	private String yLabel;
	private double begin;
	private double end;

	public EvolvedCurveSeries(List<BoxMarker> boxmarkers, List<Curve> curves) {
		super(curves);
		this.boxmarkers = boxmarkers;
	}

	public EvolvedCurveSeries(List<BoxMarker> boxmarkers, Curve... curves) {
		super(curves);
		this.boxmarkers = boxmarkers;
	}

	public EvolvedCurveSeries(List<BoxMarker> boxmarkers, String name, String xLabel, String yLabel, double begin, double end, List<Curve> curves) {
		super(curves);
		this.name = name;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
		this.boxmarkers = boxmarkers;
		addIntervalMarker(begin, end);
	}

	public EvolvedCurveSeries(List<BoxMarker> boxmarkers, String name, String xLabel, String yLabel, double begin, double end, Curve... curves) {
		super(curves);
		this.name = name;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
		this.boxmarkers = boxmarkers;
		addIntervalMarker(begin, end);
	}

	public EvolvedCurveSeries(List<Curve> curves) {
		super(curves);
	}

	public EvolvedCurveSeries(Curve... curves) {
		super(curves);
	}

	public EvolvedCurveSeries(String name, String xLabel, String yLabel, ArrayList<Curve> curves) {
		super(curves);
		this.name = name;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
	}

	public EvolvedCurveSeries(String name, String xLabel, String yLabel, Curve... curves) {
		super(curves);
		this.name = name;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
	}

	public void addBoxMarker(BoxMarker boxmarker) {
		if (this.boxmarkers == null)
			this.boxmarkers = new ArrayList<>();
		this.boxmarkers.add(boxmarker);
	}

	public void addIntervalMarker(double begin, double end) {
		this.begin = begin;
		this.end = end;
	}

	@Override
	public EvolvedCurveSeries clone() {
		EvolvedCurveSeries ecs = new EvolvedCurveSeries(super.clone().curves);
		ecs.begin = this.begin;
		ecs.end = this.end;
		ecs.name = this.name;
		ecs.xLabel = this.xLabel;
		ecs.yLabel = this.yLabel;
		if (this.boxmarkers != null)
			ecs.boxmarkers = new ArrayList<>(this.boxmarkers);
		return ecs;
	}

	public double getBegin() {
		return this.begin;
	}

	public List<BoxMarker> getBoxmarkers() {
		return this.boxmarkers;
	}

	public double getEnd() {
		return this.end;
	}

	public String getName() {
		return this.name;
	}

	public String getxLabel() {
		return this.xLabel;
	}

	public String getyLabel() {
		return this.yLabel;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setxLabel(String xLabel) {
		this.xLabel = xLabel;
	}

	public void setyLabel(String yLabel) {
		this.yLabel = yLabel;
	}
}
