/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.struct.curve;

import java.io.Serializable;

public class BoxMarker implements Serializable {
	private static final long serialVersionUID = 1L;
	public final double x;
	public final double y;
	public final double width;
	public final double height;
	public final String name;

	public BoxMarker(double x, double y, double width, double height) {
		this(x, y, width, height, null);
	}

	public BoxMarker(double x, double y, double width, double height, String name) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.name = name;
	}
}