/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.struct;

import static com.jogamp.opengl.GL.GL_FLOAT;
import static com.jogamp.opengl.GL.GL_TEXTURE_2D;
import static com.jogamp.opengl.fixedfunc.GLPointerFunc.GL_VERTEX_ARRAY;

import java.io.Serializable;
import java.nio.FloatBuffer;

import javax.vecmath.Color3f;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

public class OldObject3D implements DrawableObject3D, Serializable {
	private static final long serialVersionUID = 1L;
	public static final int POINTS = 0;
	public static final int LINES = 1;
	public static final int TEXT = -1;
	public static final int TEXTURE = -2;
	public int elementType;
	public float[] data;
	public float[] colors;
	public Color3f color;
	public int pointSize;

	public OldObject3D(int elementType, float[] data, Color3f color, int pointSize) {
		if (elementType < -2 || elementType > 1)
			throw new IllegalArgumentException("The elementType is unknow");
		if (data == null || data.length == 0)
			throw new IllegalArgumentException("Data cannot be null or wrong data size");
		if (color == null)
			throw new IllegalArgumentException("Color cannot be null");
		if (pointSize != 2 && pointSize != 3)
			throw new IllegalArgumentException("PointSize must be 2 or 3");
		this.elementType = elementType;
		this.data = data;
		this.colors = null;
		this.color = color;
		this.pointSize = pointSize;
	}

	public OldObject3D(int elementType, float[] data, float[] colors, int pointSize) {
		if (elementType < -2 || elementType > 1)
			throw new IllegalArgumentException("The elementType is unknow");
		if (data == null || data.length % pointSize != 0)
			throw new IllegalArgumentException("Data cannot be null or wrong data size");
		if (colors == null || colors.length != data.length)
			throw new IllegalArgumentException("Color cannot be null or colors size is not equal to data size");
		if (pointSize != 2 && pointSize != 3)
			throw new IllegalArgumentException("PointSize must be 2 or 3");
		this.elementType = elementType;
		this.data = data;
		this.colors = colors;
		this.color = null;
		this.pointSize = pointSize;
	}

	public OldObject3D(int elementType, float[] data, int pointSize) {
		if (elementType < -2 || elementType > 1)
			throw new IllegalArgumentException("The elementType is unknow");
		if (data == null)
			throw new IllegalArgumentException("Data cannot be null or wrong data size");
		if (pointSize != 2 && pointSize != 3)
			throw new IllegalArgumentException("PointSize must be 2 or 3");
		this.elementType = elementType;
		this.data = data;
		this.colors = null;
		this.color = null;
		this.pointSize = pointSize;
	}

	@Override
	public OldObject3D clone() {
		return this.colors != null ? new OldObject3D(this.elementType, this.data.clone(), this.colors.clone(), this.pointSize)
				: this.color != null ? new OldObject3D(this.elementType, this.data.clone(), this.color.clone(), this.pointSize) : new OldObject3D(this.elementType, this.data.clone(), this.pointSize);
	}

	@Override
	public void draw(GL2 gl, Color3f color) {
		if (this.elementType >= 0 && this.data.length != 0) {
			gl.glLineWidth(1);
			FloatBuffer dataBuffer = Buffers.newDirectFloatBuffer(this.data);
			dataBuffer.rewind();
			gl.glEnableClientState(GL_VERTEX_ARRAY);
			gl.glVertexPointer(3, GL_FLOAT, 0, dataBuffer);
			if (this.colors != null) {
				gl.glEnableClientState(GLPointerFunc.GL_COLOR_ARRAY);
				FloatBuffer colors = Buffers.newDirectFloatBuffer(this.colors);
				colors.rewind();
				gl.glColorPointer(3, GL_FLOAT, 0, colors);
				gl.glDrawArrays(this.elementType, 0, this.data.length / this.pointSize);
				gl.glDisableClientState(GLPointerFunc.GL_COLOR_ARRAY);
			} else if (this.color != null) {
				gl.glColor3f(this.color.x, this.color.y, this.color.z);
				gl.glDrawArrays(this.elementType, 0, this.data.length / this.pointSize);
			} else {
				gl.glColor3f(color.x, color.y, color.z);
				gl.glDrawArrays(this.elementType, 0, this.data.length / this.pointSize);
			}
			gl.glDisableClientState(GL_VERTEX_ARRAY);
		} else if (this.elementType == TEXT && this instanceof Text3D) {
			Text3D text3D = (Text3D) this;
			float[] data = text3D.data;
			for (int i = 0; i < data.length; i += 3) {
				gl.glPushMatrix();
				gl.glTranslated(data[i], data[i + 1], data[i + 2]);
				gl.glScalef(0.005f, 0.003f, 0.003f);
				GLUT glut = new GLUT();
				gl.glColor3f(color.x, color.y, color.z);
				glut.glutStrokeString(GLUT.STROKE_ROMAN, text3D.text[i / 3]);
				gl.glPopMatrix();
			}
		} else if (this.elementType == TEXTURE && this instanceof Texture3D) {
			Texture3D texture3D = (Texture3D) this;
			Texture texture = AWTTextureIO.newTexture(gl.getGLProfile(), texture3D.img, true);
			gl.glBindTexture(GL_TEXTURE_2D, texture.getTextureObject());

			gl.glEnable(GL_TEXTURE_2D);
			float[] data = texture3D.data;
			float trapWide = data[5] - data[1];
			float trapNarrow = data[7] - data[3];

			int indexData = 0;
			gl.glBegin(GL.GL_TRIANGLE_STRIP);
			gl.glColor3f(1, 1, 1);

			// glTexCoord4f(0,0,0,1);
			gl.glTexCoord4f(trapWide, trapWide, 0, trapWide);
			gl.glVertex3f(data[indexData++], data[indexData++], 0.01f);

			// glTexCoord4f(1,0,0,1);
			gl.glTexCoord4f(trapNarrow, 0, 0, trapNarrow);
			gl.glVertex3f(data[indexData++], data[indexData++], 0.01f);

			// glTexCoord4f(0,1,0,1);
			gl.glTexCoord4f(0, trapWide, 0, trapWide);
			gl.glVertex3f(data[indexData++], data[indexData++], 0.01f);

			// glTexCoord4f(1,1,0,1);
			gl.glTexCoord4f(0, 0, 0, trapNarrow);
			gl.glVertex3f(data[indexData++], data[indexData++], 0.01f);

			gl.glEnd();
			gl.glDisable(GL_TEXTURE_2D);
		}
	}

	@Override
	public String toString() {
		return "ElementType: " + (this.elementType == 0 ? "Points" : "Lines") + " Data size: " + this.data.length + " Point size: " + this.pointSize;
	}
}
