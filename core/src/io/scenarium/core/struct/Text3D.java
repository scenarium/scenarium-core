/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.struct;

import java.io.Serializable;

public class Text3D extends OldObject3D implements Serializable {
	private static final long serialVersionUID = 1L;
	public final String[] text;

	public Text3D(float[] coord, String[] text) {
		super(OldObject3D.TEXT, coord, 3);
		this.text = text;
	}

	@Override
	public Text3D clone() {
		return new Text3D(this.data.clone(), this.text.clone());
	}

	@Override
	public String toString() {
		return "ElementType: Text" + " Data size: " + this.data.length;
	}
}
