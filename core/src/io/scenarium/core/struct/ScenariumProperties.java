/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.struct;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import io.scenarium.core.filemanager.scenariomanager.ScenarioDescriptor;

public class ScenariumProperties {
	private static ScenariumProperties scenariumProperties;
	private ScenarioDescriptor[] recentScenarios;
	private ScenarioDescriptor[] playList;
	private int playListIndex = 0;
	private boolean synchronization = false;
	private int videoFrequencie = 40;
	private String mapTempPath;
	private Path[] externModules;

	private ScenariumProperties() {
		this.mapTempPath = System.getProperty("java.io.tmpdir");
		if (!this.mapTempPath.endsWith(File.separator))
			this.mapTempPath += File.separator;
		this.mapTempPath += "mapCache" + File.separator;
	}

	public static ScenariumProperties get() {
		if (scenariumProperties == null)
			scenariumProperties = new ScenariumProperties();
		return scenariumProperties;
	}

	public String getMapTempPath() {
		return this.mapTempPath;
	}

	public ScenarioDescriptor[] getPlayList() {
		return this.playList;
	}

	public int getPlayListIndex() {
		return this.playListIndex;
	}

	public ScenarioDescriptor[] getRecentScenarios() {
		return this.recentScenarios;
	}

	public int getVideoFrequencie() {
		return this.videoFrequencie;
	}

	public boolean isSynchronization() {
		return this.synchronization;
	}

	public void setMapTempPath(String mapTempPath) {
		Path parentPath = Paths.get(mapTempPath).getParent();
		if (parentPath != null && Files.exists(parentPath)) {
			if (!mapTempPath.endsWith(File.separator))
				mapTempPath += File.separator;
			this.mapTempPath = mapTempPath;
		}
	}

	public void setPlayList(ScenarioDescriptor[] playList) {
		this.playList = playList;
	}

	public void setPlayListIndex(int playListIndex) {
		this.playListIndex = playListIndex;
	}

	public void setRecentScenarios(ScenarioDescriptor[] recentScenarios) {
		this.recentScenarios = recentScenarios;
	}

	public void setSynchronization(boolean synchronization) {
		this.synchronization = synchronization;
	}

	public void setVideoFrequencie(int videoFrequencie) {
		this.videoFrequencie = videoFrequencie;
	}

	public Path[] getExternModules() {
		return this.externModules;
	}

	public void setExternModules(Path[] externModules) {
		this.externModules = externModules;
	}

}
