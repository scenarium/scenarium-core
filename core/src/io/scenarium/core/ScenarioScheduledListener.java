package io.scenarium.core;

import java.util.EventListener;

import io.scenarium.core.filemanager.scenariomanager.Scenario;

public interface ScenarioScheduledListener extends EventListener {
	public void scenarioScheduled(Scenario scenario, boolean schedulerChanged);
}
