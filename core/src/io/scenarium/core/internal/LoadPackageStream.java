package io.scenarium.core.internal;

import java.io.InputStream;

public class LoadPackageStream {
	private LoadPackageStream() {}

	public static InputStream getStream(String resourceName) {
		Log.verbose("Load resource: '" + resourceName + "'");
		InputStream out = LoadPackageStream.class.getResourceAsStream(resourceName);
		if (out == null)
			Log.error("Can not load resource: '" + resourceName + "'");
		return out;
	}
}
