/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.tools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.function.Function;

import io.scenarium.core.Scenarium;
import io.scenarium.core.internal.LoadPackageStream;
import io.scenarium.core.internal.Log;

public class LibraryUtils {

	private LibraryUtils() {}

	public static boolean loadLibrariesFromRessources(String... librariesNames) {
		return extractLibrariesFromRessources(LibraryUtils.class, null, null, true, librariesNames);
	}

	public static boolean loadLibrariesFromRessources(Path path, String... librariesNames) {
		return extractLibrariesFromRessources(LibraryUtils.class, null, path, true, librariesNames);
	}

	public static boolean loadLibrariesFromRessources(Class<?> resourcesClass, String... librariesNames) {
		return extractLibrariesFromRessources(resourcesClass, null, null, true, librariesNames);
	}

	public static boolean loadLibrariesFromRessources(Class<?> resourcesClass, Function<Throwable, String> loadingErrorMessageProvider, String... librariesNames) {
		return extractLibrariesFromRessources(resourcesClass, loadingErrorMessageProvider, null, true, librariesNames);
	}

	public static boolean loadLibrariesFromRessources(Class<?> resourcesClass, Path path, String... librariesNames) {
		return extractLibrariesFromRessources(resourcesClass, null, path, true, librariesNames);
	}

	public static boolean extractLibrariesFromRessources(Class<?> resourcesClass, Function<Throwable, String> loadingErrorMessageProvider, Path path, boolean loadLibraries, String... librariesNames) {
		boolean isAllAvailable = true;
		String libPath = getLibraryPath(path);
		Path p = Path.of(libPath);
		if (!Files.exists(p))
			try {
				Files.createDirectories(p);
			} catch (IOException e) {
				isAllAvailable = false;
				Log.error("Cannot create temp  library directory: " + p);
				return false;
			}
		for (String libName : librariesNames) {
			String libPat = System.mapLibraryName("");
			int iP = libPat.indexOf(".");
			String libFileName = libPat.substring(0, iP) + libName + libPat.substring(iP);
			String libFilePath = libFileName;
			if (path != null)
				libFilePath = path + "/" + libFilePath;
			InputStream libRessource = LoadPackageStream.getStream("/" + libFilePath);
			if (libRessource == null) {
				Log.error("Cannot find library " + libFilePath + " in ressource");
				isAllAvailable = false;
				break;
			}
			try {
				File tmpLibFilePath = new File(libPath + File.separator + libFileName);
				if (!tmpLibFilePath.exists()) {
					Files.copy(libRessource, tmpLibFilePath.toPath(), StandardCopyOption.REPLACE_EXISTING);
					tmpLibFilePath.deleteOnExit();
				}
				System.load(tmpLibFilePath.toString());
				Log.error(libName + " library loaded");
			} catch (IOException | UnsatisfiedLinkError e) {
				isAllAvailable = false;
				Log.error("Cannot load library: " + e.getMessage() + (loadingErrorMessageProvider == null ? "" : loadingErrorMessageProvider.apply(e)));
			}
		}
		return isAllAvailable;
	}

	public static String getLibraryPath(Path path) {
		String libPath = System.getProperty("java.io.tmpdir");
		if (!libPath.endsWith(File.separator))
			libPath += File.separator;
		libPath += Scenarium.class.getSimpleName() + File.separator;
		if (path != null) {
			libPath += path;
			if (!libPath.endsWith(File.separator))
				libPath += File.separator;
		}
		return libPath;
	}
}
