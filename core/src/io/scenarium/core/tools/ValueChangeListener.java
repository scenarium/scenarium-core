package io.scenarium.core.tools;

import java.util.EventListener;

public interface ValueChangeListener<T> extends EventListener {
	void valueChanged(T value);
}
