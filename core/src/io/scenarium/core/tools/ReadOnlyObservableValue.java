package io.scenarium.core.tools;

import javax.swing.event.EventListenerList;

public class ReadOnlyObservableValue<T> {
	private final EventListenerList listeners = new EventListenerList();
	protected T value;

	public T getValue() {
		return this.value;
	}

	public void addValueChangeListener(ValueChangeListener<T> listener) {
		this.listeners.add(ValueChangeListener.class, listener);
	}

	public void removeValueChangeListener(ValueChangeListener<T> listener) {
		this.listeners.remove(ValueChangeListener.class, listener);
	}

	@SuppressWarnings("unchecked")
	protected void fireValueChanged() {
		for (ValueChangeListener<T> listener : this.listeners.getListeners(ValueChangeListener.class))
			listener.valueChanged(this.value);
	}
}
