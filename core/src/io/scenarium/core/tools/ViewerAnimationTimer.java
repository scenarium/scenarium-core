package io.scenarium.core.tools;

import io.scenarium.core.timescheduler.VisuableSchedulable;

public interface ViewerAnimationTimer {
	public void register(VisuableSchedulable visuableSchedulable);

	public void unRegister(VisuableSchedulable visuableSchedulable);

	public void start();

	public void stop();
}
