package io.scenarium.core.tools;

import java.util.function.Supplier;

import io.scenarium.core.timescheduler.VisuableSchedulable;

public class ViewerAnimationTimerSupplier {

	private ViewerAnimationTimerSupplier() {}

	private static Supplier<ViewerAnimationTimer> instanceSupplier = () -> {

		return new ViewerAnimationTimer() {

			@Override
			public void unRegister(VisuableSchedulable visuableSchedulable) {}

			@Override
			public void stop() {}

			@Override
			public void start() {}

			@Override
			public void register(VisuableSchedulable visuableSchedulable) {}
		};
	};

	public static ViewerAnimationTimer create() {
		return instanceSupplier.get();
	}

	public static void setInstanceSupplier(Supplier<ViewerAnimationTimer> instanceSupplier) {
		ViewerAnimationTimerSupplier.instanceSupplier = instanceSupplier;
	}
}
