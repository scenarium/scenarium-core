package io.scenarium.core.tools;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class SchedulerUtils {

	private SchedulerUtils() {}

	public static ScheduledExecutorService getTimer() {
		return Executors.newScheduledThreadPool(1);
	}

	public static ScheduledExecutorService getTimer(String name) {
		return Executors.newScheduledThreadPool(1, r -> new Thread(r, name));
	}
}
