/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TimeStampSynchronizer {

	private ArrayList<ArrayList<Long>> timeStampListList;
	private ArrayList<ArrayList<Object>> objectListList;
	private Object[] objectToSend;

	private int nbData;

	private long[] timeStamps; // Buffer uses for input or output timeStamps
	private long timeStampToSend; // Buffer uses for input or output timeStamps
	private int[] nbDataMax = null; // Buffer size of each input.
	private final Function<Integer, Long> timeStampProvider;
	private int nMaxDelta = 0; // Maximal difference between timestamp for an association. Should be inferior to the half of the minimun period of the inputs.

	public TimeStampSynchronizer(int nbData, Function<Integer, Long> timeStampProvider, int nMaxDelta) {
		this.timeStampProvider = timeStampProvider;
		this.nMaxDelta = nMaxDelta;
	}

	public TimeStampSynchronizer(int nbData, Function<Integer, Long> timeStampProvider, int[] nbDataMax, int nMaxDelta) {
		this.timeStampProvider = timeStampProvider;
		this.nbDataMax = nbDataMax;
		this.nMaxDelta = nMaxDelta;
	}

	public TimeStampSynchronizer(int nbData, Function<Integer, Long> timeStampProvider, int[] nbDataMax) {
		this.timeStampProvider = timeStampProvider;
		this.nbDataMax = nbDataMax;
	}

	public TimeStampSynchronizer(int nbData, Function<Integer, Long> timeStampProvider) {
		this.timeStampProvider = timeStampProvider;
	}

	public void feed(BiConsumer<Object[], Long> processMethod, Object... data) {
		boolean full = false;
		this.nbData = data.length;
		// Initialisation des Arrays.
		if (this.objectListList == null) {
			this.timeStampListList = (ArrayList<ArrayList<Long>>) Stream.generate(ArrayList<Long>::new).limit(this.nbData).collect(Collectors.toList());
			this.objectListList = (ArrayList<ArrayList<Object>>) Stream.generate(ArrayList<Object>::new).limit(this.nbData).collect(Collectors.toList());
			this.timeStamps = new long[this.nbData];
			// this.nbDataMax = new int[nbData];
			// Arrays.fill(nbDataMax,25);
		}

		this.objectToSend = new Object[this.nbData];

		for (int i = 0; i < data.length; i++)
			if (data[i] != null) {
				long inputTs = this.timeStampProvider.apply(i);
				this.timeStamps[i] = inputTs;
				if (this.nbDataMax != null && this.nbDataMax.length > i) {
					if (this.timeStampListList.get(i).size() > this.nbDataMax[i]) {
						this.timeStampListList.get(i).remove(0);
						this.objectListList.get(i).remove(0);
					}
				} else if (this.timeStampListList.get(i).size() > 25) {
					this.timeStampListList.get(i).remove(0);
					this.objectListList.get(i).remove(0);
				}
				this.timeStampListList.get(i).add(inputTs);
				this.objectListList.get(i).add(data[i]);
			}
		Arrays.sort(this.timeStamps);
		for (int j = 0; j < data.length; j++) {
			long tsAct = this.timeStamps[j];
			if (tsAct != 0) {
				full = searchTs(tsAct, full);
				if (full)
					break;
			}
		}
		if (full) {
			processMethod.accept(this.objectToSend.clone(), this.timeStampToSend);
			Arrays.fill(this.objectToSend, null);
			Arrays.fill(this.timeStamps, 0);
		}
	}

	private boolean searchTs(long tsLf, boolean full) {
		int[] comptTab = new int[this.objectListList.size()];
		int k = 0;
		this.timeStampToSend = tsLf;
		for (k = 0; k < this.objectListList.size(); k++) {
			long tsSearch = -1L;
			int compt = 0;
			ArrayList<Long> timeStampListAct = this.timeStampListList.get(k);
			while (Math.abs(tsSearch - tsLf) > this.nMaxDelta && compt < timeStampListAct.size())
				tsSearch = timeStampListAct.get(compt++);
			if (Math.abs(tsSearch - tsLf) > this.nMaxDelta)
				break;
			if (tsSearch > this.timeStampToSend)
				this.timeStampToSend = tsSearch;
			comptTab[k] = compt - 1;
			if (k == this.objectListList.size() - 1)
				full = true;
		}
		if (full)
			for (int i = 0; i < this.objectListList.size(); i++) {
				int toIndex = comptTab[i];
				this.objectToSend[i] = this.objectListList.get(i).get(toIndex);
				toIndex++;
				this.objectListList.get(i).subList(0, toIndex).clear();
				this.timeStampListList.get(i).subList(0, toIndex).clear();
			}
		return full;
	}
}
