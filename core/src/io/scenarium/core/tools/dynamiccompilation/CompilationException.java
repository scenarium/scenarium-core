/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.tools.dynamiccompilation;

import java.util.List;

import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

public class CompilationException extends Exception {
	private static final long serialVersionUID = 1L;
	private final List<Diagnostic<? extends JavaFileObject>> errors;

	public CompilationException(List<Diagnostic<? extends JavaFileObject>> errors) {
		super();
		this.errors = errors;
	}

	public List<Diagnostic<? extends JavaFileObject>> getErrors() {
		return this.errors;
	}
}