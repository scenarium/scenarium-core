/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.tools.dynamiccompilation;

import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.SimpleJavaFileObject;
import javax.tools.ToolProvider;

import io.scenarium.core.internal.Log;

public class InlineCompiler {
	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder(64);
		sb.append("package calculator;\n");
		sb.append("public class Calculator {\n");
		sb.append("    public int compute(int a, int b) {\n");
		sb.append("        return a + b * 4;\n");
		sb.append("    }\n");
		sb.append("}\n");
		try {
			InlineCompiler ic = new InlineCompiler("calculator.Calculator", sb.toString());
			try {
				Object obj = ic.getCompileClass()[0].getConstructor().newInstance();
				Log.info("résultat: " + obj.getClass().getMethod("compute", int.class, int.class).invoke(obj, 3, 7));
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
				ex.printStackTrace();
			}
			ic.close();
		} catch (CompilationException ex) {
			for (Diagnostic<? extends JavaFileObject> error : ex.getErrors())
				Log.error("Error on line: " + error.getLineNumber() + ", message: " + error.getMessage(Locale.getDefault()));
		}
	}

	private MemoryClassLoader classLoader;

	private Class<?>[] c;

	public InlineCompiler(String className, String code) throws CompilationException {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		if (compiler == null)
			throw new RuntimeException("Could not get Java compiler. Please, ensure that JDK is used instead of JRE.");
		DiagnosticCollector<JavaFileObject> diagnosticCollector = new DiagnosticCollector<>();
		MemoryJavaFileManager fileManager = new MemoryJavaFileManager(compiler.getStandardFileManager(null, null, null));
		CompilationTask task = compiler.getTask(null, fileManager, diagnosticCollector,
				Arrays.asList("-classpath", System.getProperty("java.class.path") + System.getProperty("path.separator") + System.getProperty("jdk.module.path")), null,
				Arrays.asList(MemoryJavaFileManager.makeStringSource(className, code)));
		if (!task.call())
			throw new CompilationException(diagnosticCollector.getDiagnostics());
		this.classLoader = new MemoryClassLoader(fileManager.getClassBytes());
		this.c = new Class<?>[1];
		try {
			this.c[0] = this.classLoader.loadClass(className);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public InlineCompiler(String[] className, String[] code) throws CompilationException {
		if (className.length != className.length)
			throw new IllegalArgumentException("className and sb don't have the same length");
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		if (compiler == null)
			throw new RuntimeException("Could not get Java compiler. Please, ensure that JDK is used instead of JRE.");
		DiagnosticCollector<JavaFileObject> diagnosticCollector = new DiagnosticCollector<>();
		MemoryJavaFileManager fileManager = new MemoryJavaFileManager(compiler.getStandardFileManager(null, null, null));
		this.classLoader = new MemoryClassLoader(fileManager.getClassBytes());
		this.c = new Class<?>[className.length];
		for (int i = 0; i < className.length; i++) {
			CompilationTask task = compiler.getTask(null, fileManager, diagnosticCollector,
					Arrays.asList("-classpath", System.getProperty("java.class.path") + System.getProperty("path.separator") + System.getProperty("jdk.module.path")), null,
					Arrays.asList(MemoryJavaFileManager.makeStringSource(className[i], code[i])));
			if (!task.call())
				throw new CompilationException(diagnosticCollector.getDiagnostics());
			try {
				this.c[i] = this.classLoader.loadClass(className[i]);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public void close() {
		try {
			this.classLoader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.classLoader = null;
		this.c = null;
	}

	public Class<?>[] getCompileClass() {
		return this.c;
	}
}

class MemoryClassLoader extends URLClassLoader {
	private final Map<String, byte[]> classBytes;

	public MemoryClassLoader(Map<String, byte[]> classBytes) {
		this(classBytes, ClassLoader.getSystemClassLoader());
	}

	public MemoryClassLoader(Map<String, byte[]> classBytes, ClassLoader parent) {
		super(new URL[0], parent);
		this.classBytes = classBytes;
	}

	@Override
	protected Class<?> findClass(String className) throws ClassNotFoundException {
		byte[] buf = this.classBytes.get(className);
		if (buf != null) {
			this.classBytes.put(className, null);
			return defineClass(className, buf, 0, buf.length);
		}
		return super.findClass(className);
	}

	public Class<?> load(String className) throws ClassNotFoundException {
		return loadClass(className);
	}

	public Iterable<Class<?>> loadAll() throws ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<>(this.classBytes.size());
		for (String name : this.classBytes.keySet())
			classes.add(loadClass(name));
		return classes;
	}
}

final class MemoryJavaFileManager extends ForwardingJavaFileManager<JavaFileManager> {
	private class ClassOutputBuffer extends SimpleJavaFileObject {
		private final String name;

		ClassOutputBuffer(String name) {
			super(toURI(name), Kind.CLASS);
			this.name = name;
		}

		@Override
		public OutputStream openOutputStream() {
			return new FilterOutputStream(new ByteArrayOutputStream()) {
				@Override
				public void close() throws IOException {
					this.out.close();
					ByteArrayOutputStream bos = (ByteArrayOutputStream) this.out;
					MemoryJavaFileManager.this.classBytes.put(ClassOutputBuffer.this.name, bos.toByteArray());
				}
			};
		}
	}

	private static class StringInputBuffer extends SimpleJavaFileObject {
		final String code;

		StringInputBuffer(String fileName, String code) {
			super(toURI(fileName), Kind.SOURCE);
			this.code = code;
		}

		@Override
		public CharBuffer getCharContent(boolean ignoreEncodingErrors) {
			return CharBuffer.wrap(this.code);
		}
	}

	private static final String EXT = ".java";

	static JavaFileObject makeStringSource(String className, String code) {
		return new StringInputBuffer(className + ".java", code);
	}

	static URI toURI(String name) {
		try {
			final StringBuilder newUri = new StringBuilder();
			newUri.append("mfm:///");
			newUri.append(name.replace('.', '/'));
			if (name.endsWith(EXT))
				newUri.replace(newUri.length() - EXT.length(), newUri.length(), EXT);
			return URI.create(newUri.toString());
		} catch (Exception exp) {
			return URI.create("mfm:///com/sun/script/java/java_source");
		}
	}

	private Map<String, byte[]> classBytes;

	public MemoryJavaFileManager(JavaFileManager fileManager) {
		super(fileManager);
		this.classBytes = new HashMap<>();
	}

	@Override
	public void close() throws IOException {
		this.classBytes = null;
	}

	@Override
	public void flush() throws IOException {}

	public Map<String, byte[]> getClassBytes() {
		return this.classBytes;
	}

	@Override
	public JavaFileObject getJavaFileForOutput(JavaFileManager.Location location, String className, Kind kind, FileObject sibling) throws IOException {
		return new ClassOutputBuffer(className);
	}
}