package io.scenarium.core.tools;

public class ObservableValue<T> extends ReadOnlyObservableValue<T> {

	public ObservableValue(T value) {
		this.value = value;
	}

	public void setValue(T value) {
		this.value = value;
	}
}
