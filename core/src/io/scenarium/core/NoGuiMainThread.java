/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;

import io.beanmanager.tools.Tuple;

public class NoGuiMainThread extends Thread implements TaskConsumer {
	private final LinkedBlockingDeque<Tuple<Runnable, CountDownLatch>> blockingDeque = new LinkedBlockingDeque<>();

	public NoGuiMainThread() {
		setName("ScenariumMainThread");
	}

	@Override
	public void runTask(Runnable runnable) {
		if (Thread.currentThread().getId() == getId())
			runnable.run();
		else {
			if (!isAlive())
				start();
			try {
				this.blockingDeque.put(new Tuple<>(runnable, null));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void runTaskAndWait(Runnable runnable) {
		if (Thread.currentThread().getId() == getId())
			runnable.run();
		else {
			if (!isAlive())
				start();
			CountDownLatch cdl = new CountDownLatch(1);
			try {
				this.blockingDeque.put(new Tuple<>(runnable, cdl));
				cdl.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void run() {
		while (!isInterrupted())
			try {
				Tuple<Runnable, CountDownLatch> task = this.blockingDeque.take();
				task.getFirst().run();
				CountDownLatch cdl = task.getSecond();
				if (cdl != null)
					cdl.countDown();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}
}
