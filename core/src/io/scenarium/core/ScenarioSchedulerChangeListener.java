package io.scenarium.core;

import java.util.EventListener;

import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.timescheduler.Scheduler;

public interface ScenarioSchedulerChangeListener extends EventListener {
	public void scenarioSchedulerChanged(Scenario scenario, Scheduler newScheduler);
}
