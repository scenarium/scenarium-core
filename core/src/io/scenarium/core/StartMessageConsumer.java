package io.scenarium.core;

public interface StartMessageConsumer {

	public void consumeInformation(String message);

	public void consumeException(Exception exception);

	public void consumeError(String message);

}
