/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.math;

import io.scenarium.core.internal.Log;

public class AlgebraicDerivator implements FilterFunction {
	public static void main(String[] args) {
		AlgebraicDerivator ad = new AlgebraicDerivator();
		ad.setWindowsSize(10);
		Log.info("" + ad);
	}

	double[] coeff;
	double[] res;

	private int windowsSize;

	public double compute(double[] signal, int index) {
		double[] s = new double[this.windowsSize];
		for (int i = 0; i < this.windowsSize; i++) {
			s[i] = this.coeff[i] * signal[index];
			if (index > 0)
				index--;
			else
				index = this.windowsSize - 1;
		}
		double output = 0.0;
		// integrale par methode des trapezes
		for (int i = 1; i < this.windowsSize - 1; i++)
			output += s[i];
		output = output + 0.5 * (s[0] + s[this.windowsSize - 1]);
		return output;
	}

	@Override
	public double evaluate(double z) {
		return 0;
	}

	public int getWindowsSize() {
		return this.windowsSize;
	}

	@Override
	public void resolve(double[] x, double[] y) {
		this.res = new double[x.length];
		for (int i = 0; i < x.length; i++)
			this.res[i] = compute(y, i);
	}

	public void setWindowsSize(int windowsSize) {
		if (windowsSize <= 2)
			throw new IllegalArgumentException("windowsSize <= 2");
		this.windowsSize = windowsSize;
		this.coeff = new double[windowsSize];
		double windowLength = windowsSize - 1;
		for (int i = 0; i < windowsSize; i++)
			this.coeff[i] = 6 / (windowLength * windowLength * windowLength) * (windowLength - 2 * i);
	}
}
