/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.math;

public class ConstantFunction implements Function {
	private double value = 0;

	public ConstantFunction() {}

	public ConstantFunction(double value) {
		this.value = value;
	}

	@Override
	public ConstantFunction clone() {
		try {
			return (ConstantFunction) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public double evaluate(double x) {
		return this.value;
	}

	public double getValue() {
		return this.value;
	}

	public void setValue(double value) {
		this.value = value;
	}
}
