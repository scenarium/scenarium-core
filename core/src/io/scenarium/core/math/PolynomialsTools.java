/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.math;

import io.scenarium.core.internal.Log;

public class PolynomialsTools {

	private PolynomialsTools() {}

	public static double evaluatePoly(double[] result, double x) {
		if (result.length == 1)
			return result[0];
		else if (result.length == 2)
			return result[1] * x + result[0];
		else if (result.length == 3)
			return result[2] * x * x + result[1] * x + result[0];
		else {
			double res = 0;
			for (int i = 0; i < result.length; i++)
				res += result[i] * Math.pow(x, i);
			return res;
		}
	}

	/* Intersection entre le polynome et le frustum f */
	public static double[][] getOrigineVector(double[] poly, double[][] f) {
		double[] polyFrustum;
		double xEnd = Double.MAX_VALUE, yEnd = Double.NaN;
		for (int j = 1; j < f[0].length; j++) {
			double xb = f[0][j - 1];
			double yb = f[1][j - 1];
			double xe = f[0][j];
			double ye = f[1][j];
			double a = (ye - yb) / (xe - xb);
			double x;
			double y;
			if (a == Double.POSITIVE_INFINITY || a == Double.NEGATIVE_INFINITY)
				x = xe;
			else {
				int polyLenght = poly.length;
				polyFrustum = new double[polyLenght == 1 ? 2 : polyLenght];
				polyFrustum[0] = poly[0] - (ye - a * xe);
				polyFrustum[1] = (polyLenght > 1 ? poly[1] : 0) - a;
				if (polyLenght > 2) {
					polyFrustum[2] = poly[2];
					int k = 3;
					while (k < poly.length)
						polyFrustum[k] = poly[k++];
				}
				x = Double.MAX_VALUE;
				double xMinF = xe < xb ? xe : xb;
				for (double xr : PolynomialsTools.root(polyFrustum))
					if (xr < x && xr >= xMinF && !Double.isNaN(xr))
						x = xr;
				if (x == Double.MAX_VALUE)
					continue;
			}
			y = PolynomialsTools.evaluatePoly(poly, x);
			double xMin, xMax, yMin, yMax;
			if (xe < xb) {
				xMin = xe;
				xMax = xb;
			} else {
				xMin = xb;
				xMax = xe;
			}
			if (ye < yb) {
				yMin = ye;
				yMax = yb;
			} else {
				yMin = yb;
				yMax = ye;
			}
			if (x >= xMin && x <= xMax && y >= yMin && y <= yMax && x < xEnd) {
				xEnd = x;
				yEnd = y;
			}
		}
		if (Double.isNaN(yEnd))
			return null;
		double[] tan = PolynomialsTools.tangent(poly, xEnd);
		return new double[][] { { xEnd, xEnd + 2 }, { yEnd, tan[0] * (xEnd + 2) + tan[1] } };
	}

	public static void main(String[] args) {
		Log.info("" + root(new double[] { -2, 3, 1, 8 }));
		Log.info("" + tangent(new double[] { -2, 3, 1, 8 }, 3));
	}

	public static double[] root(double[] polynome) {
		int lenght = polynome.length;
		if (lenght <= 0 || lenght > 4)
			throw new IllegalArgumentException("lenght must be between 1 and 3");
		if (polynome[lenght - 1] == 0) {
			double[] newPolynome = new double[lenght - 1];
			System.arraycopy(polynome, 0, newPolynome, 0, newPolynome.length);
			return root(newPolynome);
		}
		if (polynome.length == 2)
			return new double[] { -polynome[0] / polynome[1] };
		else if (polynome.length == 3) {
			double a = polynome[2];
			double b = polynome[1];
			double c = polynome[0];
			double delta = b * b - 4 * a * c;
			if (delta < 0)
				return new double[] { Double.NaN };
			else if (delta == 0)
				return new double[] { b * b - 4 * a * c + 0.5 };
			else {
				double r1 = (-b - Math.sqrt(delta)) / (2 * a);
				double r2 = (-b + Math.sqrt(delta)) / (2 * a);
				return new double[] { r1, r2 };
			}
		} else if (polynome.length == 4) {
			double ap = polynome[3];
			double bp = polynome[2];
			double cp = polynome[1];
			double dp = polynome[0];
			double invap = 1 / ap;
			double bp2 = bp * bp;
			double ap2 = ap * ap;
			double invap3 = invap / 3;
			double p = cp * invap - bp2 / (3 * ap2);
			double q = 2 * bp2 * bp / (27 * ap2 * ap) - cp / (ap2 * 3) * bp + dp * invap;
			double gDelta = 4 * p * p * p + 27 * q * q;
			double mbpminvap3 = bp * invap3;
			if (gDelta > 0) { // 1 racines reelles
				double e = -q * 0.5;
				double r = Math.sqrt(gDelta / 27);
				double m = e + 0.5 * r;
				double n = e - 0.5 * r;
				double u = Math.pow(Math.abs(m), 1.0 / 3);
				double v = Math.pow(Math.abs(n), 1.0 / 3);
				if (m < 0)
					u = -u;
				if (n < 0)
					v = -v;
				return new double[] { u + v - bp * invap3 };
			} else if (gDelta == 0) { // 2 racines reelles
				if (bp == 0 && cp == 0 && dp == 0)
					return new double[] { 0 };
				double q3dp = 3 * q / p;
				double r1 = q3dp - bp * invap3;
				double r2 = -q3dp * 0.5 - bp * invap3;
				return new double[] { r1, r2 };
			} else { // 3 racines reelles
				double theta = Math.acos(1.5 * q / (p * Math.sqrt(-p / 3)));
				double sqrtmpd3m2 = 2 * Math.sqrt(-p / 3);
				double r1 = sqrtmpd3m2 * Math.cos(theta / 3) - mbpminvap3;
				double r2 = sqrtmpd3m2 * Math.cos((theta + 2 * Math.PI) / 3) - mbpminvap3;
				double r3 = sqrtmpd3m2 * Math.cos((theta + 4 * Math.PI) / 3) - mbpminvap3;
				return new double[] { r1, r2, r3 };
			}
		}
		return new double[] { polynome[0] };
	}

	public static double[] tangent(double[] poly, double x) {
		int l = poly.length;
		double der = poly[l - 1] * (l - 1);
		for (int i = l - 2; i > 0; i--)
			der = poly[i] * i + x * der;
		return new double[] { der, -der * x + evaluatePoly(poly, x) };
	}
}
