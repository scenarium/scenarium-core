/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.math.thresholding;

public class OtsuThresholding implements Thresholder {
	@Override
	public Thresholder clone() {
		return new OtsuThresholding();
	}

	@Override
	public int computeThreshold(int[] srcData) {
		int[] histData = new int[256];
		for (int i = 0; i < srcData.length; i++)
			histData[srcData[i]]++;
		// Total number of pixels
		int total = srcData.length;
		float sum = 0;
		for (int t = 0; t < 256; t++)
			sum += t * histData[t];
		float sumB = 0;
		int wB = 0;
		int wF = 0;
		float varMax = 0;
		int threshold = 0;
		for (int t = 0; t < 256; t++) {
			wB += histData[t]; // Weight Background
			if (wB == 0)
				continue;
			wF = total - wB; // Weight Foreground
			if (wF == 0)
				break;
			sumB += t * histData[t];
			float mB = sumB / wB; // Mean Background
			float mF = (sum - sumB) / wF; // Mean Foreground
			// Calculate Between Class Variance
			float varBetween = (float) wB * (float) wF * (mB - mF) * (mB - mF);
			// Check if new maximum found
			if (varBetween > varMax) {
				varMax = varBetween;
				threshold = t;
			}
		}
		return threshold;
	}

	@Override
	public String toString() {
		return "Otsu";
	}
}
