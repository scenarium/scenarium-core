/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.math;

import io.scenarium.core.internal.Log;
import io.scenarium.core.struct.curve.Curved;

public class CurveUtils {

	private CurveUtils() {}

	public static double[] add(double[] data, int incr) {
		for (int i = 0; i < data.length; i++)
			data[i] += incr;
		return data;
	}

	public static double[] derivate(double[] data, int dis) {
		double[] result = new double[data.length];
		if (data.length <= dis * 2)
			for (int i = 0; i < data.length; i++)
				data[i] = 0;
		else {
			result[0] = 0;
			int end = data.length - dis;
			for (int i = dis; i < end; i++)
				result[i] = (data[i + dis] - data[i - dis]) / 2.0;
			result[end] = 0;
		}
		return result;
	}

	public static double[] gaussianBlur(double[] data, double alpha, double epsilon) {
		double[] windows;
		double[] result = new double[data.length];
		int wid = data.length;
		double corr = 0;
		if (alpha == 0) {
			windows = new double[1];
			windows[0] = 1;
		} else {
			double val = 1;
			int i = 1;
			for (; val > epsilon; i++)
				// Critére précision un peu pourris
				val = gaussienne(alpha, i);
			i -= 2;
			windows = new double[i * 2 + 1];
			for (int j = 0; j < windows.length; j++) {
				val = gaussienne(alpha, j - i);
				corr += val;
				windows[j] = val;
			}
			double ratio = 1 / windows[(int) (windows.length / 2.0)];
			for (int j = 0; j < windows.length; j++)
				windows[j] *= ratio;

			for (int j = 0; j < windows.length / 2; j++)
				windows[j] = 0;
			corr = 1 / corr;
		}
		int halfSize = windows.length / 2;
		if (wid <= windows.length)
			for (int i = 0; i < wid; i++) {
				double gaussValue = 0;
				int index = i - halfSize;
				for (int j = 0; j < windows.length; j++)
					if (index >= 0 && index < wid)
						gaussValue += data[index++] * windows[j];
				result[i] = gaussValue;
			}
		else {
			int i = 0;
			for (; i < halfSize; i++) {
				double gaussValue = 0;
				int index = 0;
				for (int j = halfSize - i; j < windows.length; j++)
					gaussValue += data[index++] * windows[j];
				result[i] = gaussValue;
			}
			for (; i < wid - halfSize; i++) {
				double gaussValue = 0;
				int index = i - halfSize;
				for (int j = 0; j < windows.length; j++)
					gaussValue += data[index++] * windows[j];
				result[i] = gaussValue;
			}
			for (; i < wid; i++) {
				double gaussValue = 0;
				int index = i - halfSize;
				int end = wid - index;
				for (int j = 0; j < end; j++)
					gaussValue += data[index++] * windows[j];
				result[i] = gaussValue;
			}
		}
		return result;
	}

	public static double[] gaussianBlur(int[] data, double alpha) {
		double[] windows;
		double[] result = new double[data.length];
		int wid = data.length;
		double corr = 0;
		if (alpha == 0) {
			windows = new double[1];
			windows[0] = 1;
		} else {
			double val = 1;
			int i = 1;
			double limit = gaussienne(alpha, alpha * 3);
			for (; val > limit; i++)
				// Critére précision un peu pourris
				val = gaussienne(alpha, i);
			i -= 2;
			windows = new double[i * 2 + 1];
			for (int j = 0; j < windows.length; j++) {
				val = gaussienne(alpha, j - i);
				corr += val;
				windows[j] = val;
			}
			corr = 1 / corr;
		}
		int halfSize = windows.length / 2;
		if (wid <= windows.length)
			for (int i = 0; i < wid; i++) {
				double gaussValue = 0;
				int index = i - halfSize;
				for (int j = 0; j < windows.length; j++)
					if (index >= 0 && index < wid)
						gaussValue += data[index++] * windows[j];
				result[i] = gaussValue;
			}
		else {
			int i = 0;
			for (; i < halfSize; i++) {
				double sum = 0;
				double gaussValue = 0;
				int index = 0;
				for (int j = halfSize - i; j < windows.length; j++) {
					gaussValue += data[index++] * windows[j];
					sum += windows[j];
				}
				result[i] = gaussValue / sum;
			}
			for (; i < wid - halfSize; i++) {
				double gaussValue = 0;
				int index = i - halfSize;
				for (int j = 0; j < windows.length; j++)
					gaussValue += data[index++] * windows[j];
				result[i] = gaussValue * corr;
			}
			for (; i < wid; i++) {
				double sum = 0;
				double gaussValue = 0;
				int index = i - halfSize;
				int end = wid - index;
				for (int j = 0; j < end; j++) {
					gaussValue += data[index++] * windows[j];
					sum += windows[j];
				}
				result[i] = gaussValue / sum;
			}
		}
		return result;
	}

	private static double gaussienne(double alpha, double x) {
		return 1 / (alpha * Math.sqrt(2 * Math.PI)) * Math.exp(-(x * x) / (2 * alpha * alpha));
	}

	public static void main(String[] args) {
		int alpha = 1;
		Log.info("" + gaussienne(alpha, alpha * 3));
	}

	public static double maxFluctuation(Curved c) {
		double[][] data = c.getData();
		int nbElement = data[0].length;
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		for (int i = nbElement; i-- != 0;) {
			double val = data[1][i];
			if (val < min)
				min = val;
			else if (val > max)
				max = val;
		}
		return max - min;
	}

	public static double[] mul(double[] data, double mul) {
		for (int i = 0; i < data.length; i++)
			data[i] *= mul;
		return data;
	}

	public static double standartDeviation(Curved c) {
		double moy = 0, cum2 = 0;
		double[][] data = c.getData();
		int nbElement = data[0].length;
		for (int i = nbElement; i-- != 0;) {
			double val = data[1][i];
			moy += val;
			cum2 += val * val;
		}
		moy /= nbElement;
		return Math.sqrt(cum2 / nbElement - moy * moy);
	}

	public static Curved standartDeviation(Curved c1, Curved c2) {
		// double value;
		double[] c1Data = c1.getData()[1];
		double[] c2Data = c2.getData()[1];
		int size = Math.min(c1Data.length, c2Data.length);
		Curved c3 = new Curved(new double[2][size]);
		System.arraycopy((c1Data.length < c2Data.length ? c1 : c2).getData()[0], 0, c3.getData()[0], 0, size);
		double[] c3Data = c3.getData()[1];
		for (int j = 0; j < size; j++)
			c3Data[j] = c1Data[j] - c2Data[j];
		// c3Data[j] = value * value;
		return c3;
	}

	public static double standartDeviationPrecise(Curved c) {
		double moy = 0, cum2 = 0;
		double[][] data = c.getData();
		int nbElement = data[0].length;
		for (int i = nbElement; i-- != 0;)
			moy += data[1][i];
		moy /= nbElement;
		for (int i = nbElement; i-- != 0;) {
			double val = data[1][i] - moy;
			cum2 += val * val;
		}
		return Math.sqrt(cum2 / nbElement);
	}

}
