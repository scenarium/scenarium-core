/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.math;

public class CorrelationDerivator {
	private int windowsSize;
	private double[] y;

	public double evaluate(int index) {
		if (index < this.windowsSize - 1 || index > this.y.length - this.windowsSize - 1)
			return 0;
		double value = 0;
		for (int i = index; i > index - this.windowsSize; i--)
			value -= this.y[i];
		for (int i = index + 1; i < index + this.windowsSize + 1; i++)
			value += this.y[i];
		return value;
	}

	public int getWindowsSize() {
		return this.windowsSize;
	}

	public void resolve(double[] x, double[] y) {
		this.y = y;
	}

	public void setWindowsSize(int windowsSize) {
		if (windowsSize <= 2)
			throw new IllegalArgumentException("windowsSize <= 2");
		this.windowsSize = windowsSize;
	}
}
