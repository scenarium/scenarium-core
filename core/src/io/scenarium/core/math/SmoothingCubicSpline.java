/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.math;

import java.util.Arrays;
import java.util.Objects;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.scenarium.core.internal.Log;

public class SmoothingCubicSpline implements SmoothingFunction {
	public static void main(String[] args) {
		new SmoothingCubicSpline().resolve(new double[] { 1, 2, 3 }, new double[] { 3, 2, 1 });
	}

	private static double[] quincunx(double[] u, double[] v, double[] w, double[] q) {
		u[0] = 0.0D;
		v[1] = v[1] / u[1];
		w[1] = w[1] / u[1];
		for (int j = 2; j < u.length - 1; j++) {
			u[j] = u[j] - u[j - 2] * w[j - 2] * w[j - 2] - u[j - 1] * v[j - 1] * v[j - 1];
			v[j] = (v[j] - u[j - 1] * v[j - 1] * w[j - 1]) / u[j];
			w[j] = w[j] / u[j];
		}
		q[1] = q[1] - v[0] * q[0];
		for (int j = 2; j < u.length - 1; j++)
			q[j] = q[j] - v[j - 1] * q[j - 1] - w[j - 2] * q[j - 2];
		for (int j = 1; j < u.length - 1; j++)
			q[j] = q[j] / u[j];
		q[u.length - 1] = 0.0D;
		for (int j = u.length - 3; j > 0; j--)
			q[j] = q[j] - v[j] * q[j + 1] - w[j] * q[j + 2];
		return q;
	}

	private Polynomial[] splineVector;
	public double[] x;
	public double[] y;

	public double[] w;

	@PropertyInfo(info = "Smoothing parameter which represents its accuracy with respect to the nodes. 0 corresponds to a linear function. 1 corresponds to an interpolating spline")
	@NumberInfo(min = 0, max = 1)
	private double rho = 0.5;

	public SmoothingCubicSpline() {}

	public SmoothingCubicSpline(double rho) {
		this.rho = rho;
	}

	@Override
	public SmoothingFunction addPoint(double x, double y, double w) {
		int newSize = this.x.length + 1;
		double[] xp = new double[newSize];
		double[] yp = new double[newSize];
		double[] wp = new double[newSize];
		System.arraycopy(this.x, 0, xp, 0, this.x.length);
		System.arraycopy(this.y, 0, yp, 0, this.y.length);
		System.arraycopy(this.w, 0, wp, 0, this.w.length);
		int lastIndex = newSize - 1;
		xp[lastIndex] = x;
		yp[lastIndex] = y;
		wp[lastIndex] = w;
		SmoothingCubicSpline scs = new SmoothingCubicSpline();
		scs.setRho(this.rho);
		scs.resolve(xp, yp, wp);
		return scs;
	}

	@Override
	public SmoothingCubicSpline clone() {
		SmoothingCubicSpline scs = parametricClone();
		scs.setRho(this.rho);
		if (this.splineVector != null) {
			scs.splineVector = new Polynomial[this.splineVector.length];
			for (int i = 0; i < this.splineVector.length; i++)
				scs.splineVector[i] = this.splineVector[i].clone();
		}
		if (this.x != null) {
			scs.x = this.x.clone();
			scs.y = this.y.clone();
			scs.w = this.w.clone();
		}
		return scs;
	}

	public double derivate(double z) {
		int i = getFitPolynomialIndex(z);
		return this.splineVector[i].derivative(i == 0 ? z - this.x[0] : z - this.x[i - 1]);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SmoothingCubicSpline))
			return false;
		SmoothingCubicSpline s = (SmoothingCubicSpline) obj;
		return Arrays.equals(s.x, this.x) && Arrays.equals(s.y, this.y) && Arrays.equals(s.w, this.w) && this.rho == this.rho;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.x, this.y, this.w, this.rho);
	}

	@Override
	public double evaluate(double z) {
		int i = getFitPolynomialIndex(z);
		return this.splineVector[i].evaluate(i == 0 ? z - this.x[0] : z - this.x[i - 1]);
	}

	@Override
	public double[] getData() {
		double[] data = new double[2 + this.x.length * 3];
		data[0] = 1;
		data[1] = this.rho;
		for (int i = 0; i < this.x.length; i++) {
			data[i * 3 + 2] = this.x[i];
			data[i * 3 + 3] = this.y[i];
			data[i * 3 + 4] = this.w[i];
		}
		return data;
	}

	@Override
	public double[] getDefinitionInterval() {
		return new double[] { this.x[0], this.x[this.x.length - 1] };
	}

	public int getFitPolynomialIndex(double x) {
		int j = this.x.length - 1;
		if (x > this.x[j])
			return j + 1;
		int tmp = 0;
		int i = 0;
		do {
			if (i + 1 == j)
				break;
			if (x > this.x[tmp]) {
				i = tmp;
				tmp = i + (j - i) / 2;
			} else {
				j = tmp;
				tmp = i + (j - i) / 2;
			}
			if (j == 0)
				i--;
		} while (true);
		return i + 1;
	}

	@Override
	public SmoothingFunction getParallelFunction(double distance) {
		double[] xs = new double[this.x.length];
		double[] ys = new double[this.y.length];
		for (int i = 0; i < this.x.length; i++) {
			double angle = Math.atan(getTangente(this.x[i])[0]);
			xs[i] = this.x[i] - Math.sin(angle) * distance;
			ys[i] = evaluate(this.x[i]) + Math.cos(angle) * distance;
		}
		SmoothingFunction scsd = new SmoothingCubicSpline();
		scsd.resolve(xs, ys, this.w.clone());
		return scsd;
	}

	@Override
	public double[] getPoly() {
		if (this.splineVector == null)
			return null;
		double[] poly = this.splineVector[0].getCoefficient();
		double[] newPoly = new double[poly.length];
		double a = poly[3];
		double b = poly[2];
		double c = poly[1];
		double alpha = this.x[0];
		newPoly[0] = poly[0] - a * alpha * alpha * alpha + b * alpha * alpha - c * alpha;
		newPoly[1] = 3 * a * alpha * alpha - 2 * b * alpha + c;
		newPoly[2] = b - 3 * a * alpha;
		newPoly[3] = poly[3];
		return newPoly;
	}

	public double[] getPoly(double z) {
		int i = getFitPolynomialIndex(z);
		return this.splineVector[i].getCoefficient().clone();
	}

	public double getRho() {
		return this.rho;
	}

	@Override
	public double[] getTangente(double z) {
		int i = getFitPolynomialIndex(z);
		return this.splineVector[i].tangente(i == 0 ? z - this.x[0] : z - this.x[i - 1]);
	}

	@Override
	public void loadData(double[] data) {
		this.rho = data[1];
		this.x = new double[(data.length - 2) / 3];
		this.y = new double[this.x.length];
		this.w = new double[this.x.length];
		for (int i = 0; i < this.x.length; i++) {
			this.x[i] = data[i * 3 + 2];
			this.y[i] = data[i * 3 + 3];
			this.w[i] = data[i * 3 + 4];
		}
	}

	@Override
	public SmoothingCubicSpline parametricClone() {
		SmoothingCubicSpline scs = new SmoothingCubicSpline();
		scs.setRho(this.rho);
		return scs;
	}

	// @Override
	// public int getNbComputedPoints() {
	// return x.length;
	// }

	@Override
	public void resolve(double[] x, double[] y) {
		resolve(x, y, null);
	}

	@Override
	public void resolve(double[] x, double[] y, double[] w) {
		if (x.length <= 2)
			throw new IllegalArgumentException("x.length <= 2");
		if (x.length != y.length)
			throw new IllegalArgumentException("x.length != y.length");
		if (w != null && x.length != w.length)
			throw new IllegalArgumentException("x.length != w.length");
		if (this.rho < 0.0D || this.rho > 1.0D)
			throw new IllegalArgumentException("rho not in [0, 1]");
		this.splineVector = new Polynomial[x.length + 1];
		this.x = x.clone();
		this.y = y.clone();
		if (w == null) {
			this.w = new double[x.length];
			Arrays.fill(this.w, 1);
			w = this.w;
		} else
			this.w = w.clone();
		double[] h = new double[x.length];
		double[] r = new double[x.length];
		double[] u = new double[x.length];
		double[] v = new double[x.length];
		double[] wp = new double[x.length];
		double[] q = new double[x.length + 1];
		double[] sigma = new double[w.length];
		for (int i = 0; i < w.length; i++)
			sigma[i] = w[i] <= 0.0D ? 1E+100D : 1.0D / Math.sqrt(w[i]);
		int n = x.length - 1;
		double mu;
		if (this.rho <= 0.0D)
			mu = 1E+100D;
		else
			mu = 2D * (1.0D - this.rho) / (3D * this.rho);
		h[0] = x[1] - x[0];
		r[0] = 3D / h[0];
		for (int i = 1; i < n; i++) {
			h[i] = x[i + 1] - x[i];
			r[i] = 3D / h[i];
			q[i] = 3D * (y[i + 1] - y[i]) / h[i] - 3D * (y[i] - y[i - 1]) / h[i - 1];
		}
		for (int i = 1; i < n; i++) {
			u[i] = r[i - 1] * r[i - 1] * sigma[i - 1] + (r[i - 1] + r[i]) * (r[i - 1] + r[i]) * sigma[i] + r[i] * r[i] * sigma[i + 1];
			u[i] = mu * u[i] + 2D * (x[i + 1] - x[i - 1]);
			v[i] = -(r[i - 1] + r[i]) * r[i] * sigma[i] - r[i] * (r[i] + r[i + 1]) * sigma[i + 1];
			v[i] = mu * v[i] + h[i];
			wp[i] = mu * r[i] * r[i + 1] * sigma[i + 1];
		}
		q = quincunx(u, v, wp, q);
		double[] params = new double[4];
		params[0] = y[0] - mu * r[0] * q[1] * sigma[0];
		double dd = y[1] - mu * ((-r[0] - r[1]) * q[1] + r[1] * q[2]) * sigma[1];
		params[1] = (dd - params[0]) / h[0] - q[1] * h[0] / 3D;
		this.splineVector[0] = new Polynomial(params);
		params[0] = y[0] - mu * r[0] * q[1] * sigma[0];
		dd = y[1] - mu * ((-r[0] - r[1]) * q[1] + r[1] * q[2]) * sigma[1];
		params[3] = q[1] / (3D * h[0]);
		params[2] = 0.0D;
		params[1] = (dd - params[0]) / h[0] - q[1] * h[0] / 3D;
		this.splineVector[1] = new Polynomial(params);
		int j;
		for (j = 1; j < n; j++) {
			params[3] = (q[j + 1] - q[j]) / (3D * h[j]);
			params[2] = q[j];
			params[1] = (q[j] + q[j - 1]) * h[j - 1] + this.splineVector[j].getCoefficient(1);
			params[0] = r[j - 1] * q[j - 1] + (-r[j - 1] - r[j]) * q[j] + r[j] * q[j + 1];
			params[0] = y[j] - mu * params[0] * sigma[j];
			this.splineVector[j + 1] = new Polynomial(params);
		}
		j = n;
		params[3] = 0;// splineVector[j].getCoefficient(3);//0.0D;
		params[2] = this.splineVector[j].getCoefficient(2);// 0.0D;
		params[1] = this.splineVector[j].derivative(x[x.length - 1] - x[x.length - 2]);
		params[0] = this.splineVector[j].evaluate(x[x.length - 1] - x[x.length - 2]);
		this.splineVector[n + 1] = new Polynomial(params);
	}

	public void setRho(double rho) {
		this.rho = rho;
	}

	@Override
	public String toString() {
		return "SCS rho: " + this.rho;
	}

	@Override
	public void xTranslate(double delta) {
		for (Polynomial polynomial : this.splineVector)
			polynomial.getCoefficient()[0] += delta;
	}

	@Override
	public void yTranslate(double delta) {
		for (int i = 0; i < this.x.length; i++)
			this.x[i] -= delta;
	}
}

class Polynomial {
	public static void main(String[] args) {
		Log.info(Arrays.toString(new Polynomial(new double[] { 1, 2, 3 }).tangente(4)));
		Log.info("" + new Polynomial(new double[] { 1, 2, 3 }).derivative(4));
	}

	private double[] coeff;

	public Polynomial(double[] coeff) {
		this.coeff = coeff.clone();
		return;
	}

	@Override
	public Polynomial clone() {
		return new Polynomial(this.coeff);
	}

	public double derivative(double x) {
		double res = getCoeffDer(this.coeff.length - 1);
		for (int i = this.coeff.length - 2; i >= 1; i--)
			res = getCoeffDer(i) + x * res;
		return res;
	}

	public double evaluate(double x) {
		double res = this.coeff[this.coeff.length - 1];
		for (int i = this.coeff.length - 2; i >= 0; i--)
			res = this.coeff[i] + x * res;
		return res;
	}

	// TODO plus de sens, on est en degré 1
	private double getCoeffDer(int i) {
		double coeffDer = this.coeff[i];
		for (int j = i; j > i - 1; j--)
			coeffDer *= j;
		return coeffDer;
	}

	public double[] getCoefficient() {
		return this.coeff;
	}

	public double getCoefficient(int i) {
		return this.coeff[i];
	}

	public double[] tangente(double x) {
		int l = this.coeff.length;
		double der = this.coeff[l - 1] * (l - 1);
		for (int i = l - 2; i > 0; i--)
			der = this.coeff[i] * i + x * der;
		return new double[] { der, -der * x + evaluate(x) };
	}
}
