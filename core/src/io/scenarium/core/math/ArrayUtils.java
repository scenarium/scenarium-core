/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.math;

public class ArrayUtils {

	private ArrayUtils() {}

	public static double[] int2DoubleArray(int[] iArray) {
		double[] dArray = new double[iArray.length];
		for (int i = 0; i < dArray.length; i++)
			dArray[i] = iArray[i];
		return dArray;
	}

	public static double[] int2DoubleArray(int[] iArray, double delta) {
		double[] dArray = new double[iArray.length];
		for (int i = 0; i < dArray.length; i++)
			dArray[i] = iArray[i] + delta;
		return dArray;
	}

}
