/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.math.association;

public class HungarianClass {
	private static final int INFINITY_HUNGARIAN = 100000000;
	private final int n;
	private int[][] cost;
	private final int[] lx;
	private final int[] ly;
	private final int[] xy;
	private final int[] yx;
	private final boolean[] s;
	private final boolean[] t;
	private final int[] slack;
	private final int[] slackx;
	private final int[] prev;
	private int maxMatch;

	public HungarianClass(int nbelem) {
		this.n = nbelem;
		this.cost = new int[this.n][this.n];
		this.lx = new int[this.n];
		this.ly = new int[this.n];
		this.xy = new int[this.n];
		this.yx = new int[this.n];
		this.s = new boolean[this.n];
		this.t = new boolean[this.n];
		this.slack = new int[this.n];
		this.slackx = new int[this.n];
		this.prev = new int[this.n];
	}

	private void addToTree(int x, int prevx) {
		// x - current vertex,prevx - vertex from X before x in the alternating path,
		// so we add edges (prevx, xy[x]), (xy[x], x)
		this.s[x] = true; // add x to S
		this.prev[x] = prevx; // we need this when augmenting
		for (int y = 0; y < this.n; y++) // update slacks, because we add new vertex to S
			if (this.lx[x] + this.ly[y] - this.cost[x][y] < this.slack[y]) {
				this.slack[y] = this.lx[x] + this.ly[y] - this.cost[x][y];
				this.slackx[y] = x;
			}
	}

	private void augment() {
		if (this.maxMatch == this.n)
			return; // check wether matching is already perfect
		int x, y, root = 0; // just counters and root vertex
		int wr = 0, rd = 0; // wr,rd - write and read
		int[] q = new int[this.n]; // q - queue for bfs, pos in queue
		for (x = 0; x < this.n; x++)
			this.s[x] = false;

		for (x = 0; x < this.n; x++)
			this.t[x] = false;

		for (x = 0; x < this.n; x++)
			this.prev[x] = -1;

		for (x = 0; x < this.n; x++)
			// finding root of the tree
			if (this.xy[x] == -1) {
				q[wr++] = root = x;
				this.prev[x] = -2;
				this.s[x] = true;
				break;
			}

		for (y = 0; y < this.n; y++) { // initializing slack array
			this.slack[y] = this.lx[root] + this.ly[y] - this.cost[root][y];
			this.slackx[y] = root;
		}
		// second part of augment() function
		while (true) { // main cycle
			while (rd < wr) { // building tree with bfs cycle
				x = q[rd++]; // current vertex from X part
				for (y = 0; y < this.n; y++)
					// iterate through all edges in equality graph
					if (this.cost[x][y] == this.lx[x] + this.ly[y] && !this.t[y]) {
						if (this.yx[y] == -1)
							break; // an exposed vertex in Y found, so
						// augmenting path exists!
						this.t[y] = true; // else just add y to T,
						q[wr++] = this.yx[y]; // add vertex yx[y], which is matched
						// with y, to the queue
						addToTree(this.yx[y], x); // add edges (x,y) and (y,yx[y]) to the tree
					}
				if (y < this.n)
					break; // augmenting path found!
			}
			if (y < this.n)
				break; // augmenting path found!
			updateLabels(); // augmenting path not found, so improve labeling
			wr = rd = 0;
			for (y = 0; y < this.n; y++)
				// in this cycle we add edges that were added to the equality graph as a
				// result of improving the labeling, we add edge (slackx[y], y) to the tree if
				// and only if !T[y] && slack[y] == 0, also with this edge we add another one
				// (y, yx[y]) or augment the matching, if y was exposed
				if (!this.t[y] && this.slack[y] == 0) {
					if (this.yx[y] == -1) // exposed vertex in Y found - augmenting path exists!
					{
						x = this.slackx[y];
						break;
					}
					this.t[y] = true; // else just add y to T,
					if (!this.s[this.yx[y]]) {
						q[wr++] = this.yx[y]; // add vertex yx[y], which is matched with
						// y, to the queue
						addToTree(this.yx[y], this.slackx[y]); // and add edges (x,y) and (y,
						// yx[y]) to the tree
					}
				}
			if (y < this.n)
				break; // augmenting path found!
		}

		if (y < this.n) { // we found augmenting path!
			this.maxMatch++; // increment matching
			// in this cycle we inverse edges along augmenting path
			for (int cx = x, cy = y, ty; cx != -2; cx = this.prev[cx], cy = ty) {
				ty = this.xy[cx];
				this.yx[cy] = cx;
				this.xy[cx] = cy;
			}
			augment(); // recall function, go to step 1 of the algorithm
		}
	}

	public void evaluateAlgorithm(int[][] dataMat) {
		this.cost = dataMat.clone();
		this.maxMatch = 0; // number of vertices in current matching
		for (int x = 0; x < this.n; x++)
			this.xy[x] = -1;
		for (int x = 0; x < this.n; x++)
			this.yx[x] = -1;
		initLabels(); // step 0
		augment(); // steps 1-3
	}

	public int[] getxyIn() {
		return this.xy.clone();
	}

	public int[] getyxIn() {
		return this.yx.clone();
	}

	private void initLabels() {
		for (int x = 0; x < this.n; x++)
			this.lx[x] = 0;
		for (int x = 0; x < this.n; x++)
			this.ly[x] = 0;

		for (int x = 0; x < this.n; x++)
			for (int y = 0; y < this.n; y++)
				this.lx[x] = this.lx[x] > this.cost[x][y] ? this.lx[x] : this.cost[x][y];
	}

	private void updateLabels() {
		int x, y, delta = INFINITY_HUNGARIAN; // init delta as infinity
		for (y = 0; y < this.n; y++) // calculate delta using slack
			if (!this.t[y])
				delta = delta < this.slack[y] ? delta : this.slack[y];
		for (x = 0; x < this.n; x++) // update X labels
			if (this.s[x])
				this.lx[x] -= delta;
		for (y = 0; y < this.n; y++) // update Y labels
			if (this.t[y])
				this.ly[y] += delta;
		for (y = 0; y < this.n; y++) // update slack array
			if (!this.t[y])
				this.slack[y] -= delta;
	}
}
