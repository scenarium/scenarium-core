/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.math.association;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import io.scenarium.core.internal.Log;

public class AssocOdaHung<T extends Piste> {
	private static boolean comparePisteList(ArrayList<PisteTest> pistes, List<Piste> pistes2) {
		if (pistes.size() != pistes2.size())
			return false;
		for (int i = 0; i < pistes.size(); i++)
			if (!pistes.get(i).equals(pistes2.get(i)))
				return false;
		return true;
	}

	public static void main(String[] args) {
		AssocOdaHung<PisteTest> aoh = new AssocOdaHung<>(0.5f, 0.9f, 3, true);
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest(1), new CibleTest(2), new CibleTest(4))));
		if (aoh.pistes.size() != 3 || !comparePisteList(aoh.pistes, Arrays.asList(new PisteTest(0, 1f, 0.5f), new PisteTest(1, 2f, 0.5f), new PisteTest(2, 4f, 0.5f)))) {
			Log.error("Regression!!!");
			System.exit(-1);
		}
		Log.info("");
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest(1.1f), new CibleTest(2.3f), new CibleTest(4.2f))));
		if (aoh.pistes.size() != 3 || !comparePisteList(aoh.pistes, Arrays.asList(new PisteTest(0, 1.1f, 0.84669334f), new PisteTest(1, 2.3f, 0.84669334f), new PisteTest(2, 4.2f, 0.84669334f))))
			Log.error("Regression!!!");
		// System.exit(-1);
		Log.info("");
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest(1.3f), new CibleTest(2.0f), new CibleTest(4.7f), new CibleTest(6.7f))));
		if (aoh.pistes.size() != 4
				|| !comparePisteList(aoh.pistes, Arrays.asList(new PisteTest(0, 1.3f, 0.9728101f), new PisteTest(1, 2f, 0.9728101f), new PisteTest(2, 4.7f, 0.9728101f), new PisteTest(3, 6.7f, 0.5f))))
			Log.error("Regression!!!");
		// System.exit(-1);
		Log.info("");
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest(0.0f), new CibleTest(2.0f), new CibleTest(4.7f))));
		if (aoh.pistes.size() != 4 || !comparePisteList(aoh.pistes, Arrays.asList(new PisteTest(0, 1.3f, 0.8468071f), new PisteTest(1, 2f, 1), new PisteTest(2, 4.7f, 1), new PisteTest(4, 0f, 0.5f))))
			Log.error("Regression!!!");
		// System.exit(-1);
		Log.info("");
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest(1.1f), new CibleTest(2.0f), new CibleTest(4.7f))));
		if (aoh.pistes.size() != 3 || !comparePisteList(aoh.pistes, Arrays.asList(new PisteTest(0, 1.1f, 0.9728615f), new PisteTest(1, 2f, 1), new PisteTest(2, 4.7f, 1))))
			Log.error("Regression!!!");
		// System.exit(-1);
		Log.info("");
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest(1.3f), new CibleTest(2.0f), new CibleTest(4.7f))));
		if (aoh.pistes.size() != 3 || !comparePisteList(aoh.pistes, Arrays.asList(new PisteTest(0, 1.3f, 1), new PisteTest(1, 2.0f, 1), new PisteTest(2, 4.7f, 1))))
			Log.error("Regression!!!");
		// System.exit(-1);
		Log.info("");
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest(1.3f), new CibleTest(2.0f), new CibleTest(4.7f))));
		if (aoh.pistes.size() != 3 || !comparePisteList(aoh.pistes, Arrays.asList(new PisteTest(0, 1.3f, 1), new PisteTest(1, 2f, 1), new PisteTest(2, 4.7f, 1))))
			Log.error("Regression!!!");
		// System.exit(-1);
		Log.info("");
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest(1.1f), new CibleTest(1.49f), new CibleTest(2.0f), new CibleTest(4.7f))));
		if (aoh.pistes.size() != 4 || !comparePisteList(aoh.pistes, Arrays.asList(new PisteTest(0, 1.49f, 1), new PisteTest(1, 2f, 1), new PisteTest(2, 4.7f, 1), new PisteTest(5, 1.1f, 0.5f))))
			Log.error("Regression!!!");
		// System.exit(-1);
		Log.info("");
	}

	private final float tauxMassecroyance;
	private final float alphaEvolConfidence;
	private final int nbIterConf;

	private ArrayList<T> pistes = new ArrayList<>();

	private final boolean verbose;

	public AssocOdaHung(float tauxMassecroyance, float alphaEvolConfidence, int nbIterConf, boolean verbose) {
		this.tauxMassecroyance = tauxMassecroyance;
		this.alphaEvolConfidence = alphaEvolConfidence;
		this.nbIterConf = nbIterConf;
		this.verbose = verbose;
	}

	@SuppressWarnings("unchecked")
	@Override
	public AssocOdaHung<T> clone() {
		AssocOdaHung<T> a = new AssocOdaHung<>(this.tauxMassecroyance, this.alphaEvolConfidence, this.nbIterConf, this.verbose);
		ArrayList<T> pistes = new ArrayList<>();
		for (T piste : this.pistes)
			pistes.add((T) piste.clone());
		a.pistes = pistes;
		return a;
	}

	public ArrayList<T> getPistes() {
		return this.pistes;
	}

	@SuppressWarnings("unchecked")
	public int update(ArrayList<Cible> cibles) {
		if (cibles.isEmpty()) {
			// On diminue les croyances
			for (Piste piste : this.pistes) {
				piste.evolutionConfidence(this.alphaEvolConfidence, false, this.nbIterConf);
				piste.setStatus(PisteStatus.MAINTAINED);
			}
			this.pistes.removeIf(piste -> {
				if (piste.getConfidence() <= 0) {
					if (this.verbose)
						Log.info("Remove Piste: " + piste);
					return true;
				}
				return false;
			});
			return this.pistes.size();
		}
		if (this.pistes.isEmpty()) {
			for (Cible cible : cibles) {
				Piste newPiste = cible.createPisteFromCible();
				this.pistes.add((T) newPiste);
				if (this.verbose)
					Log.info("Create piste: " + newPiste);
			}
			return this.pistes.size();
		}

		// Calcul de la distance et des masses de croyance initiale entre cibles et pistes
		int nbCible = cibles.size();
		int nbPiste = this.pistes.size();
		float[][] mYMat = new float[nbCible][nbPiste];
		float[][] mnotYMat = new float[nbCible][nbPiste];

		for (int icib = 0; icib < nbCible; icib++) {
			float[] currMnotYMat = mnotYMat[icib];
			float[] currMYMat = mYMat[icib];
			for (int ipis = 0; ipis < nbPiste; ipis++) {
				// Calcul des masses de croyance initiale
				float disimalarity = 1 - this.pistes.get(ipis).matchScore(cibles.get(icib));
				if (disimalarity < this.tauxMassecroyance) {
					currMnotYMat[ipis] = 0;
					currMYMat[ipis] = (float) ((1 - sin(PI * (disimalarity / this.tauxMassecroyance - 0.5f))) / 2.0f); // PK????
				} else {
					currMYMat[ipis] = 0;
					currMnotYMat[ipis] = (float) ((1 - sin(PI * ((1.0 - disimalarity) / (1.0 - this.tauxMassecroyance) - 0.5f))) / 2.0f);
				}
			}
		}

		// %==================================================
		// %
		// % PREMIERE ETAPE : Xi en relation avec Yi
		// % Association des cibles vers les pistes
		// %==================================================
		float[][] mCrSubXy = new float[nbCible][nbPiste];
		float[][] mCrXy = new float[nbCible][nbPiste + 2];

		for (int icib = 0; icib < nbCible; icib++) { // ** Calcul des masse de croyance
			float[] currMYMat = mYMat[icib];
			float mVide = 1;
			// float m_Omega = 1;
			float[] mY = new float[nbPiste];
			for (int ipis = 0; ipis < nbPiste; ipis++) {
				mY[ipis] = currMYMat[ipis];
				for (int ipis2 = 0; ipis2 < nbPiste; ipis2++)
					if (ipis2 != ipis)
						mY[ipis] *= 1 - currMYMat[ipis2];
				mVide *= mnotYMat[icib][ipis];
				// m_Omega *= 1.0 - currM_Y_Mat[ipis];
			}
			// m_Omega -= m_Vide;
			// ** Normalisation
			float k;
			k = mVide + 0;
			for (int ipis = 0; ipis < nbPiste; ipis++)
				k += mY[ipis];

			if (k != 0) {
				mVide /= k;
				// m_Omega /= K;
				for (int ipis = 0; ipis < nbPiste; ipis++)
					mY[ipis] /= k;
			}

			// Affectation des résultat dans les matrices dédiées :
			float[] currMCrSubXy = mCrSubXy[icib];
			float[] currMCrXy = mCrXy[icib];
			for (int ipis = 0; ipis < nbPiste; ipis++) {
				currMCrSubXy[ipis] = mY[ipis];
				currMCrXy[ipis] = mY[ipis];
			}
			currMCrXy[nbPiste] = mVide;
			// currM_cr_xy[nbPiste + 1] = m_Omega;
		}

		// %==================================================
		// %
		// % DEUXIEME ETAPE : Yi en relation avec Xi
		// % association des pistes vers les cibles
		// %==================================================

		float[][] mCrSubYx = new float[nbPiste][nbCible];
		float[][] mCrYx = new float[nbPiste][nbCible + 2];
		for (int ipis = 0; ipis < nbPiste; ipis++) { // ** Calcul des masse de croyance
			float mVide = 1;
			// float m_Omega = 1;
			float[] mY = new float[nbCible];
			for (int icib = 0; icib < nbCible; icib++) {
				mY[icib] = mYMat[icib][ipis];
				for (int icib2 = 0; icib2 < nbCible; icib2++)
					if (icib2 != icib)
						mY[icib] *= 1 - mYMat[icib2][ipis];
				mVide *= mnotYMat[icib][ipis];
				// m_Omega *= 1.0 - M_Y_Mat[icib][ipis];
			}
			// m_Omega -= m_Vide;
			// ** Normalisation
			float k;
			k = mVide + 0;
			for (int icib = 0; icib < nbCible; icib++)
				k += mY[icib];
			if (k != 0) {
				mVide /= k;
				// m_Omega /= K;
				for (int icib = 0; icib < nbCible; icib++)
					mY[icib] /= k;
			}
			// Affectation des résultat dans les matrices dédiées :
			float[] currMCrSubYx = mCrSubYx[ipis];
			float[] currMCrYx = mCrYx[ipis];
			for (int icib = 0; icib < nbCible; icib++) {
				currMCrSubYx[icib] = mY[icib];
				currMCrYx[icib] = mY[icib];
			}
			currMCrYx[nbCible] = mVide;
			// currM_cr_yx[nbCible + 1] = m_Omega;
		}

		// DECISION X->Y :
		int[] decisionX = new int[nbCible]; // matrice de décision: X(i) est associé à la piste j

		for (int icib = 0; icib < nbCible; icib++) {
			float massmax = mCrXy[icib][nbPiste]; // par défaut on débute sur vide;
			int decindx = nbPiste;
			for (int ipis = 0; ipis < nbPiste; ipis++) { // On parcourt tous les Y, Vide est fait, omega on ne le prend pas en compte dans cette version
				float masstmp = mCrXy[icib][ipis];
				if (masstmp > massmax) {
					decindx = ipis;
					massmax = masstmp;
				}
			}
			decisionX[icib] = decindx;
		}

		// DECISION Y->X :
		int[] decisionY = new int[nbPiste]; // matrice de décision: Y(i) est associé à la cible i
		for (int ipis = 0; ipis < nbPiste; ipis++) {
			float[] currMCrYx = mCrYx[ipis];
			float massmax = currMCrYx[nbCible]; // par défaut on débute sur vide;
			int decindx = nbCible;
			for (int icib = 0; icib < nbCible; icib++) { // On parcourt tous les X, Vide est fait, omega on ne le prend pas en compte dans cette version
				float masstmp = currMCrYx[icib];
				if (masstmp > massmax) {
					decindx = icib;
					massmax = masstmp;
				}
			}
			decisionY[ipis] = decindx;
		}

		// soit X -> Y et Y -> X concordent alors c'est bonheur
		// sinon il faut faire plus compliqué pour lever l'ambiguité
		boolean isconflit = false;
		for (int icib = 0; icib < nbCible; icib++)
			if (decisionX[icib] != nbPiste) // si X(icib) correspond à vide, il n'y a pas de conflit
				if (decisionY[decisionX[icib]] != icib) {
					isconflit = true;
					break;
				}
		if (!isconflit)
			for (int ipis = 0; ipis < nbPiste; ipis++)
				if (decisionY[ipis] != nbCible) // si Y(icib) correspond à vide, il n'y a pas de conflit
					if (decisionX[decisionY[ipis]] != ipis) {
						isconflit = true;
						break;
					}

		if (isconflit) { // Algorithme Hongrois :
			// Cet algorithme travaille avec des entiers, donc on convertit nos flotants en x1000
			// creation de la matrice de taille nbCible+nbPiste
			int nbHung = nbCible + nbPiste;
			int[][] combinedMassMat = new int[nbHung][nbHung];
			// remplissage de CombinedMass_Mat:

			for (int icib = 0; icib < nbCible; icib++)
				// remplissage de [1:nbCible]x[1:nbPiste]
				for (int ipis = 0; ipis < nbPiste; ipis++)
					combinedMassMat[icib][ipis] = (int) (500 * (mCrXy[icib][ipis] + mCrYx[ipis][icib]));

			for (int icib = 0; icib < nbCible; icib++)
				combinedMassMat[icib][icib + nbPiste] = (int) (1000 * mCrXy[icib][nbPiste]);

			for (int ipis = 0; ipis < nbPiste; ipis++)
				combinedMassMat[ipis + nbCible][ipis] = (int) (1000 * mCrYx[ipis][nbCible]);

			HungarianClass hungClass = new HungarianClass(nbHung); // Initialisation
			hungClass.evaluateAlgorithm(combinedMassMat); // lancement de l'algo
			decisionX = hungClass.getxyIn(); // extraction des résultats
			decisionY = hungClass.getyxIn(); // extraction des résultats
		}

		// %==================================================
		// %
		// % TROISIEME ETAPE : Mise à jour des pistes
		// %
		// %==================================================
		// Mise à jour des pistes
		for (int ipis = 0; ipis < this.pistes.size(); ipis++) {
			Piste piste = this.pistes.get(ipis);
			if (decisionY[ipis] < nbCible/* && piste.matchScore(cibles.get(decision_y[ipis])) != 0 */) { // la piste est associée à une cible //TODO bug!!! one ne respecte pas le critère de 0.5 de
																											// similarity avec assoc odaHong
				Cible cible = cibles.get(decisionY[ipis]);// CurrentCibles[Decision_y[ipis]];
				piste.evolutionConfidence(this.alphaEvolConfidence, true, this.nbIterConf);
				piste.setStatus(PisteStatus.ASSOCIATED);
				if (this.verbose) {
					float score = piste.matchScore(cible);
					piste.update(cible);
					Log.info("Associate piste: " + piste + " with cible: " + cible + " score: " + score);
				} else
					piste.update(cible);
			} else {
				piste.evolutionConfidence(this.alphaEvolConfidence, false, this.nbIterConf);
				piste.setStatus(PisteStatus.MAINTAINED);
				if (this.verbose)
					Log.info("Maintain piste: " + piste);
			}
		}
		// Nouvelle piste
		for (int icib = 0; icib < nbCible; icib++)
			if (decisionX[icib] >= nbPiste) { // la cible n'est pas associée à une piste
				T newPiste = (T) cibles.get(icib).createPisteFromCible();
				newPiste.setStatus(PisteStatus.CREATED);
				this.pistes.add(newPiste);
				if (this.verbose)
					Log.info("Create piste: " + newPiste);
			}
		// Suppression des pistes avec une confiance à 0
		if (this.verbose)
			Log.info("lipi: " + this.pistes.toString());
		this.pistes.removeIf(piste -> {
			if (piste.getConfidence() <= 0) {
				if (this.verbose)
					Log.info("Remove Piste: " + piste);
				return true;
			}
			return false;
		});
		return 0;
	}
}

class CibleTest implements Cible {

	public float x;

	public CibleTest(float x) {
		this.x = x;
	}

	@Override
	public Piste createPisteFromCible() {
		return new PisteTest(this.x, 0.5f);
	}

	@Override
	public String toString() {
		return "x: " + this.x;
	}
}

class PisteTest implements Piste {
	private static int alphaDis = 1;
	private static int idCpt = 0;
	public PisteStatus status = PisteStatus.CREATED;
	private int id;
	private float x;
	public float confidence;

	public PisteTest(float x) {
		this.x = x;
		this.id = idCpt++;
	}

	public PisteTest(float x, float confidence) {
		this(x);
		this.confidence = confidence;
	}

	public PisteTest(int id, float x, float confidence) {
		this(x, confidence);
		idCpt--;
		this.id = id;
	}

	@Override
	public PisteTest clone() {
		try {
			return (PisteTest) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PisteTest))
			return false;
		PisteTest p = (PisteTest) obj;
		return this.id == p.id && this.x == p.x && this.confidence == p.confidence;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.x, this.confidence);
	}

	@Override
	public void evolutionConfidence(float alpha, boolean sens, int nbItertotal) {
		if (alpha > 0.99)
			alpha = 0.99f;

		double a = -3 * pow(alpha, 2) / 2 + this.confidence / 2 + pow(alpha, 3)
				+ sqrt(4 * pow(alpha, 3) - 3 * pow(alpha, 4) - 6 * pow(alpha, 2) * this.confidence + pow(this.confidence, 2) + 4 * pow(alpha, 3) * this.confidence) / 2;
		double t = pow(a, 0.3333) - (alpha - pow(alpha, 2)) / pow(a, 0.3333) + alpha; // Abscisse courante
		t = (t * nbItertotal + (sens ? 1 : -1)) / nbItertotal; // décalage de l'abscisse
		t = t > 1 ? 1 : t;
		t = t < 0 ? 0 : t;
		this.confidence = (float) ((3 * t * pow(1 - t, 2) + 3 * pow(t, 2) * (1 - t)) * alpha + pow(t, 3));
	}

	@Override
	public float getConfidence() {
		return this.confidence;
	}

	@Override
	public float matchScore(Cible cible) {
		return 1 / (abs(((CibleTest) cible).x - this.x) / alphaDis + 1);
	}

	@Override
	public void setStatus(PisteStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "id: " + this.id + " x: " + this.x + " confidence: " + this.confidence + " status: " + this.status;
	}

	@Override
	public void update(Cible cible) {
		this.x = ((CibleTest) cible).x;
	}
}