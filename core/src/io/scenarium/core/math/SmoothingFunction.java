/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.math;

public interface SmoothingFunction extends FilterFunction {
	public SmoothingFunction addPoint(double x, double y, double w);

	public SmoothingFunction clone();

	double[] getData();

	public double[] getDefinitionInterval();

	public SmoothingFunction getParallelFunction(double distance);

	public double[] getPoly();

	public double[] getTangente(double x);

	// public int getNbComputedPoints();

	public void loadData(double[] data);

	public SmoothingFunction parametricClone();

	public void resolve(double[] x, double[] y, double[] w);

	public void xTranslate(double delta);

	public void yTranslate(double delta);

}
