/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.editors;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import io.scenarium.core.struct.curve.Curvei;

public class CurveiEditor extends CurveEditor<Curvei> {
	@Override
	public Curvei readValue(DataInput raf) throws IOException {
		return (Curvei) super.readCurveValue(raf);
	}

	@Override
	public void writeValue(DataOutput raf, Curvei value) throws IOException {
		super.writeValue(raf, value);
	}
}
