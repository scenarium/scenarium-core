/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.core.editors;

import io.beanmanager.editors.PropertyEditor;
import io.scenarium.core.filemanager.scenariomanager.ScenarioDescriptor;

public class ScenarioDescriptorEditor extends PropertyEditor<ScenarioDescriptor> {
	private static final String SEPARATOR = ":";

	@Override
	public String getAsText() {
		ScenarioDescriptor sd = getValue();
		return sd != null ? sd.sourceType + SEPARATOR + sd.scenarioPath : null;
	}

	@Override
	public boolean hasCustomEditor() {
		return false;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text.equals(String.valueOf((Object) null))) {
			setValue(null);
			return;
		}
		int separatorIndex = text.indexOf(SEPARATOR);
		setValue(new ScenarioDescriptor(text.substring(0, separatorIndex), text.substring(separatorIndex + SEPARATOR.length())));
	}

	@Override
	public boolean canContainForbiddenCharacter() {
		return true;
	}
}
