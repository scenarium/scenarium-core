Scenarium
=========

Scenarium is a platform to create, edit and execute flow diagrams.
Scenarium permit to interconnect the functionals blocks with a simple and efficient GUI.
It includes a collection of tools for data acquisition, data recording, data visualization and data processing.

This program is developed by ```Marc Revilloud``` and free (license MPL-2)

Scenarium can work on:
  - Windows
  - MacOS
  - Linux

Dependencies:
=============

Scenarium depend on the last version of java. The development cycle is Rolling-release only.

Java
----

Install the last Java with JavaFX: https://bell-sw.com/pages/java-14 with LibericaFX

Eclipse
-------

Install the last version: https://www.eclipse.org/downloads/

Install Sources
===============

[Installation process](doc/download_project_gui.md)

Tutorials:
==========

[global overview](doc/overview_system.md)
[contribution guideline](doc/contribution_guideline.md)
